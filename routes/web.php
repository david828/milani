<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
Route::get('/home', 'HomeController@index');
Route::get('state/{country}/search', 'Adm\Location\StateController@getStates');
Route::get('city/{state}/search', 'Adm\Location\CityController@getCities');

Route::group(['namespace' => 'Site'], function () {
    Route::group(['middleware' => 'statusapp'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'Index\IndexController@index']);
        ROute::get('/css/callCss', ['as' => 'callCss', 'uses' => 'App\AppController@callCss']);
        Route::get('/politics', function(){
            return view('site.politics.index');
        });
        Route::get('/conditions', function(){
            return view('site.conditions.index');
        });
        Route::get('/resources/{resource}', ['as' => 'resource', 'uses' => 'Resources\ResourceController@view']);
        Route::get('/aboutus', ['as' => 'aboutus', 'uses' => 'AboutUs\AboutUsController@index']);
        Route::get('/products', ['as' => 'products', 'uses' => 'Products\ProductsController@index']);
        Route::get('/products/ver/{product}', ['as' => 'products.view', 'uses' => 'Products\ProductsController@view']);
        Route::post('/leave', ['as' => 'leave', 'uses' => 'Contact\ContactController@leave']);
        Route::get('/contact', ['as' => 'contact', 'uses' => 'Contact\ContactController@index']);
        Route::post('contact/{contact}', ['as' => 'contact.send', 'uses' => 'Contact\ContactController@sendMessage']);
        Route::get('/articles', ['as' => 'articles', 'uses' => 'Articles\ArticleController@index']);
        Route::get('article/ver/{article}', ['as' => 'article.view', 'uses' => 'Articles\ArticleController@view']);
        Route::post('article/comment', ['as' => 'article.comment', 'uses' => 'Articles\ArticleController@comment']);
        Route::get('/galleries', ['as' => 'galleries', 'uses' => 'Galleries\GalleriesController@index']);
        Route::get('galleries/ver/{gallery}', ['as' => 'galleries.view', 'uses' => 'Galleries\GalleriesController@view']);
        Route::get('/distribution', ['as' => 'distribution', 'uses' => 'Distribution\DistributionController@index']);
        Route::get('/cart', ['as' => 'cart', 'uses' => 'Cart\CartController@index']);
        Route::post('cart/add', ['as' => 'addcart', 'uses' => 'Cart\CartController@addCart']);
        Route::post('cart/del', ['as' => 'delcart', 'uses' => 'Cart\CartController@delCart']);
        Route::post('cart/change', ['as' => 'chcart', 'uses' => 'Cart\CartController@changeCart']);
        Route::post('/confirm_stock', ['as' => 'confirmcart', 'uses' => 'Cart\CartController@confirm']);
        Route::post('coupon/apply', ['as' => 'appCoupon', 'uses' => 'Cart\CheckoutController@apCoupon']);
        Route::get('/checkout', ['as' => 'checkout', 'uses' => 'Cart\CheckoutController@index']);
        Route::post('/tarifa-envio', ['as'=>'priceship', 'uses'=>'Cart\CheckoutController@calculateShip']);
        Route::post('/comprar', ['as' => 'cart.checkout', 'uses' => 'Cart\CheckoutController@checkOut']);
        /*********************** LOGIN ****************************/
        Route::post('/iniciar', ['as' => 'authlogin', 'uses' => 'Auth\AuthController@doLogin']);
        Route::post('/restaurar', ['as' => 'recovery', 'uses' => 'Auth\AuthController@loginRecovery']);
        Route::get('/cerrar-sesion', ['as' => 'userlogout', 'uses' => 'Auth\AuthController@logout']);
        Route::post('/registro', ['as' => 'register.data', 'uses' => 'Auth\AuthController@registerData']);
        /*********************** /LOGIN ****************************/

        Route::group(['middleware' => 'auth'], function () {
            /********************Account**********************/
            Route::get('/mi-cuenta', ['as' => 'myaccount', 'uses' => 'User\UserController@myAccount']);
            Route::post('/actualizar-datos', ['as' => 'updateaccount', 'uses' => 'User\UserController@update']);
            Route::get('/imagen', ['as' => 'imageprofile', 'uses' => 'User\UserController@image']);
            Route::post('/actualizar-imagen', ['as' => 'imageprofile.data', 'uses' => 'User\UserController@updateImage']);
            Route::get('/mi-contrasena', ['as' => 'changepassword', 'uses' => 'User\UserController@changePassword']);
            Route::post('/cambiar-contrasena', ['as' => 'changepass', 'uses' => 'User\UserController@updatePassword']);
            Route::get('/mi-direccion', ['as' => 'useraddress', 'uses' => 'User\UserController@userAddress']);
            Route::post('/borrar-direccion/{address}', ['as' => 'deleteaddress', 'uses' => 'User\UserController@deleteAddress']);
            Route::post('/cambiar-direccion/{address}', ['as' => 'changeaddress', 'uses' => 'User\UserController@changeAddress']);
            Route::post('/cerrar-cuenta', ['as' => 'deleteuser', 'uses' => 'User\UserController@deleteUser']);

            Route::get('/mis-ordenes', ['as' => 'userorders', 'uses' => 'User\UserController@userOrders']);
            Route::get('/ver-orden/{order}', ['as' => 'vieworder', 'uses' => 'User\UserController@viewOrder']);
            Route::get('/imprimir-orden/{order}', ['as' => 'printorder', 'uses' => 'User\UserController@printOrder']);
            /******************End Account********************/
        });
        Route::get('/respuesta-de-pago', ['as' => 'payment.response', 'uses' => 'Payment\PaymentController@paymentResponse']);
        Route::get('/imprimir-recibo/{invoice}', ['as' => 'payment.print', 'uses' => 'Payment\PaymentController@printInvoice']);

    });
});


Route::get('/adm', function () {
    return redirect('/login');
});

Route::group(['prefix' => 'adm'], function () {
    Route::auth();
    Route::group(['middleware' => ['auth','admin']], function () {

            Route::group(['namespace' => 'Adm'], function () {

                Route::group(['middleware' => ['auth','superadmin']], function () {
                    Route::resource('admin', 'Admin\AdminController', ['as' => 'adm']);
                    Route::get('app', ['as' => 'adm.app.configure', 'uses' => 'App\AppController@configure']);
                    Route::post('app', ['as' => 'adm.app.update', 'uses' => 'App\AppController@update']);
                    /********************PayU**********************/
                    Route::get('payu', ['as' => 'adm.payu.index', 'uses' => 'PayU\PayuController@index']);
                    Route::post('payu/update', ['as' => 'adm.payu.update', 'uses' => 'PayU\PayuController@update']);
                    /******************End PayU********************/
                    /********************Invoice**********************/
                    Route::get('invoice', ['as' => 'adm.invoice.index', 'uses' => 'Invoice\InvoiceController@index']);
                    Route::get('invoice/{invoice}', ['as' => 'adm.invoice.show', 'uses' => 'Invoice\InvoiceController@show']);
                    Route::post('invoice/{invoice}/null', ['as' => 'adm.invoice.null', 'uses' => 'Invoice\InvoiceController@invalidate']);
                    Route::post('invoice/{invoice}/send', ['as' => 'adm.invoice.send', 'uses' => 'Invoice\InvoiceController@track']);
                    Route::get('invoice/{invoice}/print', ['as' => 'adm.invoice.print', 'uses' => 'Invoice\InvoiceController@printInvoice']);
                    /******************End Invoice********************/
                });
                Route::get('home', 'Dashboard\DashboardController@index');
                Route::get('dashboard',['as' => 'adm.dashboard', 'uses' => 'Dashboard\DashboardController@index']);
                Route::resource('menu', 'Menu\MenuController', ['as' => 'adm']);
                Route::get('footer',['as' => 'adm.footer', 'uses' => 'Footer\FooterController@index']);
                Route::post('footer/update', ['as' => 'adm.footer.update', 'uses' => 'Footer\FooterController@update']);
                Route::resource('resources', 'Resources\ResourceController', ['as' => 'adm']);
                Route::resource('distribution', 'Distribution\DistributionController', ['as' => 'adm']);
                Route::resource('modals', 'Modals\ModalsController', ['as' => 'adm']);
                Route::post('modals/status', ['as' => 'adm.modals.status', 'uses' => 'Modals\ModalsController@status']);
                /********************** Home **************************/
                Route::get('header',['as' => 'adm.header', 'uses' => 'Header\HeaderController@index']);
                Route::post('header/update', ['as' => 'adm.header.update', 'uses' => 'Header\HeaderController@update']);
                Route::get('header/images', ['as' => 'adm.header.images', 'uses' => 'Header\HeaderController@images']);
                Route::post('header/links', ['as' => 'adm.headerlink', 'uses' => 'Header\HeaderController@links']);
                Route::post('header/{image}/upload', ['as' => 'adm.headerimage.upload', 'uses' => 'Header\HeaderController@upload']);
                Route::post('header/{image}/delImage', ['as' => 'adm.headerimage.destroy', 'uses' => 'Header\HeaderController@destroyImage']);
                /******************* End Home *************************/
                /***********************AboutUs************************/
                Route::get('aboutus',['as' => 'adm.aboutus', 'uses' => 'AboutUs\AboutUsController@index']);
                Route::post('aboutus/update', ['as' => 'adm.aboutus.update', 'uses' => 'AboutUs\AboutUsController@update']);
                Route::get('productdes', ['as' => 'adm.productdes.form', 'uses' => 'Productdes\ProductdesController@form']);
                Route::post('productdes/save', ['as' => 'adm.productdes.editform', 'uses' => 'Productdes\ProductdesController@updateform']);
                /***********************end AboutUS *******************/
                /********************Profile**********************/
                Route::get('profile', ['as' => 'adm.profile', 'uses' => 'Profile\ProfileController@index']);
                Route::post('profile/password', ['as' => 'adm.profile.password', 'uses' => 'Profile\ProfileController@password']);
                Route::post('profile/update', ['as' => 'adm.profile.update', 'uses' => 'Profile\ProfileController@update']);
                /******************End Profile********************/
                /*******************Configuration *****************/

                Route::get('seodata', ['as' => 'adm.seodata.configure', 'uses' => 'App\SeoController@configure']);
                Route::post('seodata', ['as' => 'adm.seodata.update', 'uses' => 'App\SeoController@update']);
                Route::resource('politics', 'Politics\PoliticsController', ['as' => 'adm']);
                Route::resource('conditions', 'Conditions\ConditionsController', ['as' => 'adm']);
                /*********************End Configuration ************/
                /*******************Social **************************/
                Route::resource('social', 'Adm\Social\SocialController', ['as' => 'adm']);
                Route::get('appsocial', ['as' => 'adm.appsocial', 'uses' => 'App\AppSocialController@addSocial']);
                Route::post('appsocial/update', ['as' => 'adm.appsocial.update', 'uses' => 'App\AppSocialController@update']);
                Route::get('appsocial/fields/{cont}', ['as' => 'adm.appsocial.fields', 'uses' => 'App\AppSocialController@fields']);
                /*****************End social ************************/
                /*************** Contacts **************************/
                Route::resource('contact', 'Contact\ContactController', ['as' => 'adm']);
                /*************** End contacts **********************/
                /********************Categories**********************/
                Route::resource('categories', 'Categories\CategoriesController', ['as' => 'adm']);
                Route::post('categories/home', ['as' => 'adm.categories.home', 'uses' => 'Categories\CategoriesController@home']);
                /******************End Categories********************/
                /********************Articles**********************/
                Route::resource('articles', 'Articles\ArticleController', ['as' => 'adm']);
                Route::post('articles/status', ['as' => 'adm.articles.status', 'uses' => 'Articles\ArticleController@status']);
                Route::get('articles/{articles}/images', ['as' => 'adm.articles.images', 'uses' => 'Articles\ArticleController@images']);
                Route::post('articles/{articles}/upload', ['as' => 'adm.articles.upload', 'uses' => 'Articles\ArticleController@upload']);
                Route::post('articles/{image}/delImage', ['as' => 'adm.articleImage.destroy', 'uses' => 'Articles\ArticleController@destroyImage']);
                Route::get('articles/{articles}/comments', ['as' => 'adm.articles.comments', 'uses' => 'Articles\ArticleController@comments']);
                Route::delete('articles/{comment}/delComment', ['as' => 'adm.articleComment.destroy', 'uses' => 'Articles\ArticleController@destroyComments']);
                /******************End Articles********************/
                /********************Galleries**********************/
                Route::resource('categoriesgal', 'Galleries\CategoriesGalController', ['as' => 'adm']);
                Route::post('categoriesgal/home', ['as' => 'adm.categoriesgal.home', 'uses' => 'Galleries\CategoriesGalController@home']);
                Route::resource('galleries', 'Galleries\GalleriesController', ['as' => 'adm']);
                Route::get('galleries/{galleries}/images', ['as' => 'adm.galleries.images', 'uses' => 'Galleries\GalleriesController@images']);
                Route::post('galleries/{galleries}/upload', ['as' => 'adm.galleries.upload', 'uses' => 'Galleries\GalleriesController@upload']);
                Route::post('galleries/{image}/delImage', ['as' => 'adm.galleryImage.destroy', 'uses' => 'Galleries\GalleriesController@destroyImage']);
                /******************End Galleries********************/
                /**************** PRODUCTS *************************/
                Route::resource('categoriesP', 'Products\CategoriesController',['as' => 'adm']);
                Route::post('categoriesP/order', ['as' => 'categoriesP.order', 'uses' => 'Products\CategoriesController@orderCat']);
                Route::resource('subcategories', 'Products\SubcategoriesController',['as' => 'adm']);
                Route::post('getsubcategories', ['as' => 'adm.getsubcategories', 'uses' => 'Products\SubcategoriesController@getSubcategories']);
                Route::resource('colors', 'Products\ColorsController',['as' => 'adm']);
                Route::resource('finish', 'Products\FinishController',['as' => 'adm']);
                Route::resource('products', 'Products\ProductsController',['as' => 'adm']);
                Route::get('products/{product}/images', ['as' => 'adm.products.images', 'uses' => 'Products\ProductsController@images']);
                Route::post('products/{product}/upload', ['as' => 'adm.products.upload', 'uses' => 'Products\ProductsController@upload']);
                Route::post('products/{image}/delImage', ['as' => 'adm.productImage.destroy', 'uses' => 'Products\ProductsController@destroyImage']);
                Route::post('products/best', ['as' => 'adm.products.best', 'uses' => 'Products\ProductsController@best']);
                Route::post('products/new', ['as' => 'adm.products.new', 'uses' => 'Products\ProductsController@newP']);
                Route::post('products/soon', ['as' => 'adm.products.soon', 'uses' => 'Products\ProductsController@Soon']);
                Route::post('products/status', ['as' => 'adm.products.status', 'uses' => 'Products\ProductsController@status']);
                Route::post('products/alt', ['as' => 'adm.imagealt', 'uses' => 'Products\ProductsController@altImage']);
                Route::get('feature',['as' => 'adm.feaproducts.index', 'uses' => 'Products\FeatureProductsController@edit']);
                Route::post('feature/update', ['as' => 'adm.feaproducts.update', 'uses' => 'Products\FeatureProductsController@update']);
                /************ END PRODUCTS *************************/

                /********************Location**********************/
                Route::resource('country', 'Location\CountryController', ['as' => 'adm']);
                Route::resource('state', 'Location\StateController', ['as' => 'adm']);
                Route::resource('city', 'Location\CityController', ['as' => 'adm']);
                Route::get('state/{country}/search', 'Location\StateController@getStates', ['as' => 'adm']);
                Route::get('city/{state}/search', 'Location\CityController@getCities', ['as' => 'adm']);
                /******************End Location********************/
                /********************Zone**********************/
                Route::resource('zone', 'Zone\ZoneController', ['as' => 'adm']);
                Route::post('zone/status', ['as' => 'adm.zone.status', 'uses' => 'Zone\ZoneController@status']);
                /******************End Zone********************/
                /********************Coupon**********************/
                Route::resource('coupon', 'Coupons\CouponController', ['as' => 'adm']);
                Route::post('coupon/status', ['as' => 'adm.coupon.status', 'uses' => 'Coupons\CouponController@status']);
                Route::get('couponHistoric', ['as' => 'adm.coupon.historic', 'uses' => 'Coupons\CouponController@historic']);
                /******************End Coupon********************/

                Route::post('orderImages', ['as' => 'adm.orderImages', 'uses' => 'OrderImages\OrderImagesController@updateOrder']);

            });

    });
});
