jQuery(document).ready(function () {

    jQuery('.minus').on('click', function(){
        var c = jQuery(this).attr('target-c');
        var p = jQuery(this).attr('target-p');
        goToChange(p,c,'m');
    });

    jQuery('.plus').on('click', function(){
        var c = jQuery(this).attr('target-c');
        var p = jQuery(this).attr('target-p');
        goToChange(p,c,'p');
    });

    jQuery('.remove').on('click', function(){
        var msg = '';
        var cls = '';
        var c = jQuery(this).attr('target-c');
        var p = jQuery(this).attr('target-p');
        var opc = p;
        if(c !== ''){
            opc += '_'+c;
        }
        jQuery('#id_p2').val(p);
        jQuery('#id_c2').val(c);
        jQuery('#msg_'+opc).html('Un momento por favor...')
            .removeClass()
            .addClass('wait_cart');
        var data = jQuery('#frmDelCart').serialize();
        var url = jQuery("#frmDelCart").attr("action");
        jQuery.post(url, data, function (result) {
            if (result.success) {
                jQuery('#row_' + opc).remove();
                jQuery('#r_msg_' + opc).remove();
                jQuery('#totalF').html(result.tMoney);
                if(result.tProducts === 0)
                {
                    jQuery('#allCart').html('<div class="col-md-12">\n' +
                        '                        <p>El carrito se encuentra vacío, puedes ver todo lo que tenemos para ti <a href="/products"> aquí</a> </p>\n' +
                        '                    </div>');
                }
                refreshCart(result.products, result.tProducts, result.tMoney);
            } else if(result.error) {
                msg = result.error;
                cls = 'error_cart';
                msgCart(msg,cls,opc);
            } else {
                msg = 'Ha ocurrido un error. Por favor intente nuevamete';
                cls = 'error_cart';
                msgCart(msg,cls,opc);
            }
        }).fail(function (result) {
            msg = 'Ha ocurrido un error. Por favor intente nuevamete';
            cls = 'error_cart';
            msgCart(msg,cls,opc);
        });
    });

    jQuery('#btn_confirm').on('click', function () {
        var btn = jQuery('#btn_confirm');
        var data = jQuery('#frmConfirm').serialize();
        var url = jQuery("#frmConfirm").attr("action");
        var button = btn.html();
        btn.html('<i class="fa fa-spinner fa-spin"></i>')
        jQuery.post(url, data,  function (result) {
            if(result.success) {
                window.location.href = "/checkout";
            } else {
                btn.html(button);
                $('#msg_confirm').html(result.msg);
                $('#msg_confirm').fadeIn();
                setTimeout(function () {
                    $('#msg_confirm').fadeOut();
                }, 5000);
            }
        }).fail(function(result) {
            btn.html(button);
            msg = 'Ha ocurrido un error. Por favor intente nuevamete';
            $('#msg_confirm').html(msg);
            $('#msg_confirm').fadeIn();
            setTimeout(function () {
                $('#msg_confirm').fadeOut();
            }, 5000);
        });
    });

});


function disabledCant(opc)
{
    if(parseInt(jQuery('#cart'+opc).val()) <= 1) {
        jQuery('#m'+opc).attr('disabled', true);
    } else {
        jQuery('#m'+opc).attr('disabled', false);
    }
}

function msgCart(msg,cls,opc)
{
    jQuery('#msg_'+opc).html(msg)
        .removeClass()
        .addClass(cls)
    jQuery('#r_msg_'+opc).fadeIn();

    setTimeout(function() {
        jQuery('#r_msg_'+opc).fadeOut();
    },5000);
}

function goToChange(pd, col, ch)
{
    var msg = '';
    var cls = '';
    var opc = pd;
    if(col !== ''){
        opc += '_'+col;
    }
    //console.log(opc);
    jQuery('#msg_'+opc).html('Un momento por favor...')
        .removeClass()
        .addClass('wait_cart');
    jQuery('#r_msg_'+opc).fadeIn();
    jQuery('#id_p').val(pd);
    jQuery('#id_c').val(col);
    jQuery('#change').val(ch);
    var data = jQuery('#frmCart').serialize();
    var url = jQuery("#frmCart").attr("action");
    jQuery.post(url, data, function (result) {
        if (result.success) {
            jQuery('#cart' + opc).val(result.tArt);
            jQuery('#sub' + opc).html(result.sTMoney);
            jQuery('#totalF').html(result.tMoney);
            disabledCant(opc);
            msg = result.success;
            cls = 'success_cart';
            msgCart(msg, cls, opc);
            refreshCart(result.products, result.tProducts, result.tMoney);
        } else if(result.error) {
            msg = result.error;
            cls = 'error_cart';
            msgCart(msg,cls,opc);
        } else {
            msg = 'Ha ocurrido un error. Por favor intente nuevamete';
            cls = 'error_cart';
            msgCart(msg,cls,opc);
        }
    }).fail(function (result) {
        msg = 'Ha ocurrido un error. Por favor intente nuevamete';
        cls = 'error_cart';
        msgCart(msg,cls,opc);
    });
}
