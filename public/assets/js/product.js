jQuery(document).ready(function () {

	if(!$('#mobile-indicator').is(':visible')) {
	    $("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, preloading:0});
	 } else {
	    $("#img_01").elevateZoom({gallery:'gal1', galleryActiveClass: 'active', zoomEnabled: false, zoomActivation: "clic"});
	 }

    //pass the images to Fancybox
    $("#img_01").bind("click", function(e) {
	if(!$('#mobile-indicator').is(':visible')) {
	    var ez =   $('#img_01').data('elevateZoom');
        }
        jQuery.fancybox(ez.getGalleryList());
        return false;
    });

    disabledCant();

    jQuery('#DivLeave').on('click', function(){
        jQuery('#emailLD').val('');
        jQuery('#hn_productLD').val(jQuery('#id_p').val());
        jQuery('#hn_colorLD').val(jQuery('#id_c').val());
    });

    jQuery('#btn-leave').on('click', function(){
        jQuery('#frmLeaveD').submit();
    });

    jQuery('#frmLeaveD').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            var buttonInfo = $('#btn-leave').html();
            $('#btn-leave').html('<i class="fa fa-spinner fa-spin"></i>');
            $('#btn-leave').attr('disabled', 'disabled');
            var url = $("#frmLeaveD").attr("action");
            var data = $("#frmLeaveD").serialize();
            $.post(url, data, function (result) {
                if(result.success) {
                    $('#error-Leave').html(result.success);
                    $('#error-Leave').fadeIn();
                    setTimeout(function () {
                        $('#error-Leave').fadeOut();
                        $('#modalData').modal('toggle');
                    }, 5000);
                } else {
                    $('#error-Leave').html(result.error);
                    $('#error-Leave').fadeIn();
                    setTimeout(function () {
                        $('#error-Leave').fadeOut();
                    }, 5000);
                }
                $('#btn-leave').html(buttonInfo).removeAttr('disabled');
            }).fail(function (result) {
                $('#error-Leave').html('Ha ocurrido un error. Por favor intente de nuevo');
                $('#error-Leave').fadeIn();
                setTimeout(function () {
                    $('#error-Leave').fadeOut();
                }, 5000);
                $('#btn-leave').html(buttonInfo).removeAttr('disabled');
            });
        }
    });

    jQuery('.opc_color').on('click', function(){
        var tar = jQuery(this).attr('opc-target');
        var nameC = jQuery(this).attr('opc-name');
        var Aimgs = jQuery('#color_'+tar).val();
        var Nimgs = jQuery('#sColor_'+tar).val();
        var stock = jQuery('#stock_'+tar).val();
        var stockT = jQuery('#stockT_'+tar).val();
        var reference = jQuery('#reference_'+tar).val();
        var SKU = jQuery('#SKU_'+tar).val();
        var imgs = '';
        if(Nimgs === '0' || typeof Nimgs === "undefined") {
            imgs = '<img id="img_01" src="/assets/images/product.png" data-zoom-image="/assets/images/product.png"  alt="product" class="img-gallery"/>\n' +
                '<div id="gal1">\n' +
                '<a href="#" data-image="/assets/images/product.png" data-zoom-image="/assets/images/product.png">\n' +
                ' <img id="img_01d" src="/assets/images/dots.png" />\n' +
                '</a>\n';
        } else {
            ar = Aimgs.substring(0,Aimgs.length - 1);
            ar = ar.split('|');
            jQuery.each(ar, function(index, value) {
                if(index === 0) {
                    imgs = '<img id="img_01" src="'+ value +'" data-zoom-image="'+ value +'"  alt="product" class="img-gallery"/>\n' +
                        '<div id="gal1">\n';
                    imgs += '<a href="#" data-image="'+ value +'" data-zoom-image="'+ value +'" class="active">\n' +
                        ' <img id="img_01d" src="/assets/images/dots.png" />\n' +
                        '</a>\n';
                } else {
                    imgs += '<a href="#" data-image="'+ value +'" data-zoom-image="'+ value +'">\n' +
                        ' <img id="img_01d" src="/assets/images/dots.png" />\n' +
                        '</a>\n';
                }
                if(index === ar.length -1) {
                    imgs += '</div>';
                }
            });
        }
        var arrows = '<div id="next-arrow"><i class="fa fa-angle-right"></i> </div>\n' +
            '<div id="prev-arrow"><i class="fa fa-angle-left"></i> </div>';
        jQuery('#gallery-pd').html(arrows+imgs);
        //initiate the plugin and pass the id of the div containing gallery images
        $("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true});

        //pass the images to Fancybox
        $("#img_01").bind("click", function(e) {
            var ez =   $('#img_01').data('elevateZoom');
            jQuery.fancybox(ez.getGalleryList());
            return false;
        });

        jQuery('.opc_color').removeClass('c-active');
        jQuery(this).addClass('c-active');
        jQuery('#id_c').val(tar);
        jQuery('#name_c').val(nameC);
        jQuery('#stockP').html(stockT);
        jQuery('#referenceP').html(reference);
        jQuery('#SKUP').html(SKU);
        jQuery('#stock').val(stock);
        jQuery('#cart').val(1);
        disabledCant();

        jQuery('#DivLeave').on('click', function(){
            jQuery('#emailLD').val('');
            jQuery('#hn_productLD').val(jQuery('#id_p').val());
            jQuery('#hn_colorLD').val(jQuery('#id_c').val());
        });
    });

    jQuery('.minus').on('click', function(){
        jQuery('#cart').val(parseInt(jQuery('#cart').val()) - 1);
        disabledCant();
    });

    jQuery('.plus').on('click', function(){
        jQuery('#cart').val(parseInt(jQuery('#cart').val()) + 1);
        disabledCant();
    });

    jQuery('#addCart').on('click', function(){
        jQuery('#frmCart').validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'div',
            errorLabelContainer: jQuery("#msg_cart"),
            ignore: [],
            rules:
                {
                    cart:
                        {
                            required: true
                        }
                },
            messages:
                {
                    cart:'Por favor elija una cantidad'
                },
            invalidHandler: function () {
                jQuery("#msg_cart").fadeIn();
                jQuery("#msg_cart").removeClass()
                    .addClass('error_cart');
                setTimeout(function() {
                    $('#msg_cart').fadeOut();
                },10000);
            },
            submitHandler: function () {
                var msg = '';
                var cls = '';
                if(parseInt(jQuery('#cart').val()) > parseInt(jQuery('#stock').val())) {
                    msg = 'La cantidad no puede superar las unidades disponibles';
                    cls = 'error_cart';
                    msgCart(msg, cls);
                } else if(jQuery('#h_color').val() === 'S' && jQuery('#id_c').val() === '') {
                    msg = 'Por favor elija un color';
                    cls = 'error_cart';
                    msgCart(msg, cls);
                } else {
                    jQuery('#addCart').attr('disabled', true);
                    var data = jQuery('#frmCart').serialize();
                    var url = jQuery("#frmCart").attr("action");
                    jQuery.post(url, data, function (result) {
                        if (result.success) {
                            msg = result.success;
                            cls = 'success_cart';
                            msgCart(msg,cls);
                            //console.log(result.products);
                            refreshCart(result.products,result.tProducts,result.tMoney);
                        } else {
                            msg = result.error;
                            cls = 'error_cart';
                            msgCart(msg,cls);
                        }
                        jQuery('#addCart').attr('disabled', false);
                    }).fail(function (result) {
                        msg = 'Ha ocurrido un error. Por favor intente nuevamete';
                        cls = 'error_cart';
                        msgCart(msg,cls);
                        jQuery('#addCart').attr('disabled', false);
                    });
                }
            }
        });
    });

    $('#gallery-pd').on('click', '#next-arrow',function () {
        var obj = $('#gal1 .active');
        if(obj.next().length) {
            $('#img_01').attr('src', obj.next().attr('data-image'));
            $('#img_01').attr('data-zoom-image', obj.next().attr('data-image'));

            obj.removeClass('active');
            obj.next().addClass('active');
        }
    });

    $('#gallery-pd').on('click', '#prev-arrow', function () {
        var obj = $('#gal1 .active');
        if(obj.prev().length) {
            $('#img_01').attr('src', obj.prev().attr('data-image'));
            $('#img_01').attr('data-zoom-image', obj.prev().attr('data-image'));

            obj.removeClass('active');
            obj.prev().addClass('active');
        }
    });
});


function disabledCant()
{
    if(parseInt(jQuery('#cart').val()) <= 1) {
        jQuery('.minus').attr('disabled', true);
    } else {
        jQuery('.minus').attr('disabled', false);
    }

    if(parseInt(jQuery('#cart').val()) >= parseInt(jQuery('#stock').val())) {
        jQuery('.plus').attr('disabled', true);
    } else {
        jQuery('.plus').attr('disabled', false);
    }
}

function msgCart(msg,cls)
{
    jQuery('#msg_cart').html(msg)
        .removeClass()
        .addClass(cls)
        .fadeIn();

    setTimeout(function() {
        $('#msg_cart').fadeOut();
    },8000);
}
