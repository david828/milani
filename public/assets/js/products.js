jQuery(document).ready(function () {
    jQuery('#order').on('change', function () {
       jQuery('#o').val(jQuery(this).val());
       jQuery('#formSearch').submit();
    });

    jQuery('#finish').on('change', function () {
        jQuery('#fin').val(jQuery(this).val());
        jQuery('#formSearch').submit();
    });

    jQuery('.opc_cat').on('click', function(){
       var opc = jQuery(this).attr('opc-target');
       jQuery('#cat').val(opc);
        jQuery('#sub').val('');
        jQuery('#formSearch').submit();
    });

    jQuery('.opc_subcat').on('click', function(){
        var opc = jQuery(this).attr('opc-target');
        jQuery('#cat').val('');
        jQuery('#sub').val(opc);
        jQuery('#formSearch').submit();
    });

    jQuery('.opc_col').on('click', function(){
        var opc = jQuery(this).attr('opc-target');
        jQuery('#col').val(opc);
        jQuery('#formSearch').submit();
    });

});
