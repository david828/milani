var slider = new MasterSlider();
slider.control('arrows');
slider.setup('owl-header', {
    width:959,
    height:681,
    space: 5,
    view: 'flow',
    loop: true,
    autoplay: true,

});

function getInstagram(clientid, userid, accessToken, num_photos)
{
    $.ajax({
        url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent?access_token=' + accessToken ,
        dataType: 'jsonp',
        type: 'GET',
        data: {client_id: clientid, count: num_photos},
        success: function(data){
            //console.log(data);
            if(data.meta.code === 200) {
                for (x in data.data) {
                    if(x % 2 === 0) {
                        $('#galeria-instagram').append('<div class="ms-slide" data-delay="8">' +
                            '<img src="/assets/vendor/masterslider/style/blank.gif" data-src="' + data.data[x].images.standard_resolution.url + '" alt="Instagram"/>' +
                            '</div>');
                    } else {
                        $('#galeria-instagram2').append('<div class="ms-slide" data-delay="8">' +
                            '<img src="/assets/vendor/masterslider/style/blank.gif" data-src="' + data.data[x].images.standard_resolution.url + '" alt="Instagram"/>' +
                            '</div>');
                    }
                }
                // Masterslider gallery
                var slider = new MasterSlider();
                slider.control('arrows');
                slider.setup('galeria-instagram', {
                    width:477,
                    height:477,
                    space: 5,
                    view: 'scale',
                    loop: true,
                    autoplay: true,
                });

                // Masterslider gallery
                var slider2 = new MasterSlider();
                slider2.control('arrows');
                slider2.setup('galeria-instagram2', {
                    width:477,
                    height:477,
                    space: 5,
                    view: 'scale',
                    loop: true,
                    autoplay: true,
                });

            }
        }
    });
}

