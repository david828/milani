jQuery(document).ready(function ($) {
    "use strict"
    jQuery(".contact-map").each(function(){
        var center = [jQuery(this).attr('data-lat'), jQuery(this).attr('data-long')];
        var icon = jQuery(this).attr('icon');
        // ------- Map ------- //
        jQuery(this).gmap3({
            map: {
                options: {
                    center: center,
                    zoom: 14,
                    scrollwheel: false
                },
            },
            marker: {
                latLng: center,
                options: {
                    icon: new google.maps.MarkerImage(
                        icon,
                        new google.maps.Size(80, 120, "px", "px")
                    )
                }
            }
        });
        // ------- Map ------- //
    });

});