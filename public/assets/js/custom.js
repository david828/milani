//[Custom Javascript]

//Project:	Cosmetic Agency Html Responsive Template
//Version:	1.1
//Primary use:	Cosmetic Agency Html Responsive Template 

//add your script here

function refreshCart(pd,tP,tM)
{
    var cod = '';
    var color;
    var tObj = jQuery('#numCart08');
    var w = tObj.css('width');
    var h = tObj.css('height');
    var b = tObj.css('border-radius');
    var l = tObj.css('line-height');
    var f = tObj.css('font-size');
    var freeship = jQuery('#freeship').html();
    if(freeship) {
        freeship = '<p id="freeship" class="cart-text">'+freeship+'</p>';
    } else {
        freeship = '';
    }
    jQuery.each(pd, function(index, value){
        color = '';
        if(value.nameColor) {
            color = ' - '+value.nameColor;
        }
        cod+= '<li>\n'+
            '<a href="/products/ver/'+value.slug+'" class="photo"><img src="'+value.image+'" class="cart-thumb" alt="product" /></a>\n'+
            '<h6><a href="/products/ver/'+value.slug+'">'+value.name+color+' </a></h6>\n'+
            '<p>'+value.cant+'x - <span class="price">'+value.priceV+'</span></p>\n'+
            '</li>\n';
    });
    cod+= '<li class="total">\n' +
        '<span class="pull-right"><strong>Total: </strong><span id="totCart08"></span></span>\n' +
        '<a href="/cart" class="btn btn-default btn-cart">Carrito</a>\n' +
        freeship +
        '</li>';
    tObj.fadeIn();
    tObj.animate({width:'30px',height:'30px','border-radius':'15px','line-height':'20px', 'font-size':'15px'},500);
    tObj.animate({width:w,height:h,'border-radius':b,'line-height':l, 'font-size':f},300);
    tObj.html(tP);
    jQuery('#listCart08').html(cod);
    jQuery('#totCart08').html(tM);
}

jQuery('#btn-forgot').on('click', function () {
   jQuery('#loginModal').modal('toggle');
   jQuery('#resetModal').modal();
});

$('#frmLogin').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-login').html();
        $('#btn-login').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmLogin").attr("action");
        var data = $("#frmData").serialize();
        console.log(data);
        $.post(url, data, function (result) {
            if (result.message) {
                jQuery('#nameS').html(result.name);
                jQuery('#loginModal').modal('toggle');
                jQuery('#successModal').modal();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                $('#error-Login').html(result.error);
                $('#error-Login').fadeIn();
                setTimeout(function () {
                    $('#error-Login').fadeOut();
                }, 5000);
                $('#btn-login').html(buttonInfo);
            }
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-Login').html(response);
            $('#error-Login').fadeIn();
            setTimeout(function () {
                $('#error-Login').fadeOut();
            }, 5000);
            $('#btn-login').html(buttonInfo);
        });
    }
});

$('#frmRegister').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-register').html();
        $('#btn-register').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmRegister").attr("action");
        var data = $("#frmData").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                jQuery('#nameS').html(result.name);
                jQuery('#loginModal').modal('toggle');
                jQuery('#successModal').modal();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                $('#error-Register').html(result.error);
                $('#error-Register').fadeIn();
                setTimeout(function () {
                    $('#error-Register').fadeOut();
                }, 5000);
                $('#btn-register').html(buttonInfo);
            }
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-Register').html(response);
            $('#error-Register').fadeIn();
            setTimeout(function () {
                $('#error-Register').fadeOut();
            }, 5000);
            $('#btn-register').html(buttonInfo);
        });
    }
});

$('#frmRecovery').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-recovery').html();
        $('#btn-recovery').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmRecovery").attr("action");
        var data = $("#frmData").serialize();
        $.post(url, data, function (result) {
            $('#error-Recovery').html(result.message);
            $('#error-Recovery').fadeIn();
            setTimeout(function () {
                $('#error-Recovery').fadeOut();
            }, 5000);
            $('#btn-recovery').html(buttonInfo);
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-Recovery').html(response);
            $('#error-Recovery').fadeIn();
            setTimeout(function () {
                $('#error-Recovery').fadeOut();
            }, 5000);
            $('#btn-recovery').html(buttonInfo);
        });
    }
});

$('#frmDelete').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-delete').html();
        $('#btn-delete').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmDelete").attr("action");
        var data = $("#frmDelete").serialize();
        $.post(url, data, function (result) {
            if(result.message) {
                $('#error-Delete').html(result.message);
                $('#error-Delete').fadeIn();
                setTimeout(function () {
                    window.location ='/';
                }, 5000);
                $('#btn-delete').html('<i class="fa fa-ban"></i>').attr('disabled',true);
            } else {
                $('#error-Delete').html(result.error);
                $('#error-Delete').fadeIn();
                setTimeout(function () {
                    $('#error-Delete').fadeOut();
                }, 5000);
                $('#btn-delete').html(buttonInfo);
            }
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-Delete').html(response);
            $('#error-Delete').fadeIn();
            setTimeout(function () {
                $('#error-Delete').fadeOut();
            }, 5000);
            $('#btn-delete').html(buttonInfo);
        });
    }
});

$('#frmLoginC').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-login-cart').html();
        $('#btn-login-cart').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmLoginC").attr("action");
        var data = $("#frmData").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                jQuery('#nameS').html(result.name);
                jQuery('#successModal').modal();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                $('#error-LoginC').html(result.error);
                $('#error-LoginC').fadeIn();
                setTimeout(function () {
                    $('#error-LoginC').fadeOut();
                }, 5000);
                $('#btn-login-cart').html(buttonInfo);
            }
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-LoginC').html(response);
            $('#error-LoginC').fadeIn();
            setTimeout(function () {
                $('#error-LoginC').fadeOut();
            }, 5000);
            $('#btn-login-cart').html(buttonInfo);
        });
    }
});

$('#frmRegisterC').validate({
    errorElement: "div",
    errorLabelContainer: $("div.error"),
    ignore: [],
    submitHandler: function () {
        var buttonInfo = $('#btn-registerC').html();
        $('#btn-registerC').html('<i class="fa fa-spinner fa-spin"></i>');
        var url = $("#frmRegisterC").attr("action");
        var data = $("#frmData").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                jQuery('#nameS').html(result.name);
                jQuery('#successModal').modal();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                $('#error-RegisterC').html(result.error);
                $('#error-RegisterC').fadeIn();
                setTimeout(function () {
                    $('#error-RegisterC').fadeOut();
                }, 5000);
                $('#btn-registerC').html(buttonInfo);
            }
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $('#error-RegisterC').html(response);
            $('#error-RegisterC').fadeIn();
            setTimeout(function () {
                $('#error-RegisterC').fadeOut();
            }, 5000);
            $('#btn-registerC').html(buttonInfo);
        });
    }
});


$('#btn-login').on('click', function(){
    $('#email').val($('#emailL').val());
    $('#password').val($('#passwordL').val());
    if($('#rememberL').is(':checked')) {
        $('#remember').prop('checked', true);
    }
    //console.log($('#emailL').val());
    $('#frmLogin').submit();
});

$('#btn-register').on('click', function () {
    if($('#passwordR').val() !== $('#repasswordR').val()) {
        $('#error-Register').html('Las contraseñas no coinciden');
        $('#error-Register').fadeIn();
        setTimeout(function () {
            $('#error-Register').fadeOut();
        }, 5000);
    } else if (!$('#politics').is(':checked')) {
        $('#error-Register').html('Por favor acepta las políticas de tratamiento de datos');
        $('#error-Register').fadeIn();
        setTimeout(function () {
            $('#error-Register').fadeOut();
        }, 5000);
    } else {
        $('#email').val($('#emailR').val());
        $('#password').val($('#passwordR').val());
        $('#name').val($('#nameR').val());
        $('#document').val($('#documentR').val());
        $('#person').val($('#personR').val());
        $('#doctype').val($('#doctypeR').val());
        $('#phone').val($('#phoneR').val());
        $('#frmRegister').submit();
    }
});


$('#btn-recovery').on('click', function(){
    $('#email').val($('#emailF').val());
    $('#frmRecovery').submit();
});

$('#btn-login-cart').on('click', function(){
    $('#email').val($('#emailLC').val());
    $('#password').val($('#passwordLC').val());
    if($('#rememberLC').is(':checked')) {
        $('#remember').prop('checked', true);
    }
    $('#frmLoginC').submit();
});

$('#btn-registerC').on('click', function () {
    if($('#passwordRC').val() !== $('#repasswordRC').val()) {
        $('#error-RegisterC').html('Las contraseñas no coinciden');
        $('#error-RegisterC').fadeIn();
        setTimeout(function () {
            $('#error-RegisterC').fadeOut();
        }, 5000);
    } else if (!$('#politicsC').is(':checked')) {
        $('#error-RegisterC').html('Por favor acepta las políticas de tratamiento de datos');
        $('#error-RegisterC').fadeIn();
        setTimeout(function () {
            $('#error-RegisterC').fadeOut();
        }, 5000);
    } else {
        $('#email').val($('#emailRC').val());
        $('#password').val($('#passwordRC').val());
        $('#name').val($('#nameRC').val());
        $('#document').val($('#documentRC').val());
        $('#person').val($('#personRC').val());
        $('#doctype').val($('#doctypeRC').val());
        $('#phone').val($('#phoneRC').val());
        $('#frmRegisterC').submit();
    }
});

$("#tx_cellphone").inputmask("999 999 9999", {"placeholder": "### ### ####"});
$("#tx_phone").inputmask("(999) 999 9999", {"placeholder": "(###) ### ####"});
$("#phoneR").inputmask("(999) 999 9999", {"placeholder": "(###) ### ####"});
$("#phoneRC").inputmask("(999) 999 9999", {"placeholder": "(###) ### ####"});

function validateEmail(email) {
    var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return re.test(email);
}


