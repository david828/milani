jQuery(document).ready(function () {

    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    /* Facturación*/
    $('#sl_country').on('change', function () {
        if(this.value === '') {
            $('#sl_state').empty();
            $('#sl_city').empty();
        } else {
            var url = $('#hn_state').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_state').empty();
                $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_state').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        }
    });

    $('#sl_state').on('change', function () {
        if(this.value === '') {
            $('#sl_city').empty();
        } else {
            var url = $('#hn_city').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_city').empty();
                $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_city').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        }
    });

    /* Envío */
    $('#sl_countryS').on('change', function () {
        if(this.value === '') {
            $('#sl_stateS').empty();
            $('#sl_cityS').empty();
        } else {
            var url = $('#hn_stateS').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_stateS').empty();
                $('#sl_stateS').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_stateS').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        }
    });

    $('#sl_stateS').on('change', function () {
        if(this.value === '') {
            $('#sl_cityS').empty();
        } else {
            var url = $('#hn_city').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_cityS').empty();
                $('#sl_cityS').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_cityS').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        }
    });

    $('#sl_cityS').on('change', function () {
        if(this.value === '') {
            $('#hn_city2').empty();
        } else {
            $('#hn_city2').val(this.value);
            var url = '/tarifa-envio';
            var data = $("#frmValues").serialize();
            jQuery.post(url, data, function (result) {
                if (result.success) {
                    newValues(result);
                } else {
                    newValues(result);
                }
                $('#hn_ship').val(result.ship);
            }).fail(function (result) {});
        }
    });

    jQuery('#btn_like_guess').on('click', function () {
       jQuery('#divLogin').fadeOut();
       jQuery('.sepCart').fadeIn();
       jQuery('#all_account').fadeIn();
    });

    jQuery('#btn_create_account').on('click', function () {
        jQuery('.sepCart').fadeOut();
        jQuery('#all_account').fadeOut();
        jQuery('#divLogin').fadeIn();
    });

    jQuery('#btn_copy_info').on('change', function () {
        if(jQuery('#btn_copy_info').is(':checked')) {
            jQuery('#sl_doctypeS').val(jQuery('#sl_doctype').val());
            jQuery('#tx_nameS').val(jQuery('#tx_name').val());
            jQuery('#tx_emailS').val(jQuery('#tx_email').val());
            jQuery('#tx_phoneS').val(jQuery('#tx_phone').val());
            jQuery('#tx_addressS').val(jQuery('#tx_address').val());
            jQuery('#tx_documentS').val(jQuery('#tx_document').val());
            jQuery('#sl_countryS').html(jQuery('#sl_country').html());
            jQuery('#sl_countryS').val(jQuery('#sl_country').val());
            jQuery('#sl_stateS').html(jQuery('#sl_state').html());
            jQuery('#sl_stateS').val(jQuery('#sl_state').val());
            jQuery('#sl_cityS').html(jQuery('#sl_city').html());
            jQuery('#sl_cityS').val(jQuery('#sl_city').val()).change();
        } else {
            jQuery('#sl_doctypeS').val('');
            jQuery('#tx_nameS').val('');
            jQuery('#tx_emailS').val('');
            jQuery('#tx_phoneS').val('');
            jQuery('#tx_addressS').val('');
            jQuery('#tx_documentS').val('');
            jQuery('#sl_countryS').val('');
            jQuery('#sl_stateS').html('');
            jQuery('#sl_cityS').html('').change();
        }
    });

    jQuery('.invoiceC').on('change', function() {
        var obj = $('#hn_id_address').val();
        if(jQuery('#btn_copy_info').is(':checked') || obj === 'IG') {
            var id = this.id;
            jQuery('#'+id+'S').val(this.value).change();
        }
    });


    jQuery('#btnCoupon').on('click', function(){
        var buttonInfo = $('#btnCoupon').html();
        $('#btnCoupon').html('<i class="fa fa-spinner fa-spin"></i>');
        $('#hn_coupon').val($('#coupon_d').val());
        var url = $("#frmCoup").attr("action");
        var data = $("#frmValues").serialize();
        jQuery.post(url, data, function (result) {
            if (result.success) {
                newValues(result);
                $('#coupon_d').prop('disabled', true);
                msg = result.success;
                cls = 'success_cart';
                msgCart(msg, cls, 'coupon');
                $('#btnCoupon').html(buttonInfo).fadeOut();
                $('#btnLimpiar').fadeIn();
            } else if (result.error) {
                newValues(result);
                msg = result.error;
                cls = 'error_cart';
                msgCart(msg, cls, 'coupon');
                $('#btnCoupon').html(buttonInfo);
            } else {
                newValues(result);
                msg = 'Ha ocurrido un error. Por favor intente nuevamete';
                cls = 'error_cart';
                msgCart(msg, cls, 'coupon');
                $('#btnCoupon').html(buttonInfo);
            }
        }).fail(function (result) {
            msg = 'Ha ocurrido un error. Por favor intente nuevamete';
            cls = 'error_cart';
            msgCart(msg, cls, 'coupon');
            $('#btnCoupon').html(buttonInfo);
        });
    });

    jQuery('#coupon_d').on('keypress', function(e){
        var tecla = (document.all) ? e.keyCode :e.which;
        return (tecla!==13);
    });

    jQuery('#btnLimpiar').on('click', function(){
        var buttonInfo = $('#btnLimpiar').html();
        $('#btnLimpiar').html('<i class="fa fa-spinner fa-spin"></i>');
        $('#hn_coupon').val('|');
        var url = $("#frmCoup").attr("action");
        var data = $("#frmValues").serialize();
        jQuery.post(url, data, function (result) {
            if (result.success) {
                newValues(result);
                $('#hn_coupon').val('');
                $('#coupon_d').val('').prop('disabled',false);
                $('#btnLimpiar').html(buttonInfo).fadeOut();
                $('#btnCoupon').fadeIn();
            } else if (result.error) {
                newValues(result);
                msg = result.error;
                cls = 'error_cart';
                msgCart(msg, cls, 'coupon');
                $('#btnLimpiar').html(buttonInfo);
            } else {
                newValues(result);
                msg = 'Ha ocurrido un error. Por favor intente nuevamete';
                cls = 'error_cart';
                msgCart(msg, cls, 'coupon');
                $('#btnLimpiar').html(buttonInfo);
            }
        }).fail(function (result) {
            msg = 'Ha ocurrido un error. Por favor intente nuevamete';
            cls = 'error_cart';
            msgCart(msg, cls, 'coupon');
            $('#btnLimpiar').html(buttonInfo);
        });
    });

    $('#btnBuy').on('click', function () {
        $('#billing-coupon').val($('#coupon_d').val());

        if (!$('#politicsB').is(':checked')) {
            $('#msg-buy').html('Por favor acepta las políticas de tratamiento de datos');
            $('#msg-buy').fadeIn();
            setTimeout(function () {
                $('#msg-buy').fadeOut();
            }, 5000);
        } else if(!validateEmail($('#tx_emailS').val()) || !validateEmail($('#tx_email').val())) {
            $('#msg-buy').html('Por favor, escribe direcciones de correo válidas.');
            $('#msg-buy').fadeIn();
        } else {
            $('#frmDataInvoice').submit();
        }
    });

    $('.customeEmail').on('change', function () {
        $('#msg-buy').fadeOut();
    });

    $('#frmDataInvoice').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.modal-checkout').modal();
        }
    });

    $('#bt_checkout').on('click', function () {
        $('.loader').fadeIn();
        var url = $("#frmCheck").attr("action");
        var data = $("#frmDataInvoice").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $('.payu-form').html(result.form);
                $("#frmPayu").submit();
            } else {
                $('.loader').fadeOut();
                $('.modal-checkout').modal('hide');
                $('#msg-buy').html(result.error);
                $('#msg-buy').fadeIn();
                setTimeout(function () {
                    $('#msg-buy').fadeOut();
                }, 5000);
            }
        }).fail(function (result) {
            $('.loader').fadeOut();
            var response = $.fn.displayError(result);
            $('#msg-buy').html(result.error);
            $('#msg-buy').fadeIn();
            setTimeout(function () {
                $('#msg-buy').fadeOut();
            }, 5000);
            $('#msg-buy').modal('hide');
        });
    });

    $('.opt_address').on('change', function() {
        var obj = $(this).attr('id');
        //console.log(obj);
        $('#hn_id_address').val(obj);
        if(obj === 'NN') {
            $('#sl_doctypeS').val('');
            $('#tx_documentS').val('');
            $('#tx_titleS').val('');
            $('#tx_nameS').val('');
            $('#tx_emailS').val('');
            $('#tx_phoneS').val('');
            $('#tx_addressS').val('');
            $('#sl_countryS').val('');
            $('#sl_stateS').empty();
            $('#sl_cityS').empty().change();
            $('#tx_code_postal').val('');
            $('#ta_additional').val('');
        }else if(obj === 'IG') {
            $('#sl_doctypeS').val($('#sl_doctype').val());
            $('#tx_documentS').val($('#tx_document').val());
            $('#tx_titleS').val('--');
            $('#tx_nameS').val($('#tx_name').val());
            $('#tx_emailS').val($('#tx_email').val());
            $('#tx_phoneS').val($('#tx_phone').val());
            $('#tx_addressS').val($('#tx_address').val());
            $('#sl_countryS').val($('#sl_country').val());
            $('#sl_countryS').val($('#sl_country').val());
            $('#sl_stateS').html($('#sl_state').html());
            $('#sl_stateS').val($('#sl_state').val());
            $('#sl_cityS').html($('#sl_city').html());
            $('#sl_cityS').val($('#sl_city').val()).change();
            $('#tx_code_postal').val('');
            $('#ta_additional').val('');
        } else {
            $('#sl_doctypeS').val($('#documenttype_'+obj).val());
            $('#tx_documentS').val($('#document_'+obj).val());
            $('#tx_titleS').val($('#title_'+obj).val());
            $('#tx_nameS').val($('#name_'+obj).val());
            $('#tx_phoneS').val($('#phone_'+obj).val());
            $('#tx_addressS').val($('#address_'+obj).val());
            $('#sl_countryS').val($('#country_'+obj).val());
            $('#sl_stateS').html($('#listState_'+obj).html());
            $('#sl_cityS').html($('#listCity_'+obj).html()).change();
            $('#tx_code_postal').val($('#zipcode_'+obj).val());
            $('#ta_additional').val($('#additional_'+obj).val());
        }
    });
});


function msgCart(msg,cls,opc)
{
    jQuery('#msg_'+opc).html(msg)
        .removeClass()
        .addClass(cls)
    jQuery('#r_msg_'+opc).fadeIn();

    setTimeout(function() {
        jQuery('#r_msg_'+opc).fadeOut();
    },7000);
}

function newValues(data)
{
    $('#hn_discount').val(data.disc);
    $('#subValue2').html(data.subtV);
    $('#discountValue').html(data.discV);
    $('#taxValue').html(data.taxV);
    $('#shipValue').html(data.shipV);
    $('#TotalValue').html(data.totalV);
}

