$(document).ready(function () {
    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            var buttonInfo = $('#bt_apply').html();
            $('#bt_apply').html('<i class="fa fa-spinner fa-spin"></i>');
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    jQuery('#msg-account').html(result.message)
                        .removeClass()
                        .addClass('success_cart')
                        .fadeIn();
                    setTimeout(function() {
                        jQuery('#msg-account').fadeOut();
                    },5000);
                } else {
                    jQuery('#msg-account').html(result.error)
                        .removeClass()
                        .addClass('error_cart')
                        .fadeIn();
                    setTimeout(function() {
                        jQuery('#msg-account').fadeOut();
                    },5000);
                }
                $('#bt_apply').html(buttonInfo);
            }).fail(function (result) {
                var response = $.fn.displayError(result);
                jQuery('#msg-account').html(response)
                    .removeClass()
                    .addClass('error_cart')
                    .fadeIn();
                setTimeout(function() {
                    jQuery('#msg-account').fadeOut();
                },5000);
                $('#bt_apply').html(buttonInfo);
            });
        }
    });

});