$(document).ready(function () {

    $('.country_class').on('change', function () {
        var tar = $(this).attr('target');
        if(this.value !== '') {
            var url = $('#hn_state').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_state_'+tar).empty();
                $('#sl_state_'+tar).append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_state_'+tar).append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_state_'+tar).empty();
            $('#sl_state_'+tar).append($('<option>').text("Seleccione una opción").attr('value', ''));
            $('#sl_city_'+tar).empty();
            $('#sl_city_'+tar).append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('.state_class').on('change', function () {
        var tar = $(this).attr('target');
        if(this.value !== '') {
            var url = $('#hn_city').val().replace('IDX', this.value);
        $.get(url, function (result) {
            result = $.parseJSON(result);
            $('#sl_city_'+tar).empty();
            $('#sl_city_'+tar).append($('<option>').text("Seleccione una opción").attr('value', ''));
            $.each(result, function (i, obj) {
                $('#sl_city_'+tar).append($('<option>').text(obj.name).attr('value', obj.num));
            });
        });
        } else {
            $('#sl_city_'+tar).empty();
            $('#sl_city_'+tar).append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('.btn_ap_address').on('click', function(){
        var tar = $(this).attr('target');
        $('#frmData_'+tar).validate({
            errorElement: "div",
            errorLabelContainer: $("div.error"),
            ignore: [],
            submitHandler: function () {
                var buttonInfo = $('#bt_apply_'+tar).html();
                $('#bt_apply_'+tar).html('<i class="fa fa-spinner fa-spin"></i>');
                $('#tx_title').val($('#tx_title_'+tar).val());
                $('#sl_documenttype').val($('#sl_documenttype_'+tar).val());
                $('#tx_document').val($('#tx_document_'+tar).val());
                $('#tx_name').val($('#tx_name_'+tar).val());
                $('#tx_phone').val($('#tx_phone_'+tar).val());
                $('#tx_address').val($('#tx_address_'+tar).val());
                $('#sl_country').val($('#sl_country_'+tar).val());
                $('#sl_state').val($('#sl_state_'+tar).val());
                $('#sl_city').val($('#sl_city_'+tar).val());
                $('#tx_zipcode').val($('#tx_zipcode_'+tar).val());
                $('#ta_additional').val($('#ta_additional_'+tar).val());
                $('#hn_id').val(tar);
                var url = $("#frmData").attr("action").replace('IDX', tar);
                var data = $("#frmData").serialize();
                $.post(url, data, function (result) {
                    if (result.message) {
                        if(result.id === 'NN'){
                            location.reload();
                        } else {
                            jQuery('#msg-account_'+tar).html(result.message)
                                .removeClass()
                                .addClass('success_cart')
                                .fadeIn();
                            setTimeout(function() {
                                jQuery('#msg-account_'+tar).fadeOut();
                            },5000);
                        }
                    } else {
                        jQuery('#msg-account_'+tar).html(result.error)
                            .removeClass()
                            .addClass('error_cart')
                            .fadeIn();
                        setTimeout(function() {
                            jQuery('#msg-account_'+tar).fadeOut();
                        },5000);
                    }
                    $('#bt_apply_'+tar).html(buttonInfo);
                }).fail(function (result) {
                    var response = $.fn.displayError(result);
                    jQuery('#msg-account_'+tar).html(response)
                        .removeClass()
                        .addClass('error_cart')
                        .fadeIn();
                    setTimeout(function() {
                        jQuery('#msg-account_'+tar).fadeOut();
                    },5000);
                    $('#bt_apply_'+tar).html(buttonInfo);
                });
            }
        });
    });

    $('.trash-address').on('click', function(){
        var tar = $(this).attr('target');
        $('#trash_'+tar).fadeOut();
        $('#confirm_'+tar).fadeIn();
    });

    $('.refuse_trash').on('click', function(){
        var tar = $(this).attr('target');
        $('#confirm_'+tar).fadeOut();
        $('#trash_'+tar).fadeIn();
    });

    $('.confirm_trash').on('click', function() {
        var tar = $(this).attr('target');
        $('#hn_trash').val(tar);
        $('#confirm_'+tar).fadeOut();
        $('#spinner_'+tar).fadeIn();
        var url = $("#frmDeleteAddress").attr("action").replace('IDX', tar);
        var data = $("#frmDeleteAddress").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $('#panel_'+tar).remove();
            } else {
                $('#spinner_'+tar).fadeOut();
                $('#error_'+tar).fadeIn();
                setTimeout(function() {
                    $('#error_'+tar).fadeOut();
                    $('#trash_'+tar).fadeIn();
                },5000);
            }
        }).fail(function (result) {
            $('#spinner_'+tar).fadeOut();
            $('#error_'+tar).fadeIn();
            setTimeout(function() {
                $('#error_'+tar).fadeOut();
                $('#trash_'+tar).fadeIn();
            },5000);
        });
    });
});