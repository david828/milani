$(document).ready(function () {


    var ratio = 1 / 1;
    $imagewidth = $('.width-calc').width();
    if ($imagewidth > 300) {
        $imagewidth = 300;
    }
    $imageheight = Math.round($imagewidth / ratio);

    /**************Upload Image Article**************/
    $image = $('.p-image');
    $image.cropper({
        aspectRatio: 1 / 1,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#articleCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas');
        var dataURL = canvas.toDataURL();
        $('.profile-avatar').attr('src', dataURL);
        $('.modal-profile').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-profile').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imágen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }
    /**************End Upload Image Article**************/

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            jQuery('#msg-account').html('<i class="fa fa-spinner fa-spin"></i>')
                .removeClass()
                .addClass('wait_cart')
                .fadeIn();
            var url = $("#frmData").attr("action");
            var data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    jQuery('#msg-account').html(result.message)
                        .removeClass()
                        .addClass('success_cart')
                        .fadeIn();
                    setTimeout(function() {
                        location.reload();
                    },5000);
                    var img = result.image;
                    $('#img-account').attr('src',img);
                } else {
                    jQuery('#msg-account').html(result.error)
                        .removeClass()
                        .addClass('error_cart')
                        .fadeIn();
                    setTimeout(function() {
                        jQuery('#msg-account').fadeOut();
                    },5000);
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                var response = $.fn.displayError(result);
                jQuery('#msg-account').html(response)
                    .removeClass()
                    .addClass('error_cart')
                    .fadeIn();
                setTimeout(function() {
                    jQuery('#msg-account').fadeOut();
                },5000);
            });
        }
    });

});