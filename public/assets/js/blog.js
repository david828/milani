jQuery(document).ready(function () {

    $('.images_blog').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        navText: [ '<b><i class="icon-arrow_back"></i></b>', '<b><i class="icon-arrow_forward"></i></b>' ],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:false
            },
            1000:{
                items:1,
                nav:true,
                loop:false
            }
        }
    });

    if ($('#total').val()  > 5) {
        $('.viewmore').show();
    }

    $('.viewmore').on('click', function () {
        var cont = parseInt($('#cont').val()) * 5;
        var i = 0;
        $('.comment1').each(function () {
            if (i < cont) {
                $(this).show();
            } else {
                return false;
            }
            i++;
        });

        if (($('#total').val()) <= cont) {
            $('.viewmore').hide();
        }
        $('#cont').val(parseInt($('#cont').val()) + 1);
    });

    $('#comment-form').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            url = $("#comment-form").attr("action");
            var formData = new FormData($("#comment-form")[0]);
            var csrf = $('input[name=_token]').val();
            jQuery.ajax(
                url, {
                    method: "POST",
                    headers: {
                        'X-CSRF-Token': csrf,
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.message == 'ok') {
                            jQuery('#comments-1').prepend('<li><div class="comment"><div class="img-thumbnail">'+
                                                '<img src="/assets/images/user.png" alt="avatar" class="avatar">'+
                                                '</div><div class="comment-block"><div class="comment-arrow"></div>'+
                                                '<span class="comment-by">'+
                                                '<strong>'+$('#name').val()+'</strong>'+
                                                '</span>'+
                                                '<p>' + $('#comment').val() + '</p>'+
                                                '<span class="date pull-right">hace 0 segundos</span>'+
                                                '</div></div></li>');
                            cant = jQuery('#num_comm').val();
                            tot = parseInt(cant) + 1;
                            jQuery('#num_comm').val(tot);
                            jQuery('#tot_com').html(tot);
                            alert('Muchas gracias por su comentario');
                        } else {
                            alert('Ha ocurrido un error, por favor intente de nuevo');
                        }
                    },
                    error: function (result) {
                        alert('Ha ocurrido un error, por favor intente de nuevo');
                    }
                });
        }
    });
});