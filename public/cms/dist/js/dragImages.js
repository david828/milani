$(document).ready(function(){
    $('#btnReorder').on('click', function() {
        $('.modal-order').modal();
        $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
        $('.image_link').attr("href","javascript:void(0);");
        $('.image_link').css("cursor","move");

    });

    $("#btnOrder").click(function(){
        $('.loading-app').fadeIn();
        $("ul.reorder-photos-list").sortable('destroy');
        var h = '';
        $("ul.reorder-photos-list li").each(function () {
            h += $(this).attr('id').substr(9) + '|';
        });
        h= h.substr(0, h.length -1);
        $('#id_array').val(h);
        url = $("#frmOrder").attr("action");
        data = $("#frmOrder").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                window.location.reload();
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });
});