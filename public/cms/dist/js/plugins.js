
function base64ToBlob(base64, mime) {
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];
    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: mime});
}

function maximoCaracteres(e,maximo,campo) {
    //console.log($('#'+campo).val().length);
    var code = e.charCode || e.keyCode;
    if (code == 37 || code == 39 || code == 8 || code == 46){
        return true;
    } else {
        if($('#'+campo).val().length > maximo){
            return false;
        } else {
            return true;
        }
    }
}

function contador(max,campo,alerta) {
    if($('#'+campo).val().length >= max){
        $('#'+alerta).text(0);
        $('#'+alerta).css('color','#FF3333');
    } else {
        $('#'+alerta).text((max-$('#'+campo).val().length));
        $('#'+alerta).css('color','#000');

    }

}