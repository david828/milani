$(function () {
    $.fn.displayError = function (result) {
        if (result.responseText && result.status != 500) {
            message = result.responseText.replace(/\u005B/g, "").replace(/\u005D/g, "");
            message = JSON.parse(message);
            arr = [];
            cont = 0;
            for (var i in message) {
                arr[cont++] = message[i];
            }
        } else {
            arr = new Array('');
        }

        return arr[0];
    };
});