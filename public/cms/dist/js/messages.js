$(function () {
    $.fn.getModal = function (data) {
        $('.msg-body').html('<p>' + data.message + '</p>');
        $('#msgModal').removeClass();
        $('#msgModal').addClass('modal fade');
        switch (data.type) {
            case "success":
                $('#msgModal').addClass('modal-success');
                break;
            case "info":
                $('#msgModal').addClass('modal-info');
                break;
            case "warning":
                $('#msgModal').addClass('modal-warning');
                break;
            case "error":
                $('#msgModal').addClass('modal-danger');
                break;
        }
        $('#msgModal').modal();
        if (data.redirect) {
            $('.btn-outline').on('click', function (event) {
                window.location = data.redirect;
            })
        }
    };
});