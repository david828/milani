$(document).ready(function () {

    $('.status-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_status').val($(this).attr('go-to'));
        targ = this;
        url = $('#frmEst').attr('action');
        data = $('#frmEst').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });
});