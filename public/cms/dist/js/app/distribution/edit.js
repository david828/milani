$(document).ready(function () {

    $('#sl_country').on('change', function () {
        if($('#sl_country').val() !== '') {
            var url = $('#hn_state').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_state').empty();
                $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_state').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_state').empty();
            $('#sl_city').empty();
        }
    });

    $('#sl_state').on('change', function () {
        if($('#sl_state').val() !== '') {
            var url = $('#hn_city').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_city').empty();
                $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_city').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_city').empty();
        }
    });


    /**************Upload Image**************/
    var ratio = 1 / 1;
    $imagewidth = $('.width-calc').width();
    if ($imagewidth > 400) {
        $imagewidth = 400;
    }
    $imageheight = Math.round($imagewidth / ratio);

    $image = $('.p-image');
    $image.cropper({
        aspectRatio: 1 / 1,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.image-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#imageCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas', {
            width: 200,
            height: 200
        });
        var dataURL = canvas.toDataURL();
        $('.modal-images').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-images').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imagen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {

            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action"); //Fijémonos que el formulario se llame igual
            fnfile = $('#hn_file').val();
            var formData = new FormData($("#frmData")[0]);
            image = fnfile;
            base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
            var blob = base64ToBlob(base64ImageContent, 'image/png');
            if(fnfile) {
                //console.log(fnfile);
                formData.append('fl_image', blob);
                $('#hn_file').val('');
            }
            var csrf = $('input[name=_token]').val();

            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    response = $.fn.displayError(result);
                    $.fn.getModal({
                        message: response,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });
});