$(document).ready(function () {

    $("#tx_date").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});

    $(".select2").select2();

    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

    $('#frmPass').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmPass").attr("action");
            data = $("#frmPass").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $("#frmData")[0].reset();
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

});


var Demo = (function () {

    function imageUpload() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $uploadCrop.cropper('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                alert("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $uploadCrop = $('#upload-demo').cropper({
            viewport: {
                width: 400,
                height: 400,
                type: 'square'
            },
            boundary: {
                width: 400,
                height: 400
            }
        });
        $('#upload').on('change', function () {
            readFile(this);
        });
        $('.upload-result').on('click', function (ev) {
                $uploadCrop.cropper('result', 'canvas').then(function (resp) {
                $('#hd_image').val(resp);
                $('.loading-app').fadeIn();
                url = $("#frmImg").attr("action");
                data = $('#frmImg').serialize();
                $.post(url, data, function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                        $('.profile-user-img, .header-avatar').attr('src', result.url);
                    } else {
                        $.fn.getModal({message: result.message, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                }).fail(function (result) {
                    response = $.fn.displayError(result);
                    $.fn.getModal({message: result.message, type: "error"});
                    $('.loading-app').fadeOut();
                });
            });
        });
    }

    function init() {
        imageUpload();
    }

    return {
        init: init
    };
})();

Demo.init();