$(document).ready(function () {
    $(".select2").select2();

    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $('.tags').tagsinput();

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $('.loading-app').fadeIn();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

});