$(document).ready(function () {
    $('#sl_country').on('change', function () {
        if(this.value !== '') {
            var url = $('#hn_state').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_state').empty();
                $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_state').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_state').empty();
            $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
            $('#sl_city').empty();
            $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('#sl_state').on('change', function () {
        if(this.value !== '') {
            var url = $('#hn_city').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_city').empty();
                $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_city').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_city').empty();
            $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {

            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action"); //Fijémonos que el formulario se llame igual
            var formData = new FormData($("#frmData")[0]);

            var csrf = $('input[name=_token]').val();

            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    msg = 'Ha ocurrido un error. </br>';
                    if(result.responseJSON['tx_email']){
                        msg+= '</br>' + result.responseJSON['tx_email'];
                    }
                    $.fn.getModal({
                        message: msg,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });

});