$(document).ready(function () {

    $('#sl_country').on('change', function () {
        if(this.value !== '') {
            var url = $('#hn_state').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_state').empty();
                $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_state').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_state').empty();
            $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
            $('#sl_city').empty();
            $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('#sl_state').on('change', function () {
        if(this.value !== '') {
            var url = $('#hn_city').val().replace('IDX', this.value);
            $.get(url, function (result) {
                result = $.parseJSON(result);
                $('#sl_city').empty();
                $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
                $.each(result, function (i, obj) {
                    $('#sl_city').append($('<option>').text(obj.name).attr('value', obj.num));
                });
            });
        } else {
            $('#sl_city').empty();
            $('#sl_city').append($('<option>').text("Seleccione una opción").attr('value', ''));
        }
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

});