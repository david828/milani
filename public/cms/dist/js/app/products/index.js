$(document).ready(function () {

    $('.trash-data').on('click', function () {
        $('#hd_trash').val($(this).attr('trash'));
        $('.trash-modal').modal();
    });

    $('.best-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_best').val($(this).attr('go-to'));
        var targ = this;
        var url = $('#frmBest').attr('action');
        var data = $('#frmBest').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });

    $('.new-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_new').val($(this).attr('go-to'));
        var targ = this;
        var url = $('#frmNew').attr('action');
        var data = $('#frmNew').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });

    $('.soon-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_soon').val($(this).attr('go-to'));
        var targ = this;
        var url = $('#frmSoon').attr('action');
        var data = $('#frmSoon').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });

    $('.status-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_status').val($(this).attr('go-to'));
        targ = this;
        url = $('#frmEst').attr('action');
        data = $('#frmEst').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });

    $('#bt_trash').on('click', function () {
        var str = $("#frmDes").attr("action");
        var url = str.replace('IDX', $('#hd_trash').val());
        var data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            $('.loading-app').fadeIn();
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('.target-' + $('#hd_trash').val()).remove();
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });
});