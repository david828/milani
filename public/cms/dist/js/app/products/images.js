$(document).ready(function () {

    function formatState (state) {
        if (!state.id) {
            return state.text;
        }
        var baseUrl = "/images/colors";
        var $state = $(
            '<span><img src="' + baseUrl + '/' + state.element.getAttribute('image') + '" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
    };

    $("#sl_color").select2({
        templateResult: formatState
    });

    $('#sl_color').on('change', function(){
        location.href = "?col="+$(this).val();
    });

    var ratio = 270 / 293;
    var $imagewidth = $('.width-calc').width();
    if ($imagewidth > 500) {
        $imagewidth = 500;
    }
    var $imageheight = Math.round($imagewidth / ratio);

    /**************Upload Image Article**************/
    var $image = $('.p-image');
    $image.cropper({
        aspectRatio: 270 / 293,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#articleCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas', {
            width: 540,
            height: 586
        });
        var dataURL = canvas.toDataURL();
        $('.profile-avatar').attr('src', dataURL);
        $('.modal-profile').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-profile').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imágen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }
    /**************End Upload Image Article**************/

    $('.view-image').on('click', function () {
        var img = $(this).attr('img-target');
        $('.content-viewer').html('<img src="' + img + '" />');
    });

    $('.delete-image').on('click', function () {
        $('#hd_trash').val($(this).attr('img-target'));
        $('.trash-modal').modal();
    });

    $('#bt_trash').on('click', function () {
        $('.loading-app').fadeIn();
        var url = $("#frmDes").attr("action");
        var data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('#img' + $('#hd_trash').val()).remove();
                $('#image_li_' + $('#hd_trash').val()).remove();
                $('.content-viewer').html('');
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });

    /*$('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            var url = $("#frmData").attr("action");
            var data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $("#frmData")[0].reset();
                    $('#gallery').html('');
                    var img = $.parseJSON(result.images);
                    var dir = result.dir;
                    $.each(img, function (i, item) {
                        $('#gallery').append('<div class="col-xs-4 col-md-4" id="img' + item.id + '">' +
                            '<div class="content-collect mb-20">' +
                            '<img src="' + dir  + item.image + '" />' +
                            '<div class="content-collect-options">' +
                            '<i class="fa fa-eye bg-blue view-image" img-target="' + dir + item.image + '"></i>' +
                            '<i class="fa fa-trash-o bg-red delete-image" img-target="' + item.id + '"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    });

                    $('.view-image').on('click', function () {
                        var img = $(this).attr('img-target');
                        $('.content-viewer').html('<img src="' + img + '" />');
                    });
                    $('.delete-image').on('click', function () {
                        $('#hd_trash').val($(this).attr('img-target'));
                        $('.trash-modal').modal();
                    });

                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                var response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });*/


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            var url = $("#frmData").attr("action");
            var formData = new FormData($("#frmData")[0]);
            var image = $('#hn_file').val();
            var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
            var blob = base64ToBlob(base64ImageContent, 'image/png');
            formData.append('hn_file', blob);
            var csrf = $('input[name=_token]').val();
            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                        $("#frmData")[0].reset();
                        $('#gallery').html('');
                        $('#galleryOrder').html('');
                        var img = $.parseJSON(result.images);
                        var dir = result.dir;
                        $.each(img, function (i, item) {
                            $('#gallery').append('<div class="col-xs-4 col-md-4" id="img' + item.id + '">' +
                                '<div class="content-collect mb-20">' +
                                '<img src="' + dir  + item.image + '" />' +
                                '<div class="content-collect-options">' +
                                '<i class="fa fa-eye bg-blue view-image" img-target="' + dir + item.image + '"></i>' +
                                '<i class="fa fa-trash-o bg-red delete-image" img-target="' + item.id + '"></i>' +
                                '</div>' +
                                '</div>' +
                                '</div>');
                            $('#galleryOrder').append('<li id="image_li_' + item.id + '" class="ui-sortable-handle">' +
                                '<a href="javascript:void(0);" style="float:none;" class="image_link">' +
                                '<img src="' + dir + item.image + '" alt="">' +
                                '</a>' +
                                '</li>');
                        });

                        $('.view-image').on('click', function () {
                            var img = $(this).attr('img-target');
                            $('.content-viewer').html('<img src="' + img + '" />');
                        });
                        $('.delete-image').on('click', function () {
                            $('#hd_trash').val($(this).attr('img-target'));
                            $('.trash-modal').modal();
                        });

                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    var response = $.fn.displayError(result);
                    $.fn.getModal({
                        message: response,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });


        }
    });

    $('.view-alt').on('click', function () {
        var image = $(this).attr('img-target');
        var alt = $(this).attr('img-alt');
        $('#hd_image').val(image);
        $('#tx_alt').val(alt);
        $('.alt-modal').modal();
    });

    $('#bt_Alt').on('click', function () {
        $('.loading-app').fadeIn();
        url = $("#frmAlt").attr("action");
        data = $("#frmAlt").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('#alt_' + $('#hd_image').val()).attr('img-alt', $('#tx_alt').val());
                $('#hd_image').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            var response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });

});