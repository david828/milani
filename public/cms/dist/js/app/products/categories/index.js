$(document).ready(function () {

    $('.trash-data').on('click', function () {
        $('#hd_trash').val($(this).attr('trash'));
        $('.trash-modal').modal();
    });

    $('#bt_trash').on('click', function () {
        str = $("#frmDes").attr("action");
        url = str.replace('IDX', $('#hd_trash').val());
        data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            $('.loading-app').fadeIn();
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('.target-' + $('#hd_trash').val()).remove();
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });

    $('.order-data').on('click', function () {
        $('.loading-app').fadeIn();
        cat = $(this).attr('data-cat');
        order = $('#tx_order' + cat).val();
        url = $(this).attr('data-target');
        token = $("input[name*='_token']").val();
        data = {hn_cat: cat, hn_order: order, _token: token};
        $.post(url, data, function (result) {
            if (result.message) {
                location.reload();
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });

    });
});