$(document).ready(function () {

    $('.select2').select2();

    function formatState (state) {
        if (!state.id) {
            return state.text;
        }
        var baseUrl = "/images/colors";
        var $state = $(
            '<span><img src="' + baseUrl + '/' + state.element.getAttribute('image') + '" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
    };

    $("#sl_color").select2({
        templateResult: formatState
    });

    $('#sl_category').on('change', function () {
        $("#sl_subcategory").html('<option value="" selected>Seleccione una opción</option>');
        $("#sl_subcategory").select2();
        var cat = $(this).val();
        if(cat !== '')
        {
            $('.charger').show();
            var csrf = $('input[name=_token]').val();
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrf,
                },
                url:'/adm/getsubcategories',
                data: {cat: cat},
                type: 'POST',
                success: function(result) {
                    if(!$.isEmptyObject(result)) {
                        $.each(result, function(key, val){
                            $('#sl_subcategory').append('<option value="'+result[key].id+'">'+result[key].name+'</option>');
                        });
                        $('#sl_subcategory').attr('disabled', false);
                    }
                    $('.charger').hide();
                },
                error: function (result) {
                    alert('Ha ocurrido un error. Por favor intente de nuevo');
                    $('.charger').hide();
                }
            });
        }
    });

    tinymce.init({ selector:'#ta_review',
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        language_url: '/cms/plugins/tinyMCE/langs/es_MX.js',
        height: 200,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });

    $('#sl_color').on('change', function () {
       $('#divNext').show();
       $('#divStock').hide();
       $('#divApply').hide();
    });

    $('#bt_next').on('click', function () {
        var aux = '';
        if(jQuery('#sl_color').val() === null) {
            aux = '<div class="row">\n' +
                    '<div class="col-md-6 col-lg-6">\n' +
                        '<div class="form-group">\n' +
                        '<label for="tx_stock">Stock</label>*\n' +
                        '<input class="form-control" id="tx_stock" required name="tx_stock" type="number" value="" > \n' +
                        '</div>\n' +
                    '</div>\n' +
                    '<div class="col-md-6 col-lg-6">\n' +
                        '<div class="form-group">\n' +
                        '<label for="tx_minstock">Stock mínimo</label>*\n' +
                        '<input class="form-control" id="tx_minstock" required name="tx_minstock" type="number" value="" >\n' +
                        '</div>\n' +
                    '</div>\n' +
                '</div>\n'+
                '<div class="row">\n' +
                    '<div class="col-md-6 col-lg-6">\n' +
                    '    <div class="form-group">\n' +
                        '<label for="tx_reference">Referencia</label>*\n' +
                        '<input class="form-control" id="tx_reference" required name="tx_reference" type="text" value="" >\n' +
                    '    </div>\n' +
                    '</div>\n' +
                    '<div class="col-md-6 col-lg-6">\n' +
                    '    <div class="form-group">\n' +
                        '<label for="tx_SKU">SKU</label>*\n' +
                        '<input class="form-control" id="tx_SKU" required name="tx_SKU" type="text" value="" >\n' +
                    '    </div>\n' +
                    '</div>\n' +
                '</div>';
        } else {
            $.each(jQuery('#sl_color').val(), function(index, value){
                var text = $('#sl_color [value='+value+']').text();
                aux+= '<div class="row">\n' +
                    '<h3 style="padding: 10px;">'+text+':</h3>\n'+
                    '<div class="col-md-6 col-lg-6">\n' +
                        '<div class="form-group">\n' +
                        '<label for="tx_stock['+value+']">Stock</label>*\n' +
                        '<input class="form-control" id="tx_stock['+value+']" required name="tx_stock['+value+']" type="number" value="" > \n' +
                        '</div>\n' +
                    '</div>\n' +
                    '<div class="col-md-6 col-lg-6">\n' +
                        '<div class="form-group">\n' +
                        '<label for="tx_minstock">Stock mínimo</label>*\n' +
                        '<input class="form-control" id="tx_minstock['+value+']" required name="tx_minstock['+value+']" type="number" value="" >\n' +
                        '</div>\n' +
                    '</div>\n' +
                '</div>\n'+
                '<div class="row">\n' +
                '<div class="col-md-6 col-lg-6">\n' +
                '    <div class="form-group">\n' +
                '<label for="tx_reference">Referencia</label>*\n' +
                '<input class="form-control" id="tx_reference['+value+']" required name="tx_reference['+value+']" type="text" value="" >\n' +
                '    </div>\n' +
                '</div>\n' +
                '<div class="col-md-6 col-lg-6">\n' +
                '    <div class="form-group">\n' +
                '<label for="tx_SKU">SKU</label>*\n' +
                '<input class="form-control" id="tx_SKU['+value+']" required name="tx_SKU['+value+']" type="text" value="" >\n' +
                '    </div>\n' +
                '</div>\n' +
                '</div>';
            });
        }
        $('#divStock').html(aux).show();
        $('#divNext').hide();
        $('#divApply').show();
    });
});