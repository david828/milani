$(document).ready(function () {


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {

            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action"); //Fijémonos que el formulario se llame igual
            var formData = new FormData($("#frmData")[0]);
            var csrf = $('input[name=_token]').val();

            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    msg = 'Ha ocurrido un error. </br>';
                    if(result.responseJSON['tx_name']){
                        msg = result.responseJSON['tx_name'][0];
                    } else {
                        msg = $.fn.displayError(result);
                    }
                    $.fn.getModal({
                        message: msg,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });
});