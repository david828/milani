$(document).ready(function () {

    $("#sl_type").select2();
    $("#sl_type").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $("#sl_type").on('change', function () {
        //console.log($(this).val());
        var aux = '<div class="form-group">\n<label for="tx_expiration"><i class="fa fa-arrow-right"></i> </label>';
        switch ($(this).val()) {
            case '1':
                aux += '<input class="form-control" id="tx_expiration" required name="tx_expiration" type="date" ></div>';
                break;
            case '2':
                aux += '<input class="form-control" id="tx_expiration" required name="tx_expiration" type="number" ></div>';
                break;
            case '3':
                aux += '<input class="form-control" style="display:none;" id="tx_expiration" required name="tx_expiration" type="text" value="Sin caducidad" ></div>';
                break;
            default:
                aux='';
        }
        $('#typeCoupon').html(aux);
    });

    $("#sl_products").select2({
        placeholder: 'Todos'
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $('#tx_password, #tx_confirmpassword').val('');
                    $("#frmData")[0].reset();
                    $(".select2").select2();
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });
});
