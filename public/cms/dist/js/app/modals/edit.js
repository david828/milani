$(document).ready(function () {

    if($('#sl_typelink').val() == '0') {
        $('#intMsg').show();
    } else if($('#sl_typelink').val() == '1') {
        $('#extMsg').show();
    }

    if ($('#sl_view').val() == 'products/ver/') {
        $('#sl_products').show();
    } else {
        $('#sl_products').hide();
    }

    tinymce.init({ selector:'#ta_text',
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        language_url: '/cms/plugins/tinyMCE/langs/es_MX.js',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });


    $('#sl_typelink').on('change', function(){
        if($(this).val() === '0') {
            $('#intMsg').show();
            $('#extMsg').hide();
        } else if($(this).val() === '1') {
            $('#intMsg').hide();
            $('#extMsg').show();
        } else {
            $('#intMsg').hide();
            $('#extMsg').hide();
        }
        //console.log($(this).val());
    });

    $('#sl_view').on('change', function(){
        var url = $(this).val();
        if (url === 'products/ver/') {
            url = url + $('#sl_products').val();
            $('#sl_products').show();
        } else {
            $('#sl_products').hide();
        }
        $('#hd_url').val(url);
    });

    $('#sl_products').on('change', function(){
        $('#hd_url').val($('#sl_view').val() + $(this).val());
    });


    /**************Upload Image Article**************/
    var ratio = 16 / 9;
    $imagewidth = $('.width-calc').width();
    if ($imagewidth > 500) {
        $imagewidth = 500;
    }
    $imageheight = Math.round($imagewidth / ratio);

    $image = $('.p-image');
    $image.cropper({
        aspectRatio: 16 / 9,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.image-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#imageCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas', {
            width: 1280,
            height: 720
        });
        var dataURL = canvas.toDataURL();
        $('.modal-images').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-images').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imagen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {

            $('.loading-app').fadeIn();
            var url = $("#frmData").attr("action"); //Fijémonos que el formulario se llame igual
            var fnfile = $('#hn_file').val();
            var formData = new FormData($("#frmData")[0]);
            var image = fnfile;
            var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
            var blob = base64ToBlob(base64ImageContent, 'image/png');
            if(fnfile) {
                //console.log(fnfile);
                formData.append('fl_image', blob);
            }
            var csrf = $('input[name=_token]').val();

            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    var response = $.fn.displayError(result);
                    $.fn.getModal({
                        message: response,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });
});