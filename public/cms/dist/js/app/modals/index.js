$(document).ready(function () {

    $('.trash-data').on('click', function () {
        $('#hd_trash').val($(this).attr('trash'));
        $('.trash-modal').modal();
    });

    $('.status-opt').on('click', function () {
        $('.loading-app').fadeIn();
        $('#hn_status').val($(this).attr('go-to'));
        targ = this;
        url = $('#frmEst').attr('action');
        data = $('#frmEst').serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $(targ).removeClass('label-success');
                $(targ).removeClass('label-warning');
                $(targ).addClass(result.message);
                $('.loading-app').fadeOut();
            }
        });
    });

    $('#bt_trash').on('click', function () {
        $('.loading-app').fadeIn();
        str = $("#frmDes").attr("action");
        url = str.replace('IDX', $('#hd_trash').val());
        data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('.target-' + $('#hd_trash').val()).remove();
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });
});