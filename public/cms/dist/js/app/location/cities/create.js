$(document).ready(function () {

    $(".select2").select2();
    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $('#sl_country').on('change', function () {
        url = $('#hn_state').val().replace('IDX', this.value);
        $.get(url, function (result) {
            result = $.parseJSON(result);
            $('#sl_state').empty();
            $('#sl_state').append($('<option>').text("Seleccione una opción").attr('value', ''));
            $.each(result, function (i, obj) {
                $('#sl_state').append($('<option>').text(obj.name).attr('value', obj.num));
            });
            $('#sl_state').select2();
        });
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $('#tx_password, #tx_confirmpassword').val('');
                    $("#frmData")[0].reset();
                    $(".select2").select2();
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });
});
