$(document).ready(function () {

    $(".select2").select2();
    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $('#tx_password, #tx_confirmpassword').val('');
                    $("#frmData")[0].reset();
                    $(".select2").select2();
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });
});
