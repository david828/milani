$(document).ready(function () {

    $(".select2").select2();

    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    });

    $(":file").filestyle({buttonName: "btn-primary"});

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        rules: {
            fl_resource: {
                required: true,
                extension: "doc|docx|pdf|ppt|pptx|xlsx|xls|txt|csv|ods|odt|zip|rar",
                filesize: 41943040
            },
        },
        messages: {
            fl_resource:{
                filesize: "No puede superar el límite de 10MB"
            }
        },
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            $.ajax({
                url: url,
                type: 'POST',
                data: new FormData($("#frmData")[0]),
                contentType: false,
                processData: false,
                cache: false,
                success: function (result, status) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                        $("#frmData")[0].reset();
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result, status, e) {
                    response = $.fn.displayError(result);
                    $.fn.getModal({
                        message: response,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });
});