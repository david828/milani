$(document).ready(function () {

    $('#tx_tags').tagsinput();
    if ($('#sl_maptype').val() == 'F') {
        $('.framemap').fadeIn();
        $('.coordinates').hide();
    } else {
        $('.framemap').hide();
        $('.coordinates').fadeIn();
    }

    $('#sl_maptype').on('change', function () {
        frm = this.value;
        if (frm == 'F') {
            $('.framemap').fadeIn();
            $('.coordinates').hide();
        } else {
            $('.framemap').hide();
            $('.coordinates').fadeIn();
        }
    });

    $('.add-mail').on('click', function () {
        idx = parseInt($('#hn_emails').val());
        $('.add-mails').before('<div class="closer-mail-' + idx + ' closer-data-fields">' +
            '<div class="form-group">' +
            '<label for="tx_email' + idx + '">' + $('.label-mail').attr('title') + ' *</label>' +
            '<div class="input-group">' +
            '<input class="form-control" id="tx_email' + idx + '" required name="tx_email[' + idx + ']" type="email">' +
            '<span class="input-group-addon close-mail" go-to="' + idx + '"><i class="fa fa-times"></i></span>' +
            '</div>' +
            '</div>' +
            '</div>');
        $('.close-mail').on('click', function () {
            $('.closer-mail-' + $(this).attr('go-to')).remove();
        });
        idx = idx + 1;
        $('#hn_emails').val(idx);
    });

    $('.close-rmail').on('click', function () {
        idx = $(this).attr('go-to');
        $('.closer-rmail-' + idx).remove();
        $('#frmData').append('<input type="hidden" value="' + idx + '" id="hn_mail[' + idx + ']" name="hn_mail[' + idx + ']">');
    });


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        rules: {
            ta_frame: {
                required: function (element) {
                    return $("#sl_maptype").val() == "F";
                }
            },
            tx_api_google: {
                required: function (element) {
                    return $("#sl_maptype").val() == "C";
                }
            },
            tx_latitude: {
                required: function (element) {
                    return $("#sl_maptype").val() == "C";
                }
            },
            tx_longitude: {
                required: function (element) {
                    return $("#sl_maptype").val() == "C";
                }
            }
        },
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            var formData = new FormData($("#frmData")[0]);
            var csrf = $('input[name=_token]').val();

            $.ajax(url, {
                method: "POST",
                headers: {
                    'X-CSRF-Token': csrf,
                },
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.message) {
                        $.fn.getModal({message: result.message, type: "success"});
                    } else {
                        $.fn.getModal({message: result.error, type: "error"});
                    }
                    $('.loading-app').fadeOut();
                },
                error: function (result) {
                    response = $.fn.displayError(result);
                    $.fn.getModal({
                        message: response,
                        type: "error"
                    });
                    $('.loading-app').fadeOut();
                }
            });
        }
    });

});