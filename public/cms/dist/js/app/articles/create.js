$(document).ready(function () {
    tinymce.init({ selector:'#ta_description',
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        language_url: '/cms/plugins/tinyMCE/langs/es_MX.js',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });

    $('#tx_tags').tagsinput();
    $('#tx_metatags').tagsinput();

    $(".select2").select2();
    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });

    $('#charNum').text(250 + ' Caracteres disponibles');
    $('#ta_resume').keyup(function () {
        var max = 250;
        var len = $(this).val().length;
        if (len >= max) {
            $('#charNum').text('Ha exedido el límite');
        } else {
            var char = max - len;
            $('#charNum').text(char + ' Caracteres disponibles');
        }
    });

    var ratio = 16 / 9;
    $imagewidth = $('.width-calc').width();
    if ($imagewidth > 800) {
        $imagewidth = 800;
    }
    $imageheight = Math.round($imagewidth / ratio);

    /**************Upload Image Article**************/
    $image = $('.p-image');
    $image.cropper({
        aspectRatio: 16 / 9,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#articleCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas');
        var dataURL = canvas.toDataURL();
        $('.profile-avatar').attr('src', dataURL);
        $('.modal-profile').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-profile').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imágen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }
    /**************End Upload Image Article**************/

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $("#frmData")[0].reset();
                    $(".select2").select2();
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

});