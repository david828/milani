$(document).ready(function () {


    var ratio = 16 / 9;
    $imagewidth = $('.width-calc').width();
    if ($imagewidth > 800) {
        $imagewidth = 800;
    }
    $imageheight = Math.round($imagewidth / ratio);

    /**************Upload Image Article**************/
    $image = $('.p-image');
    $image.cropper({
        aspectRatio: 16 / 9,
        modal: false,
        dragMode: 'move',
        autoCropArea: 1,
        zoomOnWheel: false,
        minContainerWidth: $imagewidth,
        minContainerHeight: $imageheight
    });

    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if ($image.data('cropper') && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            }

            if (data.method === 'rotate') {
                $image.cropper('clear');
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            if (data.method === 'rotate') {
                $image.cropper('crop');
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    //console.log(e.message);
                }
            }

        }
    });

    $("#articleCrop").click(function () {
        var canvas = $image.cropper('getCroppedCanvas');
        var dataURL = canvas.toDataURL();
        $('.profile-avatar').attr('src', dataURL);
        $('.modal-profile').modal('hide');
        $('#hn_file').val(dataURL);
    });

    /*Upload image profile for crop*/
    var $inputImage = $('.upload-profile');
    var URL = window.URL || window.webkitURL;
    var blobURL;
    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                    $('.modal-profile').modal();
                } else {
                    window.alert('Por favor selecciona un formato de imágen válido.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }
    /**************End Upload Image Article**************/

    $('.view-image').on('click', function () {
        img = $(this).attr('img-target');
        $('.content-viewer').html('<img src="' + img + '" />');
    });

    $('.delete-image').on('click', function () {
        $('#hd_trash').val($(this).attr('img-target'));
        $('.trash-modal').modal();
    });

    $('#bt_trash').on('click', function () {
        $('.loading-app').fadeIn();
        url = $("#frmDes").attr("action");
        data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('#img' + $('#hd_trash').val()).remove();
                $('.content-viewer').html('');
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });

    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $("#frmData")[0].reset();
                    $('#gallery').html('');
                    img = $.parseJSON(result.images);
                    dir = result.dir;
                    $.each(img, function (i, item) {
                        $('#gallery').append('<div class="col-xs-4 col-md-4" id="img' + item.idx + '">' +
                            '<div class="content-collect mb-20">' +
                            '<img src="' + dir + item.image + '" />' +
                            '<div class="content-collect-options">' +
                            '<i class="fa fa-eye bg-blue view-image" img-target="' + dir + item.image + '"></i>' +
                            '<i class="fa fa-trash-o bg-red delete-image" img-target="' + item.idx + '"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    });

                    $('.view-image').on('click', function () {
                        img = $(this).attr('img-target');
                        $('.content-viewer').html('<img src="' + img + '" />');
                    });
                    $('.delete-image').on('click', function () {
                        $('#hd_trash').val($(this).attr('img-target'));
                        $('.trash-modal').modal();
                    });

                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });

});