$(document).ready(function () {

    $(".select2").select2();
    $(".select2").on('change', function () {
        $('#' + this.id + '-error').remove();
    });


    $('.add-field').on('click', function () {
        idx = parseInt($('#hn_fields').val());
        url = $('#hn_link').val() + '/' + idx;
        $.get(url, function (result) {
            $('.add-fields').before(result);
            idx = idx + 1;
            $('#hn_fields').val(idx);
            $('.close-field').on('click', function () {
                $('.closer-field-' + $(this).attr('go-to')).remove();
            });
        });
    });

    $('.close-field').on('click', function () {
        idx = $(this).attr('go-to');
        $('.closer-field-' + idx).remove();
        $('#frmData').append('<input type="hidden" value="' + idx + '" id="hn_field[' + idx + ']" name="hn_field[' + idx + ']">');
    });


    $('#frmData').validate({
        errorElement: "div",
        errorLabelContainer: $("div.error"),
        ignore: [],
        submitHandler: function () {
            $('.loading-app').fadeIn();
            url = $("#frmData").attr("action");
            data = $("#frmData").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.loading-app').fadeOut();
            }).fail(function (result) {
                response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
                $('.loading-app').fadeOut();
            });
        }
    });
});