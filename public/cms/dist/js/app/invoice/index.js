$(document).ready(function () {

    $('.trash-data').on('click', function () {
        $('#hd_trash').val($(this).attr('trash'));
        $('.trash-modal').modal();
    });

    $('.send-data').on('click', function () {
        $('#hd_send').val($(this).attr('target'));
        $('#tx_track').val($(this).attr('idtrack'));
        $('.send-modal').modal();
    });

    $('#bt_trash').on('click', function () {
        str = $("#frmDes").attr("action");
        url = str.replace('IDX', $('#hd_trash').val());
        data = $("#frmDes").serialize();
        $.post(url, data, function (result) {
            $('.loading-app').fadeIn();
            if (result.message) {
                $.fn.getModal({message: result.message, type: "success"});
                $('.target-' + $('#hd_trash').val()).remove();
                $('.status-' + $('#hd_trash').val()).html(result.status);
                $('#hd_trash').val('');
            } else {
                $.fn.getModal({message: result.error, type: "error"});
            }
            $('.loading-app').fadeOut();
        }).fail(function (result) {
            response = $.fn.displayError(result);
            $.fn.getModal({
                message: response,
                type: "error"
            });
            $('.loading-app').fadeOut();
        });
    });

    $('#bt_send').on('click', function () {
        if($('#tx_track').val() === '') {
            var color = $('#tx_track').css('border-color');
            $('#tx_track').css('border-color','red');
            setTimeout(function () {
                $('#tx_track').css('border-color',color);
            }, 4000);
        } else {
            var btn = $('#bt_send').html();
            $('#bt_send').html('<i class="fa fa-spinner"></i>').attr('disabled','true');
            var str = $("#frmSend").attr("action");
            var url = str.replace('IDC', $('#hd_send').val());
            var data = $("#frmSend").serialize();
            $.post(url, data, function (result) {
                if (result.message) {
                    $.fn.getModal({message: result.message, type: "success"});
                    $('#hd_send').val('');
                } else {
                    $.fn.getModal({message: result.error, type: "error"});
                }
                $('.send-modal').modal('toggle');
                $('#bt_send').html(btn).removeAttr('disabled');
            }).fail(function (result) {
                $('.send-modal').modal('toggle');
                $('#bt_send').html(btn).removeAttr('disabled');
                var response = $.fn.displayError(result);
                $.fn.getModal({
                    message: response,
                    type: "error"
                });
            });
        }
    });
});