<?php


use App\Http\Controllers\Adm\App\AppController;
use App\Http\Controllers\Adm\App\SeoController;
use App\Http\Controllers\Adm\Menu\MenuController;
use App\Http\Controllers\Adm\PayU\PayuController;
use App\Http\Controllers\Site\Social\SocialController;
use App\Model\Articles\Article;
use App\Model\Conditions\Conditions;
use App\Model\Footer\Footer;
use App\Model\Galleries\Galleries;
use App\Model\Header\Header;
use App\Model\MilaniCart\MilaniCart;
use App\Model\Modals\Modals;
use App\Model\Politics\Politics;
use App\Model\Products\Categories;
use App\Model\Products\Products;
use App\Model\User\Person;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

function appData()
{
    return AppController::appData();
}

function userData()
{
    $u = Auth::User();
    if(!is_null($u->image)) {
        //dd('$u->image');
        $u->imageU = '/images/users/'.$u->image;
    } else {
        $u->imageU = '/assets/images/user2.png';
    }
    return $u;
}

function getAppSeo()
{
    return SeoController::getAppSeo();
}

function social()
{
    return SocialController::getSocial();
}

function menu()
{
    return MenuController::getMenu();
}

function footer()
{
    return Footer::find(1);
}

function countGalleries() {
    return Galleries::count();
}
function getArticles($num)
{
    Carbon::setLocale('es');
    $art = Article::where('status', 1)->with('comments')->limit($num)->orderBy('created_at', 'desc')->get();
    foreach ($art as $item) {
        $item->date = $item->created_at->diffForHumans();
        $item->day = substr($item->created_at,8,2);
        $item->month = substr($item->created_at,5,2);
        $item->month = getMonth($item->month);
        if(is_null($item->image)) {
            $item->image = 'article.png';
        }
    }
    return $art;
}

function getMonth($num)
{
    $month = '';
    switch ($num){
        case '01':
            $month = 'Ene';
            break;
        case '02':
            $month = 'Feb';
            break;
        case '03':
            $month = 'Mar';
            break;
        case '04':
            $month = 'Abr';
            break;
        case '05':
            $month = 'May';
            break;
        case '06':
            $month = 'Jun';
            break;
        case '07':
            $month = 'Jul';
            break;
        case '08':
            $month = 'Ago';
            break;
        case '09':
            $month = 'Sep';
            break;
        case '10':
            $month = 'Oct';
            break;
        case '11':
            $month = 'Nov';
            break;
        case '12':
            $month = 'Dic';
            break;
    }
    return $month;
}

function imageProduct($data, $id_color = '')
{
    $image = null;
    $alt = null;
    $array = [];
    //dd($data);
    foreach ($data->productcolor as $pc)
    {
        if($id_color === '') {
            if(count($pc->images) > 0) {
                $image = '/images/products/'.$pc->images[0]->image;
                $alt = $pc->images[0]->image;
                $array = [$image,$alt];
                return $array;
            }
        } else {
            if($id_color == $pc->id_color) {
                if (count($pc->images) > 0) {
                    $image = '/images/products/'.$pc->images[0]->image;
                    $alt = $pc->images[0]->image;
                    $array = [$image,$alt];
                    return $array;
                }
            }
        }
    }
    if(is_null($image)) {
        if(count($data->images) > 0) {
            $image = '/images/products/'.$data->images[0]->image;
            $alt = $data->images[0]->alt;
            $array = [$image,$alt];
            return $array;
        }
    }
    if(is_null($image)) {
        $image = '/assets/images/product.png';
    }
    $array = [$image,$alt];
    return $array;
}

function stockProduct($data)
{
    $st = 0;
    //dd($data);
    if(count($data->productcolor) > 0) {
        foreach ($data->productcolor as $pc) {
            $st += $pc->stock;
        }
    } else {
        $st = $data->stock;
    }
    return $st;
}

function getInstagram()
{
    return Header::home();
}

function menuP()
{
    return Categories::selectionIndex();
}

function get_cart()
{
    $ck = Cookie::get('milaniCart');
    $ar = null;
    if(!is_null($ck)) {
        $data = MilaniCart::find($ck);
        if(!is_null($data)) {
            $ar = json_decode($data->cart);
        }
    }
    $pd = '';
    $tP = 0;
    $vT = 0;
    if (!is_Array($ar)) {
        $ar = (array) $ar;
    }
    if (!is_null($ar)) {
        foreach ($ar as $item)
        {
            $col = '';
            if(!is_null($item->nameColor)) {
                $col = ' - '.$item->nameColor;
            }
            $pd.='<li>
                <a href="/products/ver/'.$item->slug.'" class="photo"><img src="'.$item->image.'" class="cart-thumb" alt="product" /></a>
                <h6><a href="/products/ver/'.$item->slug.'">'.$item->name.$col.' </a></h6>
                <p>'.$item->cant.'x - <span class="price">'.$item->priceV.'</span></p>
            </li>';
            $tP+=1;
            $vT+=$item->total;
        }
        $num = '<span class="badge" id="numCart08">'.$tP.'</span>';
    } else {
        $num = '<span class="badge" id="numCart08" style="display: none;">'.count($ar).'</span>';
    }
    $vTV = '$'.number_format($vT,0,',','.');
    $aux ='<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-shopping-bag"></i>'.$num.'</a>
            <ul class="dropdown-menu cart-list" id="listCart08">'.
                $pd.
                '<li class="total">
                    <span class="pull-right"><strong>'.trans('site.total').': </strong><span id="totCart08">'.$vTV.'</span></span>
                    <a href="/cart" class="btn btn-default btn-cart">'.trans('site.cart').'</a>';
    if(appData()->min_value != 0) {
        $aux.='<p id="freeship" class="cart-text">'. trans('app.free_shipping',['value'=>number_format(appData()->min_value, 0, ',', '.')]).trans('app.apply_').' <a href="/conditions" target="_blank">'. trans('app.condi_').'</a> </p>';
    }
    $aux.='</li>
            </ul>';
    return $aux;
}

function available()
{
    return ['' => trans('app.select'), '1' => trans('app.yes'), '0' => trans('app.not')];
}

function politics()
{
    return Politics::first();
}

function conditions()
{
    return Conditions::first();
}

function randPass($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function payuData()
{
    return PayuController::dataPayu();
}

function getPerson()
{
    return Person::selection();
}

function getDocType()
{
    return \App\Model\User\DocumentType::selection();
}

function countSale()
{
    return Products::where('sale', '=','1')
        ->where('sale_price','>',0)
        ->count();
}

function getModals()
{
    $url = Request::path();
    $modals = Modals::where('url', $url)->where('status', '1')->get();
    $html = '';
    foreach ($modals as $md) {
        $content = '';
        $btn = '';
        $text= '';
        $img = '';
        $simg = '';
        $type = '';

        if(!is_null($md->link) and $md->typelink == '1'){
            $type = 'target="_blank"';
            $link = $md->link;
        } else {
            $link = '/'. $md->link;

        }

        /*** Boton***/
        if(!is_null($md->button) and !is_null($md->link)){
            $btn= '<div style="text-align: center;"><a href="'.$link.'" '.$type.' class="btn btn-default dart-btn-sm border_2px rd-stroke-btn">'.$md->button.'</a></div>';
        }

        /***Texto***/
        if(!is_null($md->text)) {
            $text = '<div>'.$md->text.'</div>';
        }

        /*** Imagen ***/
        if(!is_null($md->image)) {
            if(!is_null($md->link)) {
                $img = '<a href="'.$link.'" '.$type.'><img src="/images/modals/'.$md->image.'" class="img-responsive imageModal" alt="Modal Image"></a>';
            } else {
                $img = '<img src="/images/modals/'.$md->image.'" class="img-responsive imageModal" alt="Modal Image">';
            }
        }

        /*** Armar el modal ***/
        if ($text != '' and $img != '') {
            $content = '<div class="row">
                            <div class="col-md-6 col-xs-12">'.$img.'</div>
                            <div class="col-md-6 col-xs-12 modal_column_2">
                                '.$text.$btn.'
                            </div>
                        </div>';
        } else if($text != '' and $img == '') {
            $content = '<div class="row">
                            <div class="col-md-12 col-xs-12 modal_column_2">
                                '.$text.$btn.'                            
                            </div>
                        </div>';
        } else if($text == '' and $img != '') {
            $simg = 'soloImage';
            $content = '<div class="imageModal">'.$img.'</div>';
        }


        if($content != '') {
            $modal = '<div id="' . $md->slug . '" class="modal fade moodleModalS" role="dialog">
                    <div class="modal-dialog moodleModal '.$simg.'">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-body">
                        <button type="button" class="close btnMoodleModal" data-dismiss="modal">&times;</button>
                        ' . $content . '
                      </div>
                    </div>
                  </div>
                </div>';
            $html .= $modal;
        }
    }
    return $html;

    $imagensola = '<div id="oe" class="modal fade moodleModalS" role="dialog">
                    <div class="modal-dialog moodleModal">
                        <!-- Modal content-->
                        <div class="modal-content" style="">
                        <div class="modal-body"  style="">
                        <button type="button" class="close btnMoodleModal" data-dismiss="modal">&times;</button>
                        <div class="imageModal">
                                <img src="http://milani.local/images/products/a7b393e085d6798190805c491dcfcd26.png" class="img-responsive" alt="Product">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>';

    $texto-$textobotton = '<div id="oe" class="modal fade moodleModalS" role="dialog">
                    <div class="modal-dialog moodleModal">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-body">
                        <button type="button" class="close btnMoodleModal" data-dismiss="modal">&times;</button>
                        <div class="row">
                          
                            <div class="col-md-12 col-xs-12 modal_column_2">
                                <div><p>Lorem ipsum dolor sit amet, </p></div>
                                                            <div style="text-align: center;"><a href="/cart" class="btn btn-default btn-cart">Carrito</a></div>
</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>';

    $imagentexto = '<div id="oe" class="modal fade moodleModalS" role="dialog">
                    <div class="modal-dialog moodleModal">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-body">
                        <button type="button" class="close btnMoodleModal" data-dismiss="modal">&times;</button>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <img src="http://milani.local/images/products/a7b393e085d6798190805c491dcfcd26.png" class="img-responsive" alt="Product">
                            </div>
                            <div class="col-md-6 col-xs-12 modal_column_2">
                                <div><p>Lorem ipsum dolor sit amet, </p></div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>';

    $imagentextoboton= '<div id="oe" class="modal fade moodleModalS" role="dialog">
                    <div class="modal-dialog moodleModal">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-body">
                        <button type="button" class="close btnMoodleModal" data-dismiss="modal">&times;</button>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <img src="http://milani.local/images/products/a7b393e085d6798190805c491dcfcd26.png" class="img-responsive" alt="Product">
                            </div>
                            <div class="col-md-6 col-xs-12 modal_column_2">
                                <div><p>Lorem ipsum dolor sit amet, </p></div>
                                <div style="text-align: center;"><a href="/cart" class="btn btn-default btn-cart">Carrito</a></div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>';
}


