<?php

namespace App\Http\Controllers\Adm\Politics;

use App\Http\Controllers\Controller;
use App\Model\Politics\Politics;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PoliticsController extends Controller
{
    public function index()
    {
        $data = Politics::first();
        if(!$data){
            $data = (object)[];
            $data->text = '';
        }
        return view('adm.politics.edit', compact('data'));
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = Politics::first();
            if(!$data){
                $data = new Politics();
            }
            $data->text = $request->get('ta_text');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
