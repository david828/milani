<?php

namespace App\Http\Controllers\Adm\Distribution;


use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Distribution\DestroyDistributionRequest;
use App\Http\Requests\Adm\Distribution\StoreDistributionRequest;
use App\Http\Requests\Adm\Distribution\UpdateDistributionRequest;
use App\Model\Distribution\Distribution;
use App\Model\Location\Cities;
use App\Model\Location\Countries;
use App\Model\Location\States;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DistributionController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/distribution/';
        $this->url = appData()->urldata . '/images/distribution/';
    }

    public function index(Request $request)
    {
        $list = Distribution::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.distribution.index', compact('list'));
    }

    public function create()
    {
        $countryList = Countries::selection();
        return view('adm.distribution.create', compact('countryList'));
    }

    public function store(StoreDistributionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Distribution();
            $data->name = $request->get('tx_name');
            $data->cellphone = $request->get('tx_cellphone');
            $data->address = $request->get('tx_address');
            $data->phone = $request->get('tx_phone');
            $data->email = $request->get('tx_email');
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 200, 'height' => 200, 'greyscale' => false])->save($dirImg);
                if (File::exists($this->dir . $data->logo)) {
                    File::delete($this->dir . $data->logo);
                }
                $data->logo = $image;
            }
            if($request->get('sl_country')) {
                $data->id_country = $request->get('sl_country');
            } else {
                $data->id_country = null;
            }
            if($request->get('sl_state')) {
                $data->id_state = $request->get('sl_state');
            } else {
                $data->id_state = null;
            }
            if($request->get('sl_city')) {
                $data->id_city = $request->get('sl_city');
            } else {
                $data->id_city = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Distribution::findOrFail($id);
        $data->countryList = Countries::selection();
        if (is_null($data->id_country)) {
            $data->stateList = [];
        } else {
            $data->stateList = States::selectionByCountry($data->id_country);
        }
        if (is_null($data->id_state)) {
            $data->cityList = [];
        } else {
            $data->cityList = Cities::selectionByState($data->id_state);
        }
        return view('adm.distribution.edit', compact('data'));
    }

    public function update($id, UpdateDistributionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Distribution::findOrFail($id);
            $data->name = $request->get('tx_name');
            $data->cellphone = $request->get('tx_cellphone');
            $data->address = $request->get('tx_address');
            $data->phone = $request->get('tx_phone');
            $data->email = $request->get('tx_email');
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 200, 'height' => 200, 'greyscale' => false])->save($dirImg);
                if (File::exists($this->dir . $data->logo)) {
                    File::delete($this->dir . $data->logo);
                }
                $data->logo = $image;
            }
            if($request->get('sl_country')) {
                $data->id_country = $request->get('sl_country');
            } else {
                $data->id_country = null;
            }
            if($request->get('sl_state')) {
                $data->id_state = $request->get('sl_state');
            } else {
                $data->id_state = null;
            }
            if($request->get('sl_city')) {
                $data->id_city = $request->get('sl_city');
            } else {
                $data->id_city = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyDistributionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Distribution::findOrFail($id);
            if (File::exists($this->dir . $data->logo)) {
                File::delete($this->dir . $data->logo);
            }
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}