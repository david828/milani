<?php

namespace App\Http\Controllers\Adm\Modals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Modals\DestroyModalsRequest;
use App\Http\Requests\Adm\Modals\StatusModalsRequest;
use App\Http\Requests\Adm\Modals\StoreModalsRequest;
use App\Http\Requests\Adm\Modals\UpdateModalsRequest;
use App\Model\Modals\Modals;
use App\Model\Products\Products;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;


class ModalsController extends Controller
{
    public function index(Request $request)
    {
        $list = Modals::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.modals.index', compact('list'));
    }

    public function create()
    {
        $view = [
            '/'=>'Inicio',
            'aboutus'=>'Nosotros',
            'products'=>'Productos',
            'products/ver/'=>'Producto',
            'galleries'=>'Galería',
            'distribution'=>'Puntos de venta',
            'articles'=>'Artículos',
            'contact'=>'Contáctanos',
            'cart'=>'Carrito',
            'checkout'=>'Checkout',
        ];

        $products = Products::selectionSlug();
        return view('adm.modals.create', compact('view', 'products'));
    }

    public function store(StoreModalsRequest $request)
    {
        DB::beginTransaction();
        try {
            $modal = new Modals();
            $dir = appData()->dirdata . '/images/modals/';
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $dir . $image;
                Image::make($imgProduct, ['width' => 1280, 'height' => 720, 'greyscale' => false])->save($dirImg);
                $modal->image = $image;
            }

            $modal->name = $request->get('tx_name');
            $modal->slug = Str::slug($request->get('tx_name'));

            if ($request->get('ta_text')) {
                $modal->text = $request->get('ta_text');
            }

            if ($request->get('tx_button')) {
                $modal->button = $request->get('tx_button');
            }
            if($request->get('tx_link')) {
                $modal->link = $request->get('tx_link');
                $modal->typelink = $request->get('sl_typelink');
            } else {
                $modal->link = null;
                $modal->typelink = null;
            }
            if ($request->get('tx_url')) {
                $modal->url = $request->get('tx_url');
            }
            $modal->status = 1;
            $modal->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Modals::findOrFail($id);
        $data->view = [
            '/'=>'Inicio',
            'aboutus'=>'Nosotros',
            'products'=>'Productos',
            'products/ver/'=>'Producto',
            'galleries'=>'Galería',
            'distribution'=>'Puntos de venta',
            'articles'=>'Artículos',
            'contact'=>'Contáctanos',
            'cart'=>'Carrito',
            'checkout'=>'Checkout',
        ];

        $data->products = Products::selectionSlug();
        $data->parts = explode('/', $data->url);
        if(count($data->parts) > 2) {
            $pd = 's';
            $data->part1 = $data->parts[0].'/'.$data->parts[1].'/';
            $data->part2 = $data->parts[2];
        } else {
            $pd = 'n';
            $data->part1 = $data->url;
            $data->part2 = null;
        }
        return view('adm.modals.edit', compact('data', 'pd'));
    }

    public function update($id, UpdateModalsRequest $request)
    {
        DB::beginTransaction();
        try {
            $modal = Modals::findOrFail($id);
            $dir = appData()->dirdata . '/images/modals/';
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $dir . $image;
                Image::make($imgProduct, ['width' => 1280, 'height' => 720, 'greyscale' => false])->save($dirImg);
                if (File::exists($dir . $modal->image)) {
                    File::delete($dir . $modal->image);
                }
                $modal->image = $image;
            }

            $modal->name = $request->get('tx_name');
            $modal->slug = Str::slug($request->get('tx_name'));

            if ($request->get('ta_text')) {
                $modal->text = $request->get('ta_text');
            } else {
                $modal->text = null;
            }
            if ($request->get('tx_button')) {
                $modal->button = $request->get('tx_button');
            } else {
                $modal->button = null;
            }
            if($request->get('tx_link')) {
                $modal->link = $request->get('tx_link');
                $modal->typelink = $request->get('sl_typelink');
            } else {
                $modal->link = null;
                $modal->typelink = null;
            }
            if ($request->get('tx_url')) {
                $modal->url = $request->get('tx_url');
            }
            $modal->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                //dd($e->getMessage());
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function status(StatusModalsRequest $request)
    {
        $data = Modals::findOrFail($request->get('hn_status'));
        $status = 'label-success';
        if ($data->status == 1) {
            $data->status = 0;
            $status = 'label-warning';
        } else {
            $data->status = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $status
            ]);
        }
    }

    public function destroy($id, DestroyModalsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Modals::findOrFail($id);
            $dir = appData()->dirdata . '/images/modals/';
            $beforefile = $dir . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
