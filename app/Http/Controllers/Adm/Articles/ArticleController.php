<?php

namespace App\Http\Controllers\Adm\Articles;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Articles\DestroyArticleCommentsRequest;
use App\Http\Requests\Adm\Articles\DestroyArticleImageRequest;
use App\Http\Requests\Adm\Articles\DestroyArticleRequest;
use App\Http\Requests\Adm\Articles\StatusArticleRequest;
use App\Http\Requests\Adm\Articles\StoreArticleRequest;
use App\Http\Requests\Adm\Articles\UpdateArticleRequest;
use App\Http\Requests\Adm\Articles\UploadArticleRequest;
use App\Model\Articles\Article;
use App\Model\Articles\ArticleComments;
use App\Model\Articles\ArticleImages;
use App\Model\Categories\Categories;
use Carbon\Carbon;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;


class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $list = Article::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.articles.index', compact('list'));
    }

    public function create()
    {
        $data = (object)[];
        $data->categories = Categories::selection();
        return view('adm.articles.create', compact('data'));
    }

    public function show($id)
    {
        Carbon::setLocale('es');
        Article::findOrFail($id);
        $data = Article::showData($id);
        $data->date = $data->created_at->diffForHumans();
        $dir = appData()->dirdata . '/images/articles/';
        if (!is_null($data->image)) {
            $beforefile = $dir . $data->image;
            if (File::exists($beforefile)) {
                $data->image = appData()->urldata . '/images/articles/' . $data->image;
            } else {
                $data->image = null;
            }
        }
        return view('adm.articles.show', compact('data'));
    }

    public function store(StoreArticleRequest $request)
    {
        DB::beginTransaction();
        try {

            $article = new Article();
            $dir = appData()->dirdata . '/images/articles/';
            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                $dirImgSm = $dir . 'sm_' . $image;
                $dirImgXs = $dir . 'xs_' . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 1200, 'height' => 675, 'greyscale' => false])->save($dirImg);
                Image::make($imgArticle, ['width' => 768, 'height' => 432, 'greyscale' => false])->save($dirImgSm);
                Image::make($imgArticle, ['width' => 368, 'height' => 207, 'greyscale' => false])->save($dirImgXs);
                $article->image = $image;
            }

            $article->id_category = $request->get('sl_categories');
            $article->id_user = Auth::id();
            $article->name = $request->get('tx_name');
            $article->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_description')) {
                $article->description = $request->get('ta_description');
            }

            if ($request->get('ta_metadesc')) {
                $article->metadescription = $request->get('ta_metadesc');
            }
            if ($request->get('tx_metatags')) {
                $article->metatags = $request->get('tx_metatags');
            }
            if ($request->get('tx_tags')) {
                $article->tags = $request->get('tx_tags');
            }

            $article->status = 1;
            $article->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Article::findOrFail($id);
        $data->categories = Categories::selection();
        return view('adm.articles.edit', compact('data'));
    }

    public function update($id, UpdateArticleRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Article::findOrFail($id);

            $dir = appData()->dirdata . '/images/articles/';
            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                $dirImgSm = $dir . 'sm_' . $image;
                $dirImgXs = $dir . 'xs_' . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 1200, 'height' => 675, 'greyscale' => false])->save($dirImg);
                Image::make($imgArticle, ['width' => 768, 'height' => 432, 'greyscale' => false])->save($dirImgSm);
                Image::make($imgArticle, ['width' => 368, 'height' => 207, 'greyscale' => false])->save($dirImgXs);
                if (!is_null($data->image)) {
                    if (File::exists($dir . $data->image)) {
                        File::delete($dir . $data->image);
                        File::delete($dir . 'sm_' . $data->image);
                        File::delete($dir . 'xs_' . $data->image);
                    }
                }
                $data->image = $image;
            }

            $data->id_category = $request->get('sl_categories');
            $data->id_user = Auth::id();
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_description')) {
                $data->description = $request->get('ta_description');
            }

            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            }
            if ($request->get('tx_tags')) {
                $data->tags = $request->get('tx_tags');
            }

            $data->status = 1;
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function status(StatusArticleRequest $request)
    {
        $data = Article::findOrFail($request->get('hn_status'));
        $status = 'label-success';
        if ($data->status == 1) {
            $data->status = 0;
            $status = 'label-warning';
        } else {
            $data->status = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $status
            ]);
        }
    }

    public function destroy($id, DestroyArticleRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Article::with('images')->findOrFail($id);
            $dir = appData()->dirdata . '/images/articles/';
            $beforefile = $dir . $data->image;
            $smbeforefile = $dir . 'sm_' . $data->image;
            $xsbeforefile = $dir . 'xs_' . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            if (File::exists($smbeforefile)) {
                File::delete($smbeforefile);
            }
            if (File::exists($xsbeforefile)) {
                File::delete($xsbeforefile);
            }

            foreach ($data->images as $img) {
                $beforefile = $dir . $img->image;
                $smbeforefile = $dir . 'sm_' . $img->image;
                $xsbeforefile = $dir . 'xs_' . $img->image;
                if (File::exists($beforefile)) {
                    File::delete($beforefile);
                }
                if (File::exists($smbeforefile)) {
                    File::delete($smbeforefile);
                }
                if (File::exists($xsbeforefile)) {
                    File::delete($xsbeforefile);
                }
            }
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function images($id)
    {
        $data = Article::with('images')->findOrFail($id);
        $data->dir = appData()->urldata . '/images/articles/';
        $dir = appData()->dirdata . '/images/articles/';
        if (File::exists($dir . $data->image)) {
            $data->image = $data->dir . 'xs_' . $data->image;
        } else {
            $data->image = 'cms/dist/img/article.png';
        }
        return view('adm.articles.images', compact('data'));
    }

    public function upload($id, UploadArticleRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Article::findOrFail($id);

            $dir = appData()->dirdata . '/images/articles/';
            $url = '/images/articles/';

            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                $dirImgSm = $dir . 'sm_' . $image;
                $dirImgXs = $dir . 'xs_' . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 1200, 'height' => 675, 'greyscale' => false])->save($dirImg);
                Image::make($imgArticle, ['width' => 768, 'height' => 432, 'greyscale' => false])->save($dirImgSm);
                Image::make($imgArticle, ['width' => 368, 'height' => 207, 'greyscale' => false])->save($dirImgXs);

                $imgFile = new ArticleImages();
                $imgFile->id_article = $data->id;
                $imgFile->image = $image;
                $imgFile->save();
            }

            $list = ArticleImages::getArticleImages($data->id);

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'dir' => $url,
                    'images' => $list->toJson()
                ]);
            }
        } catch
        (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroyImage($id, DestroyArticleImageRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = ArticleImages::where('id_article', $id)->findOrFail($request->get('hd_trash'));

            $dir = appData()->dirdata . '/images/articles/';
            $beforefile = $dir . $data->image;
            $smbeforefile = $dir . 'sm_' . $data->image;
            $xsbeforefile = $dir . 'xs_' . $data->image;

            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            if (File::exists($smbeforefile)) {
                File::delete($smbeforefile);
            }
            if (File::exists($xsbeforefile)) {
                File::delete($xsbeforefile);
            }

            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function comments($id)
    {
        $list = Article::findOrFail($id);
        $list->comments = ArticleComments::where('id_article', $id)->paginate(20);
        return view('adm.articles.comments', compact('list'));
    }

    public function destroyComments($id, DestroyArticleCommentsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = ArticleComments::findOrFail($request->get('hd_trash'));
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
