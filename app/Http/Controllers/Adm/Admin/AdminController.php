<?php

namespace App\Http\Controllers\Adm\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Adm\Admin\DestroyAdminRequest;
use App\Http\Requests\Adm\Admin\StoreAdminRequest;
use App\Http\Requests\Adm\Admin\UpdateAdminRequest;
use App\Model\Articles\Article;
use App\Model\Invoice\Invoice;
use App\Model\Location\Cities;
use App\Model\Location\Countries;
use App\Model\Location\States;
use App\Model\User\DocumentType;
use App\Model\User\Person;
use App\Model\User\UserAddress;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\Cast\Object_;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $list = User::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.admin.index', compact('list'));
    }

    public function create()
    {
        $data = (Object)[];
        $data->doctype = DocumentType::selection();
        $data->person = Person::selection();
        $data->countryList = Countries::selection();
        return view('adm.admin.create', compact('data'));
    }

    public function store(StoreAdminRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new User();
            $data->id_documenttype = $request->get('sl_documenttype');
            $data->id_person = $request->get('sl_person');
            $data->name = $request->get('tx_name');
            $data->document = $request->get('tx_document');
            $data->password = Hash::make($request->get('tx_password'));
            $data->email = $request->get('tx_email');
            $data->phone = $request->get('tx_phone');
            $data->role = $request->get('sl_role');
            if($request->get('tx_address')) {
                $data->address_invoice = $request->get('tx_address');
            }
            if($request->get('sl_city')) {
                $data->id_city = $request->get('sl_city');
            }
            $data->politics = 0;
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            //dd($e->getMessage());
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        $data->doctype = DocumentType::selection();
        $data->person = Person::selection();
        $data->countryList = Countries::selection();
        if(!is_null($data->id_city)) {
            $data->id_state = Cities::find($data->id_city)->id_state;
            $data->id_country = States::find($data->id_state)->id_country;
            $data->stateList = States::selectionByCountry($data->id_country);
            $data->cityList = Cities::selectionByState($data->id_state);
        } else {
            $data->id_state = null;
            $data->id_country = null;
            $data->stateList = [];
            $data->cityList = [];
        }
        return view('adm.admin.edit', compact('data'));
    }

    public function update($id, UpdateAdminRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = User::findOrFail($id);
            $data->id_documenttype = $request->get('sl_documenttype');
            $data->id_person = $request->get('sl_person');
            $data->name = $request->get('tx_name');
            $data->document = $request->get('tx_document');
            if ($request->get('tx_password')) {
                $data->password = Hash::make($request->get('tx_password'));
            }
            $data->email = $request->get('tx_email');
            $data->role = $request->get('sl_role');
            $data->phone = $request->get('tx_phone');
            if($request->get('tx_address')) {
                $data->address_invoice = $request->get('tx_address');
            } else {
                $data->address_invoice = null;
            }
            if($request->get('sl_city')) {
                $data->id_city = $request->get('sl_city');
            } else {
                $data->id_city = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyAdminRequest $request)
    {
        DB::beginTransaction();
        try {
            UserAddress::where('id_user', $id)->delete();
            Article::where('id_user',$id)->update(['id_user'=>null]);
            Invoice::where('id_user',$id)->update(['id_user'=>null]);
            $data = User::findOrFail($id);
            $beforefile = 'images/users/' . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
