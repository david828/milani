<?php

namespace App\Http\Controllers\Adm\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Articles\Article;
use App\Model\Invoice\Invoice;
use App\Model\Products\Categories;
use App\Model\Products\Products;
use App\Model\Products\Subcategories;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        $admin = User::getCountUsers('A') + User::getCountUsers('S');
        $data = (object)[
            'admin' => $admin,
            'users' => User::getCountUsers('U'),
            'products' => Products::getCount(),
            'inactiveproducts' => Products::getCountStatus(0),
            'activeproducts' => Products::getCountStatus(1),
            'categories' => Categories::getCount(),
            'subcategories' => Subcategories::getCount(),
            'activearticles' => Article::getCountStatus(1),
            'inactivearticles' => Article::getCountStatus(0),
            'total' => Invoice::countTotalInvoice(),
            'last' => Invoice::countLastInvoice(),
            'graphic' => Invoice::getPerMonth($months),
            'monthList' => [trans('app.jan'), trans('app.feb'), trans('app.mar'), trans('app.abr'), trans('app.may'), trans('app.jun'), trans('app.jul'), trans('app.aug'), trans('app.sep'), trans('app.oct'), trans('app.nov'), trans('app.dec')]
        ];

        return view('adm.home', compact('data'));
    }
}
