<?php

namespace App\Http\Controllers\Adm\OrderImages;



use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderImagesController extends Controller
{
    public function updateOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            $table = $request->get('table');
            $array = $request->get('id_array');
            $id_array = explode('|',$array);
            $count = 1;
            foreach ($id_array as $id){
                DB::table($table)->where('id', $id)->update(['order' => $count]);
                $count ++;
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}