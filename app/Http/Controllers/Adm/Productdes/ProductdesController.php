<?php

namespace App\Http\Controllers\Adm\Productdes;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Productdes\UpdateProductdesRequest;
use App\Model\Productdes\Productdes;
use App\Model\Products\Products;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProductdesController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/products/';
        $this->url = appData()->urldata . '/images/products/';
    }

    public function form()
    {
        $data = Productdes::find(1);
        if(!$data){
            $data = (object)[];
            $data->id = 1;
            $data->subtitle = '';
            $data->text = '';
            $data->link = '';
            $data->image = '';
            $data->title = '';
            $data->window = '';
        }
        return view('adm.productdes.form', compact('data'));
    }

    public function updateform(UpdateProductdesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Productdes::find(1);
            if(!$data){
                $data = new Productdes();
            }
            $data->id = 1;
            $data->title = $request->get('tx_title');
            $data->subtitle = $request->get('tx_subtitle');
            $data->text = $request->get('ta_text');
            $data->window = $request->get('sl_window');
            if ($request->get('tx_link')) {
                $data->link = $request->get('tx_link');
            } else {
                $data->link = null;
            }
            if ($request->file('fl_image')) {

                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 891, 'height' => 731, 'greyscale' => false])->save($dirImg);

                if (File::exists($this->dir . $data->image)) {
                    File::delete($this->dir . $data->image);
                }
                $data->image = $image;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //exit($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}