<?php

namespace App\Http\Controllers\Adm\Location;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Location\Countries\CreateCountryRequest;
use App\Http\Requests\Adm\Location\Countries\DestroyCountryRequest;
use App\Http\Requests\Adm\Location\Countries\UpdateCountryRequest;
use App\Model\Location\Countries;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        $list = Countries::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.location.countries.index', compact('list'));
    }


    public function create()
    {
        return view('adm.location.countries.create');
    }

    public function store(CreateCountryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Countries();
            $data->name = $request->get('tx_name');
            $data->code = $request->get('tx_code');
            $data->iso_code = $request->get('tx_isocode');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Countries::findOrFail($id);
        return view('adm.location.countries.edit', compact('data'));
    }

    public function update($id, UpdateCountryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Countries::findOrFail($id);

            $data->name = $request->get('tx_name');
            $data->code = $request->get('tx_code');
            $data->iso_code = $request->get('tx_isocode');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCountryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Countries::findOrFail($id);
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
