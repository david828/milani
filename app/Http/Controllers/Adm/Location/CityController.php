<?php

namespace App\Http\Controllers\Adm\Location;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Location\Cities\CreateCityRequest;
use App\Http\Requests\Adm\Location\Cities\DestroyCityRequest;
use App\Http\Requests\Adm\Location\Cities\UpdateCityRequest;
use App\Model\Location\Cities;
use App\Model\Location\Countries;
use App\Model\Location\States;
use App\Model\Zone\Zone;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $list = Cities::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.location.cities.index', compact('list'));
    }


    public function create()
    {
        $data = (object)['countryList' => Countries::selection()];
        $data->zoneList = Zone::selection();
        return view('adm.location.cities.create', compact('data'));
    }

    public function store(CreateCityRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Cities();
            $data->name = $request->get('tx_name');
            $data->id_state = $request->get('sl_state');
            $data->id_zone = $request->get('sl_zone');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Cities::findOrFail($id);
        $data->country = Cities::getCountryData($id);
        $data->countryList = Countries::selection();
        $data->stateList = States::selection();
        $data->zoneList = Zone::selection();
        return view('adm.location.cities.edit', compact('data'));
    }

    public function update($id, UpdateCityRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Cities::findOrFail($id);

            $data->name = $request->get('tx_name');
            $data->id_state = $request->get('sl_state');
            $data->id_zone = $request->get('sl_zone');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCityRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Cities::findOrFail($id);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function getCities($id)
    {
        States::findOrFail($id);
        $data = Cities::getCities($id);
        return $data->toJson();
    }
}
