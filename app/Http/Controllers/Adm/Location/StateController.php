<?php

namespace App\Http\Controllers\Adm\Location;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Location\States\CreateStateRequest;
use App\Http\Requests\Adm\Location\States\DestroyStateRequest;
use App\Http\Requests\Adm\Location\States\UpdateStateRequest;
use App\Model\Location\Countries;
use App\Model\Location\States;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $list = States::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.location.states.index', compact('list'));
    }


    public function create()
    {
        $data = (object)['countryList' => Countries::selection()];
        return view('adm.location.states.create', compact('data'));
    }

    public function store(CreateStateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new States();

            $data->name = $request->get('tx_name');
            $data->code = $request->get('tx_code');
            $data->id_country = $request->get('sl_country');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = States::findOrFail($id);
        $data->countryList = Countries::selection();
        return view('adm.location.states.edit', compact('data'));
    }

    public function update($id, UpdateStateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = States::findOrFail($id);

            $data->name = $request->get('tx_name');
            $data->code = $request->get('tx_code');
            $data->id_country = $request->get('sl_country');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyStateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = States::findOrFail($id);
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function getStates($id)
    {
        Countries::findOrFail($id);
        $data = States::getStates($id);
        return $data->toJson();
    }
}
