<?php

namespace App\Http\Controllers\Adm\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Contact\UpdateContactRequest;
use App\Model\Contact\Contact;
use App\Model\Contact\Receivers;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{

    public function index(Request $request)
    {
        $list = Contact::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.contact.index', compact('list'));
    }

    public function edit($id)
    {
        $data = Contact::findOrFail($id);
        $data->emails = Receivers::showData($id);
        $data->map = ['' => trans('app.select'), 'F' => trans('app.frame_map'), 'C' => trans('app.insert_coordinates')];
        return view('adm.contact.edit', compact('data'));
    }

    public function update($id, UpdateContactRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Contact::findOrFail($id);
            $data->name = $request->get('tx_name');
            $data->maptype = $request->get('sl_maptype');
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            }

            if ($request->get('tx_tags')) {
                $data->metatags = $request->get('tx_tags');
            }

            if ($request->get('sl_maptype') == 'F') {
                $data->framemap = $request->get('ta_frame');
            }

            if ($request->get('sl_maptype') == 'C') {
                $data->api_google = $request->get('tx_api_google');
                $data->latitude = $request->get('tx_latitude');
                $data->longitude = $request->get('tx_longitude');
            }
            $data->save();

            if ($request->get('tx_email')) {
                foreach ($request->get('tx_email') as $key => $val) {
                    $receiver = new Receivers();
                    $receiver->id_contact = $data->id;
                    $receiver->email = $val;
                    $receiver->save();
                }
            }

            if ($request->get('tx_remail')) {
                foreach ($request->get('tx_remail') as $key => $val) {
                    $receiver = Receivers::findOrFail($key);
                    $receiver->id_contact = $data->id;
                    $receiver->email = $val;
                    $receiver->save();
                }
            }

            if ($request->get('hn_mail')) {
                foreach ($request->get('hn_mail') as $key => $val) {
                    $field = Receivers::findOrFail($val);
                    $field->delete();
                }
            }

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
