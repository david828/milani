<?php

namespace App\Http\Controllers\Adm\Conditions;

use App\Http\Controllers\Controller;
use App\Model\Conditions\Conditions;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ConditionsController extends Controller
{
    public function index()
    {
        $data = Conditions::first();
        if(!$data){
            $data = (object)[];
            $data->text = '';
        }
        return view('adm.conditions.edit', compact('data'));
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = Conditions::first();
            if(!$data){
                $data = new Conditions();
            }
            $data->text = $request->get('ta_text');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
