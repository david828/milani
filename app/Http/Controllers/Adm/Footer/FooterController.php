<?php

namespace App\Http\Controllers\Adm\Footer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Footer\UpdateFooterRequest;
use App\Model\Footer\Footer;
use Exception;
use Illuminate\Support\Facades\DB;


class FooterController extends Controller
{
    public function index()
    {
        $data = Footer::first();
        if(!$data){
            $data = (object)[];
            $data->id = 1;
            $data->title_1 = '';
            $data->text_1 = '';
            $data->text_2 = '';
        }
        return view('adm.footer.edit', compact('data'));
    }

    public function update(UpdateFooterRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Footer::first();
            if(!$data){
                $data = new Footer();
            }
            $data->id = 1;
            $data->title_1 = $request->get('tx_title_1');
            $data->text_1 = $request->get('ta_text_1');
            $data->text_2 = $request->get('ta_text_2');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
