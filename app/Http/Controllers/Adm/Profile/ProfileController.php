<?php

namespace App\Http\Controllers\Adm\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Profile\PasswordProfileRequest;
use App\Http\Requests\Adm\Profile\UpdateProfileRequests;
use App\User;
use Exception;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    public function index()
    {
        $data = User::findOrFail(Auth::id());
        return view('adm.profile.index', compact('data'));
    }

    public function update(UpdateProfileRequests $request)
    {

        DB::beginTransaction();
        try {
            $data = User::FindOrFail(Auth::id());
            $data->name = $request->get('tx_name');
            $data->email = $request->get('tx_email');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }

    }

    public function password(PasswordProfileRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = User::FindOrFail(Auth::id());
            $data->password = Hash::make($request->get('tx_password'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.password_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.password_problem')
                ]);
            }
        }
    }


}
