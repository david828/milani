<?php

namespace App\Http\Controllers\Adm\Galleries;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Galleries\DestroyCategoryGalRequest;
use App\Http\Requests\Adm\Galleries\StoreCategoryGalRequest;
use App\Http\Requests\Adm\Galleries\UpdateCategoryGalRequest;
use App\Model\Galleries\CategoriesGal;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesGalController extends Controller
{

    public function index(Request $request)
    {
        $list = CategoriesGal::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.categoriesgal.index', compact('list'));
    }

    public function create()
    {
        return view('adm.categoriesgal.create');
    }

    public function store(StoreCategoryGalRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new CategoriesGal();
            $data->name = $request->get('tx_category');
            $data->slug = Str::slug($request->get('tx_category'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = CategoriesGal::findOrFail($id);
        return view('adm.categoriesgal.edit', compact('data'));
    }

    public function update($id, UpdateCategoryGalRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = CategoriesGal::findOrFail($id);
            $data->name = $request->get('tx_category');
            $data->slug = Str::slug($request->get('tx_category'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCategoryGalRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = CategoriesGal::findOrFail($id);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
