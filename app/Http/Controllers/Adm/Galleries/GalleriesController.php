<?php

namespace App\Http\Controllers\Adm\Galleries;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Galleries\DestroyGalleryImageRequest;
use App\Http\Requests\Adm\Galleries\DestroyGalleryRequest;
use App\Http\Requests\Adm\Galleries\StoreGalleryRequest;
use App\Http\Requests\Adm\Galleries\UpdateGalleryRequest;
use App\Http\Requests\Adm\Galleries\UploadGalleryRequest;
use App\Model\Galleries\CategoriesGal;
use App\Model\Galleries\Galleries;
use App\Model\Galleries\GalleryImages;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;


class GalleriesController extends Controller
{
    public function index(Request $request)
    {
        $list = Galleries::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.galleries.index', compact('list'));
    }

    public function create()
    {
        $data = (object)[];
        $data->categories = CategoriesGal::selection();
        return view('adm.galleries.create', compact('data'));
    }

    public function store(StoreGalleryRequest $request)
    {
        DB::beginTransaction();
        try {

            $gallery = new Galleries();
            $gallery->id_category = $request->get('sl_categories');
            $gallery->name = $request->get('tx_name');
            $gallery->description = $request->get('ta_description');
            $gallery->slug = Str::slug($request->get('tx_name'));
            $gallery->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Galleries::findOrFail($id);
        $data->categories = CategoriesGal::selection();
        return view('adm.galleries.edit', compact('data'));
    }

    public function update($id, UpdateGalleryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Galleries::findOrFail($id);
            $data->id_category = $request->get('sl_categories');
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $data->description = $request->get('ta_description');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyGalleryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Galleries::with('images')->findOrFail($id);
            $dir = appData()->dirdata . '/images/galleries/';
            foreach ($data->images as $img) {
                $beforefile = $dir . $img->image;
                if (File::exists($beforefile)) {
                    File::delete($beforefile);
                }
            }
            GalleryImages::deleteImages($id);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function images($id)
    {
        $data = Galleries::with('images')->findOrFail($id);
        $data->dir = appData()->urldata . '/images/galleries/';
        $dir = appData()->dirdata . '/images/galleries/';
        if (File::exists($dir . $data->image)) {
            $data->image = $data->dir .  $data->image;
        }
        return view('adm.galleries.images', compact('data'));
    }

    public function upload($id, UploadGalleryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Galleries::findOrFail($id);

            $dir = appData()->dirdata . '/images/galleries/';
            $url = '/images/galleries/';

            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 1280, 'height' => 720, 'greyscale' => false])->save($dirImg);

                $imgFile = new GalleryImages();
                $imgFile->id_gallery = $data->id;
                $imgFile->image = $image;
                $imgFile->save();
            }

            $list = GalleryImages::getGalleryImages($data->id);

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'dir' => $url,
                    'images' => $list->toJson()
                ]);
            }
        } catch
        (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroyImage($id, DestroyGalleryImageRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = GalleryImages::where('id_gallery', $id)->findOrFail($request->get('hd_trash'));
            $dir = appData()->dirdata . '/images/galleries/';
            $beforefile = $dir . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
