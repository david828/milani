<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Products\Categories\DestroyCategoriesRequest;
use App\Http\Requests\Adm\Products\Categories\OrderCategoriesRequest;
use App\Http\Requests\Adm\Products\Categories\StoreCategoriesRequest;
use App\Http\Requests\Adm\Products\Categories\UpdateCategoriesRequest;
use App\Model\Products\Categories;
use App\Model\Products\Products;
use App\Model\Products\Subcategories;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $list = Categories::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.products.categories.index', compact('list'));
    }

    public function create()
    {
        return view('adm.products.categories.create');
    }

    public function store(StoreCategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Categories();
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Categories::findOrFail($id);
        return view('adm.products.categories.edit', compact('data'));
    }

    public function update($id, UpdateCategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Categories::findOrFail($id);
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            } else {
                $data->metadescription = null;
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            } else {
                $data->metatags = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Categories::findOrFail($id);
            $prod = Products::where('id_category', $id)->get();
            $cat = Subcategories::where('id_category', $id)->get();
            if(count($prod)> 0 || count($cat)> 0){
                return response()->json([
                    'error' => trans('app.delete_cat')
                ]);
            } else {
                $data->delete();
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function orderCat(OrderCategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Categories::findOrFail($request->get('hn_cat'));
            $cats = Categories::getCatOrder($data->id);
            $count = 1;
            foreach ($cats as $item) {
                if ($count == $request->get('hn_order')) {
                    $count++;
                }
                Categories::updateOrder($item->id, $count);
                $count++;
            }
            $data->order = $request->get('hn_order');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => 'ok'
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
