<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;

use App\Http\Requests\Adm\Products\Colors\DestroyColorsRequest;
use App\Http\Requests\Adm\Products\Colors\StoreColorsRequest;
use App\Http\Requests\Adm\Products\Colors\UpdateColorsRequest;
use App\Model\Invoice\InvoiceDetails;
use App\Model\Products\Colors;
use App\Model\Products\ProductColor;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ColorsController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/colors/';
        $this->url = appData()->urldata . '/images/colors/';
    }

    public function index(Request $request)
    {
        $list = Colors::listData($request->get('search'));
        foreach ($list as $item) {
            if($item->image) {
                $item->image = $this->url . $item->image;
            } else {
                $item->image = '/assets/images/nocolor.png';
            }
        }
        $list->search = $request->get('search');
        return view('adm.products.colors.index', compact('list'));
    }

    public function create()
    {
        return view('adm.products.colors.create');
    }

    public function store(StoreColorsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Colors();
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $image = md5(time()) . '.png';
            $imgProduct = $request->file('fl_image');
            $dirImg = $this->dir . $image;
            Image::make($imgProduct, ['width' => 200, 'height' => 200, 'greyscale' => false])->save($dirImg);
            $data->image = $image;
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Colors::findOrFail($id);
        return view('adm.products.colors.edit', compact('data'));
    }

    public function update($id, UpdateColorsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Colors::findOrFail($id);
            $data->name = $request->get('tx_name');
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 200, 'height' => 200, 'greyscale' => false])->save($dirImg);
                if (File::exists($this->dir . $data->image)) {
                    File::delete($this->dir . $data->image);
                }
                $data->image = $image;
            }
            $data->slug = Str::slug($request->get('tx_name'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyColorsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Colors::findOrFail($id);
            if (File::exists($this->dir . $data->image)) {
                File::delete($this->dir . $data->image);
            }
            ProductColor::deleteColor($id);
            InvoiceDetails::where('id_color', $id)->update(['id_color' =>null]);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
