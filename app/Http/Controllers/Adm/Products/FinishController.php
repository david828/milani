<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Products\Finish\DestroyFinishRequest;
use App\Http\Requests\Adm\Products\Finish\StoreFinishRequest;
use App\Http\Requests\Adm\Products\Finish\UpdateFinishRequest;
use App\Model\Products\Finish;
use App\Model\Products\Products;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FinishController extends Controller
{
    public function index(Request $request)
    {
        $list = Finish::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.products.finish.index', compact('list'));
    }

    public function create()
    {
        return view('adm.products.finish.create');
    }

    public function store(StoreFinishRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Finish();
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Finish::findOrFail($id);
        return view('adm.products.finish.edit', compact('data'));
    }

    public function update($id, UpdateFinishRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Finish::findOrFail($id);
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyFinishRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Finish::findOrFail($id);
            Products::where('id_finish', $id)->update(['id_finish' => null]);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
