<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Products\UpdateFeatureProductsRequest;
use App\Model\Products\FeatureProducts;
use App\Model\Products\Products;
use Exception;
use Illuminate\Support\Facades\DB;

class FeatureProductsController extends Controller
{
    public function edit()
    {
        $data = FeatureProducts::find(1);
        if(!$data){
            $data = (object)[];
            $data->title_featureds = '';
            $data->title_news = '';
            $data->title_bests = '';
        }
        return view('adm.products.feature.edit', compact('data'));
    }

    public function update(UpdateFeatureProductsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = FeatureProducts::find(1);
            if(!$data){
                $data = new FeatureProducts();
            }
            $data->id = 1;
            $data->title_featureds = $request->get('sl_product1');
            $data->title_news = $request->get('sl_product2');
            $data->title_bests = $request->get('sl_product3');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //exit($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}