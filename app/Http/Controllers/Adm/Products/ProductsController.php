<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Products\BestProductRequest;
use App\Http\Requests\Adm\Products\DestroyProductImageRequest;
use App\Http\Requests\Adm\Products\DestroyProductsRequest;
use App\Http\Requests\Adm\Products\NewProductRequest;
use App\Http\Requests\Adm\Products\SoonProductRequest;
use App\Http\Requests\Adm\Products\StatusProductsRequest;
use App\Http\Requests\Adm\Products\StoreProductsRequest;
use App\Http\Requests\Adm\Products\UpdateProductsRequest;
use App\Http\Requests\Adm\Products\UploadProductRequest;
use App\Model\Invoice\InvoiceDetails;
use App\Model\Productdes\Productdes;
use App\Model\Products\Colors;
use App\Model\Products\Finish;
use App\Model\Products\ProductColor;
use App\Model\Products\ProductColorImage;
use App\Model\Products\Subcategories;
use App\Model\Products\ProductGallery;
use App\Model\Products\Products;
use App\Model\Products\Categories;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    protected $dir;
    protected $dirimg;
    protected $urlimg;

    public function __construct()
    {
        $this->dirimg = appData()->dirdata . '/images/products/';
        $this->urlimg = appData()->urldata . '/images/products/';
    }

    public function index(Request $request)
    {
        if($request->get('ck_featured'))
        {
            $best = '1';
        } else {
            $best = '';
        }
        if($request->get('ck_new'))
        {
            $new = '1';
        } else {
            $new = '';
        }
        if($request->get('ck_soon'))
        {
            $soon = '1';
        } else {
            $soon = '';
        }
        $list = Products::listData($request->get('search'), $best, $new, $soon);
        foreach ($list as $item)
        {
            $color = ProductColor::where('id_product', $item->id)->get();
            if(count($color)>0) {
                $reference = '';
                foreach ($color as $c)
                {
                    $reference = $reference . $c->reference . ' / ';
                }
                $item->reference = $reference;
            }
        }
        $list->search = $request->get('search');
        $list->bs = $best;
        $list->n = $new;
        $list->so = $soon;
        return view('adm.products.index', compact('list'));
    }

    public function create()
    {
        $cat = Categories::Selection();
        $colors = Colors::orderBy('name', 'ASC')->get();
        $finish = Finish::Selection();
        return view('adm.products.create', compact('cat', 'colors', 'finish'));
    }

    public function store(StoreProductsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Products();
            $data->id_category = $request->get('sl_category');
            if($request->get('sl_subcategory')) {
                $data->id_subcategory = $request->get('sl_subcategory');
            }
            if($request->get('sl_finish')) {
                $data->id_finish = $request->get('sl_finish');
            }
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $data->description = $request->get('ta_description');
            $data->review = $request->get('ta_review');
            if($request->get('tx_SKU')) {
            }
            $data->price = $request->get('tx_price');
            if(is_array($request->get('tx_stock'))){
                $data->stock = 0;
                $data->min_stock = 0;
                $data->SKU = 0;
                $data->reference = 0;
            } else {
                $data->stock = $request->get('tx_stock');
                $data->min_stock = $request->get('tx_minstock');
                $data->SKU = $request->get('tx_SKU');
                $data->reference = $request->get('tx_reference');
            }
            if(!is_null($request->get('ck_featured'))) {
                $data->featured = '1';
            } else {
                $data->featured = '0';
            }
            if(!is_null($request->get('ck_best'))) {
                $data->sw_best = '1';
            } else {
                $data->sw_best = '0';
            }
            if(!is_null($request->get('ck_new'))) {
                $data->new = '1';
            } else {
                $data->new = '0';
            }
            if(!is_null($request->get('ck_soon'))) {
                $data->soon = '1';
            } else {
                $data->soon = '0';
            }
            if(!is_null($request->get('ck_sale')) and !is_null($request->get('tx_sale_percent'))) {
                $data->sale_percent = $request->get('tx_sale_percent');
                $discount = $request->get('tx_price') * ($request->get('tx_sale_percent')/100);
                $data->sale_price = round(($request->get('tx_price') - $discount));
                $data->sale = '1';
            } else {
                $data->sale_price = null;
                $data->sale_percent = null;
                $data->sale = '0';
            }
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            }
            if ($request->get('tx_metatitle')) {
                $data->metatitle = $request->get('tx_metatitle');
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            }
            $data->save();
            $id = $data->id;
            $acum = 0;
            $acumMin = 0;
            if($request->get('sl_color')) {
                foreach ($request->get('sl_color') as $key => $val) {
                    $pdcol = new ProductColor();
                    $pdcol->id_product = $id;
                    $pdcol->stock = $request->get('tx_stock')[$val];
                    $pdcol->min_stock = $request->get('tx_minstock')[$val];
                    $pdcol->reference = $request->get('tx_reference')[$val];
                    $pdcol->SKU = $request->get('tx_SKU')[$val];
                    $pdcol->id_color = $val;
                    $pdcol->save();
                    $acum+=$request->get('tx_stock')[$val];
                    $acumMin+=$request->get('tx_minstock')[$val];
                }
                $dt = Products::find($id);
                $dt->stock = $acum;
                $dt->min_stock = $acumMin;
                $dt->save();
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //exit(print_r($e->getMessage()));
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Products::with('color')
            ->with('productcolor')
            ->findOrFail($id);
        $colorsSelected = [];
        foreach ($data->color as $col)
        {
            array_push($colorsSelected, $col->id);
        }
        $stockC = [];
        $stockCM = [];
        $refC = [];
        $SKUC = [];
        foreach ($data->productcolor as $item)
        {
            $stockC[$item->id_color] = $item->stock;
            $stockCM[$item->id_color] = $item->min_stock;
            $refC[$item->id_color] = $item->reference;
            $SKUC[$item->id_color] = $item->SKU;
        }
        $data->colorselected = $colorsSelected;
        $cat = Categories::Selection();
        $colors = Colors::orderBy('name', 'ASC')->get();
        $finish = Finish::Selection();
        $subcat = Subcategories::selection($data->id_category);
        //dd($subcat);
        return view('adm.products.edit', compact('data', 'cat', 'colors', 'finish', 'subcat','stockC','stockCM','refC','SKUC'));
    }

    public function update($id, UpdateProductsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Products::findOrFail($id);
            $data->id_category = $request->get('sl_category');
            if($request->get('sl_subcategory')) {
                $data->id_subcategory = $request->get('sl_subcategory');
            }
            if($request->get('sl_finish')) {
                $data->id_finish = $request->get('sl_finish');
            }
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            $data->description = $request->get('ta_description');
            $data->review = $request->get('ta_review');
            $data->price = $request->get('tx_price');
            if(is_array($request->get('tx_stock'))){
                $data->stock = 0;
                $data->min_stock = 0;
                $data->SKU = 0;
                $data->reference = 0;
            } else {
                $data->stock = $request->get('tx_stock');
                $data->min_stock = $request->get('tx_minstock');
                $data->SKU = $request->get('tx_SKU');
                $data->reference = $request->get('tx_reference');
            }
            if(!is_null($request->get('ck_featured'))) {
                $data->featured = '1';
            } else {
                $data->featured = '0';
            }
            if(!is_null($request->get('ck_best'))) {
                $data->sw_best = '1';
            } else {
                $data->sw_best = '0';
            }
            if(!is_null($request->get('ck_new'))) {
                $data->new = '1';
            } else {
                $data->new = '0';
            }
            if(!is_null($request->get('ck_soon'))) {
                $data->soon = '1';
            } else {
                $data->soon = '0';
            }
            if(!is_null($request->get('ck_sale')) and !is_null($request->get('tx_sale_percent'))) {
                $data->sale_percent = $request->get('tx_sale_percent');
                $discount = $request->get('tx_price') * ($request->get('tx_sale_percent')/100);
                $data->sale_price = round(($request->get('tx_price') - $discount));
                $data->sale = '1';
            } else {
                $data->sale_price = null;
                $data->sale_percent = null;
                $data->sale = '0';
            }
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            } else {
                $data->metadescription = null;
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            } else {
                $data->metatags = null;
            }
            if ($request->get('tx_metatitle')) {
                $data->metatitle = $request->get('tx_metatitle');
            } else {
                $data->metatitle = null;
            }
            $data->save();
            $id = $data->id;
            if ($request->get('sl_color')) {
                $color_s = $request->get('sl_color');
            } else {
                $color_s = [];
            }
            $color_g = ProductColor::getColor($id);
            $color_gs = array_diff($color_g, $color_s);     //Están guardados pero no el seledct
            $color_sg = array_diff($color_s, $color_g);     //Están en el select pero no guardados
            /*if (count($color_g) === 0) {
                ProductGallery::deleteProduct($id);
            }*/
            foreach ($color_sg as $key => $val) {
                $pdcol = new ProductColor();
                $pdcol->id_product = $id;
                $pdcol->id_color = $val;
                $pdcol->stock = 0;
                $pdcol->min_stock = 0;
                $pdcol->reference = 0;
                $pdcol->SKU = 0;
                $pdcol->save();
            }
            foreach ($color_gs as $key => $val) {
                ProductColor::deleteRelation($id,$val);
            }
            //dd($request->get('tx_stock'));
            if(is_array($request->get('tx_stock'))){
                $acum = 0;
                $acumMin = 0;
                foreach ($request->get('tx_stock') as $key => $val) {
                    ProductColor::where('id_color',$key)
                                ->where('id_product', $id)
                                ->update(['stock'=>$val,
                                    'min_stock'=>$request->get('tx_minstock')[$key],
                                    'reference'=>$request->get('tx_reference')[$key],
                                    'SKU'=>$request->get('tx_SKU')[$key],
                                ]);
                    $acum+=$request->get('tx_stock')[$key];
                    $acumMin+=$request->get('tx_minstock')[$key];
                }
                $dt = Products::find($id);
                $dt->stock = $acum;
                $dt->min_stock = $acumMin;
                $dt->save();
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyProductsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Products::findOrFail($id);
            ProductColor::deleteProduct($id);
            ProductGallery::deleteProduct($id);
            InvoiceDetails::where('id_product', $id)->update(['id_product' =>null], ['id_color' =>null]);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //exit($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function status(StatusProductsRequest $request)
    {
        $data = Products::findOrFail($request->get('hn_status'));
        $status = 'label-success';
        if ($data->status == 1) {
            $data->status = 0;
            $status = 'label-warning';
        } else {
            $data->status = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $status
            ]);
        }
    }

    /********* MANEJO DE GALERIAS *****************/

    public function images($id, Request $request)
    {
        $color = $request->get('col');
        $data = Products::with(['images' => function ($qr) use ($color) {
                    $qr->orderBy('order', 'ASC');
            }])
            ->with('color')
            ->with(['productcolor' => function ($qr) use ($color) {
                if(!is_null($color)) {
                    $qr->where('id_color', $color);
                }
                $qr->with(['images' => function ($qr) use ($color) {
                    $qr->orderBy('order', 'ASC');
                }]);
            }])
            ->findOrFail($id);
        if(count($data->color) > 0)
        {
            $switch = 'S';
            $images = $data->productcolor[0]->images;
        } else {
            $switch = 'N';
            $images = $data->images;
        }
        $selectcolor = ProductColor::selection($id);
        $data->dir = appData()->urldata . '/images/products/';
        return view('adm.products.images', compact('data', 'switch', 'color', 'selectcolor', 'images'));
    }

    public function upload($id, UploadProductRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Products::findOrFail($id);
            $dir = appData()->dirdata . '/images/products/';
            $url = '/images/products/';

                /*
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgProducts = $dir . $image;
                $dirImg = $dir . $image;
                file_put_contents($dirImg, $content);
*/
                $image = md5(time()) . '.png';
                $imgProducts = $request->file('hn_file');
                $dirImg = $dir . $image;
                Image::make($imgProducts, ['width' => 540, 'height' => 586, 'greyscale' => false])->save($dirImg);
                //dd('llega');
                if ($request->get('switch') == 'S') {
                    $imgFile = new ProductColorImage();
                    $imgFile->id_productcolor = $request->get('hn_color');
                    $imgFile->image = $image;
                    $imgFile->save();
                    $list = ProductColorImage::where('id_productcolor', $request->get('hn_color'))->get();
                } else {
                    $imgFile = new ProductGallery();
                    $imgFile->id_product = $data->id;
                    $imgFile->image = $image;
                    $imgFile->save();
                    $list = ProductGallery::where('id_product', $data->id)->get();
                }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'dir' => $url,
                    'images' => $list->toJson(),
                    'color' => $request->get('hn_color')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroyImage($id, DestroyProductImageRequest $request)
    {
        DB::beginTransaction();
        try {
            if($request->get('switch') == 'S') {
                $data = ProductColorImage::findOrFail($request->get('hd_trash'));
            } else {
                $data = ProductGallery::findOrFail($request->get('hd_trash'));
            }
            $dir = appData()->dirdata . '/images/products/';
            $beforefile = $dir . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //exit(print_r($e->getMessage()));
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    /**************** ALT ******************/
    public function altImage(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->get('alt_switch') == 'S') {
                $data = ProductColorImage::findOrFail($request->get('hd_image'));
            } else {
                $data = ProductGallery::findOrFail($request->get('hd_image'));
            }
            if($request->get('tx_alt')) {
                $data->alt = $request->get('tx_alt');
            } else {
                $data->alt = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    /**************** SWITCH ******************/
    public function best(BestProductRequest $request)
    {
        $data = Products::findOrFail($request->get('hn_best'));
        $best = 'label-success';
        if ($data->featured == 1) {
            $data->featured = 0;
            $best = 'label-warning';
        } else {
            $data->featured = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $best
            ]);
        }
    }

    public function newP(NewProductRequest $request)
    {
        $data = Products::findOrFail($request->get('hn_new'));
        $new = 'label-success';
        if ($data->new == 1) {
            $data->new = 0;
            $new = 'label-warning';
        } else {
            $data->new = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $new
            ]);
        }
    }

    public function Soon(SoonProductRequest $request)
    {
        $data = Products::findOrFail($request->get('hn_soon'));
        $soon = 'label-success';
        if ($data->soon == 1) {
            $data->soon = 0;
            $soon = 'label-warning';
        } else {
            $data->soon = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $soon
            ]);
        }
    }
}
