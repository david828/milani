<?php

namespace App\Http\Controllers\Adm\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Products\Subcategories\DestroySubcategoriesRequest;
use App\Http\Requests\Adm\Products\Subcategories\StoreSubcategoriesRequest;
use App\Http\Requests\Adm\Products\Subcategories\UpdateSubcategoriesRequest;
use App\Model\Products\Products;
use App\Model\Products\Subcategories;
use App\Model\Products\Categories;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SubcategoriesController extends Controller
{
    public function index(Request $request)
    {
        $list = Subcategories::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.products.subcategories.index', compact('list'));
    }

    public function create()
    {
        $categories = Categories::Selection();
        return view('adm.products.subcategories.create', compact('categories'));
    }

    public function store(StoreSubcategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Subcategories();
            $data->name = $request->get('tx_name');
            $data->id_category = $request->get('sl_category');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Subcategories::findOrFail($id);
        $categories = Categories::Selection();
        return view('adm.products.subcategories.edit', compact('data', 'categories'));
    }

    public function update($id, UpdateSubcategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Subcategories::findOrFail($id);
            $data->name = $request->get('tx_name');
            $data->id_category = $request->get('sl_category');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($request->get('ta_metadesc')) {
                $data->metadescription = $request->get('ta_metadesc');
            } else {
                $data->metadescription = null;
            }
            if ($request->get('tx_metatags')) {
                $data->metatags = $request->get('tx_metatags');
            } else {
                $data->metatags = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroySubcategoriesRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Subcategories::findOrFail($id);
            $prod = Products::where('id_subcategory', $id)->get();
            if(count($prod)> 0){
                return response()->json([
                    'error' => trans('app.delete_subcat')
                ]);
            } else {
                $data->delete();
            }            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function getSubcategories(Request $request)
    {
        $cat = $request->get('cat');

        return Subcategories::getSubcategories($cat);
    }
}
