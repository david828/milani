<?php

namespace App\Http\Controllers\Adm\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Categories\DestroyCategoryRequest;
use App\Http\Requests\Adm\Categories\StoreCategoryRequest;
use App\Http\Requests\Adm\Categories\UpdateCategoryRequest;
use App\Model\Categories\Categories;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{

    public function index(Request $request)
    {
        $list = Categories::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.categories.index', compact('list'));
    }

    public function create()
    {
        return view('adm.categories.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Categories();
            $data->name = $request->get('tx_category');
            $data->slug = Str::slug($request->get('tx_category'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Categories::findOrFail($id);
        return view('adm.categories.edit', compact('data'));
    }

    public function update($id, UpdateCategoryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Categories::findOrFail($id);
            $data->name = $request->get('tx_category');
            $data->slug = Str::slug($request->get('tx_category'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCategoryRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Categories::findOrFail($id);
            $data->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }
}
