<?php

namespace App\Http\Controllers\Adm\PayU;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Payu\UpdatePayuRequest;
use App\Model\PayU\Payu;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class PayuController extends Controller
{
    public function index()
    {
        $data = Payu::first();
        if (is_null($data)) {
            $data = (object)[];
            $data->accountid = null;
            $data->merchantid = null;
            $data->apilogin = null;
            $data->apikey = null;
            $data->publickey = null;
            $data->responseurl = null;
            $data->requesturl = null;
            $data->testing = null;
            $data->confirmationurl = null;
        }
        return view('adm.payu.index', compact('data'));
    }

    public function update(UpdatePayuRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Payu::first();
            if (is_null($data)) {
                $data = new Payu();
            }
            $data->accountid = $request->get('tx_accountid');
            $data->merchantid = $request->get('tx_merchantid');
            $data->apilogin = $request->get('tx_apilogin');
            $data->apikey = $request->get('tx_apikey');
            $data->publickey = $request->get('tx_publickey');
            $data->responseurl = $request->get('tx_responseurl');
            $data->requesturl = $request->get('tx_requesturl');
            $data->confirmationurl = $request->get('tx_confirmationurl');
            $data->testing = $request->get('sl_testing');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public static function dataPayu()
    {
        $data = Payu::first();
        if ($data->testing == 1) {
            $data->accountid = '512321';
            $data->merchantid = '508029';
            $data->apilogin = 'pRRXKOl8ikMmt9u';
            $data->apikey = '4Vj8eK4rloUd272L48hsrarnUA';
            $data->requesturl = 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu';
        }
        return $data;
    }
}
