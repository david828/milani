<?php

namespace App\Http\Controllers\Adm\Coupons;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Coupon\CreateCouponRequest;
use App\Http\Requests\Adm\Coupon\DestroyCouponRequest;
use App\Http\Requests\Adm\Coupon\StatusCouponRequest;
use App\Http\Requests\Adm\Coupon\UpdateCouponRequest;
use App\Model\Coupons\CouponHistory;
use App\Model\Coupons\Coupons;
use App\Model\Products\Products;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{
    public function index(Request $request)
    {
        $list = Coupons::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.coupon.index', compact('list'));
    }


    public function create()
    {
        $data = [
            ''=>trans('app.select'),
            '1'=>trans('app.by_date'),
            '2'=>trans('app.by_uses'),
            '3'=>trans('app.no_expiration'),
        ];
        $products = Products::selection();
        return view('adm.coupon.create', compact('data', 'products'));
    }

    public function store(CreateCouponRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Coupons();
            $data->coupon = $request->get('tx_coupon');
            $data->discount = $request->get('tx_discount');
            $data->expirationtype = $request->get('sl_type');
            $data->expiration = $request->get('tx_expiration');
            $data->status = 1;
            $pd = $request->get('sl_products');
            if(is_null($pd)) {
                $data->products = 0;
            } else {
                $pd = implode('|',$pd);
                $data->products = $pd;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.store_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Coupons::findOrFail($id);
        $data->couponList = [
            ''=>trans('app.select'),
            '1'=>trans('app.by_date'),
            '2'=>trans('app.by_uses'),
            '3'=>trans('app.no_expiration'),
        ];
        if(is_null($data->products) || $data->products == '0'){
            $data->products = null;
        } else {
            $data->products = explode('|',$data->products);
        }
        $products = Products::selection();
        return view('adm.coupon.edit', compact('data', 'products'));
    }

    public function update($id, UpdateCouponRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Coupons::findOrFail($id);
            $data->coupon = $request->get('tx_coupon');
            $data->discount = $request->get('tx_discount');
            $data->expirationtype = $request->get('sl_type');
            $data->expiration = $request->get('tx_expiration');
            $pd = $request->get('sl_products');
            if(is_null($pd)) {
                $data->products = 0;
            } else {
                $pd = implode('|',$pd);
                $data->products = $pd;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyCouponRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Coupons::findOrFail($id);
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function status(StatusCouponRequest $request)
    {
        $data = Coupons::findOrFail($request->get('hn_status'));
        $status = 'label-success';
        if ($data->status == 1) {
            $data->status = 0;
            $status = 'label-warning';
        } else {
            $data->status = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $status
            ]);
        }
    }

    public function historic (Request $request)
    {
        $c = $request->get('c');
        $d = $request->get('d');
        $h = $request->get('h');
        $list = CouponHistory::listData($c, $d, $h);
        $listCoupon = CouponHistory::selector();
        foreach ($list as $item) {
            $item->percent = round($item->percent, 2);
            $item->value = '$' . number_format($item->value, 0, ',', '.');
            $item->date = Carbon::parse($item->created_at)->format('d/m/Y');
        }
        return view('adm.coupon.historic', compact('list', 'listCoupon', 'c', 'h', 'd'));
    }
}
