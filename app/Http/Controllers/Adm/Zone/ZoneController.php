<?php

namespace App\Http\Controllers\Adm\Zone;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Zone\StatusZoneRequest;
use App\Http\Requests\Adm\Zone\UpdateZoneRequest;
use App\Model\Location\Cities;
use App\Model\Zone\Zone;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ZoneController extends Controller
{
    public function index(Request $request)
    {
        $list = Zone::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.zone.index', compact('list'));
    }

    public function edit($id)
    {
        $data = Zone::findOrFail($id);
        return view('adm.zone.edit', compact('data'));
    }

    public function update($id, UpdateZoneRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Zone::find($id);
            $data->price = $request->get('tx_price');
            if($request->get('ck_ship')) {
                $data->ship_free = 1;
            } else {
                $data->ship_free = 0;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(), $e->getLine(), $e->getFile());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function status(StatusZoneRequest $request)
    {
        $data = Zone::findOrFail($request->get('hn_status'));
        $status = 'label-success';
        if ($data->status == 1) {
            $data->status = 0;
            $status = 'label-warning';
        } else {
            $data->status = 1;
        }
        $data->save();
        if ($request->ajax()) {
            return response()->json([
                'message' => $status
            ]);
        }
    }

    public static function getShip($idCity)
    {
        $city = Cities::find($idCity);
        return Zone::getShip($city->id_zone);
    }
}
