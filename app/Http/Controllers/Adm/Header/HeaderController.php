<?php

namespace App\Http\Controllers\Adm\Header;

use App\Http\Controllers\Controller;

use App\Http\Requests\Adm\Header\DestroyHeaderImageRequest;
use App\Http\Requests\Adm\Header\UpdateHeaderRequest;
use App\Http\Requests\Adm\Header\UploadHeaderImageRequest;
use App\Model\Header\Header;
use App\Model\Header\HeaderImages;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class HeaderController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/header/';
        $this->url = appData()->urldata . '/images/header/';
    }

    public function index()
    {
        $data = Header::find(1);
        if(!$data){
            $data = (object)[];
            $data->id = 1;
            $data->pos_1 = '0';
            $data->pos_2 = '0';
            $data->client_id = '';
            $data->user_id = '';
            $data->accessToken = '';
            $data->num_photos = '';
        }
        $data->images = HeaderImages::orderBy('order','ASC')->get();
        return view('adm.header.edit', compact('data'));
    }

    public function update(UpdateHeaderRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Header::find(1);
            if(!$data){
                $data = new Header();
            }
            $data->pos_1 = $request->get('hn_pos_1');
            $data->pos_2 = $request->get('hn_pos_2');
            $data->client_id = $request->get('tx_client');
            $data->user_id = $request->get('tx_user');
            $data->accessToken = $request->get('tx_accesstoken');
            $data->num_photos = $request->get('tx_numphotos');
            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 960, 'height' => 200, 'greyscale' => false])->save($dirImg);
                if (File::exists($this->dir . $data->image_2)) {
                    File::delete($this->dir . $data->image_2);
                }
                $data->image_2 = $image;
            }
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function images()
    {
        $data = HeaderImages::orderBy('order','ASC')->get();
        $data->dir = appData()->urldata . '/images/header/';
        return view('adm.header.images', compact('data'));
    }

    public function upload($id, UploadHeaderImageRequest $request)
    {
        DB::beginTransaction();
        try {

            $dir = $this->dir;
            $url = '/images/header/';
            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 959, 'height' => 681, 'greyscale' => false])->save($dirImg);
                $imgFile = new HeaderImages();
                $imgFile->id_header = 1;
                $imgFile->image = $image;
                $imgFile->save();
            }

            $list = HeaderImages::orderBy('order','ASC')->get();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'dir' => $url,
                    'images' => $list->toJson()
                ]);
            }
        } catch
        (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroyImage($id, DestroyHeaderImageRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = HeaderImages::findOrFail($request->get('hd_trash'));
            $dir = appData()->dirdata . '/images/header/';
            $beforefile = $dir . $data->image;

            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    /**************** LINKS ******************/
    public function links(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = HeaderImages::findorFail($request->get('hd_header'));
            if($request->get('tx_link')) {
                $data->link = $request->get('tx_link');
                $data->typelink = $request->get('sl_typelink');
            } else {
                $data->link = null;
                $data->typelink = null;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
