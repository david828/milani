<?php

namespace App\Http\Controllers\Adm\App;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\App\UpdateAppSeoRequest;
use App\Model\Seo\AppSeo;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class SeoController extends Controller
{
    public function configure()
    {
        $data = AppSeo::find(1);
        if(!$data){
            $data = (object)[];
            $data->id = 1;
            $data->application_name = '';
            $data->charset = '';
            $data->metadescription = '';
            $data->metatags = '';
            $data->author = '';
            $data->robots = '';
            $data->language = '';
            $data->google_analytics = '';
            $data->pixel_facebook = '';
            $data->hotjar = '';
            $data->metadescription_blog = '';
            $data->metatags_blog = '';
            $data->metadescription_portfolio = '';
            $data->metatags_portfolio = '';
            $data->metadescription_aboutus = '';
            $data->metatags_aboutus = '';
            $data->metadescription_contact = '';
            $data->metatags_contact = '';
        }
        return view('adm.seodata.index', compact('data'));
    }

    public function update(UpdateAppSeoRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = AppSeo::find(1);
            if(!$data){
                $data = new AppSeo();
                $data->id_app = 1;
            }
            $data->application_name = $request->get('tx_name');
            $data->charset = $request->get('tx_charset');
            $data->metadescription = $request->get('ta_metadesc');
            $data->metatags = $request->get('tx_tags');
            if ($request->get('ta_ganalytics')) {
                $data->google_analytics = $request->get('ta_ganalytics');
            }
            if ($request->get('ta_pixel')) {
                $data->pixel_facebook = $request->get('ta_pixel');
            } else {
                $data->pixel_facebook = null;
            }
            if ($request->get('ta_hotjar')) {
                $data->hotjar = $request->get('ta_hotjar');
            } else {
                $data->hotjar = null;
            }
            if ($request->get('ta_chat')) {
                $data->chat = $request->get('ta_chat');
            } else {
                $data->chat = null;
            }
            if ($request->get('ta_metadesc_aboutus')) {
                $data->metadescription_aboutus = $request->get('ta_metadesc_aboutus');
            }
            if ($request->get('tx_tags_aboutus')) {
                $data->metatags_aboutus = $request->get('tx_tags_aboutus');
            }
            if ($request->get('ta_metadesc_portfolio')) {
                $data->metadescription_portfolio = $request->get('ta_metadesc_portfolio');
            }
            if ($request->get('tx_tags_portfolio')) {
                $data->metatags_portfolio = $request->get('tx_tags_portfolio');
            }
            if ($request->get('ta_metadesc_contact')) {
                $data->metadescription_contact = $request->get('ta_metadesc_contact');
            }
            if ($request->get('tx_tags_contact')) {
                $data->metatags_contact = $request->get('tx_tags_contact');
            }
            if ($request->get('ta_metadesc_blog')) {
                $data->metadescription_blog = $request->get('ta_metadesc_blog');
            }
            if ($request->get('tx_tags_blog')) {
                $data->metatags_blog = $request->get('tx_tags_blog');
            }
            $data->author = $request->get('tx_author');
            $data->robots = $request->get('tx_robots');
            $data->language = $request->get('tx_language');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public static function getAppSeo()
    {
        return AppSeo::getSeo();
    }
}
