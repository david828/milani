<?php

namespace App\Http\Controllers\Adm\App;


use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\App\UpdateAppRequest;
use App\Model\App\App;
use App\Model\App\Currency;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AppController extends Controller
{

    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/app/';
        $this->url = appData()->urldata . '/images/app/';
    }

    public static function appData()
    {
        return App::with('currency')->firstOrFail();
    }

    public function configure()
    {
        $data = App::firstOrFail();
        $data->directory = env('DOCUMENT_ROOT');
        $data->currencyList = Currency::selection();
        return view('adm.app.index', compact('data'));
    }

    public function update(UpdateAppRequest $request)
    {
        $data = App::firstOrFail();
        DB::beginTransaction();
        try {
            $data->color = $request->get('tx_color');
            $data->name = $request->get('tx_name');
            $data->email = $request->get('tx_email');
            $data->phone = $request->get('tx_phone');
            $data->cellphone = $request->get('tx_mobile');
            $data->address = $request->get('tx_address');
            $data->website = $request->get('tx_website');
            $data->dirdata = $request->get('tx_dir');
            $data->urldata = $request->get('tx_url');
            $data->copyright = $request->get('tx_copyright');
            $data->id_currency = $request->get('sl_currency');
            $data->tax_value = $request->get('tx_tax');
            $data->min_value = $request->get('tx_min_value');
            if ($request->file('fn_file')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fn_file');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 178, 'height' => 57, 'greyscale' => false])->save($dirImg);

                if (File::exists($this->dir . $data->logo)) {
                    File::delete($this->dir . $data->logo);
                }
                $data->logo = $image;
            }
            $data->name_boucher = $request->get('tx_name_boucher');
            $data->email_boucher = $request->get('tx_email_boucher');
            $data->phone_boucher = $request->get('tx_phone_boucher');
            $data->address_boucher = $request->get('tx_address_boucher');
            $data->website_boucher = $request->get('tx_website_boucher');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
