<?php

namespace App\Http\Controllers\Adm\App;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\AppSocial\AppSocialRequest;
use App\Http\Requests\Adm\AppSocial\AppaboutusRequest;
use App\Model\Social\AppSocial;
use App\Model\Social\Social;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class AppSocialController extends Controller
{
    public function addSocial()
    {
        $data = (object)[];
        $data->appsocial = AppSocial::with('social')->get();
        $data->socialList = Social::selection();
        return view('adm.appsocial.edit', compact('data'));
    }

    public function fields($cont)
    {
        $data = (object)[];
        $data->cont = $cont;
        $data->socialList = Social::selection();
        return (string)view('adm.appsocial.fields', compact('data'));
    }

    public function update(AppSocialRequest $request)
    {

        DB::beginTransaction();
        try {
            AppSocial::deleteSocial(appData()->id);

            if ($request->get('sl_social')) {
                foreach ($request->get('sl_social') as $key => $val) {
                    $social = new AppSocial();
                    $social->id_app = appData()->id;
                    $social->id_social = $request->get('sl_social')[$key];
                    $social->link = $request->get('tx_link')[$key];
                    $social->save();
                }
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e);
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }
}
