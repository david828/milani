<?php

namespace App\Http\Controllers\Adm\Menu;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Menu\UpdateMenuRequest;
use App\Model\Menu\Menu;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MenuController extends Controller
{
    public function index()
    {
        $list = Menu::listData();
        return view('adm.menu.index', compact('list'));
    }

    public function edit($id)
    {
        $data = Menu::findOrFail($id);
        return view('adm.menu.edit', compact('data'));
    }

    public function update($id, UpdateMenuRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Menu::find($id);
            $data->title = $request->get('tx_title');
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(), $e->getLine(), $e->getFile());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public static function getMenu()
    {
        return Menu::getMenu();
    }

}
