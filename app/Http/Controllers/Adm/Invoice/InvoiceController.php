<?php

namespace App\Http\Controllers\Adm\Invoice;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Invoice\AnnulInvoiceRequest;
use App\Http\Requests\Adm\Invoice\TrackInvoiceRequest;
use App\Model\Invoice\Invoice;
use App\Model\Invoice\InvoiceAddress;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $list = Invoice::listData($request->get('search'));
        $list->search = $request->get('search');
        //dd($list);
        return view('adm.invoice.index', compact('list'));
    }

    public function show($id)
    {
        $data = Invoice::with('status')->with('details')->findOrFail($id);
        $address = InvoiceAddress::where('id_invoice', $id)->first();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        return view('adm.invoice.show', compact('data', 'address'));
    }

    public function printInvoice($id)
    {
        $data = Invoice::with('status')->with('details')->findOrFail($id);
        $address = InvoiceAddress::where('id_invoice', $id)->first();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        return view('adm.invoice.print', compact('data', 'address'));
    }

    public function invalidate($id, AnnulInvoiceRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Invoice::findOrFail($id);
            $data->id_status = 3;
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('invoice.annul_success'),
                    'status' => trans('invoice.annulled'),
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('invoice.annul_problem')
                ]);
            }
        }
    }

    public function track($id, TrackInvoiceRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Invoice::findOrFail($id);
            $data->idtrack = $request->get('tx_track');
            $data->save();
            DB::commit();

            $email = (Object)[];
            $email->toEmail = $data->customer_email;
            $emailData['emailData']['idtrack'] = $data->idtrack;
            $emailData['emailData']['id_invoice'] = $data->id;
            $emailData['emailData']['invoice'] = $data->reference;
            $email->emailData = $emailData;
            $fromEmail = ['email' => env('MAIL_USER'), 'name' => appData()->name];
            $send = $this->sendMail($email, $fromEmail);
            if ($send === false) {
                return response()->json([
                    'error' => trans('invoice.error')
                ]);
            }
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('invoice.success'),
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                //dd($e->getMessage());
                return response()->json([
                    'error' => trans('invoice.error')
                ]);
            }
        }
    }

    protected function sendMail($data, $from)
    {
        $to = $data->toEmail;
        $content = $data->emailData;
        $sent = Mail::send('mails.sendcustomer', $content, function ($message) use ($from, $to) {
            $message->to($to);
            $message->from($from["email"], $from["name"]);
            $message->subject(trans('invoice.notification'));
        });
        return true;
    }
}
