<?php

namespace App\Http\Controllers\Adm\AboutUs;

use App\Http\Controllers\Controller;

use App\Http\Requests\Adm\AboutUs\UpdateAboutUsRequest;
use App\Model\AboutUs\AboutUs;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class AboutUsController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/images/aboutus/';
        $this->url = appData()->urldata . '/images/aboutus/';
    }

    public function index()
    {
        $data = AboutUs::find(1);
        if(!$data){
            $data = (object)[];
            $data->id = 1;
            $data->text = '';
        }
        return view('adm.aboutus.edit', compact('data'));
    }

    public function update(UpdateAboutUsRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = AboutUs::find(1);
            if(!$data){
                $data = new AboutUs();
            }
            $data->id = 1;
            $data->text = $request->get('ta_text');

            if ($request->file('fl_image')) {
                $image = md5(time()) . '.png';
                $imgProduct = $request->file('fl_image');
                $dirImg = $this->dir . $image;
                Image::make($imgProduct, ['width' => 600, 'height' => 600, 'greyscale' => false])->save($dirImg);
                if (File::exists($this->dir . $data->image)) {
                    File::delete($this->dir . $data->image);
                }
                $data->image = $image;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getFile(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

}
