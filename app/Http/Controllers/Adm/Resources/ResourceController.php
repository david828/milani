<?php

namespace App\Http\Controllers\Adm\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Resources\DestroyResourceRequest;
use App\Http\Requests\Adm\Resources\StoreResourceRequest;
use App\Http\Requests\Adm\Resources\UpdateResourceRequest;
use App\Model\Resources\Resources;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ResourceController extends Controller
{

    protected $dir;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/resources/';
    }

    public function index(Request $request)
    {
        $list = Resources::listData($request->get('search'));
        $list->search = $request->get('search');
        return view('adm.resources.index', compact('list'));
    }

    public function show($id)
    {
        $data = Resources::where('slug', $id)->firstOrFail();
        $file = $this->dir . $data->file;
        $extension = pathinfo($data->file, PATHINFO_EXTENSION);
        if (!File::exists($file)) {
            return new Response('forbidden', 403);
        } else {
            return response()->download($file, $data->name . '.' . $extension);
        }
    }

    public function create()
    {
        $data = (object)[];
        return view('adm.resources.create', compact('data'));
    }

    public function store(StoreResourceRequest $request)
    {
        DB::beginTransaction();
        try {
            if ($request->file('fl_resource')) {
                $file = $request->file('fl_resource')->getClientOriginalName();
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $filename = Str::slug($request->get('tx_name')) . '.' . $extension;
                if ($request->file('fl_resource')->move($this->dir, $filename)) {
                    $data = new Resources();
                    $data->name = $request->get('tx_name');
                    $data->slug = Str::slug($request->get('tx_name'));
                    $data->file = $filename;
                    $data->save();
                    DB::commit();
                    $message = ['message' => trans('app.store_success')];
                } else {
                    $message = ['error' => trans('app.upload_problem')];
                }
            } else {
                $message = ['error' => trans('app.upload_problem')];
            }
            if ($request->ajax()) {
                return response()->json($message);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.store_problem')
                ]);
            }
        }
    }

    public function edit($id)
    {
        $data = Resources::findOrFail($id);
        return view('adm.resources.edit', compact('data'));
    }

    public function update($id, UpdateResourceRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Resources::findOrFail($id);
            $file = false;
            if ($request->file('fl_resource')) {
                $file = $request->file('fl_resource')->getClientOriginalName();
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $filename = Str::slug($request->get('tx_name')) . '.' . $extension;
                if ($request->file('fl_resource')->move($this->dir, $filename)) {
                    $beforefile = $this->dir . $data->file;
                    if (File::exists($beforefile)) {
                        File::delete($beforefile);
                    }
                    $file = true;
                }
            }
            $data->name = $request->get('tx_name');
            $data->slug = Str::slug($request->get('tx_name'));
            if ($file === true) {
                $data->file = $filename;
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function destroy($id, DestroyResourceRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = Resources::findOrFail($id);
            $data->delete();
            $beforefile = $this->dir . $data->file;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

}
