<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Jobs\AdminMailJob;
use App\Jobs\CustomerMailJob;
use App\Jobs\StockMailJob;
use App\Model\Invoice\Invoice;
use App\Model\Invoice\InvoiceDetails;
use App\Model\Products\ProductColor;
use App\Model\Products\Products;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{

    public function index(Request $request)
    {
        /*
         * merchant_id
         * state_pol                Indica el estado de la transacción en el sistema.
         * response_code_pol        El código de respuesta de PayU.
         * reference_sale           Es la referencia de la venta o pedido. Deber ser único por cada transacción que se envía al sistema.
         * reference_pol            La referencia o número de la transacción generado en Pay
         * transaction_date         La fecha en que se realizó la transacción.
         * email_buyer              Campo que contiene el correo electrónico del comprador para notificarle el resultado de la transacción por correo electrónico. Se recomienda hacer una validación si se toma este dato en un formulario.
         * account_number_ach       Identificador de la transacción.
         * account_type_ach         Identificador de la transacción.
         * sign                     Es la firma digital creada para cada uno de las transacciones.
         * date                     Fecha de la operación.
         */
        try {
            $invoice = Invoice::where('reference', $request->get('reference_sale'))->firstOrFail();
            if ($invoice->id_status != 2) {
                switch ($request->get('state_pol')) {
                    case 4:
                        $invoice->id_status = 2;
                        break;
                    case 5:
                        $invoice->id_status = 4;
                        break;
                    case 6:
                        $invoice->id_status = 3;
                        break;
                }

                Carbon::setLocale('es');
                $invoice->transaction_date = $request->get('transaction_date');
                $invoice->operation_date = Carbon::createFromFormat('Y.m.d H:i:s', $request->get('date'))->toDateTimeString();
                $invoice->reference_pol = $request->get('reference_pol');
                $invoice->account_number_ach = $request->get('account_number_ach');
                $invoice->save();
                $details = InvoiceDetails::getDetails($invoice->id);

                if ($invoice->id_status == 2) {
                    $minStock = [];
                    foreach ($details as $item) {
                        $product = Products::find($item->id_product);
                        $product->stock = $product->stock - $item->quantity;
                        $product->best_seller = $product->best_seller + 1;
                        $product->save();
                        if(!is_null($item->id_color)) {
                            $pc = ProductColor::where('id_color', $item->id_color)
                                ->where('id_product', $item->id_product)
                                ->first();
                            $pc->stock = $pc->stock - $item->quantity;
                            $pc->save();
                            if($pc->stock <= $pc->min_stock) {
                                $st = (object)[];
                                $st->id = $product->id;
                                $st->name = $product->name;
                                $st->stock = $pc->stock;
                                $st->min_stock = $pc->min_stock;
                                array_push($minStock,$st);
                            }
                        } else {
                            if ($product->stock <= $product->min_stock) {
                                $st2 = (object)[];
                                $st2->id = $product->id;
                                $st2->name = $product->name;
                                $st2->stock = $product->stock;
                                $st2->min_stock = $product->min_stock;
                                array_push($minStock,$st2);
                            }
                        }
                    }
                    if(count($minStock) > 0) {
                        $this->dispatch(new StockMailJob($invoice, $minStock));
                    }
                }
                $this->dispatch(new CustomerMailJob($invoice, $request));
                $this->dispatch(new AdminMailJob($invoice, $request));
            }
        } catch (Exception $e) {
            $file_request_txt = fopen("logPagos/errorPago_".$request->get('reference_sale').".txt", "w");
            fwrite($file_request_txt, $e->getMessage(), $e->getLine());
            fclose($file_request_txt);
        }
    }
}
