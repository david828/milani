<?php

namespace App\Http\Controllers\Site\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function callCss()
    {
        $color = appData()->color;
        return response()->view('site.css.callCss', compact('color'))->header('Content-type','text/css');
    }
}
