<?php

namespace App\Http\Controllers\Site\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\User\DestroyAddressUserRequest;
use App\Http\Requests\Site\User\DestroyUserRequest;
use App\Http\Requests\Site\User\UpdateUserAddressRequest;
use App\Http\Requests\Site\User\UpdateUserPasswordRequest;
use App\Http\Requests\Site\User\UpdateUserRequest;
use App\Http\Requests\Site\User\UploadImageRequest;
use App\Model\Articles\Article;
use App\Model\Invoice\Invoice;
use App\Model\Invoice\InvoiceAddress;
use App\Model\Location\Cities;
use App\Model\Location\Countries;
use App\Model\Location\States;
use App\Model\User\DocumentType;
use App\Model\User\Person;
use App\Model\User\UserAddress;
use App\User;
use Carbon\Carbon;
use Exception;
use Folklore\Image\Facades\Image;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function myAccount()
    {
        $id = Auth::id();
        $data = User::getData($id);
        $data->doctype = DocumentType::selection();
        $data->person = Person::selection();
        return view('site.account.index', compact('data'));
    }


    public function update(UpdateUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = User::findOrFail(Auth::id());
            $data->id_documenttype = $request->get('sl_documenttype');
            $data->id_person = $request->get('sl_person');
            $data->name = $request->get('tx_name');
            $data->document = $request->get('tx_document');
            $data->email = $request->get('tx_email');
            $data->phone = $request->get('tx_phone');
            $data->save();

            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success')
                ]);
            }

        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function image()
    {
        return view('site.account.image');
    }

    public function updateImage(UploadImageRequest $request)
    {
        DB::beginTransaction();
        try {
            $dir = appData()->dirdata . '/images/users/';
            $url = '/images/users/';
            $dataUrl = $request->get('hn_file');
            if ($dataUrl != '' && !is_null($dataUrl)) {
                $data = User::findOrFail(Auth::id());
                $beforefile = $dir . $data->image;
                list($meta, $content) = explode(',', $dataUrl);
                $content = base64_decode($content);
                $image = md5(time()) . '.png';
                $imgArticle = $dir . $image;
                $dirImg = $dir . $image;
                file_put_contents($dirImg, $content);
                Image::make($imgArticle, ['width' => 120, 'height' => 120, 'greyscale' => false])->save($dirImg);
                $data->image = $image;
                $data->save();
                if (File::exists($beforefile)) {
                    File::delete($beforefile);
                }
                $response = $url.$data->image;
            }
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'image' => $response
                ]);
            }
        } catch
        (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function changePassword()
    {
        return view('site.account.password');
    }

    public function updatePassword(UpdateUserPasswordRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = User::findOrFail(Auth::id());
            $data->password = Hash::make($request->get('tx_password'));
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.password_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.password_problem')
                ]);
            }
        }
    }

    public function userAddress()
    {
        $id = Auth::id();
        $data = User::getData($id);
        //dd($data->address[0]);
        $data->countryList = Countries::selection();
        $data->stateList = [];
        $data->cityList = [];
        foreach ($data->address as $item)
        {
            $item->location = UserAddress::getStateData($item->id);
            $item->stateList = States::selectionByCountry($item->location->countryId);
            $item->cityList = Cities::selectionByState($item->location->stateId);

        }
        $data->doctype = DocumentType::selection();
        return view('site.account.address', compact('data'));
    }

    public function changeAddress($id,UpdateUserAddressRequest $request)
    {
        DB::beginTransaction();
        try {
            if ($id === 'NN' ) {
                $data = new UserAddress();
            } else {
                $data = UserAddress::findOrFail($id);
            }
            $data->id_user = Auth::id();
            $data->title = $request->get('tx_title');
            $data->id_documenttype = $request->get('sl_documenttype');
            $data->name = $request->get('tx_name');
            $data->document = $request->get('tx_document');
            $data->phone = $request->get('tx_phone');
            $data->address = $request->get('tx_address');
            $data->id_city = $request->get('sl_city');
            if ($request->get('tx_zipcode')) {
                $data->post_code = $request->get('tx_zipcode');
            }
            if ($request->get('ta_additional')) {
                $data->additional = $request->get('ta_additional');
            }
            $data->save();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.edit_success'),
                    'id' => $id
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.edit_problem')
                ]);
            }
        }
    }

    public function deleteAddress($id, DestroyAddressUserRequest $request)
    {
        DB::beginTransaction();
        try {
            UserAddress::where('id', $id)->delete();
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => 'ok'
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function deleteUser(DestroyUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $id = Auth::id();
            $data = User::findOrFail($id);
            if(!(Hash::check($request->get('password'), $data->password) && $request->get('email') === Auth::user()->email)) {
                if ($request->ajax()) {
                    return response()->json([
                        'error' => trans('account.error_info')
                    ]);
                }
            }
            UserAddress::where('id_user', $id)->delete();
            Article::where('id_user',$id)->update(['id_user'=>null]);
            Invoice::where('id_user',$id)->update(['id_user'=>null]);
            $beforefile = 'images/users/' . $data->image;
            if (File::exists($beforefile)) {
                File::delete($beforefile);
            }
            $data->delete();
            DB::commit();
            Auth::logout();
            if ($request->ajax()) {
                return response()->json([
                    'message' => trans('app.delete_success')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('app.delete_problem')
                ]);
            }
        }
    }

    public function userOrders()
    {
        $user = Auth::id();
        $data = Invoice::userInvoice($user);
        foreach ($data as $item) {
            $item->date = Carbon::parse($item->invoice_date)->format('d/m/Y');
        }
        return view('site.account.orders', compact('data'));
    }

    public function viewOrder($id)
    {
        $user = Auth::id();
        $data = Invoice::with('status')->with('details')->where('id_user', $user)->findOrFail($id);
        $address = InvoiceAddress::where('id_invoice', $id)->first();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        return view('site.account.orderdata', compact('data', 'address'));
    }

    public function printOrder($id)
    {
        $user = Auth::id();
        $data = Invoice::with('status')->with('details')->where('id_user', $user)->findOrFail($id);
        $address = InvoiceAddress::where('id_invoice', $id)->first();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        return view('site.payment.print', compact('data', 'address'));
    }
}
