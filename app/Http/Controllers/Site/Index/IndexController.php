<?php

namespace App\Http\Controllers\Site\Index;

use App\Http\Controllers\Controller;
use App\Model\Header\Header;
use App\Model\Header\HeaderImages;
use App\Model\Productdes\Productdes;
use App\Model\Products\FeatureProducts;
use App\Model\Products\Products;
use Illuminate\Support\Facades\File;

class IndexController extends Controller
{
    protected $dir;
    protected $url;

    public function __construct()
    {
        $this->dir = appData()->dirdata;
        $this->url = appData()->urldata;
    }
    public function index()
    {
        $home = (object)[];
        $home->best = Products::getSwitch('best_seller');
        $home->new = Products::getSwitch('new');
        $home->featured = Products::getSwitch('featured');
        $home->productdes = Productdes::home();
        $home->header = Header::home();
        $home->imgheader = HeaderImages::home();
        $home->prodtitle = FeatureProducts::first();
        return view('site.index.index', compact('home'));
    }
}
