<?php

namespace App\Http\Controllers\Site\Cart;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Cart\AddCartRequest;
use App\Http\Requests\Site\Cart\ChangeCartRequest;
use App\Http\Requests\Site\Cart\DelCartRequest;
use App\Model\MilaniCart\MilaniCart;
use App\Model\Products\ProductColor;
use App\Model\Products\Products;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;

class CartController extends Controller
{
    public function index()
    {
        $ck = Cookie::get('milaniCart');
        $data = [];
        if (!is_null($ck)) {
            $ex = MilaniCart::find($ck);
            if (!is_null($ex)) {
                $data = json_decode($ex->cart);
            }
        }
        $total = 0;
        if (!is_Array($data)) {
            $data = (array) $data;
        }
        if (count($data) > 0) {
            foreach ($data as $item) {
                $total += $item->total;
            }
        }
        $totalV = '$' . number_format($total, 0, ',', '.');
        return view('site.cart.index', compact('data', 'total', 'totalV'));
    }

    public function addCart(AddCartRequest $request)
    {
        $id = $request->get('id_p');
        $cart = $request->get('cart');
        $id_color = $request->get('id_c');
        $name_color = $request->get('name_c');
        $stock = $request->get('stock');
        $ck = Cookie::get('milaniCart');
        $pos = $id . '_' . $id_color;
        if (is_null($ck)) { //Todavía no está creada la cookie
            $ar = (object) [];
            $data = $this->llenarData($id, $cart, $id_color, $name_color);
            $ar->$pos = $data;
        } else {
            $dataC = MilaniCart::find($ck);
            if (!is_null($dataC)) {
                $ar = json_decode($dataC->cart);
                $sw = 0;
                foreach ($ar as $item) {
                    if ($item->id == $id && $item->idColor == $id_color) {
                        $item->cant += $cart;
                        $item->total = $item->cant * $item->price;
                        $item->priceV = '$' . number_format($item->price, 0, ',', '.');
                        $item->totalV = '$' . number_format($item->total, 0, ',', '.');
                        $sw = 1;
                        if ($item->cant > $stock) {
                            return response()->json([
                                'error' => trans('site.full_cart'),
                            ]);
                        }
                        break;
                    }
                }
                if ($sw === 0) {
                    $data = $this->llenarData($id, $cart, $id_color, $name_color);
                    $ar->$pos = $data;
                    //array_push($ar,$data);
                }
            } else {
                $ar = (object) [];
                $data = $this->llenarData($id, $cart, $id_color, $name_color);
                $ar->$pos = $data;
            }
        }
        $total = 0;
        $tP = 0;
        foreach ($ar as $item) {
            $total += $item->total;
            $tP += 1;
        }
        $tM = '$' . number_format($total, 0, ',', '.');
        //dd($tP);
        $minutos = 4320;
        $dataC = null;
        if (!is_null($ck)) {
            $dataC = MilaniCart::find($ck);
        }
        if (is_null($dataC)) {
            $dataC = new MilaniCart();
        }
        $dataC->cart = json_encode($ar);
        $dataC->save();
        $idCart = $dataC->id;
        $new_ck = cookie('milaniCart', $idCart, $minutos);
        return response()->json([
            'success' => trans('site.success_cart'),
            'products' => $ar,
            'tProducts' => $tP,
            'tMoney' => $tM,
        ])->cookie($new_ck);
    }

    public function changeCart(ChangeCartRequest $request)
    {
        $total = 0;
        $color = '';
        $tP = 0;
        $prod = $request->get('id_p');
        $ch = $request->get('change');
        if ($request->get('id_c')) {
            $color = $request->get('id_c');
        }
        if ($color === '') {
            $data = Products::find($prod);
            $stock = $data->stock;
        } else {
            $data = ProductColor::where('id_color', $color)->where('id_product', $prod)->first();
            $stock = $data->stock;
        }
        $ck = Cookie::get('milaniCart');
        $ar = (object) [];
        if (!is_null($ck)) {
            $data = MilaniCart::find($ck);
            if (!is_null($data)) {
                $ar = json_decode($data->cart);
            }
        }
        if ($color == '') {
            foreach ($ar as $item) {
                if ($item->id == $prod) {
                    if ($ch === 'm') {
                        $item->cant -= 1;
                    } else {
                        $item->cant += 1;
                        if ($item->cant > $stock) {
                            return response()->json([
                                'error' => trans('site.full_cart'),
                            ]);
                        }
                    }
                    $item->total = $item->cant * $item->price;
                    $item->priceV = '$' . number_format($item->price, 0, ',', '.');
                    $item->totalV = '$' . number_format($item->total, 0, ',', '.');
                    $nCant = $item->cant;
                    $totalV = $item->totalV;
                }
                $total += $item->total;
                $tP += 1;
            }
        } else {
            foreach ($ar as $key => $item) {
                if ($item->id == $prod && $item->idColor == $color) {
                    if ($ch === 'm') {
                        $item->cant -= 1;
                    } else {
                        $item->cant += 1;
                        if ($item->cant > $stock) {
                            return response()->json([
                                'error' => trans('site.full_cart'),
                            ]);
                        }
                    }
                    $nCant = $item->cant;
                    $item->total = $item->cant * $item->price;
                    $item->priceV = '$' . number_format($item->price, 0, ',', '.');
                    $item->totalV = '$' . number_format($item->total, 0, ',', '.');
                    $totalV = $item->totalV;
                }
                $total += $item->total;
                $tP += 1;
            }
        }
        $tM = '$' . number_format($total, 0, ',', '.');
        $minutos = 4320;
        $dataC = null;
        if (!is_null($ck)) {
            $dataC = MilaniCart::find($ck);
        }
        if (is_null($dataC)) {
            $dataC = new MilaniCart();
        }
        $dataC->cart = json_encode($ar);
        $dataC->save();
        $idCart = $dataC->id;
        $new_ck = cookie('milaniCart', $idCart, $minutos);
        return response()->json([
            'success' => trans('site.success_change'),
            'products' => $ar,
            'tProducts' => $tP,
            'tArt' => $nCant,
            'total' => $total,
            'sTMoney' => $totalV,
            'tMoney' => $tM,
        ])->cookie($new_ck);
    }

    public function delCart(DelCartRequest $request)
    {
        $total = 0;
        $color = '';
        $k = '';
        $prod = $request->get('id_p2');
        if ($request->get('id_c2')) {
            $color = $request->get('id_c2');
        }
        $ck = Cookie::get('milaniCart');
        $ar = (object) [];
        if (!is_null($ck)) {
            $data = MilaniCart::find($ck);
            if (!is_null($data)) {
                $ar = json_decode($data->cart);
            }
        }
        //dd($ar);
        $sT = 0;
        if ($color === '') {
            foreach ($ar as $key => $item) {
                if ($item->id == $prod) {
                    $k = $key;
                    break;
                }
            }
        } else {
            foreach ($ar as $key => $item) {
                if ($item->id == $prod && $item->idColor == $color) {
                    $k = $key;
                    break;
                }
            }
        }
        if ($k === '') {
            return response()->json([
                'error' => trans('site.error_cart'),
            ]);
        }
        if (is_array($ar)) {
            unset($ar[$k]);
        } else {
            unset($ar->$k);
        }
        $tP = 0;
        foreach ($ar as $key => $item) {
            $total += $item->total;
            $tP += 1;
        }
        $ar = (object) $ar;
        $tM = '$' . number_format($total, 0, ',', '.');
        $minutos = 4320;
        $dataC = null;
        if (!is_null($ck)) {
            $dataC = MilaniCart::find($ck);
        }
        if (is_null($dataC)) {
            $dataC = new MilaniCart();
        }
        $dataC->cart = json_encode($ar);
        $dataC->save();
        $idCart = $dataC->id;
        $new_ck = cookie('milaniCart', $idCart, $minutos);
        return response()->json([
            'success' => trans('site.success_change'),
            'products' => $ar,
            'tProducts' => $tP,
            'tMoney' => $tM,
            'total' => $total,
        ])->cookie($new_ck);
    }

    public function llenarData($id, $cart, $id_color, $name_color)
    {
        $pd = Products::getProduct($id, $id_color);
        $data = (object) [];
        $data->id = $id;
        $data->name = $pd->name;
        $data->price = $pd->price;
        $data->money = $pd->money;
        $data->image = $pd->image;
        $data->slug = $pd->slug;
        $data->idColor = $id_color;
        $data->nameColor = $name_color;
        $data->cant = $cart;
        $data->total = $cart * $pd->price;
        $data->priceV = '$' . number_format($data->price, 0, ',', '.');
        $data->totalV = '$' . number_format($data->total, 0, ',', '.');
        return $data;
    }

    public function confirm(Request $request)
    {
        $ck = Cookie::get('milaniCart');
        $ar = (object) [];
        if (!is_null($ck)) {
            $data = MilaniCart::find($ck);
            if (!is_null($data)) {
                $ar = json_decode($data->cart);
            }
        }
        foreach ($ar as $item) {
            $data = Products::revStock($item->id, $item->idColor);
            if ($data->stock < $item->cant) {
                if (is_null($item->nameColor)) {
                    $name = $item->name;
                } else {
                    $name = $item->name . ' - ' . $item->nameColor;
                }
                return response()->json([
                    'error' => 'no_stock',
                    'msg' => 'El producto ' . $name . ' no tiene suficiente stock en este momento. Por favor disminuir el número de unidades.',
                ]);
            }
        }
        return response()->json([
            'success' => 'ok',
        ]);
    }
}
