<?php

namespace App\Http\Controllers\Site\Cart;

use App\Http\Controllers\Adm\Zone\ZoneController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Cart\CouponRequest;
use App\Model\Coupons\CouponHistory;
use App\Model\Coupons\Coupons;
use App\Model\Invoice\Invoice;
use App\Model\Invoice\InvoiceAddress;
use App\Model\Invoice\InvoiceDetails;
use App\Model\Location\Cities;
use App\Model\Location\Countries;
use App\Model\Location\States;
use App\Model\MilaniCart\MilaniCart;
use App\Model\User\DocumentType;
use App\Model\User\Person;
use App\Model\User\UserAddress;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;


class CheckoutController extends Controller
{
    public function index()
    {
        $ck = Cookie::get('milaniCart');
        $data = [];
        if(!is_null($ck)) {
            $ex =  MilaniCart::find($ck);
            if(!is_null($ex)) {
                $data = json_decode($ex->cart);
            }
        }
        $subtotal = 0;
        if (!is_Array($data)) {
            $data = (array) $data;
        }
        if(count($data) > 0) {
            foreach($data as $item)
            {
                $subtotal+=$item->total;
            }
        } else {
            $data  = [];
        }
        $tax = 0;
        if(appData()->tax_value != 0) {
            $subtotal = $subtotal / ((appData()->tax_value / 100) + 1);
            $tax = $subtotal * (appData()->tax_value / 100);
        }
        $data2 = (object)[];
        $data2->useraddress = null;
        $data2->location = null;
        $data2->stateList = [];
        $data2->cityList = [];
        $ship = 0;
        $shipV = 0;
        $data2->countryList = Countries::selection();
        if(Auth::check()) {
            $data2->address = User::getData(Auth::id())->address;
            foreach ($data2->address as $item)
            {
                $item->location = UserAddress::getStateData($item->id);
                $item->stateList = States::selectionByCountry($item->location->countryId);
                $item->cityList = Cities::selectionByState($item->location->stateId);
                $zone = ZoneController::getShip($item->id_city);
                $ship = $zone->price;
                $shipV = $ship;
                $free = $zone->ship_free;
                if($subtotal > appData()->min_value && appData()->min_value != 0 && $free == 1) {
                    $shipV = 0;
                }
                $item->ship = $ship;
                $item->shipV = '$'.number_format($ship,0,',','.');

            }
        }
        $user = $this->getUser();
        if (!is_null($user->id_city) && $user->id_city !== 0) {
            $user->location = UserAddress::getList($user->id_city);
            $user->cityId = $user->location->cityId;
            $user->countryId = $user->location->countryId;
            $user->stateId = $user->location->stateId;
            $user->stateList = States::selectionByCountry($user->location->countryId);
            $user->cityList = Cities::selectionByState($user->location->stateId);
            $zone = ZoneController::getShip($user->id_city);
            $ship = $zone->price;
            $shipV = $ship;
            $free = $zone->ship_free;
            if($subtotal > appData()->min_value && appData()->min_value != 0 && $free == 1) {
                $shipV = 0;
            }
        } else {
            $user->stateList = [];
            $user->cityList = [];
            $user->countryId = null;
            $user->cityId = null;
            $user->stateId = null;
        }
        $data2->subtotal = $subtotal;
        $data2->ship = $ship;
        $data2->total = $subtotal + $tax + $ship;
        if($shipV == 0) {
            $data2->total = $subtotal + $tax;
        }
        $data2->taxV = '$'.number_format($tax,0,',','.');
        $data2->subtotalV = '$'.number_format($subtotal,0,',','.');
        $data2->shipV = '$'.number_format($shipV,0,',','.');
        $data2->totalV = '$'.number_format($data2->total,0,',','.');
        return view('site.cart.checkout', compact('data', 'data2', 'user'));
    }

    public function apCoupon(CouponRequest $request)
    {
        $cp = $request->get('hn_coupon');
        $subt = $request->get('hn_subtotal');
        $disc = 0;
        $tax = 0;
        if($request->get('hn_city2')) {
            $city = $request->get('hn_city2');
            $zone = ZoneController::getShip($city);
            $ship = $zone->price;
            $shipV = $ship;
            $free = $zone->ship_free;
            if ($subt > appData()->min_value && appData()->min_value != 0 && $free != 0) {
                $shipV = 0;
            }
        } else {
            $ship = $request->get('hn_ship');
            $shipV = $ship;
        }
        if(appData()->tax_value != 0) {
            $tax = $subt * (appData()->tax_value / 100);
        }

        $total = $subt - $disc + $tax + $ship;
        if($shipV == 0) {
            $total = $subt - $disc + $tax;
        }
        $subt2 = $subt - $disc;
        $subt2V = '$' . number_format($subt2, 0, ',', '.');
        $taxV = '$' . number_format($tax, 0, ',', '.');
        $shipV = '$' . number_format($shipV, 0, ',', '.');
        $totalV = '$' . number_format($total, 0, ',', '.');
        if($cp === '|') {
            return response()->json([
                'success' => 'ok',
                'disc' => 0,
                'discV' => '$ 0',
                'taxV' => $taxV,
                'subtV' => $subt2V,
                'ship' => $ship,
                'shipV' => $shipV,
                'totalV' => $totalV
            ]);
        }
        $data = Coupons::select('*')
            ->where('coupon',$cp)
            ->where('status','1')
            ->first();
        if (is_null($data)) {
            return response()->json([
                'error' => trans('site.no_coupon'),
                'disc' => 0,
                'discV' => '$ 0',
                'subtV' => $subt2V,
                'taxV' => $taxV,
                'ship' => $ship,
                'shipV' => $shipV,
                'totalV' => $totalV
            ]);
        }
        $sw = 0;
        switch ($data->expirationtype){
            case '1':
                $exp = $data->expiration;
                $rExp = strtotime($exp);
                $today = strtotime(date("d-m-Y H:i:00",time()));;
                if($today <= $rExp) {
                    $sw = 1;
                }
                break;

            case '2':
                if($data->expiration > 0) {
                    $sw = 1;
                }
                break;
            case '3':
                $sw = 1;
        };
        if ($sw === 0) {
            return response()->json([
                'error' => trans('site.no_coupon'),
                'disc' => 0,
                'discV' => '$ 0',
                'subtV' => $subt2V,
                'taxV' => $taxV,
                'ship' => $ship,
                'shipV' => $shipV,
                'totalV' => $totalV
            ]);
        } else {
            if($data->products == 0) {
                $disc = $subt * ($data->discount / 100);
            } else {
                $products = explode('|',$data->products);
                $ck = Cookie::get('milaniCart');
                $dataCk = [];
                if(!is_null($ck)) {
                    $ex =  MilaniCart::find($ck);
                    if(!is_null($ex)) {
                        $dataCk = json_decode($ex->cart);
                    }
                }
                if(count($dataCk) > 0) {
                    foreach($dataCk as $item)
                    {
                        if(in_array($item->id,$products))
                        {
                            if(appData()->tax_value != 0) {
                                $it = $item->total / ((appData()->tax_value / 100) + 1);
                            } else {
                                $it = $item->total;
                            }
                            $disc+=$it * ($data->discount / 100);
                        }
                    }
                }
            }
            if(appData()->tax_value != 0) {
                $tax = ($subt-$disc) * (appData()->tax_value / 100);
            }

            $subt2 = $subt - $disc;

            if($request->get('hn_city2')) {
                $city = $request->get('hn_city2');
                $zone = ZoneController::getShip($city);
                $ship = $zone->price;
                $shipV = $ship;
                $free = $zone->ship_free;
                if ($subt2 > appData()->min_value && appData()->min_value != 0 && $free != 0) {
                    $shipV = 0;
                }
            } else {
                $ship = $request->get('hn_ship');
                $shipV = $ship;
            }
            $discV = '$' . number_format($disc, 0, ',', '.');
            $total = $subt - $disc + $tax + $ship;
            if($shipV == 0) {
                $total = $subt - $disc + $tax;
            }
            $subt2V = '$' . number_format($subt2, 0, ',', '.');
            $taxV = '$' . number_format($tax, 0, ',', '.');
            $shipV = '$' . number_format($shipV, 0, ',', '.');
            $totalV = '$' . number_format($total, 0, ',', '.');
            return response()->json([
                'success' => trans('site.success_coupon'),
                'disc' => $disc,
                'discV' => $discV,
                'subtV' => $subt2V,
                'taxV' => $taxV,
                'ship' => $ship,
                'shipV' => $shipV,
                'totalV' => $totalV
            ]);
        }
    }

    public function getUser()
    {
        if(Auth::check()) {
            $data = userData();
            /*$location = UserAddress::find(Auth::id());
            if(!is_null($location)) {
                $data->nameS = $location->name;
                $data->documentS = $location->document;
                $data->doctypeS = $location->id_documenttype;
                $data->address = $location->address;
                $data->post_code = $location->post_code;
                $data->additional = $location->additional;
                $data->phoneS = $location->phone;
                $data->emailS = $location->email;
            } else {
                $data->nameS = $data->name;
                $data->documentS = $data->document;
                $data->doctypeS = $data->id_documenttype;
                $data->address = null;
                $data->post_code = null;
                $data->additional = null;
                $data->phoneS = $data->phone;
                $data->emailS = $data->email;
            }*/
        } else {
            $data = (object)[];
            $data->name = null;
            $data->document = null;
            $data->id_person = null;
            $data->id_documenttype = null;
            $data->phone = null;
            $data->cellphone = null;
            $data->email = null;
            $data->address_invoice= null;
            $data->post_code= null;
            $data->additional= null;
            $data->id_city= null;
        }
        return $data;
    }

    public function calculateShip(Request $request)
    {
        $city = $request->get('hn_city2');
        $subt = $request->get('hn_subtotal');
        $disc = $request->get('hn_discount');
        $zone = ZoneController::getShip($city);
        $ship = $zone->price;
        $shipV = $ship;
        $free = $zone->ship_free;
        if(($subt - $disc) > appData()->min_value && appData()->min_value != 0 && $free != 0) {
            $shipV = 0;
        }
        $tax = 0;
        if(appData()->tax_value != 0) {
            $tax = ($subt - $disc) * (appData()->tax_value / 100);
        }
        $total = $subt - $disc + $tax + $ship;
        if($shipV == 0) {
            $total = $subt - $disc + $tax;
        }
        $taxV = '$' . number_format($tax, 0, ',', '.');
        $shipV = '$' . number_format($shipV, 0, ',', '.');
        $discV = '$' . number_format($disc, 0, ',', '.');
        $totalV = '$' . number_format($total, 0, ',', '.');
        return response()->json([
            'success' => 'ok',
            'disc' => $disc,
            'discV' => $discV,
            'taxV' => $taxV,
            'ship' => $ship,
            'shipV' => $shipV,
            'totalV' => $totalV
        ]);
    }

    public function checkOut(Request $request)
    {
        DB::beginTransaction();
        try {

            $ck = Cookie::get('milaniCart');
            $cookie = [];
            if(!is_null($ck)) {
                $ex =  MilaniCart::find($ck);
                if(!is_null($ex)) {
                    $cookie = json_decode($ex->cart);
                }
            }
            $data = (object)[];
            $data->priceSubtotal = 0;
            if(count($cookie) > 0) {
                foreach($cookie as $item)
                {
                    $data->priceSubtotal+=$item->total;
                }
            } else {
                if ($request->ajax()) {
                    return response()->json([
                        'error' => trans('site.pay_problem')
                    ]);
                }
            }
            if(appData()->tax_value != 0) {
                $data->priceSubtotal = round($data->priceSubtotal / ((appData()->tax_value / 100) + 1),2);
            }
            $data->discountP = 0;
            $couponH = null;
            if($request->get('billing-coupon')) {
                $cp = $request->get('billing-coupon');
                $coupon = Coupons::select('*')
                    ->where('coupon',$cp)
                    ->where('status','1')
                    ->first();
                if (!is_null($coupon)) {
                    switch ($coupon->expirationtype){
                        case '1':
                            $exp = $coupon->expiration;
                            $rExp = strtotime($exp);
                            $today = $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));;
                            if($today <= $rExp) {
                                $ar = $this->calcDiscount($cookie, $coupon);
                                $data->discountP = $ar[0];
                                $cookie = $ar[1];
                                $couponH = $cp;
                            }
                            break;

                        case '2':
                            if($coupon->expiration > 0) {
                                $ar = $this->calcDiscount($cookie, $coupon);
                                $data->discountP = $ar[0];
                                $cookie = $ar[1];
                                $aux = Coupons::where('coupon',$cp)->first();
                                $aux->expiration = $aux->expiration - 1;
                                $aux->save();
                                $couponH = $cp;
                            }
                            break;
                        case '3':
                            $ar = $this->calcDiscount($cookie, $coupon);
                            $data->discountP = $ar[0];
                            $cookie = $ar[1];
                            $couponH = $cp;
                    };
                }
            }
            //dd($data->discountP);
            $data->discount = $data->discountP / 100;
            $data->value = $data->priceSubtotal;
            if($data->discountP != 0) {
                $data->value = round($data->priceSubtotal - ($data->priceSubtotal * ($data->discountP / 100)),2);
            }
            $data->tax = 0;
            $data->taxP = appData()->tax_value;
            if($data->taxP != 0) {
                $data->tax = round($data->value * ($data->taxP / 100),2);
            }

            $zone = ZoneController::getShip($request->get('sl_cityS'));
            $data->ship = $zone->price;
            $free = $zone->ship_free;
            if($data->value > appData()->min_value && appData()->min_value != 0 && $free == 1) {
                $data->ship = 0;
            }
            if(Auth::check()) {
                $user = Auth::id();
                if($request->get('ck_refresh')) {
                    if ($request->get('hn_id_address') !== 'IG') {
                        $address = UserAddress::find($request->get('hn_id_address'));
                        if (is_null($address)) {
                            $address = new UserAddress();
                            $address->id_user = $user;
                        }
                        $address->id_documenttype = $request->get('sl_doctypeS');
                        $address->document = $request->get('tx_documentS');
                        $address->name = $request->get('tx_nameS');
                        $address->title = $request->get('tx_titleS');
                        $address->phone = $request->get('tx_phoneS');
                        $address->email = $request->get('tx_emailS');
                        $address->address = $request->get('tx_addressS');
                        $address->id_city = $request->get('sl_cityS');
                        if ($request->get('tx_code_postal')) {
                            $address->post_code = $request->get('tx_code_postal');
                        }
                        if ($request->get('ta_additional')) {
                            $address->additional = $request->get('ta_additional');
                        }
                        $address->save();
                    }

                    $info = User::find($user);
                    $info->id_person = $request->get('sl_person');
                    $info->id_documenttype = $request->get('sl_doctype');
                    $info->document = $request->get('tx_document');
                    $info->name = $request->get('tx_name');
                    $info->email = $request->get('tx_email');
                    $info->address_invoice = $request->get('tx_address');
                    $info->id_city = $request->get('sl_city');
                    if($request->get('tx_phone')) {
                        $info->phone = $request->get('tx_phone');
                    } else {
                        $info->phone = null;

                    }
                    if($request->get('tx_cellphone')) {
                        $info->cellphone = $request->get('tx_cellphone');
                    } else {
                        $info->cellphone = null;

                    }
                    $info->save();
                }
            } else {
                $user = null;
            }
            $per = Person::find($request->get('sl_person'));
            $p = $per->name;
            $dT = DocumentType::find($request->get('sl_doctype'));
            $d = $dT->lite;

            //Para el envío
            $country = Countries::findOrFail($request->get('sl_countryS'));
            $data->countryIso = $country->iso_code;
            $city = Cities::findOrFail($request->get('sl_cityS'));
            $data->cityName = $city->name;
            $data->state = States::findOrFail($request->get('sl_stateS'))->name;
            $data->addressS = $request->get('tx_address');
            $data->phone = $request->get('tx_phone');
            $data->email = $request->get('tx_email');
            $data->name = $request->get('tx_name');
            $data->priceTotal = $data->value + $data->tax + $data->ship;

            //Para la facturacion
            $countryInv = Countries::findOrFail($request->get('sl_country'))->name;
            $stateInv = States::findOrFail($request->get('sl_state'))->name;
            $cityInv = Cities::findOrFail($request->get('sl_city'))->name;
            $add = $request->get('tx_address').', '.$cityInv.', '.$stateInv.', '.$countryInv;
            //dd($add . ' - ' . $request->get('tx_addressS').', '.$city->name.', '.$data->state.', '.$country->name);
            $invoice = new Invoice();
            $invoice->id_user = $user;
            $invoice->id_status = 1;
            $invoice->politics = 1;
            $invoice->customer_person = $p;
            $invoice->customer_documenttype = $d;
            $invoice->customer_document = $request->get('tx_document');
            $invoice->customer_name = $request->get('tx_name');
            $invoice->customer_address = $add;
            $invoice->customer_phone = $request->get('tx_phone');
            $invoice->customer_cellphone = $request->get('tx_cellphone');
            $invoice->customer_email = $request->get('tx_email');
            $invoice->invoice_date = date('Y-m-d');
            $invoice->subtotal = $data->priceSubtotal;
            $invoice->discount = $data->discountP;
            $invoice->tax = $data->taxP;
            $invoice->shipping = $data->ship;
            $invoice->total = $data->priceTotal;
            $invoice->coupon = $couponH;
            $invoice->save();

            $invoice->reference = 'MC'.$invoice->id.time();
            $invoice->save();

            if(!is_null($couponH)) {
                $history = new CouponHistory();
                $history->coupon = $couponH;
                $history->invoice = $invoice->reference;
                $history->percent = $data->discountP;
                $history->value = $data->priceSubtotal;
                $history->save();
            }

            $invaddress = new InvoiceAddress();
            $invaddress->id_invoice = $invoice->id;
            $invaddress->documenttype = DocumentType::find($request->get('sl_doctypeS'))->lite;
            $invaddress->document = $request->get('tx_documentS');
            $invaddress->name = $request->get('tx_nameS');
            $invaddress->email = $request->get('tx_emailS');
            $invaddress->phone = $request->get('tx_phoneS');
            $invaddress->address = $request->get('tx_addressS').', '.$city->name.', '.$data->state.', '.$country->name;
            $invaddress->post_code = $request->get('tx_code_postal');
            $invaddress->additional = $request->get('ta_additional');
            $invaddress->save();

            foreach ($cookie as $item) {
                $details = new InvoiceDetails();
                $details->id_invoice = $invoice->id;
                $details->id_product = $item->id;
                if(!is_null($item->idColor)) {
                    $details->id_color = $item->idColor;
                }
                $desc = $item->name;
                if(!is_null($item->nameColor)) {
                    $desc.= ' - '.$item->nameColor;
                }
                if(property_exists($item,'pD')) {
                    $details->discount = $item->pD;
                }
                $details->description = $desc;
                $details->quantity = $item->cant;
                $details->total = $item->total;
                $details->save();
            }
            $data->invoice = $invoice->reference;
            if ($data->taxP == 0) {
                $data->value = 0;
            }
            $sha = payuData()->apikey . '~' . payuData()->merchantid . '~' . $invoice->reference . '~' . $data->priceTotal . '~' . appData()->currency[0]->name;
            $sha = md5($sha);
            $data->signature = $sha;
            $view = (String)view('site.payu.form', compact('data'));
            //dd($view);
            DB::commit();
            if ($request->ajax()) {
                return response()->json([
                    'message' => 'ok',
                    'form' => $view
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(),$e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('site.pay_problem')
                ]);
            }
        }

    }

    public function calcDiscount($ck, $cp)
    {
        if($cp->products == 0){
            if(count($ck) > 0) {
                foreach ($ck as $item) {
                    $item->pD = $cp->discount;
                }
            }
            return [$cp->discount,$ck];
        } else {
            $products = explode('|',$cp->products);
            $tDisc = 0;
            $subtWI = 0;
            if(count($ck) > 0) {
                foreach($ck as $item)
                {
                    if(in_array($item->id,$products))
                    {
                        if(appData()->tax_value != 0) {
                            $tDisc+= ($item->total / ((appData()->tax_value / 100) + 1)) * ($cp->discount/100);

                        } else {
                            $tDisc+= ($item->total)*($cp->discount / 100);
                        }
                        $item->pD = $cp->discount;
                    }
                    if(appData()->tax_value != 0) {
                        $subtWI+= $item->total / ((appData()->tax_value / 100) + 1);

                    } else {
                        $subtWI+= $item->total;
                    }
                }
            }
            return [round((($tDisc*100)/$subtWI),4),$ck];
        }
    }
}
