<?php

namespace App\Http\Controllers\Site\Resources;

use App\Http\Controllers\Controller;
use App\Http\Requests\Adm\Resources\DestroyResourceRequest;
use App\Http\Requests\Adm\Resources\StoreResourceRequest;
use App\Http\Requests\Adm\Resources\UpdateResourceRequest;
use App\Model\Resources\Resources;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ResourceController extends Controller
{

    protected $dir;

    public function __construct()
    {
        $this->dir = appData()->dirdata . '/resources/';
    }

    public function view($id)
    {
        $data = Resources::where('slug', $id)->firstOrFail();
        $file = $this->dir . $data->file;
        if (!File::exists($file)) {
            return new Response('forbidden', 403);
        } else {
            return redirect('/resources/'.$data->file);
        }
    }
}
