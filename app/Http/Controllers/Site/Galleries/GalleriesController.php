<?php

namespace App\Http\Controllers\Site\Galleries;

use App\Http\Controllers\Controller;
use App\Model\Galleries\Galleries;
use App\Model\Galleries\CategoriesGal;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests;

class GalleriesController extends Controller
{

    public function index(Request $request)
    {
        $dir = appData()->dirdata . '/images/galleries/';
        $list = (object)[];
        $list->all = Galleries::index($request->get('search'), $request->get('cat'));
        $list->search = $request->get('search');
        $list->cat = $request->get('cat');
        $cat = CategoriesGal::all();
        return view('site.galleries.index', compact('list', 'cat'));
    }

    public function view($slug)
    {
        $data = Galleries::with('category')
            ->with('images')
            ->where('slug', $slug)
            ->firstOrFail();
        $cat = CategoriesGal::all();
        return view('site.galleries.view', compact('data', 'cat'));
    }
}
