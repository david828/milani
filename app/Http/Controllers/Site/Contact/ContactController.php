<?php

namespace App\Http\Controllers\Site\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Contact\SendContactRequest;
use App\Http\Requests\Site\Contact\SendLeaveRequest;
use App\Model\Contact\Contact;
use App\Model\Contact\Receivers;
use App\Model\Products\Colors;
use App\Model\Products\Products;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Expr\Cast\Object_;


class ContactController extends Controller
{
    public function index()
    {
        $contact = Contact::find(1);
        return view('site.contact.index', compact('contact'));
    }

    public function sendMessage(SendContactRequest $request)
    {
        $data = Contact::findOrFail(1);
        $data->emails = Receivers::showData(1);
        $emailData = ['emailData' => []];
        $emailData['emailData']['author'] = $request->get('nameCon');
        $emailData['emailData']['email'] = $request->get('emailCon');
        $emailData['emailData']['type'] = $request->get('type');
        $emailData['emailData']['message'] = $request->get('message');
        $emailData['emailData']['title'] = $data->name;
        $data->emailData = $emailData;
        $toEmail = [];
        foreach ($data->emails as $item) {
            $toEmail[] = $item->email;
        }
        $data->toEmail = $toEmail;
        $fromEmail = ['email' => env('MAIL_USER'), 'name' => appData()->name];
        $send = $this->sendMail($data, $fromEmail);
        $message = 'ok';
        if ($send === false) {
            $message = trans('contact.no_sent');
        }
        return response()->json([
                'message' => $message
            ]);
    }

    protected function sendMail($data, $from)
    {
        $to = $data->toEmail;
        $content = $data->emailData;
        $sent = Mail::send('site.mail.contact', $content, function ($message) use ($from, $to) {
            $message->to($to);
            $message->from($from["email"], $from["name"]);
            $message->subject(trans('contact.new_message'));
        });
        return true;
    }

    public function leave(SendLeaveRequest $request)
    {
        $data = (Object)[];
        $data->toEmail = appData()->email;
        $product = Products::findOrFail($request->get('hn_productLD'))->name;
        if(!is_null($request->get('hn_colorLD'))) {
            $color = Colors::findOrFail($request->get('hn_colorLD'))->name;
            $product .= ' - ' . $color;

        }
        $emailData = ['emailData' => []];
        $emailData['emailData']['product'] = $product;
        $emailData['emailData']['email'] = $request->get('emailLD');
        $data->emailData = $emailData;
        $fromEmail = ['email' => env('MAIL_USER'), 'name' => appData()->name];
        $send = $this->sendMailLeave($data, $fromEmail);
        if ($send === false) {
            return response()->json([
                'error' => trans('contact.no_sent')
            ]);
        } else {
            return response()->json([
                'success' => trans('products.congrats')
            ]);
        }
    }

    protected function sendMailLeave($data, $from)
    {
        $to = $data->toEmail;
        $content = $data->emailData;
        $sent = Mail::send('site.mail.leave', $content, function ($message) use ($from, $to) {
            $message->to($to);
            $message->from($from["email"], $from["name"]);
            $message->subject(trans('products.interes'));
        });
        return true;
    }

}
