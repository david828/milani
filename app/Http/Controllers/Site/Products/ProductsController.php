<?php

namespace App\Http\Controllers\Site\Products;

use App\Http\Controllers\Controller;
use App\Model\Products\Categories;
use App\Model\Products\Finish;
use App\Model\Products\ProductColor;
use App\Model\Products\Products;
use App\Model\Products\Subcategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $opc = [];
        $opc['search'] =$request->get('search');
        $opc['cat'] = $request->get('cat');
        $opc['sub'] = $request->get('sub');
        $opc['fin'] = $request->get('fin');
        $opc['col'] = $request->get('col');
        $opc['ord'] = $request->get('o');
        $list = Products::index($opc['search'],$opc['cat'],$opc['sub'],$opc['fin'],$opc['col'],$opc['ord']);
        $list->cat = Categories::selectionIndex();
        $list->fin = Finish::selectionIndex();
        $list->col = ProductColor::index();
        $metatags = getAppSeo()->metatags;
        $metadescription = getAppSeo()->metadescription;
        if(!is_null($request->get('sub'))) {
            $sub = Subcategories::where('slug', $request->get('sub'))->first();
            $metatags .= ',' . $sub->metatags;
            $metadescription .= '. ' . $sub->metadescription;
        }
        if(!is_null($request->get('cat'))) {
            $cat = Categories::where('slug', $request->get('cat'))->first();
            if(!is_null($cat)) {
                $metatags .= ',' . $cat->metatags;
                $metadescription .= '. ' . $cat->metadescription;
            }
        }
        return view('site.products.index', compact('list', 'opc','metatags','metadescription'));
    }

    public function view($slug)
    {
        $data = Products::viewP($slug);
        $cont = Products::where('slug',$slug)->first();
        $cont->save();
        //dd($data);
        if(is_null($data)) {
            return redirect()->route('products');
        } else {
            return view('site.products.view', compact('data'));
        }
    }
}
