<?php

namespace App\Http\Controllers\Site\AboutUs;

use App\Http\Controllers\Controller;
use App\Model\AboutUs\AboutUs;
use Illuminate\Support\Facades\File;


class AboutUsController extends Controller
{
    public function index()
    {
        $home = AboutUs::find(1);
        if (File::exists(appData()->dirdata . '/images/aboutus/' . $home->image)) {
            $home->image = '/images/aboutus/' . $home->image;
        } else {
            $home->image = '/assets/images/aboutus.png';
        }
        return view('site.aboutus.index', compact('home'));
    }
}
