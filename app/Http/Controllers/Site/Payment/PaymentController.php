<?php

namespace App\Http\Controllers\Site\Payment;

use App\Http\Controllers\Controller;
use App\Model\Invoice\Invoice;
use App\Model\Invoice\InvoiceAddress;
use App\Model\MilaniCart\MilaniCart;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;

class PaymentController extends Controller
{
    public function paymentResponse(Request $request)
    {
        $data = Invoice::with('status')->with('details')->where('reference', $request->get('referenceCode'))->firstOrFail();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        $ck = Cookie::get('milaniCart');
        if(!is_null($ck)) {
            MilaniCart::find($ck)->delete();
        }
        setcookie ("milaniCart");
        unset ($_COOKIE ["milaniCart"]);
        return view('site.payment.response', compact('data'));
    }

    public function printInvoice($id)
    {
        $data = Invoice::with('status')->with('details')->findOrFail($id);
        $address = InvoiceAddress::where('id_invoice', $id)->first();
        $data->discount_value = $data->subtotal * ($data->discount / 100);
        $data->subtotal2 = $data->subtotal - $data->discount_value;
        $data->tax_value = ($data->subtotal - $data->discount_value) * ($data->tax / 100);
        $data->date = Carbon::parse($data->invoice_date)->format('d/m/Y');
        return view('site.payment.print', compact('data', 'address'));
    }
}
