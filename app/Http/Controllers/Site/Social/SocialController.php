<?php

namespace App\Http\Controllers\Site\Social;

use App\Http\Controllers\Controller;
use App\Model\Social\AppSocial;
use Illuminate\Http\Request;

use App\Http\Requests;

class SocialController extends Controller
{
    public static function getSocial()
    {
        return AppSocial::getSocial(1);
    }
}
