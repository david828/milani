<?php

namespace App\Http\Controllers\Site\Articles;

use App\Http\Controllers\Controller;
use App\Model\Articles\Article;
use App\Model\Articles\ArticleComments;
use App\Model\Categories\Categories;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ArticleController extends Controller
{

    public function index(Request $request)
    {
        Carbon::setLocale('es');
        $dir = appData()->dirdata . '/images/articles/';
        $list = (object)[];
        $list->all = Article::index($request->get('search'), $request->get('cat'), $request->get('et'));
        $list->search = $request->get('search');
        $list->cat = $request->get('cat');
        $list->et = $request->get('et');
        foreach ($list->all as $item) {
            $item->date = $item->created_at->diffForHumans();
            if(is_null($item->image)) {
                $item->image = 'article.png';
            }
        }
        $cat = Categories::all();
        return view('site.articles.index', compact('list', 'cat'));
    }

    public function view($slug)
    {
        Carbon::setLocale('es');
        $data = Article::with('category')
            ->with('comments')
            ->with('images')
            ->with('user')
            ->where('slug', $slug)
            ->firstOrFail();
        $data->date = $data->created_at->diffForHumans();
        $data->day = substr($data->created_at,8,2);
        $data->month = substr($data->created_at,5,2);
        $data->month = getMonth($data->month);
        $dir = appData()->dirdata . '/images/articles/';
        if (!is_null($data->image)) {
            $beforefile = $dir . $data->image;
            if (File::exists($beforefile)) {
                $data->image = appData()->urldata . '/images/articles/' . $data->image;
            } else {
                $data->image = null;
            }
        }
        foreach ($data->comments as $com) {
            $com->date = $com->created_at->diffForHumans();
        }
        $data->tags = explode(',', $data->tags);
        $cat = Categories::all();
        return view('site.articles.view', compact('data', 'cat'));
    }

    public function comment(Request $request)
    {
        DB::beginTransaction();
        try {
            $comment = new ArticleComments();
            $comment->id_article = $request->get('id_article');
            $comment->name = $request->get('name');
            $comment->email = $request->get('email');
            $comment->comment = $request->get('comment');
            $comment->save();
            DB::commit();
            return response()->json(['message' => 'ok']);
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage());
            return response()->json(['error' => 'error']);
        }
    }
}
