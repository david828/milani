<?php

namespace App\Http\Controllers\Site\Distribution;

use App\Http\Controllers\Controller;
use App\Model\Distribution\Distribution;
use Illuminate\Http\Request;


class DistributionController extends Controller
{
    public function index(Request $request)
    {
        $list = Distribution::siteData($request->get('search'));
        $list->search = $request->get('search');
        foreach ($list as $item)
        {
            if (!is_null($item->logo)) {
                $item->image = '/images/distribution/' .  $item->logo;
            } else {
                $item->image = '/assets/images/distribution.png';
            }
            $location = '';
            if(!is_null($item->address)) {
                $location = $item->address;
            }
            if(!is_null($item->city)) {
                if($location !== '') {
                    $location .= ', ' . $item->city->name;
                } else {
                    $location = $item->city->name;
                }
            }
            if(!is_null($item->state)) {
                if($location !== '') {
                    $location .= ', ' . $item->state->name;
                } else {
                    $location = $item->state->name;
                }
            }
            /*if(!is_null($item->country)) {
                if($location !== '') {
                    $location .= ', ' . $item->country->name;
                } else {
                    $location = $item->country->name;
                }
            }*/
            if($location !== '') {
                $location = trans('distribution.address').': '.$location;
            }
            $item->location = $location;
            $phones = '';
            if(!is_null($item->phone)) {
                $phones = $item->phone;
            }
            if(!is_null($item->cellphone)) {
                if($phones !== '') {
                    $phones .= ' - ' . $item->cellphone;
                } else {
                    $phones = $item->cellphone;
                }
            }
            if($phones !== '') {
                $phones = trans('distribution.phones').': '.$phones;
            }
            $item->phones = $phones;
            if(!is_null($item->email)) {
                $item->email = trans('distribution.email').': '.$item->email;
            } else {
                $item->email = '';
            }
        }
        return view('site.distribution.index', compact('list'));
    }
}
