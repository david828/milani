<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Auth\LoginRequest;
use App\Http\Requests\Site\Auth\RecoveryRequest;
use App\Http\Requests\Site\Auth\RegisterRequest;
use App\User;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function doLogin(LoginRequest $request)
    {
        $remember = (Input::has('remember')) ? true : false;
        $message = [];
        if (!Auth::attempt($request->only('email', 'password'), $remember)) {
            $message = ['error' => trans('login.login_problems')];
        } else {
            $name = Auth::user()->name;
            $message = ['message' => 'ok', 'name' => $name];
        }
        if ($request->ajax()) {
            return response()->json($message);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function registerData(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            if (!Auth::check()) {
                $user = new User();
                $user->id_person = $request->get('person');
                $user->id_documenttype = $request->get('doctype');
                $user->name = $request->get('name');
                $user->document = $request->get('document');
                $user->email = $request->get('email');
                $user->phone = $request->get('phone');
                $user->password = Hash::make($request->get('password'));
                $user->role = 'U';
                $user->politics = 1;
                $user->save();
            }
            DB::commit();
            if (Auth::attempt($request->only('email', 'password'))) {
                $name = Auth::user()->name;
                $message = ['message' => 'ok', 'name' => $name];
            } else {
                $message = ['error' => trans('login.login_problems')];
            }
            if ($request->ajax()) {
                return response()->json($message);
            }
        } catch (Exception $e) {
            DB::rollback();
            //dd($e->getMessage(), $e->getLine());
            if ($request->ajax()) {
                return response()->json([
                    'error' => trans('site.register_problem')
                ]);
            }
        }
    }

    public function loginRecovery(RecoveryRequest $request)
    {
        try {
            $data = User::dataMail($request->get('email'));
            $user = User::findOrFail($data->id);
            $pass = randPass();
            $user->password = Hash::make($pass);
            $user->save();

            $emailData = [
                'name' => $user->name,
                'document' => $user->document,
                'password' => $pass,
                'mail' => $user->email,
                'fromMail' => env('MAIL_USERNAME'),
                'fromName' => appData()->name
            ];
            $send = $this->sendMailRecover($emailData);
            $message = trans('email.mail_sent');
            if ($send === false) {
                $message = str_replace('[company]', appData()->name, trans('email.mail_not_sent'));
            }
            return response()->json([
                'message' => $message
            ]);
        } catch (Exception $e) {
            $message = str_replace('[company]', appData()->name, trans('email.mail_not_sent'));
            return response()->json([
                'message' => $message
            ]);
        }
    }


    public function sendMailRecover($data)
    {
        $sent = Mail::send('mails.recoverpass', $data, function ($message) use ($data) {
            $message->to($data['mail'], $data['name']);
            $message->from($data['fromMail'], $data['fromName']);
            $message->subject(trans('email.mail_subject_recover'));
        });
        return true;
    }
}