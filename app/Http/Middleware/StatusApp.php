<?php

namespace App\Http\Middleware;

use Closure;

class StatusApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $app = appData()->id_status;
        if ($app == 1) {
            return response()->view('site.index.index');
        }

        return $next($request);
    }
}
