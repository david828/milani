<?php

namespace App\Http\Requests\Site\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_title' => 'required',
            'tx_address' => 'required',
            'tx_name' => 'required',
            'tx_document' => 'required',
            'sl_documenttype' => 'required',
            'tx_phone' => 'required',
            'sl_country' => 'required|integer|exists:countries,id',
            'sl_state' => 'required|integer|exists:states,id',
            'sl_city' => 'required|integer|exists:cities,id',
            'tx_zipcode' => '',
            'hn_id' => 'required',
        ];
    }
}
