<?php

namespace App\Http\Requests\Site\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::id();
        return [
            'sl_person' => 'required',
            'sl_documenttype' => 'required',
            'tx_document' => 'required',
            'tx_name' => 'required',
            'tx_email' => 'required|unique:users,email,' . $id . ',id',
            'tx_phone' => 'required',
        ];
    }
}
