<?php

namespace App\Http\Requests\Site\Cart;

use Illuminate\Foundation\Http\FormRequest;

class AddCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_p' => 'required|exists:products,id',
            'cart' => 'required',
        ];

        if($this->request->get('h_color') == 'S') {
            $rules['id_c'] = 'required|exists:colors,id';
        }
        return $rules;
    }
}
