<?php

namespace App\Http\Requests\Site\Cart;

use Illuminate\Foundation\Http\FormRequest;

class DelCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_p2' => 'required|exists:products,id',
        ];

        if($this->request->get('id_c2')) {
            $rules['id_c2'] = 'required|exists:colors,id';
        }
        return $rules;
    }
}
