<?php

namespace App\Http\Requests\Adm\Modals;

use Illuminate\Foundation\Http\FormRequest;

class StatusModalsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hn_status' => 'required|exists:modals,id'
        ];
    }
}
