<?php

namespace App\Http\Requests\Adm\Modals;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UpdateModalsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ida = $this->modal;
        $rule = [
            'tx_name' => 'required|unique:modals,name,'. $ida . ',id',
            'tx_url' => 'required',
        ];
        if($this->request->get('fl_image')){
            $rule['fl_image'] = 'required|image|mimes:jpg, gif';
        }

        return $rule;
    }
}
