<?php

namespace App\Http\Requests\Adm\Contact;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->contact;
        $rule = [
            'tx_name' => 'required|unique:contact,name,' . $id . ',id',
            'sl_maptype' => 'required',
        ];

        if ($this->request->get('sl_maptype') == 'F'){
            $rule['ta_frame'] = 'required';
        }

        if ($this->request->get('sl_maptype') == 'C'){
            $rule['tx_latitude'] = 'required';
            $rule['tx_longitude'] = 'required';
            $rule['tx_api_google'] = 'required';
        }

        if ($this->request->get('tx_email')) {
            foreach ($this->request->get('tx_email') as $key => $val) {
                $rule['tx_email.' . $key] = 'required|email';
            }
        }

        if ($this->request->get('tx_remail')) {
            foreach ($this->request->get('tx_remail') as $key => $val) {
                $rule['tx_remail.' . $key] = 'required|email';
            }
        }

        if ($this->request->get('hn_mail')) {
            foreach ($this->request->get('hn_mail') as $key => $val) {
                $rule['hn_mail.' . $key] = 'integer|exists:receivers,id';
            }
        }

        return $rule;
    }
}
