<?php

namespace App\Http\Requests\Adm\AboutUs;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAboutUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'ta_text' => 'required',
        ];
        if($this->request->get('fl_image')){
            $rule['fl_image'] = 'required|image|mimes:jpg, gif';
        }
        return $rule;
    }
}
