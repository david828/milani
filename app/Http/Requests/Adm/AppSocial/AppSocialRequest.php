<?php

namespace App\Http\Requests\Adm\AppSocial;

use Illuminate\Foundation\Http\FormRequest;

class AppSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [];
        if ($this->request->get('sl_social')) {
            foreach ($this->request->get('sl_social') as $key => $val) {
                $rule['sl_social.' . $key] = 'required|integer|exists:social,id';
            }
        }

        if ($this->request->get('tx_link')) {
            foreach ($this->request->get('tx_link') as $key => $val) {
                $rule['tx_link.' . $key] = 'required';
            }
        }

        return $rule;
    }
}
