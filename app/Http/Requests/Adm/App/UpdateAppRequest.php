<?php

namespace App\Http\Requests\Adm\App;

use Illuminate\Foundation\Http\FormRequest;


class UpdateAppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rule = [
            'tx_color' => 'required',
            'tx_name' => 'required',
            'tx_email' => 'required|email',
            'tx_phone' => 'required',
            'tx_mobile' => 'required',
            'tx_address' => 'required',
            'tx_website' => 'required',
            'tx_name_boucher' => 'required',
            'tx_email_boucher' => 'required|email',
            'tx_phone_boucher' => 'required',
            'tx_address_boucher' => 'required',
            'tx_website_boucher' => 'required',
            'tx_copyright' => 'required',
            'tx_dir' => 'required',
            'tx_url' => 'required|url',
            'sl_currency' => 'required|integer|exists:currency,id',
            'tx_tax' => 'required|numeric|between:0,100',
            'tx_min_value' => 'required'
        ];

        if ($this->request->get('fn_file')) {
            $rule['fn_file'] = 'image|mimes:gif,jpeg,png|dimensions:min_width=200,min_height=200,ratio=1/1';
        }
        return $rule;
    }
}
