<?php

namespace App\Http\Requests\Adm\App;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAppSeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_name' => 'required',
            'tx_charset' => 'required',
            'ta_metadesc' => 'required',
            'tx_tags' => 'required',
            'tx_author' => 'required',
            'tx_robots' => 'required',
            'tx_language' => 'required'
        ];
    }
}
