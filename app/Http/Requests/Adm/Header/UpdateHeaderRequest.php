<?php

namespace App\Http\Requests\Adm\Header;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHeaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'hn_pos_1' => 'required',
            'hn_pos_2' => 'required',
            'tx_client' => 'required',
            'tx_user' => 'required',
            'tx_accesstoken' => 'required',
            'tx_numphotos' => 'required'
        ];

        if($this->request->get('fl_image')){
            $rule['fl_image'] = 'required|image|mimes:jpg, gif';
        }

        return $rule;
    }
}
