<?php

namespace App\Http\Requests\Adm\Header;

use Illuminate\Foundation\Http\FormRequest;

class DestroyHeaderImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hd_trash' => 'required|exists:headerimages,id'
        ];
    }
}
