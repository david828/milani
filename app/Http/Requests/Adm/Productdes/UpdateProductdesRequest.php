<?php

namespace App\Http\Requests\Adm\Productdes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductdesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'tx_subtitle' => 'required',
            'ta_text' => 'required',
            'tx_link' => '',
            'tx_title' => 'required',
        ];
        if($this->request->get('fl_image'))
        {
            $rule['fl_image'] = 'required|image|mimes:gif,jpeg,png';
        }
        return $rule;

    }
}
