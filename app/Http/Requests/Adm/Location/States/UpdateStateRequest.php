<?php

namespace App\Http\Requests\Adm\Location\States;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UpdateStateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->country;
        return [
            'tx_name' => 'required',
            'tx_code' => 'required|integer',
            'sl_country' => 'required|integer|exists:countries,id',
        ];
    }
}
