<?php

namespace App\Http\Requests\Adm\Location\Cities;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UpdateCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->city;
        return [
            'tx_name' => 'required',
            'sl_country' => 'required|integer|exists:countries,id',
            'sl_state' => 'required|integer|exists:states,id',
            'sl_zone' => 'required|integer|exists:zone,id',
        ];
    }
}
