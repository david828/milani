<?php

namespace App\Http\Requests\Adm\Location\Cities;

use Illuminate\Foundation\Http\FormRequest;

class CreateCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
            'tx_name' => 'required',
            'sl_country' => 'required|integer|exists:countries,id',
            'sl_state' => 'required|integer|exists:states,id',
            'sl_zone' => 'required|integer|exists:zone,id'
        ];

    }
}
