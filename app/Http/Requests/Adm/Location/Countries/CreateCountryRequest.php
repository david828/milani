<?php

namespace App\Http\Requests\Adm\Location\Countries;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CreateCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_name' => 'required|unique:countries,name',
            'tx_code' => 'required|integer|unique:countries,code',
            'tx_isocode' => 'required|alpha|unique:countries,iso_code'
        ];

    }
}
