<?php

namespace App\Http\Requests\Adm\Categories;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->category;
        return [
            'tx_category' => 'required|unique:article_categories,name,' . $id . ',id'
        ];
    }
}
