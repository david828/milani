<?php

namespace App\Http\Requests\Adm\Payu;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UpdatePayuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_accountid' => 'required|integer',
            'tx_merchantid' => 'required|integer',
            'tx_apilogin' => 'required|',
            'tx_apikey' => 'required|',
            'tx_publickey' => 'required|',
            'tx_requesturl' => 'required|url',
            'tx_responseurl' => 'required|url',
            'tx_confirmationurl' => 'required|url',
            'sl_testing' => 'required|integer|between:0,1',
        ];
    }
}
