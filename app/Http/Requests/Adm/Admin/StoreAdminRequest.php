<?php

namespace App\Http\Requests\Adm\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sl_role' => 'required',
            'sl_person' => 'required',
            'sl_documenttype' => 'required',
            'tx_document' => 'required',
            'tx_name' => 'required',
            'tx_password' => 'required|min:6',
            'tx_email' => 'required|email|unique:users,email',
            'tx_phone' => 'required|integer'
        ];
    }
}
