<?php

namespace App\Http\Requests\Adm\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->admin;
        $rule = [
            'sl_role' => 'required',
            'sl_person' => 'required',
            'sl_documenttype' => 'required',
            'tx_document' => 'required',
            'tx_name' => 'required',
            'tx_email' => 'required|unique:users,email,' . $id . ',id',
            'tx_phone' => 'required|integer'
        ];

        if ($this->request->get('tx_password')) {
            $rule['tx_password'] = 'min:6';
        }
        return $rule;

    }
}
