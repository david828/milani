<?php

namespace App\Http\Requests\Adm\Galleries;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UpdateGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ida = $this->gallery;
        $rule = [
            'tx_name' => 'required|unique:galleries,name,'. $ida . ',id',
            'sl_categories' => 'required|integer|exists:gallery_categories,id',
            'ta_description' => 'required',
        ];
        return $rule;
    }
}
