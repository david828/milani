<?php

namespace App\Http\Requests\Adm\Galleries;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryGalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_category' => 'required|unique:gallery_categories,name'
        ];
    }
}
