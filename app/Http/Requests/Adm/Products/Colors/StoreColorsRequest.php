<?php

namespace App\Http\Requests\Adm\Products\Colors;

use Illuminate\Foundation\Http\FormRequest;

class StoreColorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'tx_name' => 'required|unique:colors,name',
            'fl_image' => 'required|image|mimes:jpg,png,gif',
        ];
    }
}
