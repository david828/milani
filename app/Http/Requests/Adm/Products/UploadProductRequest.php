<?php

namespace App\Http\Requests\Adm\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UploadProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        /*Validator::extend('imageable', function ($attribute, $value, $params, $validator) {
            $value = str_replace('data:image/png;base64,', '', $value);
            $image = base64_decode($value);
            $f = finfo_open();
            $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);
            return $result == 'image/png';
        });*/

        return [
            'hn_file' => 'required|image|mimes:jpg,png,gif|dimensions:min_width=540,max_width=1200,ratio:270/293|max:2048',
            'switch' => 'required'
        ];
    }

    public function messages(){
        return [
            'hn_file.dimensions'=>'Las dimensiones de la imagen deben estar entre un 540 y 1200 pixeles de ancho.',
            'hn_file.max'=>'El tamaño máximo de la imagen es 2 MB',

        ];
    }
}
