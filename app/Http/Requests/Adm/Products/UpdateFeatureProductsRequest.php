<?php

namespace App\Http\Requests\Adm\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFeatureProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'sl_product1' => 'required',
            'sl_product3' => 'required',
            'sl_product2' => 'required',
        ];

        return $rule;
    }
}
