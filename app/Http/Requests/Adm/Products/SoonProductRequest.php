<?php

namespace App\Http\Requests\Adm\Products;

use Illuminate\Foundation\Http\FormRequest;

class SoonProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hn_soon' => 'required|exists:products,id'
        ];
    }
}
