<?php

namespace App\Http\Requests\Adm\Products\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateCategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->get('tx_id');
        return [
            'tx_name' => 'required|unique:categories,name,'. $id . ',id',
        ];

    }
}
