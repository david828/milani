<?php

namespace App\Http\Requests\Adm\Products\Categories;

use Illuminate\Foundation\Http\FormRequest;

class OrderCategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hn_cat' => 'required|exists:categories,id',
            'hn_order' => 'required|integer'
        ];
    }
}
