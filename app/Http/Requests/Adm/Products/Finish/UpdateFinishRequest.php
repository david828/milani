<?php

namespace App\Http\Requests\Adm\Products\Finish;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFinishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->finish;
        return [
            'tx_name' => 'required|unique:finish,name,'. $id . ',id',
        ];

    }
}
