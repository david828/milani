<?php

namespace App\Http\Requests\Adm\Products;

use Illuminate\Foundation\Http\FormRequest;

class DestroyProductImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'switch' => 'required'
        ];

        if($this->request->get('switch') == 'S') {
            $rule['hd_trash'] = 'required|exists:productcolorimage,id';
        } else {
            $rule['hd_trash'] = 'required|exists:productgallery,id';

        }
        return $rule;
    }
}
