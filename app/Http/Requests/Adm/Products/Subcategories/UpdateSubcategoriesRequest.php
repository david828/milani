<?php

namespace App\Http\Requests\Adm\Products\Subcategories;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubcategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->subcategory;
        return [
            'sl_category' => 'required|exists:categories,id',
            'tx_name' => 'required|unique:subcategories,name,'. $id . ',id',
        ];

    }
}
