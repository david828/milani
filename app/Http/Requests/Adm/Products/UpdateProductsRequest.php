<?php

namespace App\Http\Requests\Adm\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->product;
        $rule = [
            'sl_category' => 'required|exists:categories,id',
            'tx_name' => 'required|unique:products,name,'. $id . ',id',
            'ta_description' => 'required',
            'ta_review' => '',
            'tx_price' => 'required',
        ];

        if($this->request->get('sl_subcategory')){
            $rule['sl_subcategory'] = 'required|exists:subcategories,id';
        }

        if($this->request->get('sl_finish')) {
            $rule['sl_finish'] = 'required|exists:finish,id';

        }

        if(is_array($this->request->get('sl_color'))) {
            foreach ($this->request->get('sl_color') as $key => $val) {
                $rules['sl_color.' . $key] = 'required|integer|exists:colors,id';
            }
        }

        if(is_array($this->request->get('tx_stock'))) {
            foreach ($this->request->get('tx_stock') as $key => $val) {
                $rules['tx_stock.' . $key] = 'required|integer';
                $rules['tx_minstock.' . $key] = 'required|integer';
                $rules['tx_reference.' . $key] = 'required';
                $rules['tx_SKU.' . $key] = 'required';
            }
        } else {
            $rules['tx_stock'] = 'required|integer';
            $rules['tx_minstock'] = 'required|integer';
            $rules['tx_reference'] = 'required';
            $rules['tx_SKU'] = 'required';
        }

        return $rule;
    }
}
