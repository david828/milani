<?php

namespace App\Http\Requests\Adm\Coupon;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CreateCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_coupon' => 'required|alpha_num|min:4|unique:coupons,coupon',
            'tx_discount' => 'required|integer|min:1,max:100',
            'tx_expiration' => 'required',
            'sl_type' => 'required|integer|min:1,max:3',
        ];
    }
}
