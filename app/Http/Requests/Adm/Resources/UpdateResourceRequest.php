<?php

namespace App\Http\Requests\Adm\Resources;

use Illuminate\Foundation\Http\FormRequest;

class UpdateResourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->resource;
        return [
            'fl_resource' => 'mimes:doc,docx,pdf,ppt,pptx,xlsx,xls,txt,csv,ods,odt,zip,rar|max:10240',
            'tx_name' => 'required|unique:resources,name,' . $id . ',id'
        ];
    }
}
