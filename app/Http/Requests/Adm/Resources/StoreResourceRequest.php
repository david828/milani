<?php

namespace App\Http\Requests\Adm\Resources;

use Illuminate\Foundation\Http\FormRequest;

class StoreResourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fl_resource' => 'required|mimes:doc,docx,pdf,ppt,pptx,xlsx,xls,txt,csv,ods,odt,zip,rar|max:40960',
            'tx_name' => 'required|unique:resources,name'
        ];
    }
}
