<?php

namespace App\Http\Requests\Adm\Distribution;

use Illuminate\Foundation\Http\FormRequest;

class StoreDistributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'tx_name' => 'required',
            'tx_cellphone' => '',
            'tx_address' => '',
            'tx_phone' => '',
            'tx_email' => '',
            'fl_image' => 'image|mimes:gif,jpeg,png',
        ];
        if($this->request->get('sl_country')) {
            $rule['sl_country'] = 'integer|exists:countries,id';
        }
        if($this->request->get('sl_state')) {
            $rule['sl_state'] = 'integer|exists:states,id';
        }
        if($this->request->get('sl_city')) {
            $rule['sl_city'] = 'integer|exists:cities,id';
        }
        return $rule;
    }
}
