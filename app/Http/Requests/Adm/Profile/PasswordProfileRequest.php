<?php

namespace App\Http\Requests\Adm\Profile;

use Illuminate\Foundation\Http\FormRequest;

class PasswordProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tx_password' => 'required|min:8',
            'tx_rpassword' => 'required|min:8|same:tx_password'
        ];
    }
}
