<?php

namespace App\Model\Footer;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{

    protected $table = 'footer';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title_1',
        'text_1',
        'text_2'
    ];
}