<?php

namespace App\Model\Categories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Categories extends Model
{
    use softdeletes;
    protected $table = 'article_categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
    protected $dates = ['deleted_at'];


    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->orWhere('name', 'LIKE', "%$data%");
        }
    }

    protected function listData($request)
    {
        return Categories::DataSearch($request)->paginate(20);
    }

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Categories::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function listCategories()
    {
        return Categories::get();
    }
}
