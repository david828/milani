<?php

namespace App\Model\MilaniCart;

use Illuminate\Database\Eloquent\Model;

class MilaniCart extends Model
{

    protected $table = 'milanicart';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cart'
    ];
}