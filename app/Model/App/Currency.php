<?php

namespace App\Model\App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Currency::select('id', 'name')
            ->where('id', '!=', 0)
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }
}
