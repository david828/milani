<?php

namespace App\Model\App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{

    protected $table = 'app';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'logo',
        'email',
        'phone',
        'cellphone',
        'fax',
        'address',
        'website',
        'copyright',
        'status',
        'dirdata',
        'urldata',
        'tax_value',
        'id_currency',
        'min_value',
    ];

    public function currency()
    {
        return $this->hasMany('App\Model\App\Currency', 'id', 'id_currency');
    }
}