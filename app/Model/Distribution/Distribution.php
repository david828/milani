<?php

namespace App\Model\Distribution;

use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    protected $table = 'distribution';
    protected $primaryKey = 'id';
    protected $fillable = ['logo', 'name', 'cellphone', 'address', 'phone', 'email', 'id_country', 'id_state', 'id_city'];

    public function city()
    {
        return $this->hasOne('App\Model\Location\Cities', 'id', 'id_city');
    }

    public function state()
    {
        return $this->hasOne('App\Model\Location\States', 'id', 'id_state');
    }

    public function country()
    {
        return $this->hasOne('App\Model\Location\Countries', 'id', 'id_country');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('email', 'LIKE', "%$data%");
                $qr->orWhere('address', 'LIKE', "%$data%");
                $qr->orWhereHas('city',function ($q) use($data){
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('state',function ($q) use($data){
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('country',function ($q) use($data){
                    $q->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return Distribution::DataSearch($request)->with('city')->orderBy('name', 'ASC')->paginate(20);
    }

    static function siteData($search)
    {
        return Distribution::DataSearch($search)
            ->with('city')
            ->with('state')
            ->with('country')
            ->orderBy('name', 'ASC')
            ->paginate(10);
    }

    public static function searchG($data)
    {
        $result = Distribution::where('name', 'LIKE', "%$data%")
            ->orderBy('name','ASC')
            ->get();
        return $result;
    }
}