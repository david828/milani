<?php

namespace App\Model\Resources;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{

    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug',
        'file'
    ];



    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
            });

        }
    }

    protected function listData($request)
    {
        return Resources::DataSearch($request)->paginate(20);
    }

    protected function getResource($res)
    {
        return Resources::where('id', $res)->paginate(20);
    }

}
