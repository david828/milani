<?php

namespace App\Model\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use softdeletes;
    protected $table = 'contact';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'metadescription',
        'metatags',
        'maptype',
        'framemap',
        'api_google',
        'latitude',
        'longitude',
        'status'
    ];
    protected $dates = ['deleted_at'];

    public function receivers()
    {
        return $this->hasMany('App\Model\Contact\Receivers', 'id_contact', 'id');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->orWhere('name', 'LIKE', "%$data%");
        }
    }

    protected function listData($request)
    {
        return Contact::DataSearch($request)->paginate(20);
    }


}
