<?php

namespace App\Model\Contact;

use Illuminate\Database\Eloquent\Model;


class Receivers extends Model
{
    protected $table = 'receivers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_contact',
        'email',
    ];
    protected $dates = ['deleted_at'];

    protected function showData($contact)
    {
        return Receivers::where('id_contact', $contact)->get();
    }
}
