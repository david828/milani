<?php

namespace App\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $table = 'paymentstatus';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
}
