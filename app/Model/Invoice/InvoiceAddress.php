<?php

namespace App\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

class InvoiceAddress extends Model
{

    protected $table = 'invoiceaddress';
    protected $primaryKey = 'id_invoice';
    protected $fillable = [
        'address',
        'documenttype',
        'document',
        'name',
        'phone',
        'email',
        'post_code',
        'additional'
    ];
}
