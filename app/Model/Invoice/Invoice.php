<?php

namespace App\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    protected $primaryKey = 'id';
    protected $fillable = [
        'reference',
        'id_user',
        'id_status',
        'customer_name',
        'customer_address',
        'customer_phone',
        'customer_email',
        'invoice_date',
        'description',
        'subtotal',
        'discount',
        'shipping',
        'tax',
        'coupon',
        'total',
        'transaction_date',
        'operation_date',
        'reference_pol',
        'account_number_ach'
    ];

    public function details()
    {
        return $this->hasMany('App\Model\Invoice\InvoiceDetails', 'id_invoice', 'id');
    }

    public function status()
    {
        return $this->hasMany('App\Model\Invoice\PaymentStatus', 'id', 'id_status');
    }


    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('customer_name', 'LIKE', "%$data%");
                $qr->orWhere('customer_address', 'LIKE', "%$data%");
                $qr->orWhere('customer_phone', 'LIKE', "%$data%");
                $qr->orWhere('customer_email', 'LIKE', "%$data%");
                $qr->orWhere('invoice_date', 'LIKE', "%$data%");
                $qr->orWhere('subtotal', 'LIKE', "%$data%");
                $qr->orWhere('discount', 'LIKE', "%$data%");
                $qr->orWhere('shipping', 'LIKE', "%$data%");
                $qr->orWhere('tax', 'LIKE', "%$data%");
                $qr->orWhere('total', 'LIKE', "%$data%");
                $qr->orWhereHas('status', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('details', function ($q) use ($data) {
                    $q->where('description', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return Invoice::DataSearch($request)->with('status')->orderBy('created_at', 'DESC')->paginate(20);
    }

    protected function countUserInvoice($user)
    {
        return Invoice::where('id_user', $user)->sum('total');
    }

    protected function countTotalInvoice()
    {
        return Invoice::where('id_status', 2)->sum('total');
    }

    protected function countLastInvoice()
    {
        return Invoice::where('id_status', 2)->orderBy('id', 'DESC')->first();
    }

    protected function getLastUserInvoice($user)
    {
        return Invoice::where('id_user', $user)->orderBy('id', 'DESC')->first();
    }

    protected function getPerMonth($months)
    {
        $total = [];
        foreach ($months as $m) {
            $total[] = Invoice::whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', $m)->where('id_status', 2)->sum('total');
        }
        return $total;
    }

    protected function userInvoice($user)
    {
        return Invoice::with('status')->where('id_user', $user)->paginate(10);
    }

    protected function updInvoiceUserN($user)
    {
        return Invoice::where('id_user', $user)->update(['id_user' => null]);
    }
}
