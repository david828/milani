<?php

namespace App\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    protected $table = 'invoicedetails';
    protected $primaryKey = 'id_invoice';
    protected $fillable = [
        'id_product',
        'id_unit',
        'description',
        'quantity',
        'total'
    ];


    protected function getDetails($invoice)
    {
        return InvoiceDetails::where('id_invoice', $invoice)->get();
    }

    protected function updateDetailProduct($product)
    {
        return InvoiceDetails::where('id_product', $product)->update(['id_product' => null]);
    }

    protected function updateUnitProduct($unit)
    {
        return InvoiceDetails::where('id_unit', $unit)->update(['id_unit' => null]);
    }
}
