<?php

namespace App\Model\Galleries;

use Illuminate\Database\Eloquent\Model;

class Galleries extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'galleries';
    protected $fillable = [
        'id_category',
        'name',
        'slug',
        'description',
    ];

    public function category()
    {
        return $this->hasMany('App\Model\Galleries\CategoriesGal', 'id', 'id_category');
    }

    public function images()
    {
        return $this->hasMany('App\Model\Galleries\GalleryImages', 'id_gallery', 'id');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != ''){
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($qh) use ($data) {
                    $qh->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return Galleries::DataSearch($request)->with('category')->paginate(20);
    }

    static function index($search, $cat)
    {
        return Galleries::DataSearch($search)
            ->where(function ($qr) use ($cat) {
                if($cat != '') {
                    $qr->whereHas('category',function ($q) use($cat){
                        $q->where('slug','=',$cat);
                    });
                }
            })
            ->with('category')
            ->with('images')
            ->orderBy('created_at', 'desc')
            ->paginate(9);
    }

    protected function showData($id)
    {
        return Galleries::with('category')->where('id', $id)->first();
    }

    public static function searchG($data)
    {
        $result = Galleries::where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('description', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($qh) use ($data) {
                    $qh->where('name', 'LIKE', "%$data%");
                });
            })
            ->orderBy('name', 'ASC')
            ->get();
        return $result;
    }
}
