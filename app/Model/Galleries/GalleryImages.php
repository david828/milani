<?php

namespace App\Model\Galleries;

use Illuminate\Database\Eloquent\Model;

class GalleryImages extends Model
{
    protected $table = 'galleryimages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_gallery',
        'image',
    ];

    protected function deleteImages($gallery)
    {
        return GalleryImages::where('id_gallery', $gallery)->delete();
    }

    protected function getGalleryImages($gallery)
    {
        return GalleryImages::select('id as idx', 'image')->where('id_gallery', $gallery)->get();
    }
}
