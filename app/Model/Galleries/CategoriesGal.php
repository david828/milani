<?php

namespace App\Model\Galleries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class CategoriesGal extends Model
{
    use softdeletes;
    protected $table = 'gallery_categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
    protected $dates = ['deleted_at'];


    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->orWhere('name', 'LIKE', "%$data%");
        }
    }

    protected function listData($request)
    {
        return CategoriesGal::DataSearch($request)->paginate(20);
    }

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = CategoriesGal::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function listCategories()
    {
        return CategoriesGal::get();
    }
}
