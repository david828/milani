<?php

namespace App\Model\Header;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{

    protected $table = 'header';
    protected $primaryKey = 'id';
    protected $fillable = [
        'pos_1',
        'pos_2',
        'client_id',
        'user_id',
        'accessToken',
        'num_photos',
        'image_2'
    ];

    static function home()
    {
        return Header::find(1);
    }
}