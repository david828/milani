<?php

namespace App\Model\Header;

use Illuminate\Database\Eloquent\Model;

class HeaderImages extends Model
{

    protected $table = 'headerimages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_header',
        'image',
        'order'
    ];

    static function home()
    {
        return HeaderImages::orderBy('order','ASC')->get();
    }
}