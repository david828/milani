<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title'
    ];

    protected function listData()
    {
        return Menu::orderBy('order', 'ASC')->paginate(20);
    }

    protected function getMenu()
    {
        return Menu::orderBy('id', 'ASC')->get();
    }
}
