<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{

    protected $table = 'documenttype';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];


    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = DocumentType::select('id', 'name')
            ->where('id', '!=', 0)
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }
}
