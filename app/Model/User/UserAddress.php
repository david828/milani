<?php

namespace App\Model\User;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{

    protected $table = 'useraddress';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_user',
        'title',
        'address',
        'id_city',
        'id_documenttype',
        'document',
        'name',
        'phone',
        'email',
        'post_code',
        'additional'
    ];


    protected function getStateData($id)
    {
        return UserAddress::select('c.id as cityId','c.name as cityName', 'ct.id as countryId', 'ct.iso_code as countryIso', 'ct.name as countryName', 'st.id as stateId', 'st.name as stateName')
            ->join('cities as c', 'c.id', '=', 'useraddress.id_city')
            ->join('states as st', 'c.id_state', '=', 'st.id')
            ->join('countries as ct', 'ct.id', '=', 'st.id_country')
            ->find($id);
    }

    protected function getList($city)
    {
        return User::select('c.id as cityId','c.name as cityName', 'ct.id as countryId', 'ct.iso_code as countryIso', 'ct.name as countryName', 'st.id as stateId', 'st.name as stateName')
            ->join('cities as c', 'c.id', '=', 'users.id_city')
            ->join('states as st', 'c.id_state', '=', 'st.id')
            ->join('countries as ct', 'ct.id', '=', 'st.id_country')
            ->where('c.id', $city)
            ->first();
    }
}
