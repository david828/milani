<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    protected $table = 'person';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];


    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Person::select('id', 'name')
            ->where('id', '!=', 0)
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }
}
