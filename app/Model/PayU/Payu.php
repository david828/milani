<?php

namespace App\Model\PayU;

use Illuminate\Database\Eloquent\Model;

class Payu extends Model
{
    protected $table = 'payu';
    protected $primaryKey = 'accountid';
    protected $fillable = [
        'merchantid',
        'apilogin',
        'apikey',
        'publickey',
        'responseurl',
        'requesturl',
        'confirmationurl',
        'testing'
    ];


}
