<?php

namespace App\Model\Modals;

use Illuminate\Database\Eloquent\Model;

class Modals extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'modals';
    protected $fillable = [
        'id',
        'name',
        'slug',
        'image',
        'text',
        'button',
        'link',
        'typelink',
        'url',
        'status',
        'created_at',
        'updated_at'
    ];

    public function scopeDataSearch($query, $data)
    {
        if ($data != ''){
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('text', 'LIKE', "%$data%");
            });
        }
    }

    protected function listData($request)
    {
        return Modals::DataSearch($request)->paginate(20);
    }
}
