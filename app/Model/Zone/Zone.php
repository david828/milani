<?php

namespace App\Model\Zone;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zone';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'price',
        'status'
    ];

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
            });
        }
    }

    protected function listData($request)
    {
        return Zone::DataSearch($request)->orderBy('id', 'DESC')->paginate(20);
    }

    protected function availableZone()
    {
        return Zone::where('status', 1)->get();
    }

    protected function selection()
    {
        $field = [];
        $data = Zone::select('id', 'name')
            ->where('status', 1)
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function getShip($id)
    {
        $data = Zone::find($id);
        return $data;
    }
}
