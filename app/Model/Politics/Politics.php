<?php

namespace App\Model\Politics;

use Illuminate\Database\Eloquent\Model;

class Politics extends Model
{
    protected $table = 'politics';
    protected $fillable = ['text'];
}