<?php

namespace App\Model\Social;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'social';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'icon'
    ];

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Social::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }
}
