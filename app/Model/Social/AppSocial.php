<?php

namespace App\Model\Social;

use Illuminate\Database\Eloquent\Model;

class AppSocial extends Model
{
    protected $table = 'appsocial';
    protected $primaryKey = 'id_app';
    protected $fillable = [
        'id_social',
        'link'
    ];

    public function social()
    {
        return $this->hasMany('App\Model\Social\Social', 'id', 'id_social');
    }

    protected function deleteSocial($app)
    {
        return AppSocial::where('id_app', $app)->delete();
    }

    protected function getSocial($app)
    {
        return AppSocial::with('social')->where('id_app', $app)->get();
    }

}
