<?php

namespace App\Model\Conditions;

use Illuminate\Database\Eloquent\Model;

class Conditions extends Model
{
    protected $table = 'conditions';
    protected $fillable = ['text'];
}