<?php

namespace App\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    use SoftDeletes;
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'name'
    ];

    protected $dates = ['deleted_at'];

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('code', 'LIKE', "%$data%");
            });
        }
    }

    protected function listData($request)
    {
        return Countries::DataSearch($request)->orderBy('id', 'DESC')->paginate(20);
    }

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Countries::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }
}
