<?php

namespace App\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cities extends Model
{

    use SoftDeletes;
    protected $table = 'cities';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_state',
        'name',
        'id_zone'
    ];

    protected $dates = ['deleted_at'];

    public function state()
    {
        return $this->hasMany('App\Model\Location\States', 'id', 'id_state');
    }

    public function zone()
    {
        return $this->hasMany('App\Model\Zone\Zone', 'id', 'id_zone');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('cities.name', 'LIKE', "%$data%");
                $qr->orWhere('countries.name', 'LIKE', "%$data%");
                $qr->orWhereHas('state', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('zone', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return Cities::DataSearch($request)
            ->select('cities.*', 'countries.name as country')
            ->with('state')
            ->with('zone')
            ->join('states as st', 'cities.id_state', '=', 'st.id')
            ->join('countries', 'countries.id', '=', 'st.id_country')
            ->orderBy('id', 'DESC')
            ->paginate(20);
    }

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = Cities::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function selectionByState($state)
    {
        $field = ['' => trans('app.select')];
        $data = Cities::select('id', 'name')
            ->where('id_state', $state)
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function getCountryData($city)
    {
        return Cities::select('ct.id as idCountry')
            ->join('states as st', 'cities.id_state', '=', 'st.id')
            ->join('countries as ct', 'ct.id', '=', 'st.id_country')
            ->where('cities.id', $city)
            ->first();
    }

    protected function getLocationData($city)
    {
        return Cities::select('ct.id as idCountry', 'st.id as idState')
            ->join('states as st', 'cities.id_state', '=', 'st.id')
            ->join('countries as ct', 'ct.id', '=', 'st.id_country')
            ->where('cities.id', $city)
            ->first();
    }

    protected function getCities($state)
    {
        return Cities::select('id as num', 'name')->where('id_state', $state)->orderBy('name', 'ASC')->get();
    }
}
