<?php

namespace App\Model\Location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class States extends Model
{

    use SoftDeletes;
    protected $table = 'states';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_country',
        'code',
        'name'
    ];

    protected $dates = ['deleted_at'];

    public function country()
    {
        return $this->hasMany('App\Model\Location\Countries', 'id', 'id_country');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('code', 'LIKE', "%$data%");
                $qr->orWhereHas('country', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return States::DataSearch($request)->with('country')->orderBy('id', 'DESC')->paginate(20);
    }

    protected function selection()
    {
        $field = ['' => trans('app.select')];
        $data = States::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function selectionByCountry($country)
    {
        $field = ['' => trans('app.select')];
        $data = States::select('id', 'name')
            ->where('id_country', $country)
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function getStates($country)
    {
        return States::select('id as num', 'name')->where('id_country', $country)->orderBy('name', 'ASC')->get();
    }

}
