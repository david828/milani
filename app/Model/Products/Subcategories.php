<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class Subcategories extends Model
{

    protected $table = 'subcategories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_category',
        'name',
        'slug'
    ];

    public function category()
    {
        return $this->hasMany('App\Model\Products\Categories', 'id', 'id_category');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    static function listData($request)
    {
        return Subcategories::DataSearch($request)
            ->with('category')
            ->orderBy('id', 'ASC')->paginate(20);
    }

    static public function selection($type = '')
    {
        $field = [];
        $data=Subcategories::select('id','name')
            ->where(function ($qr) use ($type) {
                if($type !='')
                {
                    $qr->where('id_category', $type);
                }
            })
            ->orderBy('name','ASC')
            ->get();
        foreach($data as $item){
            $field[$item->id]=$item->name;
        }
        return $field;
    }

    static public function getSubcategories($type = '')
    {
        $data=Subcategories::select('id','name')
            ->where(function ($qr) use ($type) {
                if($type !='')
                {
                    $qr->where('id_category', $type);
                }
            })
            ->orderBy('name','ASC')
            ->get();
        return $data;
    }

    protected function getCount()
    {
        return Subcategories::where('id', '!=', 0)->count();
    }
}