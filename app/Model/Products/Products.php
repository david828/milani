<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_category',
        'id_subcategory',
        'id_finish',
        'name',
        'slug',
        'description',
        'review',
        'reference',
        'SKU',
        'price',
        'sale',
        'sale_price',
        'best_seller',
        'sw_best',
        'new',
        'stock',
        'status',
        'min_stock',
        'featured'
    ];

    public function category()
    {
        return $this->hasMany('App\Model\Products\Categories', 'id', 'id_category');
    }

    public function subcategory()
    {
        return $this->hasMany('App\Model\Products\Subcategories', 'id', 'id_subcategory');
    }

    public function finish()
    {
        return $this->hasMany('App\Model\Products\Finish', 'id', 'id_finish');
    }

    public function color()
    {
        return $this->belongsToMany('App\Model\Products\Colors', 'productcolor', 'id_product', 'id_color');
    }

    public function productcolor()
    {
        return $this->hasMany('App\Model\Products\ProductColor', 'id_product', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Model\Products\ProductGallery', 'id_product', 'id');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('reference', 'LIKE', "%$data%");
                $qr->orWhere('SKU', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('subcategory', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('color', function ($q) use ($data) {
                    $q->where('SKU', 'LIKE', "%$data%");
                    $q->orWhere('reference', 'LIKE', "%$data%");
                });

            });
        }
    }

    static function listData($request, $best = '', $new = '', $soon ='')
    {
        return Products::DataSearch($request)
            ->where(function ($qr) use ($best, $new, $soon) {
                if($best != '') {
                    $qr->where('featured', '1');
                }
                if($new != '') {
                    $qr->where('new', '1');
                }
                if($soon != '') {
                    $qr->where('soon', '1');
                }
            })
            ->with('category')
            ->with('subcategory')
            ->with('finish')
            ->with('images')
            ->with('color')
            ->orderBy('id', 'ASC')->paginate(20);
    }

    public static function index($search, $cat, $sub, $fin, $col, $ord)
    {
        switch ($ord) {
            case 'n':
                $column = 'new';
                $column2 = 'categories.order';
                $order = 'DESC';
                $order2 = 'ASC';
                break;
            case 'b':
                $column = 'sw_best';
                $column2 = 'best_seller';
                $order = 'DESC';
                $order2 = 'DESC';
                break;
            case 'lp':
                $column = 'price';
                $column2 = 'price';
                $order = 'ASC';
                $order2 = 'ASC';
                break;
            case 'mp':
                $column = 'price';
                $column2 = 'price';
                $order = 'DESC';
                $order2 = 'DESC';
                break;
            case 'c':
                $column = 'categories.order';
                $column2 = 'new';
                $order = 'ASC';
                $order2 = 'DESC';
                break;
            default:
                $column = 'categories.order';
                $column2 = 'new';
                $order = 'ASC';
                $order2 = 'DESC';
        }
        $data = Products::select(['products.*','categories.order'])
            ->with(['images' => function ($qr) {
                $qr->orderBy('order', 'ASC');
                $qr->orderBy('created_at', 'ASC');
            }])
            ->with(['productcolor' => function ($q) {
                $q->with(['images' => function ($qr) {
                    $qr->orderBy('order', 'ASC');
                    $qr->orderBy('created_at', 'ASC');
                }]);
            }])
            ->where(function ($qr) use ($search) {
                if($search != '') {
                    $qr->where(function ($qe) use ($search) {
                        $qe->orWhere('products.name', 'LIKE', "%$search%");
                        $qe->orWhere('reference', 'LIKE', "%$search%");
                        $qe->orWhere('SKU', 'LIKE', "%$search%");
                        $qe->orWhere('description', 'LIKE', "%$search%");
                    });
                }
            })
            ->where(function ($qr) use ($cat) {
                if($cat != '') {
                    if($cat == 'sale') {
                        $qr->where('sale', '1');
                        $qr->where('sale_price', '>', 0);
                    } else {
                        $qr->whereHas('category',function ($q) use($cat){
                            $q->where('slug','=',$cat);
                        });
                    }
                }
            })
            ->where(function ($qr) use ($sub) {
                if($sub != '') {
                    $qr->whereHas('subcategory',function ($q) use($sub){
                        $q->where('slug','=',$sub);
                    });
                }
            })
            ->where(function ($qr) use ($fin) {
                if($fin != '') {
                    $qr->whereHas('finish',function ($q) use($fin){
                        $q->where('slug','=',$fin);
                    });
                }
            })
            ->where(function ($qr) use ($col) {
                if($col != '') {
                    $qr->whereHas('color',function ($q) use($col){
                        $q->where('slug','=',$col);
                    });
                }
            })
            ->where('status','1')
            ->join('categories','products.id_category', '=','categories.id')
            ->orderBy($column, $order)
            ->orderBy($column2, $order2)
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
            //->toSql();
            //dd($data);
        foreach($data as $b) {
            $getImage = imageProduct($b);
            $b->image = $getImage[0];
            $b->alt = $getImage[1];
            $b->st = stockProduct($b);
            $b->money = '$'.number_format($b->price,0,',','.');
            if(!is_null($b->sale_price) and $b->sale == '1') {
                $b->sale_money = '$'.number_format($b->sale_price,0,',','.');
                $b->sale_percent = $b->sale_percent.'%';
            }
        }
        return $data;
    }

    public static function viewP($slug)
    {
        $data = Products::with(['images' => function ($qr) {
                $qr->orderBy('order', 'ASC');
                $qr->orderBy('created_at', 'ASC');
            }])
            ->with(['productcolor' => function ($q) {
                $q->with(['images' => function ($qr) {
                    $qr->orderBy('order', 'ASC');
                    $qr->orderBy('created_at', 'ASC');
                }]);
                $q->with('color');
            }])
            ->with('category')
            ->with('subcategory')
            ->with('finish')
            ->where('slug',$slug)
            ->where('status','1')
            ->first();
        if(!is_null($data)) {
            $getImage = imageProduct($data);
            $data->image = $getImage[0];
            $data->alt = $getImage[1];
            $data->money = '$'.number_format($data->price,0,',','.');
            if(!is_null($data->sale_price)) {
                $data->sale_money = '$'.number_format($data->sale_price,0,',','.');
            }
        }
        return $data;
    }

    public static function getSwitch($opc)
    {
        switch ($opc) {
            case 'new':
                $data = Products::with('images')
                    ->with(['productcolor' => function ($q) {
                        $q->with('images');
                    }])
                    ->orderBy('created_at', 'DESC')
                    ->where('new','1')
                    ->where('status','1')
                    ->get();
                break;
            case 'best_seller':
                $data = Products::with('images')
                    ->with(['productcolor' => function ($q) {
                        $q->with('images');
                    }])
                    ->orderBy('sw_best', 'DESC')
                    ->orderBy('best_seller', 'DESC')
                    ->orderBy('created_at', 'DESC')
                    ->where('status','1')
                    ->take(12)
                    ->get();
                break;
            case 'featured':
                $data = Products::with('images')
                    ->with(['productcolor' => function ($q) {
                        $q->with('images');
                    }])
                    ->orderBy('created_at', 'DESC')
                    ->where('featured','1')
                    ->where('status','1')
                    ->get();
                break;
            default:
                $data = Products::with('images')
                    ->with(['productcolor' => function ($q) {
                        $q->with('images');
                    }])
                    ->orderBy('created_at', 'DESC')
                    ->where('new','1')
                    ->where('status','1')
                    ->get();
        }
        foreach($data as $b) {
            $getImage = imageProduct($b);
            $b->image = $getImage[0];
            $b->alt = $getImage[1];
            $b->st = stockProduct($b);
            $b->money = '$'.number_format($b->price,0,',','.');
            if(!is_null($b->sale_price) and $b->sale == '1') {
                $b->sale_money = '$'.number_format($b->sale_price,0,',','.');
                $b->sale_percent = $b->sale_percent.'%';
            }
        }
        //dd($data);
        return $data;
    }

    public static function getProduct($id, $color = '')
    {
        $data = Products::with('images')
            ->with(['productcolor' => function ($q) {
                $q->with('images');
            }])
            ->where('status','1')
            ->find($id);
        $getImage = imageProduct($data, $color);
        $data->image = $getImage[0];
        $data->alt = $getImage[1];
        if($data->sale == '1' and !is_null($data->sale_price)) {
            $data->money = '$'.number_format($data->sale_price,0,',','.');
            $data->price = $data->sale_price;
        } else {
            $data->money = '$'.number_format($data->price,0,',','.');
        }
        return $data;
    }

    public static function selectDes()
    {
        $field = ['' => trans('app.select')];
        $data=Products::select('id','name')
            ->orderBy('name','ASC')
            ->where('status','1')
            ->get();
        foreach($data as $item){
            $field[$item->id]=$item->name;
        }
        return $field;
    }

    public static function searchG($data)
    {
        $result = Products::where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('description', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('subcategory', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
                $qr->orWhereHas('colors', function ($q) use ($data) {
                    $q->where('name', 'LIKE', "%$data%");
                });
            })
            ->where('status','1')
            ->orderBy('name','ASC')
            ->get();
        return $result;
    }

    protected function getCount()
    {
        return Products::count();
    }

    protected function getCountStatus($status)
    {
        return Products::where('status', $status)->count();
    }

    protected function selection()
    {
        $data = Products::select('id', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->id] = $for->name;
        }
        return $field;
    }

    protected function selectionSlug()
    {
        $data = Products::select('slug', 'name')
            ->orderBy('name', 'ASC')
            ->get();
        foreach ($data as $for) {
            $field[$for->slug] = $for->name;
        }
        return $field;
    }

    protected function revStock($id, $color)
    {
        if(is_null($color)) {
            $data = Products::find($id);
            return $data;
        } else {
            $data = Products::with(['productcolor' => function ($q) use ($color) {
                $q->where('id_color', $color);
            }])
            ->find($id);
            if(count($data->productcolor) > 0) {
                $data->stock = $data->productcolor[0]->stock;
            }

        }
        return $data;
    }

}