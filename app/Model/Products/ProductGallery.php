<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class ProductGallery extends Model
{
    protected $table = 'productgallery';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_product',
        'image',
        'order'
    ];

    public static function deleteProduct($id)
    {
        $data = ProductGallery::where('id_product', $id)->get();
        foreach($data as $img)
        {
            if (File::exists(appData()->dirdata . '/images/products/' . $img->image)) {
                File::delete(appData()->dirdata . '/images/products/' . $img->image);
            }
        }
        ProductGallery::where('id_product', $id)->delete();
    }
}