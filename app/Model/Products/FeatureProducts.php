<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class FeatureProducts extends Model
{

    protected $table = 'featureproducts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title_featureds',
        'title_news',
        'title_bests',
        'id_4'
    ];
}