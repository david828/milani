<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class Finish extends Model
{

    protected $table = 'finish';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug'
    ];

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
            });
        }
    }

    static function listData($request)
    {
        return Finish::DataSearch($request)->orderBy('id', 'ASC')->where('id', '<>', 0)->paginate(20);
    }


    static public function selection()
    {
        $field = ['' => trans('app.select')];
        $data=Finish::select('id','name')
            ->where('id', '<>', 0)
            ->orderBy('name','ASC')
            ->get();
        foreach($data as $item){
            $field[$item->id]=$item->name;
        }
        return $field;
    }

    static public function selectionIndex()
    {
        $data=Finish::select('id','name','slug')
            ->where('id', '<>', 0)
            ->orderBy('name','ASC')
            ->get();
        return $data;
    }
}