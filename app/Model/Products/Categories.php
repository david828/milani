<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{

    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'order',
        'slug'
    ];

    public function subcategory()
    {
        return $this->hasMany('App\Model\Products\Subcategories', 'id_category', 'id');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
            });
        }
    }

    static function listData($request)
    {
        return Categories::DataSearch($request)->orderBy('order', 'ASC')->where('id', '<>', 0)->paginate(20);
    }


    static public function selection()
    {
        $field = ['' => trans('app.select')];
        $data=Categories::select('id','name')
            ->where('id', '<>', 0)
            ->orderBy('name','ASC')
            ->get();
        foreach($data as $item){
            $field[$item->id]=$item->name;
        }
        return $field;
    }

    static public function selectionIndex()
    {
        $data=Categories::select('id','name','slug')
            ->where('id', '<>', 0)
            ->orderBy('order','ASC')
            ->with('subcategory')
            ->get();
        return $data;
    }

    protected function getCatOrder($cat)
    {
        return Categories::where('id', '!=', $cat)->orderBy('order', 'ASC')->get();
    }

    protected function updateOrder($cat, $order)
    {
        return Categories::where('id', $cat)->update(['order' => $order]);
    }

    protected function getCount()
    {
        return Categories::where('id', '!=', 0)->count();
    }
}