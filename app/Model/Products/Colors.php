<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{

    protected $table = 'colors';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug',
        'image'
    ];

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
            });
        }
    }

    static function listData($request)
    {
        return Colors::DataSearch($request)->orderBy('id', 'ASC')->paginate(20);
    }
}