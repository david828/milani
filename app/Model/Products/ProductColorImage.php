<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class ProductColorImage extends Model
{
    protected $table = 'productcolorimage';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_productcolor',
        'image',
        'order'
    ];
}