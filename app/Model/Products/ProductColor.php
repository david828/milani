<?php

namespace App\Model\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class ProductColor extends Model
{
    protected $table = 'productcolor';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_product',
        'id_color',
        'stock',
        'min_stock',
        'reference',
        'SKU',
    ];

    public function color()
    {
        return $this->hasMany('App\Model\Products\Colors', 'id', 'id_color');
    }

    public function images()
    {
        return $this->hasMany('App\Model\Products\ProductColorImage', 'id_productcolor', 'id');
    }

    public static function getColor($id)
    {
        $list = ProductColor::where('id_product', $id)->get();
        $colors = [];
        foreach ($list as $t)
        {
            $colors[] = $t->id_color;
        }
        return $colors;
    }

    public static function selection($product)
    {
        return ProductColor::where('id_product', $product)->with('color')->get();
    }

    public static function index()
    {
        return ProductColor::distinct('id_color')
            ->join('colors', 'productcolor.id_color','=', 'colors.id')
            ->get(['id_color as id', 'name', 'image', 'slug']);
    }

    public static function deleteRelation($prod,$col)
    {
        $list = ProductColor::where('id_product', $prod)->where('id_color', $col)->get();
        foreach ($list as $item)
        {
            $data = ProductColorImage::where('id_productcolor', $item->id)->get();
            foreach($data as $img)
            {
                if (File::exists(appData()->dirdata . '/images/products/' . $img->image)) {
                    File::delete(appData()->dirdata . '/images/products/' . $img->image);
                }
            }
            ProductColorImage::where('id_productcolor', $item->id)->delete();
        }
        ProductColor::where('id_product', $prod)->where('id_color', $col)->delete();
    }

    public static function deleteColor($id)
    {
        $list = ProductColor::where('id_color', $id)->get();
        foreach ($list as $item)
        {
            $data = ProductColorImage::where('id_productcolor', $item->id)->get();
            foreach($data as $img)
            {
                if (File::exists(appData()->dirdata . '/images/products/' . $img->image)) {
                    File::delete(appData()->dirdata . '/images/products/' . $img->image);
                }
            }
            ProductColorImage::where('id_productcolor', $item->id)->delete();
        }
        ProductColor::where('id_color', $id)->delete();
    }

    public static function deleteProduct($id)
    {
        $list = ProductColor::where('id_product', $id)->get();
        foreach ($list as $item)
        {
            $data = ProductColorImage::where('id_productcolor', $item->id)->get();
            foreach($data as $img)
            {
                if (File::exists(appData()->dirdata . '/images/products/' . $img->image)) {
                    File::delete(appData()->dirdata . '/images/products/' . $img->image);
                }
            }
            ProductColorImage::where('id_productcolor', $item->id)->delete();
        }
        ProductColor::where('id_product', $id)->delete();
    }
}