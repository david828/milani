<?php

namespace App\Model\Articles;

use Illuminate\Database\Eloquent\Model;

class ArticleComments extends Model
{
    protected $table = 'articlecomments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_article',
        'name',
        'email',
        'comment',
    ];

    protected function deleteComments($article)
    {
        return ArticleComments::where('id_article', $article)->delete();
    }
}
