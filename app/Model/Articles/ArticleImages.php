<?php

namespace App\Model\Articles;

use Illuminate\Database\Eloquent\Model;

class ArticleImages extends Model
{
    protected $table = 'articleimages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_article',
        'image',
    ];

    protected function deleteImages($article)
    {
        return ArticleImages::where('id_article', $article)->delete();
    }

    protected function getArticleImages($article)
    {
        return ArticleImages::select('id as idx', 'image')->where('id_article', $article)->get();
    }
}
