<?php

namespace App\Model\Articles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use softdeletes;
    protected $primaryKey = 'id';
    protected $table = 'articles';
    protected $fillable = [
        'id_category',
        'id_user',
        'name',
        'slug',
        'description',
        'image',
        'tags',
        'status',
        'metadescription',
        'metatags'
    ];
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->hasMany('App\Model\Categories\Categories', 'id', 'id_category');
    }

    public function images()
    {
        return $this->hasMany('App\Model\Articles\ArticleImages', 'id_article', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Model\Articles\ArticleComments', 'id_article', 'id');
    }

    public function user()
    {
        return $this->hasMany('App\User', 'id', 'id_user');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != ''){
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($qh) use ($data) {
                    $qh->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return Article::DataSearch($request)->with('category')->with('user')->paginate(20);
    }

    static function index($search, $cat, $et)
    {
        return Article::DataSearch($search)
            ->where(function ($qr) use ($cat, $et) {
                if($et != '') {
                    $qr->where('tags', 'LIKE', "%$et%");
                }
            })
            ->where(function ($qr) use ($cat) {
                if($cat != '') {
                    $qr->whereHas('category',function ($q) use($cat){
                        $q->where('slug','=',$cat);
                    });
                }
            })
            ->where('status', 1)
            ->with('category')
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate(9);
    }

    protected function showData($id)
    {
        return Article::with('category')->with('user')->where('id', $id)->first();
    }

    protected function relatedPost($category, $count)
    {
        return Article::with('category')->where('id_category', $category)->orderBy('id', 'DESC')->limit($count)->get();
    }

    protected function getNext($date)
    {
        return Article::orderBy('created_at', 'asc')->where('created_at', '>', $date)->with('images')->first();
    }

    protected function getPrev($date)
    {
        return Article::orderBy('created_at', 'desc')->where('created_at', '<', $date)->with('images')->first();
    }

    public static function searchG($data)
    {
        $result = Article::where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('metadescription', 'LIKE', "%$data%");
                $qr->orWhere('metatags', 'LIKE', "%$data%");
                $qr->orWhere('description', 'LIKE', "%$data%");
                $qr->orWhereHas('category', function ($qh) use ($data) {
                    $qh->where('name', 'LIKE', "%$data%");
                });
            })
            ->orderBy('name', 'ASC')
            ->get();
        return $result;
    }

    protected function getCountStatus($status)
    {
        return Article::where('status', $status)->count();
    }
}
