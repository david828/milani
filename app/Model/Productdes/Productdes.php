<?php

namespace App\Model\Productdes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Productdes extends Model
{
    protected $table = 'productdes';
    protected $primaryKey = 'id';
    protected $fillable = ['image', 'subtitle', 'text', 'link', 'title', 'window'];

    public static function home()
    {
        return Productdes::find(1);
    }

}