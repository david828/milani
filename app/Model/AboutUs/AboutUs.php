<?php

namespace App\Model\AboutUs;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{

    protected $table = 'aboutus';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image',
        'text'
    ];
}