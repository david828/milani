<?php

namespace App\Model\Coupons;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    protected $table = 'coupons';
    protected $primaryKey = 'id';
    protected $fillable = [
        'coupon',
        'discount',
        'expirationtype',
        'expiration',
        'status',
        'products'
    ];

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('coupon', 'LIKE', "%$data%");
                $qr->orWhere('discount', 'LIKE', "%$data%");
            });
        }
    }

    protected function listData($request)
    {
        return Coupons::DataSearch($request)->orderBy('id', 'DESC')->paginate(20);
    }
}
