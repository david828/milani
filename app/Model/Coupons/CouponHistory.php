<?php

namespace App\Model\Coupons;

use Illuminate\Database\Eloquent\Model;

class CouponHistory extends Model
{
    protected $table = 'coupon_history';
    protected $fillable = [
        'coupon',
        'invoice',
        'percent',
        'value',
    ];

    public function relInvoice()
    {
        return $this->hasOne('App\Model\Invoice\Invoice', 'reference', 'invoice');
    }

    protected function listData($c, $d, $h)
    {
        return CouponHistory::with('relInvoice')
        ->whereHas('relInvoice', function ($q){
            $q->where('id_status', 2);
        })
        ->where(function ($q) use ($c) {
            if($c == '*') {

            } else if($c !='' and !is_null($c)){
                $q->where('coupon', $c);
            } else {
                $q->where('coupon', null);
            }
        })
        ->where(function ($q) use ($d) {
            if($d !='' and !is_null($d)){
                $q->where('created_at', '>=', $d);
            }
        })
        ->where(function ($q) use ($h) {
            if($h !='' and !is_null($h)){
                $q->where('created_at', '<=', $h);
            }
        })
        ->orderBy('created_at', 'DESC')->paginate(20);
    }

    protected function selector()
    {
        $data = CouponHistory::distinct('coupon')->orderBy('coupon', 'ASC')->get();
        $array = [];
        $array[''] = trans('app.select');
        $array['*'] = trans('app.all');
        foreach ($data as $item) {
            $array[$item->coupon] = $item->coupon;
        }
        return $array;
    }
}
