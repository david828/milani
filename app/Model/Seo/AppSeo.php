<?php

namespace App\Model\Seo;

use Illuminate\Database\Eloquent\Model;

class AppSeo extends Model
{
    protected $table = 'appseo';
    protected $primaryKey = 'id_app';
    protected $fillable = [
        'application_name',
        'charset',
        'metadescription',
        'metatags',
        'google_analytics',
        'pixel_facebook',
        'hotjar',
        'author',
        'robots',
        'metadescription_blog',
        'metatags_blog',
        'language'
    ];

    protected function getSeo()
    {
        return AppSeo::where('id_app', 1)->first();
    }
}
