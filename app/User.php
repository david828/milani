<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_documenttype',
        'id_person',
        'document',
        'name',
        'lastname',
        'email',
        'password',
        'phone',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function address()
    {
        return $this->hasMany('App\Model\User\UserAddress', 'id_user', 'id');
    }

    public function city()
    {
        return $this->hasOne('App\Model\Location\Cities','id','id_city');
    }

    public function scopeDataSearch($query, $data)
    {
        if ($data != "") {
            $query->where(function ($qr) use ($data) {
                $qr->orWhere('name', 'LIKE', "%$data%");
                $qr->orWhere('document', 'LIKE', "%$data%");
                $qr->orWhere('email', 'LIKE', "%$data%");
                $qr->orWhere('phone', 'LIKE', "%$data%");
                $qr->orWhereHas('city',function ($q) use($data){
                    $q->where('name', 'LIKE', "%$data%");
                });
            });
        }
    }

    protected function listData($request)
    {
        return User::DataSearch($request)->orderBy('id', 'DESC')->with('city')->paginate(20);
    }

    protected function dataMail($mail)
    {
        return User::select('id')->where('email', $mail)->first();
    }

    protected function getData($user)
    {
        return User::with('address')->where('id', $user)->first();
    }

    protected function getCountUsers($role)
    {
        return User::where('role', $role)->count();
    }
}
