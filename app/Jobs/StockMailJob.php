<?php

namespace App\Jobs;

use App\Mail\MailStockAdmin;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class StockMailJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user;
    protected $emailData;

    public function __construct($invoice, $minStock)
    {
        $operation = Carbon::parse($invoice->operation_date)->format('d/m/Y');
        $emailData = [
            'products' => $minStock,
            'date' => $operation,
        ];
        $this->emailData = $emailData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(appData()->email)->send(new MailStockAdmin($this->emailData));
    }
}
