<?php

namespace App\Jobs;

use App\Mail\MailPaymentCustomer;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CustomerMailJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user;
    protected $emailData;

    public function __construct($invoice, $request)
    {

        $operation = Carbon::parse($invoice->operation_date)->format('d/m/Y');
        $emailData = [
            'id_invoice' => $invoice->id,
            'doctype' => $invoice->customer_documenttype,
            'document' => $invoice->customer_document,
            'name' => $invoice->customer_name,
            'mail' => $invoice->customer_email,
            'fromMail' => env('MAIL_USER'),
            'fromName' => appData()->name,
            'date' => $operation,
            'amount' => $invoice->total,
            'invoice' => $invoice->reference,
            'status' => trans('payment.statepol_' . $request->get('state_pol'))
        ];
        $this->emailData = $emailData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->emailData['mail'])->send(new MailPaymentCustomer($this->emailData));
    }
}
