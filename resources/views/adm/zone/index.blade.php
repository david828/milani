@extends('adm.layouts.page')

@section('title')
    {!! trans('app.zone') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.zone') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.zone') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                            <div class="box-tools">
                                {!! Form::model(Request::only(['buscar']), ['route'=>'adm.zone.index', 'style'=>'width: 150px;', 'method'=>'GET', 'rol'=>'search']) !!}
                                <div class="input-group">
                                    {!! Form::text('search',$list->search,['class'=>'form-control input-sm pull-right', 'placeholder'=>''.trans("ph.search").'']) !!}
                                    <div class="input-group-btn">
                                        {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            {!! Form::open(['route'=>['adm.zone.status'],'method'=>'POST','name'=>'frmEst','id'=>'frmEst','role'=>'form']) !!}
                            {!! Form::hidden('hn_status',null,['id'=>'hn_status']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.number') !!}</th>
                                        <th>{!! trans('app.id') !!}</th>
                                        <th>{!! trans('app.name') !!}</th>
                                        <th>{!! trans('app.price') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @foreach($list as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $loop->iteration !!}</td>
                                            <td>{!! $for->id !!}</td>
                                            <td>{!! $for->name !!}</td>
                                            <td>$ {!! $for->price !!}</td>
                                            <td>
                                                @php( $status='label-success' )
                                                @if($for->status==0)
                                                    @php( $status='label-warning' )
                                                @endif
                                                {!! html_entity_decode(link_to('#','<span class="label '.$status.' status-opt" go-to="'.$for->id.'"><i class="fa fa-power-off"></i></span>',['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.status_info')])) !!}
                                                {!! html_entity_decode(link_to_route('adm.zone.edit','<span class="label label-info"><i class="fa fa-pencil"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.edit_info')])) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(Request::only(['search']))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/zone/index.js') !!}
@endsection