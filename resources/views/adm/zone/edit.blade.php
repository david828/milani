@extends('adm.layouts.page')

@section('title')
    {!! trans('app.zone') !!}
@endsection
@section('cssBottom')
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.zone') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.zone.index',trans('app.zone')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.zone.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <h4>{!! trans('app.price_ship').': '.$data->name !!}</h4>
                    <div class="form-group">
                        {!! Form::label('tx_price',trans('app.price')) !!} *
                        {!! Form::number('tx_price',$data->price,['class'=>'form-control','id'=>'tx_price','required','number','min'=>'0','minlength'=>'0']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::checkbox('ck_ship','ship',($data->ship_free == '1')? 'true':'', ['id' => 'ck_ship', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                        {!! Form::label('ck_ship',trans('app.free_ship')) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/zone/edit.js') !!}
@endsection