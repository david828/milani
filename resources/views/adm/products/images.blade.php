@extends('adm.layouts.page')

@section('title')
    {!! trans('app.products') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap3-wysiwyg-master/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
    {!! Html::style('cms/dist/css/dragImages.css') !!}
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.productImage.destroy',$data->id],'method'=>'POST','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('switch',$switch,['id'=>'switch']) !!}
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-order" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('order.title_order') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div id="reorder-helper" class="light_box">
                        {!! trans('order.drag') !!}<br>
                        {!! trans('order.save') !!}
                    </div>
                    <div class="gallery">
                        <ul class="reorder_ul reorder-photos-list" id="galleryOrder">
                            @foreach($images as $items)
                                <li id="image_li_{!! $items->id !!}" class="ui-sortable-handle">
                                    <a href="javascript:void(0);" style="float:none;" class="image_link">
                                        {!! Html::image($data->dir.$items->image,null) !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::open(['route'=>['adm.orderImages'],'method'=>'POST','name'=>'frmOrder','id'=>'frmOrder','role'=>'form','enctype'=>'multipart/form-data']) !!}
                    {!! Form::hidden('table',($switch =='N')?'productgallery':'productcolorimage',['id'=>'table']) !!}
                    {!! Form::hidden('id_array',null,['id'=>'id_array']) !!}
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelOrder','name'=>'bt_cancelOrder','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('site.apply'),['id'=>'btnOrder','name'=>'btnOrder','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-profile" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                {!! Form::open(['route'=>['adm.products.upload',$data->id],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('site.change_profile_image') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div class="panel-body text-center crop-images-d">
                        {!! Html::image('cms/dist/img/article-bg.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body docs-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelProfile','name'=>'bt_cancelProfile','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    @if($switch == 'S')
                        {!! Form::hidden('hn_color',$data->productcolor[0]->id,['id'=>'hn_color']) !!}
                    @endif
                    {!! Form::hidden('switch',$switch,['id'=>'switch']) !!}
                    {!! Form::submit(trans('site.apply'),['id'=>'articleCrop','name'=>'articleCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade alt-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.imagealt'],'method'=>'POST','name'=>'frmAlt','id'=>'frmAlt','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('products.change_alt') !!}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('tx_alt','alt') !!}
                        {!! Form::text('tx_alt',null,['class'=>'form-control','id'=>'tx_alt']) !!}
                    </div>
                    {!! Form::hidden('alt_switch',$switch,['id'=>'alt_switch']) !!}
                    {!! Form::hidden('hd_image',null,['id'=>'hd_image']) !!}
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancelA','id'=>'bt_cancelA','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_Alt','id'=>'bt_Alt','class'=>'btn btn-success','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        @section('here') {!! trans('app.products') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.products.index',trans('app.products')) !!}</li>
            <li>{!! trans('app.images') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            <div class="box box-primary"  style="display: {!! ($switch == 'S')? 'block':'none' !!}">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! $data->name !!}</h3>
                    <div class="form-group">
                        {!! Form::label('sl_color',trans('app.colors')) !!}
                        <select name="sl_color" class="form-control select2" id="sl_color">
                            <option value="">{!! trans('app.select') !!}</option>
                            @foreach($selectcolor as $c)
                                <option value="{!! $c->id_color !!}" image="{!! $c->color[0]->image !!}" {!! ($c->id_color == $color)? 'selected':'' !!}>
                                    {!! $c->color[0]->name !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @if((!is_null($color)) || ($switch =='N'))
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.upload_image') !!}</h3>
                </div>
                <div class="box-body">
                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*', 'motor' => '0']) !!}
                            <div class="input-file-button" data-toggle="tooltip">
                                <i class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                        </label>
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.product_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <div class="user-block">
                                {!! Html::image('assets/images/gallery.png',$data->name,['class'=>'img-circle']) !!}
                                <span class="username">{!! $data->name !!}</span>
                                <button type="button" class="btn-primary" style="margin-bottom: 10px; float:right;" id="btnReorder" >
                                    <i class="fa fa-bars"></i>&nbsp;{!! trans('order.reorder') !!}
                                </button>
                            </div>
                        </div>
                        <div class="box-body text-justify">
                            <div id="gallery">
                                @foreach($images as $items)
                                    <div class="col-xs-4 col-md-4" id="img{!! $items->id !!}">
                                        <div class="content-collect mb-20">
                                            {!! Html::image($data->dir.$items->image,'imagen') !!}
                                            <div class="content-collect-options">
                                                <i class="fa fa-eye bg-blue view-image"
                                                   img-target="{!! $data->dir.$items->image !!}"></i>
                                                <i class="fa fa-font bg-green view-alt" id="alt_{!! $items->id !!}"
                                                   img-target="{!! $items->id !!}" img-alt="{!! $items->alt !!}"></i>
                                                <i class="fa fa-trash-o bg-red delete-image"
                                                   img-target="{!! $items->id !!}"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-picture-o"></i>
                            <h3 class="box-title">{!! trans('app.image_viewer') !!}</h3>
                        </div>
                        <div class="box-body">
                            <div class="content-viewer">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/products/images.js') !!}
    {!! Html::script('cms/plugins/jQueryUI/jquery-ui.js') !!}
    {!! Html::script('cms/dist/js/dragImages.js') !!}
@endsection