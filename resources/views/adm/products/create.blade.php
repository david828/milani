@extends('adm.layouts.page')

@section('title')
    {!! trans('app.products') !!}
@endsection

@section('cssTop')
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.products') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.products.index',trans('app.products')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.products.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            {!! Form::checkbox('ck_featured','0','', ['id' => 'ck_featured', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                            {!! Form::label('ck_featured',trans('products.featureds')) !!}
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {!! Form::checkbox('ck_best','0','', ['id' => 'ck_best', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                            {!! Form::label('ck_best',trans('products.best_seller')) !!}
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {!! Form::checkbox('ck_new','0','', ['id' => 'ck_new', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                            {!! Form::label('ck_new',trans('products.new')) !!}
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {!! Form::checkbox('ck_soon','0','', ['id' => 'ck_soon', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                            {!! Form::label('ck_soon',trans('products.soon')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_category',trans('app.category')) !!} *
                        {!! Form::select('sl_category',$cat,null,['class'=>'form-control select2','id'=>'sl_category','required']) !!}
                    </div>
                    <div class="charger out">
                        <span>{!! trans('app.loading') !!}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_subcategory',trans('app.subcategory')) !!}
                        {!! Form::select('sl_subcategory',[],null,['class'=>'form-control select2','id'=>'sl_subcategory','placeholder' => trans('app.select'), 'disabled']) !!}
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                {!! Form::label('sl_color',trans('app.colors')) !!}
                                <select name="sl_color[]" class="form-control select2" multiple="multiple" id="sl_color">
                                    @foreach($colors as $c)
                                        <option value="{!! $c->id !!}" image="{!! $c->image !!}">
                                            {!! $c->name !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                {!! Form::label('sl_finish',trans('app.finish')) !!}
                                {!! Form::select('sl_finish',$finish,null,['class'=>'form-control select2','id'=>'sl_finish']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('products.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_price',trans('products.price')) !!} *
                        {!! Form::number('tx_price',null,['class'=>'form-control','id'=>'tx_price','required']) !!}
                    </div>
                    <div class="form-group" style="margin-bottom: 0">
                        {!! Form::checkbox('ck_sale','0','', ['id' => 'ck_sale', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                        {!! Form::label('ck_sale',trans('products.sale')) !!}
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon">%</span>
                        {!! Form::number('tx_sale_percent',null,['class'=>'form-control','id'=>'tx_sale_percent','min'=>'0','max'=>'100','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_description',trans('products.description')) !!} *
                        {!! Form::textarea('ta_description',null,['class'=>'form-control','id'=>'ta_description','rows'=>'2','required', 'maxlength' => '800']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_review',trans('products.review')) !!}
                        {!! Form::textarea('ta_review',null,['class'=>'form-control','id'=>'ta_review','required','rows'=>'2']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_metatitle',trans('products.metatitle')) !!}
                        {!! Form::text('tx_metatitle',null,['class'=>'form-control','id'=>'tx_metatitle']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_metadesc',trans('app.metadescription')) !!}
                        {!! Form::textarea('ta_metadesc',null,['class'=>'form-control','id'=>'ta_metadesc','rows'=>2]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_metatags',trans('app.metatags')) !!}
                        {!! Form::text('tx_metatags',null,['class'=>'form-control','id'=>'tx_metatags']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    <div id="divNext">
                        {!! Form::button(trans('app.next'),['class'=>'btn btn-primary','name'=>'bt_next','id'=>'bt_next']) !!}
                    </div>
                    <div id="divStock"></div>
                    <div id="divApply" style="display: none;">
                        {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3al1mo8b88yduuh3a908fpkkhcp5a7byljbyuz9zr8smqb8p') !!}
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/dist/js/app/products/create.js') !!}
    {!! Html::script('cms/dist/js/app/products/products.js') !!}
@endsection