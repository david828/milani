@extends('adm.layouts.page')

@section('title')
    {!! trans('app.products') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.products.destroy','IDX'],'method'=>'DELETE','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.products') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.products') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                            <div class="box-tools" style="position:relative; float:right">
                                {!! Form::model(Request::only(['buscar']), ['route'=>'adm.products.index', 'style'=>'width: 300px;', 'method'=>'GET', 'rol'=>'search']) !!}
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <div class="input-group">
                                            {!! Form::text('search',$list->search,['class'=>'form-control input-sm pull-right', 'placeholder'=>''.trans("ph.search").'']) !!}
                                            <div class="input-group-btn">
                                                {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        {!! Form::checkbox('ck_featured','1',($list->bs == '1')? 'true' : '', ['id' => 'ck_featured', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                                        {!! Form::label('ck_featured',trans('products.featureds')) !!}
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        {!! Form::checkbox('ck_new','1',($list->n == '1')? 'true' : '', ['id' => 'ck_new', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                                        {!! Form::label('ck_new',trans('products.new')) !!}
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        {!! Form::checkbox('ck_soon','1',($list->so == '1')? 'true' : '', ['id' => 'ck_soon', 'style' => 'margin-top: 10px; margin-bottom: 10px;']) !!}
                                        {!! Form::label('ck_soon',trans('products.soon')) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {!! Form::open(['route'=>['adm.products.best'],'method'=>'POST','name'=>'frmBest','id'=>'frmBest','role'=>'form']) !!}
                        {!! Form::hidden('hn_best',null,['id'=>'hn_best']) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['route'=>['adm.products.new'],'method'=>'POST','name'=>'frmNew','id'=>'frmNew','role'=>'form']) !!}
                        {!! Form::hidden('hn_new',null,['id'=>'hn_new']) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['route'=>['adm.products.soon'],'method'=>'POST','name'=>'frmSoon','id'=>'frmSoon','role'=>'form']) !!}
                        {!! Form::hidden('hn_soon',null,['id'=>'hn_soon']) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['route'=>['adm.products.status'],'method'=>'POST','name'=>'frmEst','id'=>'frmEst','role'=>'form']) !!}
                        {!! Form::hidden('hn_status',null,['id'=>'hn_status']) !!}
                        {!! Form::close() !!}
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>No.</th>
                                        <th>{!! trans('app.id') !!}</th>
                                        <th>{!! trans('app.category') !!}</th>
                                        <th>{!! trans('products.name') !!}</th>
                                        <th title="{!! trans('products.featureds') !!}">{!! trans('products.featureds.') !!}</th>
                                        <th title="{!! trans('products.new') !!}">{!! trans('products.new.') !!}</th>
                                        <th title="{!! trans('products.soon') !!}">{!! trans('products.soon.') !!}</th>
                                        <th title="{!! trans('products.status') !!}">{!! trans('products.status.') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @php $count=1 @endphp
                                    @foreach($list as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $loop->iteration !!}</td>
                                            <td>{!! $for->id !!}</td>
                                            <td>{!! (count($for->category) > 0)? $for->category[0]->name : ''!!}</td>
                                            <td>{!! $for->name !!}</td>
                                            <td>
                                                @php( $best='label-success' )
                                                @if($for->featured==0)
                                                    @php( $best='label-warning' )
                                                @endif
                                                {!! html_entity_decode(link_to('#','<span class="label '.$best.' best-opt" go-to="'.$for->id.'"><i class="fa fa-exchange"></i></span>',['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('products.featureds')])) !!}
                                            </td>
                                            <td>
                                                @php( $new='label-success' )
                                                @if($for->new==0)
                                                    @php( $new='label-warning' )
                                                @endif
                                                {!! html_entity_decode(link_to('#','<span class="label '.$new.' new-opt" go-to="'.$for->id.'"><i class="fa fa-exchange"></i></span>',['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('products.new')])) !!}
                                            </td>
                                            <td>
                                                @php( $soon='label-success' )
                                                @if($for->soon==0)
                                                    @php( $soon='label-warning' )
                                                @endif
                                                {!! html_entity_decode(link_to('#','<span class="label '.$soon.' soon-opt" go-to="'.$for->id.'"><i class="fa fa-exchange"></i></span>',['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('products.soon')])) !!}
                                            </td>
                                            <td>
                                                @php( $status='label-success' )
                                                @if($for->status==0)
                                                    @php( $status='label-warning' )
                                                @endif
                                                {!! html_entity_decode(link_to('#','<span class="label '.$status.' status-opt" go-to="'.$for->id.'"><i class="fa fa-power-off"></i></span>',['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.status_info')])) !!}
                                            </td>
                                            <td>
                                                {!! html_entity_decode(link_to_route('adm.products.images','<span class="label label-success"><i class="fa fa-picture-o"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.show_gallery')])) !!}
                                                {!! html_entity_decode(link_to_route('adm.products.edit','<span class="label label-info"><i class="fa fa-pencil"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.edit_info')])) !!}
                                                <span class="label label-danger span-link trash-data"
                                                      trash="{!! $for->id !!}" data-toggle="tooltip"
                                                      data-placement="left"
                                                      title="{!! trans('app.delete_info') !!}">
                                                    <i class="fa fa-trash"></i>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(['search' => Request::get('search'), 'ck_featured' => Request::get('ck_featured'), 'ck_new' => Request::get('ck_new'), 'ck_soon' => Request::get('ck_soon')])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="add-data">
        {!! html_entity_decode(link_to_route('adm.products.create','<i class="fa fa-plus"></i> '.trans('app.register'),null,['class'=>'btn btn-app bg-aqua'])) !!}
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/products/index.js') !!}
@endsection