@extends('adm.layouts.page')

@section('title')
    {!! trans('app.categories') !!}
@endsection

@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.categories') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.products.index', trans('app.products')) !!}</li>
            <li>{!! link_to_route('adm.categoriesP.index',trans('app.categories')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.categoriesP.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('products.name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                        {!! Form::hidden('tx_id',$data->id,['id' => 'tx_id']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_metadesc',trans('app.metadescription')) !!}
                        {!! Form::textarea('ta_metadesc',$data->metadescription,['class'=>'form-control','id'=>'ta_metadesc','rows'=>2]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_metatags',trans('app.metatags')) !!}
                        {!! Form::text('tx_metatags',$data->metatags,['class'=>'form-control','id'=>'tx_metatags']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/dist/js/app/products/categories/edit.js') !!}
@endsection