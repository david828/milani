@extends('adm.layouts.page')

@section('title')
    {!! trans('app.categories') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.categoriesP.destroy','IDX'],'method'=>'DELETE','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.categories') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.products.index', trans('app.products')) !!}</li>
            <li>{!! trans('app.categories') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                            <div class="box-tools">
                                {!! Form::model(Request::only(['buscar']), ['route'=>'adm.categoriesP.index', 'style'=>'width: 150px;', 'method'=>'GET', 'rol'=>'search']) !!}
                                <div class="input-group">
                                    {!! Form::text('search',$list->search,['class'=>'form-control input-sm pull-right', 'placeholder'=>''.trans("ph.search").'']) !!}
                                    <div class="input-group-btn">
                                        {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.id') !!}</th>
                                        <th>{!! trans('products.name') !!}</th>
                                        <th>{!! trans('products.order') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @php $count=1 @endphp
                                    @foreach($list as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $for->id !!}</td>
                                            <td>{!! $for->name !!}</td>
                                            <td>
                                                {!! Form::number('tx_order'.$for->id,$for->order,['id'=>'tx_order'.$for->id,'class'=>'form-control order-field']) !!}
                                                <span class="label label-success span-link order-data"
                                                      data-cat="{!! $for->id !!}"
                                                      data-target="{!! route('categoriesP.order') !!}">
                                                    <i class="fa fa-refresh"></i>
                                                </span>
                                            </td>
                                            <td>
                                                {!! html_entity_decode(link_to_route('adm.categoriesP.edit','<span class="label label-info"><i class="fa fa-pencil"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.edit_info')])) !!}
                                                <span class="label label-danger span-link trash-data"
                                                      trash="{!! $for->id !!}" data-toggle="tooltip"
                                                      data-placement="left"
                                                      title="{!! trans('app.delete_info') !!}">
                                                    <i class="fa fa-trash"></i>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(Request::only(['search']))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="add-data">
        {!! html_entity_decode(link_to_route('adm.categoriesP.create','<i class="fa fa-plus"></i> '.trans('app.register'),null,['class'=>'btn btn-app bg-aqua'])) !!}
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/products/categories/index.js') !!}
@endsection