@extends('adm.layouts.page')

@section('title')
    {!! trans('products.titles') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="content-wrapper">
        @section('here') {!! trans('products.titles') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('products.titles') !!}</li>
        @endsection
        @include('adm.layouts.bread')
            <section class="content width-calc">
                {!! Form::open(['route'=>['adm.feaproducts.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                    </div>
                    <div class="box-body">
                        <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                        <div class="form-group">
                            {!! Form::label('sl_product1',trans('products.featureds')) !!} *
                            {!! Form::text('sl_product1',$data->title_featureds,['class'=>'form-control','id'=>'sl_product1','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sl_product2',trans('products.new')) !!} *
                            {!! Form::text('sl_product2',$data->title_news,['class'=>'form-control','id'=>'sl_product2','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sl_product3',trans('products.best_seller')) !!} *
                            {!! Form::text('sl_product3',$data->title_bests,['class'=>'form-control','id'=>'sl_product3','required']) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/products/feature.js') !!}
@endsection