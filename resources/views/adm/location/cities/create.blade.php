@extends('adm.layouts.page')

@section('title')
    {!! trans('app.cities') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.cities') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.city.index',trans('app.cities')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.city.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_country',trans('app.country'),['class'=>'control-label']) !!} *
                        {!! Form::select('sl_country',$data->countryList,null,['id'=>'sl_country','class'=>'form-control select2','required','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_state',trans('app.state'),['class'=>'control-label']) !!} *
                        {!! Form::hidden('hn_state',url('adm/state/IDX/search'),['id'=>'hn_state']) !!}
                        {!! Form::select('sl_state',[],null,['id'=>'sl_state','class'=>'form-control select2','required','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_zone',trans('app.zone'),['class'=>'control-label']) !!} *
                        {!! Form::select('sl_zone',$data->zoneList,null,['id'=>'sl_zone','class'=>'form-control select2','required','number']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/location/cities/create.js') !!}
@endsection