@extends('adm.layouts.page')

@section('title')
    {!! trans('app.countries') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.countries') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.country.index',trans('app.countries')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.country.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_code',trans('app.code')) !!} *
                        {!! Form::number('tx_code',null,['class'=>'form-control','id'=>'tx_code','required','number','min'=>'0','minlength'=>'0']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_isocode',trans('app.isocode')) !!} *
                        {!! Form::text('tx_isocode',null,['class'=>'form-control','id'=>'tx_isocode','required']) !!}
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/location/countries/create.js') !!}
@endsection