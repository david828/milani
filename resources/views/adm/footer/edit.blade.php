@extends('adm.layouts.page')

@section('title')
    {!! trans('app.footer') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.footer') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.footer') !!}</li>
        @endsection
        @include('adm.layouts.bread')
            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                    </div>
                    {!! Form::open(['route'=>['adm.footer.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                    <div class="box-body">
                        <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::text('tx_title_1',$data->title_1,['class'=>'form-control','id'=>'tx_title_1']) !!}
                                <br>
                                {!! Form::textarea('ta_text_1',$data->text_1,['class'=>'form-control','id'=>'ta_text_1','required','maxlength' => '150', 'rows' => '4']) !!}
                            </div>
                            <div class="col-md-4">
                                <div class="bloque" style="height: 100%;">
                                    <h4>{!! trans('app.bloq') . ' - ' . trans('app.contact') !!}</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h4>PayU:</h4>
                                <br>
                                {!! Form::textarea('ta_text_2',$data->text_2,['class'=>'form-control','id'=>'ta_text_2','required', 'rows' => '4']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/footer/index.js') !!}
@endsection