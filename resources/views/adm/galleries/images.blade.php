@extends('adm.layouts.page')

@section('title')
    {!! trans('app.galleries') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap3-wysiwyg-master/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.galleryImage.destroy',$data->id],'method'=>'POST','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-profile" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.galleries.upload',$data->id],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('site.change_profile_image') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div class="panel-body text-center crop-images-d">
                        {!! Html::image('cms/dist/img/article-bg.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body docs-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelProfile','name'=>'bt_cancelProfile','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                    {!! Form::submit(trans('site.apply'),['id'=>'articleCrop','name'=>'articleCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.galleries') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.galleries.index',trans('app.galleries')) !!}</li>
            <li>{!! trans('app.images') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.upload_image') !!}</h3>
                </div>
                <div class="box-body">
                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip"><i
                                        class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                        </label>

                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.gallery_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <div class="user-block">
                                {!! Html::image('assets/images/gallery.png',$data->name,['class'=>'img-circle']) !!}
                                <span class="username">{!! $data->name !!}</span>
                            </div>
                        </div>
                        <div class="box-body text-justify">
                            <div id="gallery">
                                @foreach($data->images as $items)
                                    <div class="col-xs-4 col-md-4" id="img{!! $items->id !!}">
                                        <div class="content-collect mb-20">
                                            {!! Html::image($data->dir.$items->image,null) !!}
                                            <div class="content-collect-options">
                                                <i class="fa fa-eye bg-blue view-image"
                                                   img-target="{!! $data->dir.$items->image !!}"></i>
                                                <i class="fa fa-trash-o bg-red delete-image"
                                                   img-target="{!! $items->id !!}"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-picture-o"></i>
                            <h3 class="box-title">{!! trans('app.image_viewer') !!}</h3>
                        </div>
                        <div class="box-body">
                            <div class="content-viewer">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/galleries/images.js') !!}
@endsection