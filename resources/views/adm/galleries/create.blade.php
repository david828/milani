@extends('adm.layouts.page')

@section('title')
    {!! trans('app.galleries') !!}
@endsection

@section('cssBottom')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.galleries') !!}@endsection
        @section('breadcumb')
        <li>{!! link_to_route('adm.galleries.index',trans('app.galleries')) !!}</li>
        <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            {!! Form::open(['route'=>['adm.galleries.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_categories',trans('app.blogcategories')) !!} *
                        {!! Form::select('sl_categories',$data->categories,null,['class'=>'form-control select2','data-placeholder'=>trans('app.select'),'id'=>'sl_categories','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_description',trans('app.description')) !!} *
                        {!! Form::textarea('ta_description',null,['class'=>'form-control','id'=>'ta_description','required','rows'=>'3']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>

            </div>
            {!! Form::close() !!}

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/maskedInput/jquery.maskedinput.min.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/galleries/create.js') !!}
@endsection