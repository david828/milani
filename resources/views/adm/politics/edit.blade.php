@extends('adm.layouts.page')

@section('title')
    {!! trans('app.politics') !!}
@endsection
@section('cssBottom')
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.politics') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.politics.update','id'=>'1'],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('ta_text',trans('app.edit')) !!}
                        {!! Form::textarea('ta_text',$data->text,['class'=>'form-control','id'=>'ta_text']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3al1mo8b88yduuh3a908fpkkhcp5a7byljbyuz9zr8smqb8p') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/politics/edit.js') !!}
@endsection