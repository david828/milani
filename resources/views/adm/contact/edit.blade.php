@extends('adm.layouts.page')

@section('title')
    {!! trans('app.contact') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.contact') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.contact.index',trans('app.contact')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                    {!! Form::hidden('hn_emails',2,['id'=>'hn_emails']) !!}
                </div>
                {!! Form::open(['route'=>['adm.contact.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_maptype',trans('app.map_type')) !!} *
                        {!! Form::select('sl_maptype',$data->map,$data->maptype,['class'=>'form-control','id'=>'sl_maptype','required']) !!}
                    </div>
                    <div class="form-group framemap">
                        {!! Form::label('ta_frame',trans('app.framemap')) !!} *
                        {!! Form::textarea('ta_frame',$data->framemap,['class'=>'form-control','id'=>'ta_frame','rows'=>2]) !!}
                    </div>
                    <div class="form-group coordinates">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('tx_api_google',trans('contact.api_google')) !!} *
                                    {!! Form::text('tx_api_google',$data->api_google,['class'=>'form-control','id'=>'tx_api_google', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('tx_latitude',trans('app.latitude')) !!} *
                                    {!! Form::text('tx_latitude',$data->latitude,['class'=>'form-control','id'=>'tx_latitude', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('tx_longitude',trans('app.longitude')) !!} *
                                    {!! Form::text('tx_longitude',$data->longitude,['class'=>'form-control','id'=>'tx_longitude', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_metadesc',trans('app.metadescription')) !!}
                        {!! Form::textarea('ta_metadesc',$data->metadescription,['class'=>'form-control','id'=>'ta_metadesc','rows'=>2]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_tags',trans('app.metatags')) !!}
                        {!! Form::text('tx_tags',$data->metatags,['class'=>'form-control','id'=>'tx_tags']) !!}
                    </div>
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.receivers') !!}</h3>
                </div>
                <div class="box-body">

                    @foreach($data->emails as $item)
                        <div class="closer-rmail-{!! $item->id !!} closer-data-fields">
                            @if($loop->index==0)
                                <div class="form-group">
                                    {!! Form::label('tx_remail'.$item->id,trans('app.email'),['class'=>'label-mail','title'=>trans('app.email')]) !!}
                                    *
                                    {!! Form::email('tx_remail['.$item->id.']',$item->email,['class'=>'form-control','id'=>'tx_remail'.$item->id,'required']) !!}
                                </div>
                            @else
                                <div class="form-group">
                                    {!! Form::label('tx_remail'.$item->id,trans('app.email'),['class'=>'label-mail','title'=>trans('app.email')]) !!}
                                    *
                                    <div class="input-group">
                                        {!! Form::email('tx_remail['.$item->id.']',$item->email,['class'=>'form-control','id'=>'tx_remail'.$item->id,'required']) !!}
                                        <span class="input-group-addon close-rmail" go-to="{!! $item->id !!}"><i
                                                    class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                    <div class="add-mails">
                        <button class="btn btn-primary pull-right add-mail" type="button">
                            <i class="fa fa-plus"></i> {!! trans('app.add_receiver') !!}</button>
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/contact/edit.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
@endsection