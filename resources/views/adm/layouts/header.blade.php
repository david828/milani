<header class="main-header">
    <!-- Logo -->
    <a href="/adm" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>{!! appData()->name  !!}</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{!! trans('app.navigation') !!}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {!! Html::image('cms/dist/img/user.jpg',null,['class'=>'user-image']) !!}
                        <span class="hidden-xs">{!! userData()->name !!}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            {!! Html::image('cms/dist/img/user.jpg',null,['class'=>'img-circle']) !!}
                            <p>
                                {!! userData()->name !!}
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                {!! link_to_route('adm.profile',trans('app.profile'),null,['class'=>'btn btn-default btn-flat']) !!}
                            </div>
                            <div class="pull-right">
                                {!! link_to('/logout',trans('app.logout'),['class'=>'btn btn-default btn-flat']) !!}
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>