<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! appData()->name !!} - @yield('title', appData()->name)</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {!! Html::style('cms/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@yield('cssTop')
{!! Html::style('cms/dist/css/AdminLTE.min.css') !!}
{!! Html::style('cms/dist/css/general.css') !!}
{!! Html::style('cms/dist/css/skins/_all-skins.min.css') !!}
@yield('cssBottom')
    <link rel="icon" href="{!! url('assets/images/favicon.png') !!}" sizes="32x32"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="loading-app">
    <div class="bg-load"></div>
    <div class="text-load">
        {!! trans('app.loading') !!}
    </div>
</div>

<!-- Site wrapper -->
<div class="wrapper">
    @include('adm.layouts.header')
    @include('adm.layouts.menu')
    @yield('content')

    <footer class="main-footer">

        <strong><a href="#">{!! appData()->name !!}</a>.</strong>
    </footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
{!! Html::script('cms/plugins/jQuery/jQuery-2.1.4.min.js') !!}
<!-- Bootstrap 3.3.5 -->
{!! Html::script('cms/bootstrap/js/bootstrap.min.js') !!}
<!-- SlimScroll -->
{!! Html::script('cms/plugins/slimScroll/jquery.slimscroll.min.js') !!}
<!-- FastClick -->
{!! Html::script('cms/plugins/fastclick/fastclick.min.js') !!}
<!-- AdminLTE App -->
{!! Html::script('cms/dist/js/app.min.js') !!}
<!-- AdminLTE for demo purposes -->
{!! Html::script('cms/dist/js/demo.js') !!}
{!! Html::script('cms/dist/js/display-errors.js') !!}
{!! Html::script('cms/dist/js/plugins.js') !!}
@yield('jsBottom')
</body>
</html>