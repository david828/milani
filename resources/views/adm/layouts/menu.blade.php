<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                {!! Html::image('cms/dist/img/user.jpg',null,['class'=>'img-circle']) !!}
            </div>
            <div class="pull-left info">
                <p>{!! userData()->firstname !!}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{!! trans('app.main_nav') !!}</li>
            <li>{!! html_entity_decode(link_to('https://oroogga.ngdesk.com ','<i class="fa fa-question-circle-o"></i> <span>'.trans('app.helpdesk').'</span>',['target'=>'_blank'])) !!}</li>
            <li>
                {!! html_entity_decode(link_to('/adm/home','<i class="fa fa-dashboard"></i> <span>'.trans('app.dashboard').'</span>')) !!}
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>{!! trans('app.configuration') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->role == 'S')
                        <li>{!! html_entity_decode(link_to_route('adm.app.configure','<i class="fa fa-cogs"></i> <span>'.trans('app.generics').'</span>')) !!}</li>
                    @endif
                    <li>{!! html_entity_decode(link_to_route('adm.seodata.configure','<i class="fa fa-search"></i> <span>'.trans('app.seo').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.appsocial','<i class="fa fa-share-alt"></i> <span>'.trans('app.social_network').'</span>')) !!}</li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>{!! trans('app.politics_menu') !!}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>{!! html_entity_decode(link_to_route('adm.politics.index','<i class="fa fa-book"></i> <span>'.trans('app.politics').'</span>')) !!}</li>
                            <li>{!! html_entity_decode(link_to_route('adm.conditions.index','<i class="fa fa-book"></i> <span>'.trans('app.conditions').'</span>')) !!}</li>
                        </ul>
                    </li>
                    <li>{!! html_entity_decode(link_to_route('adm.contact.index','<i class="fa fa-envelope"></i> <span>'.trans('app.contact').'</span>')) !!}</li>
                </ul>
            </li>
            @if(Auth::user()->role == 'S')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>{!! trans('app.pays') !!}</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>{!! html_entity_decode(link_to_route('adm.invoice.index','<i class="fa fa-money"></i> <span>'.trans('app.invoicing').'</span>')) !!}</li>
                        <li>{!! html_entity_decode(link_to_route('adm.payu.index','<i class="fa fa-credit-card"></i> <span>'.trans('app.payu').'</span>')) !!}</li>
                    </ul>
                </li>
                <li>
                    {!! html_entity_decode(link_to_route('adm.admin.index','<i class="fa fa-user"></i> <span>'.trans('app.users').'</span>')) !!}
                </li>
            @endif
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-map"></i>
                    <span>{!! trans('app.locations') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.country.index','<i class="fa fa-map-marker"></i>'.trans('app.countries').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.state.index','<i class="fa fa-map-marker"></i>'.trans('app.states').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.city.index','<i class="fa fa-map-marker"></i>'.trans('app.cities').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.zone.index','<i class="fa fa-map-marker"></i> <span>'.trans('app.zone').'</span>')) !!}</li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bookmark"></i>
                    <span>{!! trans('app.page') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.modals.index','<i class="fa fa-external-link"></i> <span>'.trans('app.modals').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.menu.index','<i class="fa fa-list-alt"></i> <span>'.trans('app.menu').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.header','<i class="fa fa-th-large"></i> <span>'.trans('app.slides').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.productdes.form','<i class="fa fa-line-chart"></i> <span>'.trans('app.product_dest').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.aboutus','<i class="fa fa-university"></i> <span>'.trans('app.aboutus').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.footer','<i class="fa fa-sort-amount-asc"></i> <span>'.trans('app.footer').'</span>')) !!}</li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-briefcase"></i>
                    <span>{!! trans('app.products') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.categoriesP.index','<i class="fa fa-minus-square"></i> <span>'.trans('app.categories').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.subcategories.index','<i class="fa fa-minus-square"></i> <span>'.trans('app.subcategories').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.finish.index','<i class="fa fa-minus-square"></i> <span>'.trans('app.finish').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.colors.index','<i class="fa fa-minus-square"></i> <span>'.trans('app.colors').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.products.index','<i class="fa fa-briefcase"></i> <span>'.trans('app.products').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.feaproducts.index','<i class="fa fa-th"></i> <span>'.trans('products.titles').'</span>')) !!}</li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-archive"></i>
                    <span>{!! trans('app.galleries') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.categoriesgal.index','<i class="fa fa-circle-o"></i>'.trans('app.blogcategories').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.galleries.index','<i class="fa fa-folder-open"></i> <span>'.trans('app.galleries').'</span>')) !!}</li>
                </ul>
            </li>
            <li>
                {!! html_entity_decode(link_to_route('adm.resources.index','<i class="fa fa-file-o"></i> <span>'.trans('app.resources').'</span>')) !!}
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>{!! trans('app.articles') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.categories.index','<i class="fa fa-circle-o"></i>'.trans('app.blogcategories').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.articles.index','<i class="fa fa-newspaper-o"></i> <span>'.trans('app.articles').'</span>')) !!}</li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tag"></i>
                    <span>{!! trans('app.coupons') !!}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>{!! html_entity_decode(link_to_route('adm.coupon.index','<i class="fa fa-tag"></i> <span>'.trans('app.list').'</span>')) !!}</li>
                    <li>{!! html_entity_decode(link_to_route('adm.coupon.historic','<i class="fa fa-history"></i> <span>'.trans('app.historic').'</span>')) !!}</li>
                </ul>
            </li>
            <li>{!! html_entity_decode(link_to_route('adm.distribution.index','<i class="fa fa-book"></i> <span>'.trans('app.distribution').'</span>')) !!}</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>