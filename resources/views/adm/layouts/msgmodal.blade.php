<div class="modal fade" id="msgModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
            </div>
            <div class="modal-body msg-body">

            </div>
            <div class="modal-footer">
                {!! Form::button(trans('app.accept'),['class'=>'btn btn-outline','data-dismiss'=>'modal']) !!}
            </div>
        </div>
    </div>
</div>