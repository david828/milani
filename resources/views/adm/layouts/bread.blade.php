<section class="content-header">
    <h1>
        <small>@yield('here')</small>
    </h1>
    <ol class="breadcrumb">
        <li>{!! html_entity_decode(link_to('/adm','<i class="fa fa-dashboard"></i>'.trans('app.home'))) !!}</li>
        @yield('breadcumb')
    </ol>
</section>