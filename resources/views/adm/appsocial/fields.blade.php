<div class="closer-field-{!! $data->cont !!} closer-data-fields">
    <div class="col-md-12">
        <hr>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('sl_social'.$data->cont,trans('app.social_network')) !!} *
            {!! Form::select('sl_social['.$data->cont.']',$data->socialList,null,['class'=>'form-control','id'=>'sl_social'.$data->cont,'required']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('tx_link'.$data->cont,trans('app.profile')) !!}*
            <div class="input-group">
                {!! Form::text('tx_link['.$data->cont.']',null,['class'=>'form-control','id'=>'tx_link'.$data->cont,'required']) !!}
                <span class="input-group-addon close-field" go-to="{!! $data->cont !!}">
                        <i class="fa fa-times"></i>
                    </span>
            </div>
        </div>
    </div>
</div>