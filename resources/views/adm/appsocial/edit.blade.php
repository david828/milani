@extends('adm.layouts.page')

@section('title')
    {!! trans('app.social_network') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.social_network') !!} @endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.appsocial',trans('app.social_network')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        {!! Form::open(['route'=>['adm.appsocial.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
        <section class="content width-calc">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.social_network') !!}</h3>
                    {!! Form::hidden('hn_fields',(sizeof($data->appsocial)+1),['id'=>'hn_fields']) !!}
                    {!! Form::hidden('hn_link',url('/adm/appsocial/fields/'),['id'=>'hn_link']) !!}
                </div>
                <div class="box-body">
                    <div class="row">
                        @if(!$data->appsocial->isEmpty())
                            @foreach($data->appsocial as $item)
                                <div class="closer-field-{!! $loop->iteration !!} closer-data-fields">
                                    @if($loop->index>0)
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                    @endif
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('sl_social'.$loop->iteration,trans('app.social_network')) !!}
                                            *
                                            {!! Form::select('sl_social['.$loop->iteration.']',$data->socialList,$item->id_social,['class'=>'form-control','id'=>'sl_social'.$loop->iteration,'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('tx_link'.$loop->iteration,trans('app.profile')) !!} *
                                            <div class="input-group">
                                                {!! Form::text('tx_link['.$loop->iteration.']',$item->link,['class'=>'form-control','id'=>'tx_link'.$loop->iteration,'required']) !!}
                                                <span class="input-group-addon close-field"
                                                      go-to="{!! $loop->iteration !!}">
                                                    <i class="fa fa-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="col-md-12 add-fields">
                            <button class="btn btn-primary pull-right add-field" type="button">
                                <i class="fa fa-plus"></i> {!! trans('app.add') !!}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content width-calc">
            <div class="box box-info">
                <div class="box-body">
                    <div class="box-footer">
                        {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                    </div>
                </div>
            </div>
        </section>
        {!! Form::close() !!}
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/appsocial/edit.js') !!}
@endsection