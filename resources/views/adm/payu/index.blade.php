@extends('adm.layouts.page')

@section('title')
    {!! trans('app.payu') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.payu') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.payu') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.payu.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_accountid',trans('payu.accountId')) !!} *
                        {!! Form::number('tx_accountid',$data->accountid,['class'=>'form-control','id'=>'tx_accountid','required','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_merchantid',trans('payu.merchantId')) !!} *
                        {!! Form::number('tx_merchantid',$data->merchantid,['class'=>'form-control','id'=>'tx_merchantid','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_apilogin',trans('payu.apiLogin')) !!} *
                        {!! Form::text('tx_apilogin',$data->apilogin,['class'=>'form-control','id'=>'tx_apilogin','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_apikey',trans('payu.apikey')) !!} *
                        {!! Form::text('tx_apikey',$data->apikey,['class'=>'form-control','id'=>'tx_apikey','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_publickey',trans('payu.publickey')) !!} *
                        {!! Form::text('tx_publickey',$data->publickey,['class'=>'form-control','id'=>'tx_publickey','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_requesturl',trans('payu.requesturl')) !!} *
                        {!! Form::text('tx_requesturl',$data->requesturl,['class'=>'form-control','id'=>'tx_requesturl','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_responseurl',trans('payu.responseurl')) !!} *
                        {!! Form::text('tx_responseurl',$data->responseurl,['class'=>'form-control','id'=>'tx_responseurl','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_confirmationurl',trans('payu.confirmationurl')) !!} *
                        {!! Form::text('tx_confirmationurl',$data->confirmationurl,['class'=>'form-control','id'=>'tx_confirmationurl','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_testing',trans('payu.testing')) !!} *
                        {!! Form::select('sl_testing',available(),$data->testing,['class'=>'form-control select2','id'=>'sl_testing','required']) !!}
                    </div>

                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/payu/index.js') !!}
@endsection