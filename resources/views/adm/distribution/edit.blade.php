@extends('adm.layouts.page')

@section('title')
    {!! trans('app.distribution') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade modal-images" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.add_images') !!}</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body text-center crop-images-d other-w">
                        {!! Html::image('images/app/barcode.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body image-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer image-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelOther','name'=>'bt_cancelOther','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('site.apply'),['id'=>'imageCrop','name'=>'imageCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.distribution') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.distribution.index',trans('app.distribution')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.distribution.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip"><i
                                        class="fa fa-image"></i> {!! trans('app.upload_logo') !!}</div>
                        </label>
                        {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.distribution_image').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('distribution.name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_email',trans('distribution.email')) !!} *
                        {!! Form::email('tx_email',$data->email,['class'=>'form-control','id'=>'tx_email']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_phone',trans('distribution.phone')) !!} *
                        {!! Form::text('tx_phone',$data->phone,['class'=>'form-control','id'=>'tx_phone']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_cellphone',trans('distribution.cellphone')) !!} *
                        {!! Form::text('tx_cellphone',$data->cellphone,['class'=>'form-control','id'=>'tx_cellphone']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_address',trans('distribution.address')) !!} *
                        {!! Form::text('tx_address',$data->address,['class'=>'form-control','id'=>'tx_address']) !!}
                    </div>
                    <div class="form-group selectpicker-wrapper">
                        {!! Form::label('sl_country',trans('distribution.country')) !!}
                        {!! Form::select('sl_country',$data->countryList,$data->id_country,['id'=>'sl_country','class'=>'form-control select2']) !!}
                    </div>
                    <div class="form-group selectpicker-wrapper">
                        {!! Form::label('sl_state',trans('distribution.state')) !!}
                        {!! Form::hidden('hn_state',url('state/IDX/search'),['id'=>'hn_state']) !!}
                        {!! Form::select('sl_state',$data->stateList,$data->id_state,['id'=>'sl_state','class'=>'form-control select2']) !!}
                    </div>
                    <div class="form-group selectpicker-wrapper">
                        {!! Form::label('sl_city',trans('distribution.city')) !!}
                        {!! Form::hidden('hn_city',url('city/IDX/search'),['id'=>'hn_city']) !!}
                        {!! Form::select('sl_city',$data->cityList,$data->id_city,['id'=>'sl_city','class'=>'form-control select2','required','data-placeholder'=>trans('app.city')]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/distribution/edit.js') !!}
@endsection