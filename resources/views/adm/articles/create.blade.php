@extends('adm.layouts.page')

@section('title')
    {!! trans('app.articles') !!}
@endsection

@section('cssBottom')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade modal-profile" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('site.change_profile_image') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div class="panel-body text-center crop-images-d">
                        {!! Html::image('cms/dist/img/article-bg.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body docs-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelProfile','name'=>'bt_cancelProfile','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('site.apply'),['id'=>'articleCrop','name'=>'articleCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.articles') !!}@endsection
        @section('breadcumb')
        <li>{!! link_to_route('adm.articles.index',trans('app.articles')) !!}</li>
        <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            {!! Form::open(['route'=>['adm.articles.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>

                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>

                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip"><i
                                        class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                        </label>
                        {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.article_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>


                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_categories',trans('app.blogcategories')) !!} *
                        {!! Form::select('sl_categories',$data->categories,null,['class'=>'form-control select2','data-placeholder'=>trans('app.select'),'id'=>'sl_categories','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_tags',trans('app.tags')) !!}
                        {!! Form::text('tx_tags',null,['class'=>'form-control','id'=>'tx_tags']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_description',trans('app.description')) !!} *
                        {!! Form::textarea('ta_description',null,['class'=>'form-control','id'=>'ta_description','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_metadesc',trans('app.metadescription')) !!}
                        {!! Form::textarea('ta_metadesc',null,['class'=>'form-control','id'=>'ta_metadesc','rows'=>2]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_metatags',trans('app.metatags')) !!}
                        {!! Form::text('tx_metatags',null,['class'=>'form-control','id'=>'tx_metatags']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>

            </div>
            {!! Form::close() !!}

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3al1mo8b88yduuh3a908fpkkhcp5a7byljbyuz9zr8smqb8p') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/maskedInput/jquery.maskedinput.min.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/articles/create.js') !!}
@endsection