@extends('adm.layouts.page')

@section('title')
    {!! trans('app.articles') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.articles') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.articles.index',trans('app.articles')) !!}</li>
            <li>{!! trans('app.show_info') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <div class="user-block">
                                {!! Html::image('cms/dist/img/user.jpg',null,['class'=>'img-circle']) !!}
                                <span class="username">{!! $data->user[0]->firstname.' '.$data->user[0]->lastname !!}</span>
                                <span class="description">{!! trans('app.published') !!} - {!! $data->date !!}</span>
                            </div>
                        </div>
                        <div class="box-body">
                            <blockquote>
                                <p>
                                <h3>{!! $data->name !!}</h3></p>
                            </blockquote>
                            <p>

                        </div>
                        <div class="box-body text-justify">
                            <p>{!! $data->description !!}</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-body text-center img-100">
                            @if(!is_null($data->image))
                                {!! Html::image($data->image,null) !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
