@extends('adm.layouts.page')

@section('title')
    {!! trans('app.articles') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap3-wysiwyg-master/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.articleComment.destroy','IDX'],'method'=>'DELETE','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.comments') . ': ' . $list->name !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.articles.index',trans('app.articles')) !!}</li>
            <li>{!! trans('app.comments') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->comments->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->comments->lastPage() !!}</span>
                            </h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->comments->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.name') !!}</th>
                                        <th>{!! trans('app.email') !!}</th>
                                        <th>{!! trans('app.comments') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @php $count=1 @endphp
                                    @foreach($list->comments as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $for->name !!}</td>
                                            <td>{!! $for->email !!}</td>
                                            <td>{!! $for->comment !!}</td>
                                            <td>
                                                <span class="label label-danger span-link trash-data"
                                                      trash="{!! $for->id !!}" data-toggle="tooltip"
                                                      data-placement="left"
                                                      title="{!! trans('app.delete_info') !!}">
                                                    <i class="fa fa-trash"></i>
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->comments->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->comments->total() !!}</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/articles/comments.js') !!}
@endsection