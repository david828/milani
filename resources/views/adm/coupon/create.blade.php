@extends('adm.layouts.page')

@section('title')
    {!! trans('app.coupons') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.coupons') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.coupon.index',trans('app.coupons')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.coupon.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_coupon',trans('app.coupon_code')) !!} *
                        {!! Form::text('tx_coupon',null,['class'=>'form-control','id'=>'tx_coupon','required','minlength'=>'4']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_discount',trans('app.discount')) !!} *
                        <div class="input-group">
                            <span class="input-group-addon">%</span>
                            {!! Form::number('tx_discount',null,['class'=>'form-control','id'=>'tx_discount','required','number','min'=>'1','max'=>'100']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('sl_type',trans('app.coupon_type'),['class'=>'control-label']) !!} *
                                {!! Form::select('sl_type',$data,null,['id'=>'sl_type','class'=>'form-control select2','required']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="typeCoupon"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('sl_products',trans('app.apply_products')) !!}
                                {!! Form::select('sl_products[]', $products, [], ['class' => 'form-control select2', 'multiple', 'id'=>'sl_products']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/coupon/create.js') !!}
@endsection