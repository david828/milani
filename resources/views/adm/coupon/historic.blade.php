@extends('adm.layouts.page')

@section('title')
    {!! trans('app.coupons') . ' - ' . trans('app.historic') !!}
@endsection
@section('content')

    <div class="content-wrapper">
        @section('here') {!! trans('app.coupons') . ' - ' . trans('app.historic') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.coupon.index',trans('app.coupons')) !!}</li>
            <li>{!! trans('app.historic') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="box-tools">
                                {!! Form::open(['route'=>'adm.coupon.historic', 'method'=>'GET']) !!}
                                <div>
                                    <div style="width: 32%; display: inline-block;">{!! trans('app.coupon') !!}: </div>
                                    <div style="width: 32%; display: inline-block;">{!! trans('app.from') !!}: </div>
                                    <div style="width: 32%; display: inline-block;">{!! trans('app.to') !!}: </div>
                                </div>
                                <div class="input-group">
                                    {!! Form::select('c',$listCoupon,$c,['id'=> 'c', 'class'=>'form-control input-sm', 'style' => 'width:32%; margin:0 5px;']) !!}
                                    {!! Form::date('d',$d,['id'=> 'd', 'class'=>'form-control input-sm', 'style' => 'width:32%; margin:0 5px;', 'placeholder' => 'Desde']) !!}
                                    {!! Form::date('h',$h,['id'=> 'h', 'class'=>'form-control input-sm', 'style' => 'width:32%; margin:0 5px;']) !!}
                                    <div class="input-group-btn">
                                        {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit', 'style' => 'background-color: #3c8dbc; color: #fff']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="box-body table-responsive" style="padding: 40px 0;">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <th>{!! trans('app.coupon') !!}</th>
                                            <th>{!! trans('app.invoice') !!}</th>
                                            <th>{!! trans('app.percent') !!}</th>
                                            <th>{!! trans('app.brutal_value') !!}</th>
                                        </tr>
                                        @foreach($list as $for)
                                        <tr>
                                            <td>{!! $for->coupon !!}</td>
                                            <td>{!! html_entity_decode(link_to_route('adm.invoice.show',$for->invoice,['id'=>$for->relInvoice->id], ['target' => '_blank'])) !!}</td>
                                            <td>{!! $for->percent !!}%</td>
                                            <td>{!! $for->value !!}</td>
                                            <td>{!! $for->date !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>
                            {!! $list->appends(['c' => Request::get('c'),'d' =>  Request::get('d'),'h' =>  Request::get('h')])->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/coupon/index.js') !!}
@endsection