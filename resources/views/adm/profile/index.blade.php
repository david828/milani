@extends('adm.layouts.page')

@section('title')
    {!! trans('app.profile') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="content-wrapper" style="min-height: 1126px;">
        @section('here') {!! trans('app.profile') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.profile') !!}</li>
        @endsection
        @include('adm.layouts.bread')


        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            {!! Html::image('cms/dist/img/user.jpg',null,['class'=>'profile-user-img img-responsive img-circle']) !!}
                            <h3 class="profile-username text-center">{!! $data->name !!}</h3>
                            <p class="text-muted text-center">{!! $data->email !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#formData" data-toggle="tab">{!! trans('user.data') !!}</a></li>
                            <li><a href="#pass" data-toggle="tab">{!! trans('user.password') !!}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="formData">

                                {!! Form::open(['route'=>['adm.profile.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                                <div class="box-body">
                                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                                    <div class="form-group">
                                        {!! Form::label('tx_name',trans('user.name')) !!} *
                                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tx_email',trans('user.email')) !!} *
                                        {!! Form::email('tx_email',$data->email,['class'=>'form-control','id'=>'tx_email','required','email']) !!}
                                    </div>
                                </div>
                                <div class="box-footer">
                                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane" id="pass">
                                {!! Form::open(['route'=>['adm.profile.password'],'method'=>'POST','name'=>'frmPass','id'=>'frmPass','role'=>'form']) !!}
                                <div class="box-body">
                                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                                    <div class="form-group">
                                        {!! Form::label('tx_password',trans('user.new_password')) !!} *
                                        {!! Form::password('tx_password',['class'=>'form-control','id'=>'tx_password','required','minlength'=>'8']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('tx_rpassword',trans('user.repeat_password')) !!} *
                                        {!! Form::password('tx_rpassword',['class'=>'form-control','id'=>'tx_rpassword','required','equalTo'=>'#tx_password','minlength'=>'8']) !!}
                                    </div>
                                </div>
                                <div class="box-footer">
                                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'p_apply','id'=>'p_apply']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/profile/index.js') !!}

@endsection