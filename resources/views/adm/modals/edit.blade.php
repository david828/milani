@extends('adm.layouts.page')

@section('title')
    {!! trans('app.modals') !!}
@endsection

@section('cssBottom')
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade modal-images modal-images" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.add_images') !!}</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body text-center crop-images-d other-w">
                        {!! Html::image('images/app/barcode.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body image-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer image-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelOther','name'=>'bt_cancelOther','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('site.apply'),['id'=>'imageCrop','name'=>'imageCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
            </div>
        </div>
    </div>


    <div class="content-wrapper">
        @section('here') {!! trans('app.modals') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.modals.index',trans('app.modals')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">
            {!! Form::open(['route'=>['adm.modals.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>

                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>

                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip"><i class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                        </label>
                        {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.modal_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!}
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ta_text',trans('app.text')) !!}
                        {!! Form::textarea('ta_text',$data->text,['class'=>'form-control','id'=>'ta_text']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_button',trans('app.text_button')) !!}
                        {!! Form::text('tx_button',$data->button,['class'=>'form-control','id'=>'tx_button']) !!}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('sl_typelink',trans('header.typelink')) !!}
                                {!! Form::select('sl_typelink',[''=>trans('app.select'),'0'=>trans('header.int'),'1'=>trans('header.ext')],$data->typelink,['class'=>'form-control','id'=>'sl_typelink']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sl_typelink">
                                    <span id="intMsg" style="display: none">{!! appData()->urldata !!}/</span>
                                    <span id="extMsg" style="display: none">{!! trans('app.link') !!}:</span>
                                </label>
                                {!! Form::text('tx_link',$data->link,['class'=>'form-control','id'=>'tx_link']) !!}
                                <p>{!! trans('app.text_link') !!}</p>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <h3>{!! trans('app.page_to_open') !!}</h3>
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::select('sl_view',$data->view,$data->part1,['class'=>'form-control select2','id'=>'sl_view']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::select('sl_products',$data->products,$data->part2,['class'=>'form-control select2','id'=>'sl_products' ]) !!}
                        </div>
                        {!! Form::hidden('tx_url',$data->url,['id' => 'hd_url']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
            </div>
            {!! Form::close() !!}

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3al1mo8b88yduuh3a908fpkkhcp5a7byljbyuz9zr8smqb8p') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/maskedInput/jquery.maskedinput.min.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/modals/edit.js') !!}
@endsection