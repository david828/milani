@extends('adm.layouts.page')

@section('title')
    {!! trans('app.seo') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
    {!! Html::style('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="content-wrapper">
        @section('here') {!! trans('app.seo') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')

        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    {!! Form::open(['route'=>['adm.seodata.update',$data->id],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                        </div>

                        <div class="box-body">
                            <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                            <div class="form-group">
                                {!! Form::label('tx_name',trans('app.application_name')) !!} *
                                {!! Form::text('tx_name',$data->application_name,['class'=>'form-control','id'=>'tx_name','required','maxlength'=>20]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('tx_charset',trans('app.charset')) !!} *
                                {!! Form::text('tx_charset',$data->charset,['class'=>'form-control','id'=>'tx_charset','required','maxlength'=>20]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ta_metadesc',trans('app.metadescription')) !!} *
                                {!! Form::textarea('ta_metadesc',$data->metadescription,['class'=>'form-control','id'=>'ta_metadesc','required','rows'=>2]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('tx_tags',trans('app.metatags')) !!} *
                                {!! Form::text('tx_tags',$data->metatags,['class'=>'form-control tags','id'=>'tx_tags','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('tx_author',trans('app.author')) !!} *
                                {!! Form::text('tx_author',$data->author,['class'=>'form-control','id'=>'tx_author','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('tx_robots',trans('app.robots')) !!}
                                {!! Form::text('tx_robots',$data->robots,['class'=>'form-control tags','id'=>'tx_robots','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('tx_language',trans('app.language')) !!} *
                                {!! Form::text('tx_language',$data->language,['class'=>'form-control','id'=>'tx_language','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ta_ganalytics',trans('app.google_analytics')) !!}
                                {!! Form::textarea('ta_ganalytics',$data->google_analytics,['class'=>'form-control','id'=>'ta_ganalytics','rows'=>5]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ta_pixel',trans('app.follow_scripts')) !!}
                                {!! Form::textarea('ta_pixel',$data->pixel_facebook,['class'=>'form-control','id'=>'ta_pixel','rows'=>5]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ta_hotjar',trans('app.follow_scripts')) !!}
                                {!! Form::textarea('ta_hotjar',$data->hotjar,['class'=>'form-control','id'=>'ta_hotjar','rows'=>5]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ta_chat','Chat') !!}
                                {!! Form::textarea('ta_chat',$data->chat,['class'=>'form-control','id'=>'ta_chat','rows'=>5]) !!}
                            </div>
                        </div>
                        <div class="box-footer">
                            {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">{!! trans('app.help_seo') !!}</h3>
                        </div>
                        <div class="box-body">
                            <div class="box-group" id="accordion">
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                                {!! htmlspecialchars(trans('app.help_title')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="box-body">
                                            <p>{!! htmlspecialchars(trans('app.help_text_p1')) !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                                {!! htmlspecialchars(trans('app.help_title_2')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! htmlspecialchars(trans('app.help_text_p2')) !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p3')) !!}</b></p>
                                            <p>{!! trans('app.help_text_p4') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                                {!! htmlspecialchars(trans('app.help_title_3')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! htmlspecialchars(trans('app.help_text_p5')) !!}</p>
                                            <p>{!! htmlspecialchars(trans('app.help_text_p6')) !!}</p>
                                            <p>{!! trans('app.importance_medium') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                                {!! htmlspecialchars(trans('app.help_title_4')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p8') !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p9')) !!}</b></p>
                                            <p>{!! htmlspecialchars(trans('app.help_text_p10')) !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p11')) !!}</b></p>
                                            <p><b>{!! trans('app.help_text_p12') !!}</b></p>
                                            <ul>
                                                <li>{!! trans('app.help_text_p13') !!}</li>
                                                <li>{!! trans('app.help_text_p14') !!}</li>
                                            </ul>
                                            <p>{!! trans('app.importance_high') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                                {!! htmlspecialchars(trans('app.help_title_5')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p15') !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p16')) !!}</b></p>
                                            <p>{!! trans('app.help_text_p17') !!}</p>
                                            <p>{!! trans('app.importance_high') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                                {!! htmlspecialchars(trans('app.help_title_6')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse6" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p18') !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p19')) !!}</b></p>
                                            <p>{!! trans('app.help_text_p20') !!}</p>
                                            <p>{!! trans('app.importance_high') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                                                {!! htmlspecialchars(trans('app.help_title_7')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse7" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p21') !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p22')) !!}</b></p>
                                            <p>{!! trans('app.importance_high') !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                                                {!! htmlspecialchars(trans('app.help_title_8')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse8" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p23') !!}</p>
                                            <ul>
                                                <li>{!! trans('app.help_text_p24') !!}</li>
                                                <li>{!! trans('app.help_text_p25') !!}</li>
                                                <li>{!! trans('app.help_text_p26') !!}</li>
                                                <li>{!! trans('app.help_text_p27') !!}</li>
                                                <li>{!! trans('app.help_text_p28') !!}</li>
                                                <li>{!! trans('app.help_text_p29') !!}</li>
                                                <li>{!! trans('app.help_text_p31') !!}</li>
                                                <li>{!! trans('app.help_text_p32') !!}</li>
                                                <li>{!! trans('app.help_text_p33') !!}</li>
                                                <li>{!! trans('app.help_text_p34') !!}</li>
                                            </ul>
                                            <p>{!! trans('app.help_text_p35') !!}</p>
                                            <p>
                                                <b>{!! htmlspecialchars(trans('app.help_text_p36')) !!}</b> {!! trans('app.help_text_p37') !!}
                                            </p>
                                            <p>
                                                <b>{!! htmlspecialchars(trans('app.help_text_p38')) !!}</b> {!! trans('app.help_text_p39') !!}
                                            </p>
                                            <p>
                                                <b>{!! htmlspecialchars(trans('app.help_text_p40')) !!}</b> {!! trans('app.help_text_p41') !!}
                                            </p>
                                            <p>
                                                <b>{!! htmlspecialchars(trans('app.help_text_p42')) !!}</b> {!! trans('app.help_text_p43') !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                                                {!! htmlspecialchars(trans('app.help_title_9')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse9" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p44') !!}</p>
                                            <p><b>{!! htmlspecialchars(trans('app.help_text_p45')) !!}</b></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                                                {!! htmlspecialchars(trans('app.help_title_10')) !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse10" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <p>{!! trans('app.help_text_p46') !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/seo/edit.js') !!}
@endsection