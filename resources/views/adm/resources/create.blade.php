@extends('adm.layouts.page')

@section('title')
    {!! trans('app.resources') !!}
@endsection
@section('cssTop')
    {!! Html::style('cms/plugins/select2/select2.min.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.resources') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.resources.index',trans('app.resources')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            {!! Form::open(['route'=>['adm.resources.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>

                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('fl_resource',trans('resources.file')) !!} *
                        {!! Form::file('fl_resource',null,['id'=>'fl_resource','class'=>'form-control filestyle','data-buttonName'=>'btn-primary']) !!}
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.file_resource') !!}</small>
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('resources.name')) !!} *
                        {!! Form::text('tx_name',null,['class'=>'form-control','id'=>'tx_firstname','required']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>

            </div>
            {!! Form::close() !!}

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/maskedInput/jquery.maskedinput.min.js') !!}
    {!! Html::script('cms/plugins/maskedInput/bootstrap-filestyle.min.js') !!}
    {!! Html::script('cms/plugins/maskedInput/ajax-file-upload.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/resources/create.js') !!}
@endsection