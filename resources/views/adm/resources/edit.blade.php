@extends('adm.layouts.page')

@section('title')
    {!! trans('app.resources') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.resources') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.resources.index',trans('app.resources')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            {!! Form::open(['route'=>['adm.resources.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>

                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('fl_resource',trans('app.image')) !!}
                        {!! Form::file('fl_resource',null,['id'=>'fl_resource','class'=>'form-control filestyle','data-buttonName'=>'btn-primary']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_firstname','required']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>

            </div>
            {!! Form::close() !!}

        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/maskedInput/jquery.maskedinput.min.js') !!}
    {!! Html::script('cms/plugins/maskedInput/bootstrap-filestyle.min.js') !!}
    {!! Html::script('cms/plugins/maskedInput/ajax-file-upload.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/resources/edit.js') !!}
@endsection