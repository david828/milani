@extends('adm.layouts.page')

@section('title')
    {!! trans('app.invoicing') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.invoice.null','IDX'],'method'=>'POST','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('invoice.want_annul') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade send-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.invoice.send','IDC'],'method'=>'POST','name'=>'frmSend','id'=>'frmSend','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('invoice.track') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_send',null,['id'=>'hd_send']) !!}
                    {!! Form::text('tx_track',null,['id'=>'tx_track', 'class'=>'form-control']) !!}
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancelS','id'=>'bt_cancelS','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('invoice.send'),['name'=>'bt_send','id'=>'bt_send','class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.invoicing') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.invoicing') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                            <div class="box-tools">
                                {!! Form::model(Request::only(['buscar']), ['route'=>'adm.invoice.index', 'style'=>'width: 150px;', 'method'=>'GET', 'rol'=>'search']) !!}
                                <div class="input-group">
                                    {!! Form::text('search',$list->search,['class'=>'form-control input-sm pull-right', 'placeholder'=>''.trans("ph.search").'']) !!}
                                    <div class="input-group-btn">
                                        {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.number') !!}</th>
                                        <th>{!! trans('app.id') !!}</th>
                                        <th>{!! trans('app.status') !!}</th>
                                        <th>{!! trans('app.name') !!}</th>
                                        <th>{!! trans('app.date') !!}</th>
                                        <th>{!! trans('invoice.subtotal') !!}</th>
                                        <th>{!! trans('invoice.discount') !!}</th>
                                        <th>{!! trans('invoice.tax') !!}</th>
                                        <th>{!! trans('invoice.total') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @foreach($list as $for)
                                        <tr>
                                            <td>{!! $loop->iteration !!}</td>
                                            <td>{!! $for->id !!}</td>
                                            <td class="status-{!! $for->id !!}">{!! $for->status[0]->name !!}</td>
                                            <td>{!! $for->customer_name !!}</td>
                                            <td>{!! $for->invoice_date !!}</td>
                                            <td>$ {!! number_format($for->subtotal) !!}</td>
                                            <td>{!! number_format($for->discount) !!}%</td>
                                            <td>{!! number_format($for->tax) !!}%</td>
                                            <td>$ {!! number_format($for->total) !!}</td>
                                            <td>
                                                {!! html_entity_decode(link_to_route('adm.invoice.show','<span class="label label-info"><i class="fa fa-eye"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.show_info')])) !!}
                                                @if($for->id_status!=3)
                                                    <span class="label label-success span-link send-data target-{!! $for->id !!}"
                                                          idtrack="{!! $for->idtrack !!}" data-toggle="tooltip"  target="{!! $for->id !!}"
                                                          data-placement="left"
                                                          title="{!! trans('invoice.track') !!}" style="margin-right: 1px;">
                                                        <i class="fa fa-send"></i>
                                                    </span>
                                                    <span class="label label-danger span-link trash-data target-{!! $for->id !!}"
                                                          trash="{!! $for->id !!}" data-toggle="tooltip"
                                                          data-placement="left"
                                                          title="{!! trans('invoice.annul') !!}">
                                                        <i class="fa fa-trash"></i>
                                                    </span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(Request::only(['search']))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/invoice/index.js') !!}
@endsection