@extends('adm.layouts.page')

@section('title')
    {!! trans('app.invoicing') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="content-wrapper">
        @section('here') {!! trans('app.invoicing') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.invoice.index',trans('app.invoicing')) !!}</li>
            <li>{!! trans('app.show_info') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="invoice">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {!! appData()->name_boucher !!}
                        <small class="pull-right">{!! trans('invoice.date') !!}: {!! date('d/m/Y') !!}</small>
                    </h2>
                </div><!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    {!! trans('invoice.from') !!}
                    <address>
                        <strong>{!! appData()->name_boucher !!}</strong><br>
                        {!! appData()->address_boucher !!}<br>
                        {!! trans('invoice.phone') !!}: {!! appData()->phone_boucher !!}<br>
                        {!! trans('invoice.email') !!}: {!! appData()->email_boucher !!}<br>
                        {!! trans('invoice.website') !!}: {!! appData()->website_boucher !!}<br>
                    </address>
                </div><!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    {!! trans('invoice.for') !!}
                    <address>
                        <strong>{!! $data->customer_name !!}</strong><br>
                        {!! $data->customer_documenttype . ' - ' . $data->customer_document !!}<br>
                        {!! $data->customer_address !!}<br>
                        {!! trans('invoice.phone') !!}: {!! $data->customer_phone !!}<br>
                        {!! trans('invoice.email') !!}: {!! $data->customer_email !!}
                    </address>
                </div><!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>{!! trans('invoice.invoice') !!} #{!! $data->id !!}</b><br>
                    <b>{!! trans('invoice.reference') !!}:</b> {!! $data->reference !!}<br>
                    <b>{!! trans('invoice.date') !!}:</b> {!! $data->date !!}<br>
                    <b>{!! trans('invoice.status') !!}:</b> {!! $data->status[0]->name !!}
                </div><!-- /.col -->
            </div><!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{!! trans('invoice.quantity') !!}</th>
                            <th>{!! trans('invoice.product').' / '.trans('invoice.service') !!}</th>
                            <th>{!! trans('invoice.discount') !!}</th>
                            <th>{!! trans('invoice.subtotal') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data->details as $item)
                            <tr>
                                <td>{!! $item->quantity !!}</td>
                                <td>{!! $item->description !!}</td>
                                <td>{!! (!is_null($item->discount))? $item->discount : '0'  !!}%</td>
                                <td>${!! number_format($item->total) !!}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        {!! trans('invoice.note') !!}
                    </p>
                    @if(!is_null($address))
                    <div>
                        <h4>{!! trans('cart.shipping_data') !!}</h4>
                        <p>{!! $address->documenttype.' - '.$address->document !!}</p>
                        <p>{!! $address->name !!}</p>
                        <p>{!! $address->email.' - '. $address->phone!!}</p>
                        <p>{!! $address->address !!}</p>
                        <p>{!! $address->post_code !!}</p>
                        <p>{!! $address->additional !!}</p>
                    </div>
                    @endif
                </div><!-- /.col -->
                <div class="col-xs-6">
                    <p class="lead">{!! trans('invoice.date') !!} {!! $data->date !!}</p>
                    @if(!is_null($data->coupon))
                        <p class="text-muted well well-sm no-shadow">
                            <b>{!! trans('invoice.app_coupon') !!}: {!! $data->coupon !!}</b>
                        </p>
                    @endif
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">{!! trans('invoice.value') !!}:</th>
                                <td>${!! number_format($data->subtotal) !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('invoice.discount') !!} ({!! number_format($data->discount) !!}%)</th>
                                <td>${!! number_format($data->discount_value) !!}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">{!! trans('invoice.subtotal') !!}:</th>
                                <td>${!! number_format($data->subtotal2) !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('invoice.tax') !!} ({!! number_format($data->tax) !!}%)</th>
                                <td>${!! number_format($data->tax_value) !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('invoice.shipping') !!}:</th>
                                <td>${!! number_format($data->shipping) !!}</td>
                            </tr>
                            <tr>
                                <th>{!! trans('invoice.total') !!}:</th>
                                <td>${!! number_format($data->total) !!}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-xs-12">
                    {!! html_entity_decode(link_to_route('adm.invoice.print','<i class="fa fa-print"></i> '.trans('invoice.print'),['id'=>$data->id],['class'=>'btn btn-primary pull-right','target'=>'_blank'])) !!}
                </div>
            </div>
        </section>
        <div class="clearfix"></div>

    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/invoice/index.js') !!}
@endsection