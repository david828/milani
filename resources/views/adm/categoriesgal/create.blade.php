@extends('adm.layouts.page')

@section('title')
    {!! trans('app.blogcategories') !!}
@endsection
@section('content')
    <div class="modal fade" id="msgModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body msg-body">

                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.accept'),['class'=>'btn btn-outline','data-dismiss'=>'modal']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.blogcategories') !!}@endsection
        @section('breadcumb')
                <li>{!! link_to_route('adm.galleries.index',trans('app.galleries')) !!}</li>
                <li>{!! link_to_route('adm.categoriesgal.index',trans('app.blogcategories')) !!}</li>
            <li>{!! trans('app.register') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.register_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.categoriesgal.store'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group">
                        {!! Form::label('tx_category',trans('app.name')) !!}
                        {!! Form::text('tx_category',null,['class'=>'form-control','id'=>'tx_category','required']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/categoriesgal/create.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
@endsection