@extends('adm.layouts.page')

@section('title')
    {!! trans('app.homepage') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/morris/morris.css') !!}
@endsection
@section('content')

    <div class="content-wrapper">
        @section('here') {!! trans('app.homepage') !!}@endsection

        @include('adm.layouts.bread')

        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{!! $data->admin !!}</h3>
                            <p>{!! trans('app.admins') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.admin.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{!! $data->products !!}</h3>
                            <p>{!! trans('app.products') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tags"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.products.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{!! $data->categories !!}</h3>
                            <p>{!! trans('app.productcategories') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-star"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.categoriesP.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{!! $data->subcategories !!}</h3>
                            <p>{!! trans('app.productsubcategories') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-circle-o"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.subcategories.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-black-gradient">
                        <div class="inner">
                            <h3>{!! $data->users !!}</h3>
                            <p>{!! trans('app.users') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.admin.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-fuchsia">
                        <div class="inner">
                            <h3>@if(!is_null($data->last))
                                    ${!! number_format($data->last->total, 0, '.', ',') !!} @else $0 @endif</h3>
                            <p>{!! trans('invoice.last_sold') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.invoice.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-xs-6">
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>${!! number_format($data->total, 0, '.', ',') !!}</h3>
                            <p>{!! trans('invoice.total_sold') !!}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-money"></i>
                        </div>
                        {!! html_entity_decode(link_to_route('adm.invoice.index',trans('app.more_info').' <i class="fa fa-arrow-circle-right"></i>',null,['class'=>'small-box-footer'])) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#revenue-chart"
                                                  data-toggle="tab">{!! trans('app.graphic') !!}</a></li>
                            <li class="pull-left header"><i class="fa fa-inbox"></i> {!! trans('app.sales') !!}</li>
                        </ul>
                        <div class="tab-content no-padding">
                            <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="revenue-chart"
                                 style="position: relative; height: 300px;"></div>
                        </div>
                    </div><!-- /.nav-tabs-custom -->


                </section><!-- /.Left col -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{!! trans('app.status') !!}</span>
                            <span class="info-box-number">{!! (appData()->status)? '':'' !!}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-tags"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{!! trans('app.products') !!}</span>
                            <span class="info-box-number">{!! trans('app.active_s') !!}
                                : {!! $data->activeproducts !!}</span>
                            <span class="info-box-number">{!! trans('app.inactive_s') !!}
                                : {!! $data->inactiveproducts !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-newspaper-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{!! trans('app.articles') !!}</span>
                            <span class="info-box-number">{!! trans('app.active_s') !!}
                                : {!! $data->activearticles !!}</span>
                            <span class="info-box-number">{!! trans('app.inactive_s') !!}
                                : {!! $data->inactivearticles !!}</span>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>

@endsection

@section('jsBottom')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! Html::script('cms/plugins/morris/morris.min.js') !!}
    <script>
        $(function () {

            "use strict";

            /* Morris.js Charts */
            // Sales chart
            var area = new Morris.Bar({
                element: 'revenue-chart',
                resize: true,
                data: [
                        @foreach($data->monthList as $key => $val)
                        {
                            y: '{!! $val !!} ', item1: '{!! $data->graphic[$key] !!}'
                        }
                        @if (!$loop->last), @endif
                        @endforeach
                ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['Ventas'],
                barColors: ['#a0d0e0'],
                hideHover: 'auto'
            });
            //Fix for charts under tabs
            $('.box ul.nav a').on('shown.bs.tab', function () {
                area.redraw();
            });


        });
    </script>
@endsection