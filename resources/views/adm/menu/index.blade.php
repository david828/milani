@extends('adm.layouts.page')

@section('title')
    {!! trans('app.menu') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.menu') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.menu') !!}</li>
        @endsection
        @include('adm.layouts.bread')

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.number') !!}</th>
                                        <th>{!! trans('app.sections') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @php $count=1 @endphp
                                    @foreach($list as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $loop->iteration !!}</td>
                                            <td>{!! $for->title !!}</td>
                                            <td>
                                                {!! html_entity_decode(link_to_route('adm.menu.edit','<span class="label label-info"><i class="fa fa-pencil"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.edit_info')])) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(Request::only(['search']))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
@endsection