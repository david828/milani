@extends('adm.layouts.page')

@section('title')
    {!! trans('app.header') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection

@php
$aux1 = '<div class="col-md-12 col-lg-12 col-sm-12 square">
            <h1>2</h1>
        </div>';
$aux2 = '<div class="col-md-6 col-lg-6 col-sm-6 square">
            <h1>3</h1>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 square">
            <h1>4</h1>
        </div>';
if($data->pos_2 === 0)
{
    $box3 = $aux1;
    $box4 = $aux2;
} else {
    $box3 = $aux2;
    $box4 = $aux1;
}

$box1 = '<div class="square">
            <h1>1</h1>
        </div>';
$box2 = '<div class="row fil1" id="box-4">'.$box4.'</div>
        <div class="row fil1" id="box-3">'.$box3.'</div>';
@endphp

@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade modal-images" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.add_images') !!}</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body text-center crop-images-d other-w">
                        {!! Html::image('images/app/barcode.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body image-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer image-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelOther','name'=>'bt_cancelOther','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('site.apply'),['id'=>'imageCrop','name'=>'imageCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.header') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.header') !!}</li>
        @endsection
        @include('adm.layouts.bread')
            <section class="content width-calc">
                {!! Form::open(['route'=>['adm.header.update'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                    </div>
                    <div class="box-body">
                        <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3>{!! trans('header.distribution') !!}</h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-0">
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1 ">
                                <button type="button" class="btn-header" title="{!! trans('header.change') !!}" id="change-v"><i class="fa fa-exchange"></i> </button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-11">
                                <div class="row header-box">
                                    <div class="col-md-6 col-lg-6 col-sm-6" id="box-2">
                                        {!! ($data->pos_1 == 0)?$box2:$box1 !!}
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-6" id="box-1">
                                        {!! ($data->pos_1 == 0)?$box1:$box2 !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-push-3 col-lg-push-3 col-md-6 col-lg-6 col-sm-6">
                                    <button type="button" class="btn-header" title="{!! trans('header.change') !!}" id="change-h"><i class="fa fa-exchange"></i> </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::hidden('hn_pos_1',$data->pos_1,['id'=>'hn_pos_1']) !!}
                        {!! Form::hidden('hn_pos_2',$data->pos_2,['id'=>'hn_pos_2']) !!}
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3>{!! '1. ' . trans('header.slide') !!}</h3>
                            </div>
                            @foreach($data->images as $img)
                                <div class="col-lg-3 col-md-3 col-xs-6">
                                    {!! Html::Image('images/header/'. $img->image, 'Header', ['style' => 'max-width:100%']) !!}
                                </div>
                            @endforeach
                        </div>
                        <br>
                        {!! html_entity_decode(link_to_route('adm.header.images','<span>'.trans('app.modify_images').'</span>')) !!}
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3>{!! '2. ' . trans('header.image') !!}</h3>
                                <div>
                                    <figure class="tg-docimg" style = 'margin-top: 10px;'>
                                        <label class="tg-uploadimg" for="inputImage">
                                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                                            <div class="input-file-button" data-toggle="tooltip"><i
                                                        class="fa fa-image"></i> {!! trans('app.upload_mosaic') !!}</div>
                                        </label>
                                        {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.header2_size').'. '.trans('note.image_extensions') !!}</small>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3>{!! '3 y 4. ' . trans('header.instagram') !!}</h3>
                                <p class="text-yellow">{!! trans('header.instagram_fields') !!}</p>
                                <div class="form-group">
                                    {!! Form::label('tx_client','ClientID') !!} *
                                    {!! Form::text('tx_client',$data->client_id,['class'=>'form-control','id'=>'tx_client','required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('tx_user','UserID') !!} *
                                    {!! Form::text('tx_user',$data->user_id,['class'=>'form-control','id'=>'tx_user','required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('tx_accesstoken','AccessToken') !!} *
                                    {!! Form::text('tx_accesstoken',$data->accessToken,['class'=>'form-control','id'=>'tx_accesstoken','required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('tx_numphotos',trans('header.numphotos')) !!} *
                                    {!! Form::number('tx_numphotos',$data->num_photos,['class'=>'form-control','id'=>'tx_numphotos','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </section>
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/select2/select2.full.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/header/edit.js') !!}
@endsection