@extends('adm.layouts.page')

@section('title')
    {!! trans('app.header') !!}
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap3-wysiwyg-master/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
    {!! Html::style('cms/dist/css/dragImages.css') !!}
@endsection

@section('content')
    @include('adm.layouts.msgmodal')

    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.headerimage.destroy','IDI'],'method'=>'POST','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade link-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.headerlink'],'method'=>'POST','name'=>'frmLink','id'=>'frmLink','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('header.change_link') !!}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('sl_typelink',trans('header.typelink')) !!} *
                        </div>
                        <div class="col-md-6">
                            {!! Form::select('sl_typelink',[''=>trans('app.select'),'0'=>trans('header.int'),'1'=>trans('header.ext')],null,['class'=>'form-control','id'=>'sl_typelink']) !!}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <span id="intMsg" style="display: none">{!! appData()->urldata !!}/</span>
                            <span id="extMsg" style="display: none">{!! trans('app.link') !!}:</span>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('tx_link',null,['class'=>'form-control','id'=>'tx_link','placeholder'=>trans('header.empty')]) !!}
                        </div>
                    </div>
                    {!! Form::hidden('hd_header',null,['id'=>'hd_header']) !!}
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancelL','id'=>'bt_cancelL','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_Link','id'=>'bt_Link','class'=>'btn btn-success','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-profile" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.headerimage.upload','IDI'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('site.change_profile_image') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div class="panel-body text-center crop-images-d">
                        {!! Html::image('cms/dist/img/article-bg.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body docs-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelProfile','name'=>'bt_cancelProfile','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                    {!! Form::submit(trans('site.apply'),['id'=>'articleCrop','name'=>'articleCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-order" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('order.title_order') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div id="reorder-helper" class="light_box">
                        {!! trans('order.drag') !!}<br>
                        {!! trans('order.save') !!}
                    </div>
                    <div class="gallery">
                        <ul class="reorder_ul reorder-photos-list" id="galleryOrder">
                            @foreach($data as $items)
                                <li id="image_li_{!! $items->id !!}" class="ui-sortable-handle">
                                    <a href="javascript:void(0);" style="float:none;" class="image_link">
                                        {!! Html::image($data->dir.$items->image,null) !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::open(['route'=>['adm.orderImages'],'method'=>'POST','name'=>'frmOrder','id'=>'frmOrder','role'=>'form','enctype'=>'multipart/form-data']) !!}
                        {!! Form::hidden('table','headerimages',['id'=>'table']) !!}
                        {!! Form::hidden('id_array',null,['id'=>'id_array']) !!}
                        {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelOrder','name'=>'bt_cancelOrder','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::button(trans('site.apply'),['id'=>'btnOrder','name'=>'btnOrder','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.header') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.header',trans('app.header')) !!}</li>
            <li>{!! trans('app.images') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content width-calc">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.upload_image') !!}</h3>
                </div>
                <div class="box-body">
                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip"><i
                                        class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                        </label>
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.header1_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn-primary" style="margin-bottom: 10px;" onclick="location.href = '/adm/header'">
                        <i class="fa fa-arrow-left"></i>&nbsp;{!! trans('app.throwback') !!}
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <div class="user-block">
                                <span class="username">{!! trans('app.images') !!}</span>
                                <button type="button" class="btn-primary" style="margin-bottom: 10px; float:right;" id="btnReorder" >
                                    <i class="fa fa-bars"></i>&nbsp;{!! trans('order.reorder') !!}
                                </button>
                            </div>
                        </div>
                        <div class="box-body text-justify">
                            <div id="gallery">
                                @foreach($data as $items)
                                    <div class="col-xs-4 col-md-4" id="img{!! $items->id !!}">
                                        <div class="content-collect mb-20">
                                            {!! Html::image($data->dir.$items->image,null) !!}
                                            <div class="content-collect-options">
                                                <i class="fa fa-eye bg-blue view-image"
                                                   img-target="{!! $data->dir.$items->image !!}"></i>
                                                <i class="fa fa-link bg-green view-link" id="link_{!! $items->id !!}"
                                                   img-target="{!! $items->id !!}"
                                                   img-link="{!! $items->link !!}"
                                                   img-tLink="{!! $items->typelink !!}"></i>
                                                <i class="fa fa-trash-o bg-red delete-image"
                                                   img-target="{!! $items->id !!}"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-picture-o"></i>
                            <h3 class="box-title">{!! trans('app.image_viewer') !!}</h3>
                        </div>
                        <div class="box-body">
                            <div class="content-viewer">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/header/images.js') !!}
    {!! Html::script('cms/plugins/jQueryUI/jquery-ui.js') !!}
    {!! Html::script('cms/dist/js/dragImages.js') !!}
@endsection