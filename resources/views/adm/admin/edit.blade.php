@extends('adm.layouts.page')

@section('title')
    {!! trans('app.admin') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')

    <div class="content-wrapper">
        @section('here') {!! trans('app.admin') !!}@endsection
        @section('breadcumb')
            <li>{!! link_to_route('adm.admin.index',trans('app.admin')) !!}</li>
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>
                {!! Form::open(['route'=>['adm.admin.update','id'=>$data->id],'method'=>'PUT','name'=>'frmData','id'=>'frmData','role'=>'form']) !!}
                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_role',trans('user.user_type')) !!} *
                        {!! Form::select('sl_role',[''=> trans('app.select'),'A' => trans('user.admin'),'S' => trans('user.super'),'U' => trans('user.user')],$data->role,['class'=>'form-control select2','id'=>'sl_role','required']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_person',trans('user.type')) !!} *
                        {!! Form::select('sl_person',$data->person,$data->id_person,['class'=>'form-control select2','id'=>'sl_person','required']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_documenttype',trans('user.doctype')) !!} *
                        {!! Form::select('sl_documenttype',$data->doctype,$data->id_documenttype,['class'=>'form-control select2','id'=>'sl_documenttype','required']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('tx_document',trans('user.document')) !!} *
                        {!! Form::text('tx_document',$data->document,['class'=>'form-control','id'=>'tx_document','required']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('tx_name',trans('user.name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('tx_phone',trans('user.phone')) !!} *
                        {!! Form::number('tx_phone',$data->phone,['class'=>'form-control','id'=>'tx_phone','required','number']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('tx_email',trans('user.email')) !!} *
                        {!! Form::email('tx_email',$data->email,['class'=>'form-control','id'=>'tx_email','required','email']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('tx_password',trans('user.password')) !!}
                        {!! Form::password('tx_password',['class'=>'form-control','id'=>'tx_password','minlength'=>'6']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('tx_address',trans('user.address')) !!}
                        {!! Form::text('tx_address',$data->address_invoice,['class'=>'form-control','id'=>'tx_address']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_country',trans('user.country')) !!}
                        {!! Form::select('sl_country',$data->countryList,$data->id_country,['class'=>'form-control','id'=>'sl_country']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_state',trans('user.state')) !!}
                        {!! Form::select('sl_state',$data->stateList,$data->id_state,['class'=>'form-control','id'=>'sl_state']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('sl_city',trans('user.city')) !!}
                        {!! Form::select('sl_city',$data->cityList,$data->id_city,['class'=>'form-control','id'=>'sl_city']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
    {!! Form::hidden('hn_state',url('state/IDX/search'),['id'=>'hn_state']) !!}
    {!! Form::hidden('hn_city',url('city/IDX/search'),['id'=>'hn_city']) !!}
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/dist/js/app/admin/edit.js') !!}
@endsection