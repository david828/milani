@extends('adm.layouts.page')

@section('title')
    {!! trans('app.admin') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="modal fade trash-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route'=>['adm.admin.destroy','IDX'],'method'=>'DELETE','name'=>'frmDes','id'=>'frmDes','role'=>'form']) !!}
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('app.message_alert') !!}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('hd_trash',null,['id'=>'hd_trash']) !!}
                    <p>{!! trans('app.want_delete') !!}</p>
                </div>
                <div class="modal-footer">
                    {!! Form::button(trans('app.cancel'),['name'=>'bt_cancel','id'=>'bt_cancel','class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                    {!! Form::button(trans('app.accept'),['name'=>'bt_trash','id'=>'bt_trash','class'=>'btn btn-danger','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        @section('here') {!! trans('app.admin') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.admin') !!}</li>
        @endsection
        @include('adm.layouts.bread')
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                {!! trans('pagination.currentPage') !!}
                                <span class="badge bg-primary">{!! $list->currentPage() !!}</span>
                                {!! trans('pagination.lastPage') !!}
                                <span class="badge bg-primary">{!! $list->lastPage() !!}</span>
                            </h3>
                            <div class="box-tools">
                                {!! Form::model(Request::only(['buscar']), ['route'=>'adm.admin.index', 'style'=>'width: 150px;', 'method'=>'GET', 'rol'=>'search']) !!}
                                <div class="input-group">
                                    {!! Form::text('search',$list->search,['class'=>'form-control input-sm pull-right', 'placeholder'=>''.trans("ph.search").'']) !!}
                                    <div class="input-group-btn">
                                        {!! Form::button('<i class="fa fa-search"></i>',['class'=>'btn btn-sm btn-default','role'=>'button','type'=>'submit']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            @if($list->isEmpty())
                                <div class="box-body">{!! trans('app.no_result') !!}</div>
                            @else
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>{!! trans('app.number') !!}</th>
                                        <th>{!! trans('app.id') !!}</th>
                                        <th>{!! trans('user.document') !!}</th>
                                        <th>{!! trans('app.name') !!}</th>
                                        <th>{!! trans('user.email') !!}</th>
                                        <th>{!! trans('user.phone') !!}</th>
                                        <th>{!! trans('user.city') !!}</th>
                                        <th>{!! trans('app.actions') !!}</th>
                                    </tr>
                                    @php $count=1 @endphp
                                    @foreach($list as $for)
                                        <tr class="target-{!! $for->id !!}">
                                            <td>{!! $loop->iteration !!}</td>
                                            <td>{!! $for->id !!}</td>
                                            <td>{!! $for->document !!}</td>
                                            <td>{!! $for->name !!}</td>
                                            <td>{!! $for->email !!}</td>
                                            <td>{!! $for->phone !!}</td>
                                            <td>{!! (!is_null($for->city))? $for->city->name :'' !!}</td>
                                            <td>
                                                {!! html_entity_decode(link_to_route('adm.admin.edit','<span class="label label-info"><i class="fa fa-pencil"></i></span>',['id'=>$for->id],['data-toggle'=>'tooltip','data-placement'=>'left','title'=>trans('app.edit_info')])) !!}
                                                <span class="label label-danger span-link trash-data"
                                                      trash="{!! $for->id !!}" data-toggle="tooltip"
                                                      data-placement="left"
                                                      title="{!! trans('app.delete_info') !!}">
                                        <i class="fa fa-trash"></i>
                                    </span>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <div class="box-footer clearfix">
                            <div class="panel-body pull-right">
                                <span class="badge bg-success">{!! $list->count() !!}</span>
                                {!! trans('pagination.count') !!}
                                <span class="badge bg-success">{!! $list->total() !!}</span>
                            </div>

                            {!! $list->appends(Request::only(['search']))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="add-data">
        {!! html_entity_decode(link_to_route('adm.admin.create','<i class="fa fa-plus"></i> '.trans('app.register'),null,['class'=>'btn btn-app bg-aqua'])) !!}
    </div>
@endsection
@section('jsBottom')
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/admin/index.js') !!}
@endsection