@extends('adm.layouts.page')

@section('title')
    {!! trans('app.configuration') !!}
@endsection
@section('cssTop')
@endsection
@section('cssBottom')
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection
@section('content')
    @include('adm.layouts.msgmodal')
    <div class="content-wrapper">
        @section('here') {!! trans('app.configuration') !!}@endsection
        @section('breadcumb')
            <li>{!! trans('app.edit') !!}</li>
        @endsection
        @include('adm.layouts.bread')


        <div class="modal fade modal-images" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{!! trans('app.add_images') !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::hidden('logo',null,['id'=>'logo']) !!}
                        <div class="panel-body text-center crop-images-d other-w">
                            {!! Html::image('images/app/barcode.png',null,['class'=>'p-image']) !!}
                        </div>

                        <div class="panel-body image-buttons">
                            <div class="btn-group">
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                            </div>

                            <div class="btn-group">
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                            </div>

                            <div class="btn-group">
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                            </div>

                            <div class="btn-group">
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                            </div>

                            <div class="btn-group">
                                {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer image-buttons">
                        {!! Form::button(trans('app.cancel'),['id'=>'bt_cancelOther','name'=>'bt_cancelOther','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                        {!! Form::button(trans('app.apply'),['id'=>'imageCrop','name'=>'imageCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                    </div>
                </div>
            </div>
        </div>

        <section class="content width-calc">
            {!! Form::open(['route'=>['adm.app.update',$data->id],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{!! trans('app.edit_form') !!}</h3>
                </div>

                <div class="box-body">
                    <p class="text-yellow">{!! trans('app.required_fields') !!}</p>
                    <figure class="tg-docimg">
                        <label class="tg-uploadimg" for="inputImage">
                            {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                            <div class="input-file-button" data-toggle="tooltip">
                                <i class="fa fa-image"></i> {!! trans('app.upload_logo') !!}
                            </div>
                        </label>
                        {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                        <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.icon_size').'. '.trans('note.image_extensions') !!}</small>
                    </figure>
                    <div class="form-group">
                        {!! Form::label('tx_color',trans('app.main_color')) !!}:
                        {!! Form::color('tx_color',$data->color,['id'=>'tx_color','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_name',trans('app.app_name')) !!} *
                        {!! Form::text('tx_name',$data->name,['class'=>'form-control','id'=>'tx_name','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_email',trans('app.email')) !!} *
                        {!! Form::email('tx_email',$data->email,['class'=>'form-control','id'=>'tx_email','required','email']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_phone',trans('app.phone_main')) !!} *
                        {!! Form::text('tx_phone',$data->phone,['class'=>'form-control','id'=>'tx_phone','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_mobile',trans('app.mobile')) !!} *
                        {!! Form::text('tx_mobile',$data->cellphone,['class'=>'form-control','id'=>'tx_mobile','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_address',trans('app.address_main')) !!} *
                        {!! Form::text('tx_address',$data->address,['class'=>'form-control','id'=>'tx_address','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_website',trans('app.website')) !!} *
                        {!! Form::text('tx_website',$data->website,['class'=>'form-control','id'=>'tx_website','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_copyright',trans('app.copyright')) !!} *
                        {!! Form::text('tx_copyright',$data->copyright,['class'=>'form-control','id'=>'tx_copyright','required']) !!}
                    </div>
                    @if(Auth::user()->role=='S')
                        <div class="form-group">
                            {!! Form::label('tx_dir',trans('app.dir_data')) !!} *
                            {!! Form::text('tx_dir',$data->dirdata,['class'=>'form-control','id'=>'tx_dir','required']) !!}
                            <smal class="text-light-blue">
                                <b>{!! trans('app.dir_data_help') !!}</b> {!! $data->directory !!}
                            </smal>
                        </div>
                        <div class="form-group">
                            {!! Form::label('tx_url',trans('app.url_data')) !!} *
                            {!! Form::text('tx_url',$data->urldata,['class'=>'form-control','id'=>'tx_url','required','url']) !!}
                        </div>
                    @endif
                    <div class="input-group">
                        {!! Form::label('tx_tax',trans('app.tax')) !!} *
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon">%</span>
                        {!! Form::number('tx_tax',$data->tax_value,['class'=>'form-control','id'=>'tx_tax','required','min'=>'0','max'=>'100','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sl_currency',trans('app.currency')) !!} *
                        {!! Form::select('sl_currency',$data->currencyList,$data->id_currency,['id'=>'sl_currency','data-placeholder'=>trans('app.select'),'class'=>'form-control select2','required','number']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_min_value',trans('app.min_value')) !!} *
                        {!! Form::number('tx_min_value',$data->min_value,['class'=>'form-control','id'=>'tx_min_value','required']) !!}
                    </div>
                    <hr/>
                    <h3>{!! trans('app.data_boucher') !!}</h3>
                    <div class="form-group">
                        {!! Form::label('tx_name_boucher',trans('app.name')) !!} *
                        {!! Form::text('tx_name_boucher',$data->name_boucher,['class'=>'form-control','id'=>'tx_name_boucher','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_email_boucher',trans('app.email')) !!} *
                        {!! Form::email('tx_email_boucher',$data->email_boucher,['class'=>'form-control','id'=>'tx_email_boucher','required','email']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_phone_boucher',trans('app.phone')) !!} *
                        {!! Form::text('tx_phone_boucher',$data->phone_boucher,['class'=>'form-control','id'=>'tx_phone_boucher','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_address_boucher',trans('app.address')) !!} *
                        {!! Form::text('tx_address_boucher',$data->address_boucher,['class'=>'form-control','id'=>'tx_address_boucher','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tx_website_boucher',trans('app.website_b')) !!} *
                        {!! Form::text('tx_website_boucher',$data->website_boucher,['class'=>'form-control','id'=>'tx_website_boucher','required']) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {!! Form::submit(trans('app.apply'),['class'=>'btn btn-primary','name'=>'bt_apply','id'=>'b_apply']) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </section>
    </div>

@endsection
@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
    {!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
    {!! Html::script('cms/plugins/input-mask/jquery.inputmask.extensions.js') !!}
    {!! Html::script('cms/dist/js/messages.js') !!}
    {!! Html::script('cms/dist/js/app/configuration/edit.js') !!}
@endsection