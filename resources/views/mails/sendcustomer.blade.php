<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Su pedido ha sido enviado</title>

    <style type="text/css">
        /* Take care of image borders and formatting */

        img {
            max-width: 600px;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a {
            border: 0;
            outline: none;
        }

        a img {
            border: none;
        }

        /* General styling */

        td, h1, h2, h3 {
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 400;
        }

        td {
            font-size: 13px;
            line-height: 150%;
            text-align: left;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%;
            height: 100%;
            color: #37302d;
            background: #ffffff;
        }

        table {
            border-collapse: collapse !important;
        }

        h1, h2, h3 {
            padding: 0;
            margin: 0;
            color: #444444;
            font-weight: 400;
            line-height: 110%;
        }

        h1 {
            font-size: 35px;
        }

        h2 {
            font-size: 30px;
        }

        h3 {
            font-size: 24px;
        }

        h4 {
            font-size: 18px;
            font-weight: normal;
        }

        .important-font {
            color: #21BEB4;
            font-weight: bold;
        }

        .hide {
            display: none !important;
        }

        .force-full-width {
            width: 100% !important;
        }

        td.desktop-hide {
            font-size: 0;
            height: 0;
            display: none;
            color: #ffffff;
        }


    </style>

    <style type="text/css" media="screen">
        @media screen {
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400);

            /* Thanks Outlook 2013! */
            td, h1, h2, h3 {
                font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif !important;
            }
        }
    </style>

    <style type="text/css" media="only screen and (max-width: 600px)">
        /* Mobile styles */
        @media only screen and (max-width: 600px) {

            table[class="w320"] {
                width: 320px !important;
            }

            table[class="w300"] {
                width: 300px !important;
            }

            table[class="w290"] {
                width: 290px !important;
            }

            td[class="w320"] {
                width: 320px !important;
            }

            td[class~="mobile-padding"] {
                padding-left: 14px !important;
                padding-right: 14px !important;
            }

            td[class*="mobile-padding-left"] {
                padding-left: 14px !important;
            }

            td[class*="mobile-padding-right"] {
                padding-right: 14px !important;
            }

            td[class*="mobile-block"] {
                display: block !important;
                width: 100% !important;
                text-align: left !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                padding-bottom: 15px !important;
            }

            td[class*="mobile-no-padding-bottom"] {
                padding-bottom: 0 !important;
            }

            td[class~="mobile-center"] {
                text-align: center !important;
            }

            table[class*="mobile-center-block"] {
                float: none !important;
                margin: 0 auto !important;
            }

            *[class*="mobile-hide"] {
                display: none !important;
                width: 0 !important;
                height: 0 !important;
                line-height: 0 !important;
                font-size: 0 !important;
            }

            td[class*="mobile-border"] {
                border: 0 !important;
            }

            td[class*="desktop-hide"] {
                display: block !important;
                font-size: 13px !important;
                height: 61px !important;
                padding-top: 10px !important;
                padding-bottom: 10px !important;
                color: #444444 !important;
            }
        }
    </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
      bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tr>
        <td align="center" valign="top" bgcolor="#ffffff" width="100%">

            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td style="background:#EEEEEE" width="100%">
                        <center>
                            <table cellspacing="0" cellpadding="0" width="600" class="w320">
                                <tr>
                                    <td valign="top" class="mobile-block mobile-no-padding-bottom mobile-center"
                                        style="background:{!! appData()->color !!};padding:10px 10px 10px 20px; text-align: center">
                                        {!! html_entity_decode(link_to(appData()->website,Html::image(appData()->urldata.'/images/app/'.appData()->logo,appData()->name),['style'=>'text-decoration:none;'])) !!}

                                    </td>
                                </tr>
                            </table>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="background-color:#f8f8f8;border-bottom:1px solid #e7e7e7;">

                        <center>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="w320"
                                   style="height:100%;">
                                <tr>
                                    <td valign="top" class="mobile-padding" style="padding:20px;">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="padding-right:20px">
                                                    <p>Tú pedido ha sido enviado.</p>
                                                    <p>Puedes entrar a consultar tu pedido aquí:
                                                        <a style="color:{!! appData()->color !!};" href="{!! appData()->urldata !!}/imprimir-recibo/{!! $emailData['id_invoice'] !!}">{!! $emailData['invoice'] !!}</a>
                                                    </p>
                                                    <p>También puedes ver el estado de envío <a style="color:{!! appData()->color !!};" href="{!! env('SEARCH_COORD') !!}" target="_blank">aquí</a> ingresando este número de guía: {!! $emailData['idtrack'] !!} </p>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="padding-top:35px;">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="mobile-hide"
                                                                style="vertical-align:top;">
                                                                {!! trans('email.thanks_for_buy') !!}.
                                                                <br>
                                                                <h5>{!! appData()->email !!}</h5>
                                                                <h4>{!! appData()->name !!}</h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align:top;" class="desktop-hide">
                                                                {!! trans('email.thanks_for_buy') !!}.<br>
                                                                {!! appData()->email !!} / {!! appData()->name !!}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#1f1f1f;">
                        <center>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="w320"
                                   style="height:100%;color:#ffffff" bgcolor="#1f1f1f">
                                <tr>
                                    <td align="right" valign="middle" class="mobile-padding"
                                        style="font-size:12px;padding:20px; background-color:#1f1f1f; color:#ffffff; text-align:left; ">
                                        <a style="color:#ffffff;"
                                           href="mailto:{!! appData()->email !!}">{!! trans('email.contact_us') !!}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a style="color:#ffffff;" href="#">{!! appData()->number_contact !!}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a style="color:#ffffff;" href="#">{!! appData()->address !!}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a style="color:#ffffff;"
                                           href="{!! appData()->urldata !!}">{!! appData()->website !!}</a>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>