@extends('mails.layoutmailone')
@section('title-name')
    {!! trans('email.recovery_pass') !!}
@endsection
@section('title-message') {!! $name !!} @endsection
@section('data-message')
    {!! trans('email.recovery_message') !!}
    <b>{!! $password !!}</b>
@endsection
