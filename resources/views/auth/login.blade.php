@extends('adm.layouts.login')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{!! appData()->name !!}</b></a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{!! trans('login.administrator') !!}</p>
            {!! Form::open(['url' => '/login','method'=>'POST', 'class' => 'form, form-signin', 'name'=>'frmLogin', 'id' => 'frmLogin']) !!}
            {!! csrf_field() !!}
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email')]) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
            <div class="form-group has-feedback{!! $errors->has('password') ? ' has-error' : '' !!}">
                {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('ph.password')]) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            {!! Form::checkbox('remember',null,null,['id'=>'remember']) !!}  {!! trans('login.remember') !!}
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    {!! Form::submit(trans('app.signin'),['class'=>'btn btn-primary btn-block btn-flat']) !!}
                </div><!-- /.col -->
            </div>
            {!! Form::close() !!}
            {!! link_to('password/reset',trans('login.forgot')) !!}
        </div><!-- /.login-box-body -->
    </div>
@endsection