@extends('adm.layouts.login')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{!! appData()->name !!}</b></a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{!! trans('login.reset_password') !!}</p>
            {!! Form::open(['url' => '/password/reset','method'=>'POST', 'class' => 'form, form-signin', 'name'=>'frmLogin', 'id' => 'frmLogin']) !!}
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email'),'required', 'autofocus']) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
            <div class="form-group has-feedback{!! $errors->has('password') ? ' has-error' : '' !!}">
                {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('ph.password'),'id'=>'password','required']) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
            <div class="form-group has-feedback{!! $errors->has('password_confirmation') ? ' has-error' : '' !!}">
                {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>trans('ph.confirm_password'),'id'=>'password-confirm','required']) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if ($errors->has('password_confirmation'))
                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::submit(trans('login.reset_password'),['class'=>'btn btn-primary btn-block btn-flat']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            {!! link_to('/login',trans('login.login_link'),['class'=>'login-link']) !!}
        </div>
    </div>
@endsection
