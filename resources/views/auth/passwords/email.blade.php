@extends('adm.layouts.login')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{!! appData()->name !!}</b></a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{!! trans('login.reset_password') !!}</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            {!! Form::open(['url' => '/password/email','method'=>'POST', 'class' => 'form, form-signin', 'name'=>'frmLogin', 'id' => 'frmLogin']) !!}
            {{ csrf_field() }}
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email')]) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::submit(trans('login.send_password_link'),['class'=>'btn btn-primary btn-block btn-flat']) !!}
                </div><!-- /.col -->
            </div>

            {!! Form::close() !!}
            {!! link_to('/login',trans('login.login_link'),['class'=>'login-link']) !!}
        </div>
    </div>

@endsection
