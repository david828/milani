<!DOCTYPE html>
<html lang="{!! getAppSeo()->language !!}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{!! appData()->name !!}</title>
    @if(!is_null(getAppSeo()->charset))
        <meta charset="{!! getAppSeo()->charset !!}">
    @else
        <meta charset="UTF-8">
    @endif
<!-- meta -->
    <meta name="application-name" content="{!! getAppSeo()->application_name !!}"/>
    <meta name="author" content="{!! getAppSeo()->author !!}">
    <meta name="description" content="@yield('metadesc',getAppSeo()->metadescription)">
    <meta name="keywords" content="@yield('keywords',getAppSeo()->metatags)">
    <meta name="robots" content="{!! getAppSeo()->robots !!}">
    <link rel="icon" href="{!! url('assets/images/favicon.png') !!}" sizes="32x32"/>
    <!-- Custom Fonts -->
    {!! Html::style('assets/fonts/futura/fonts.css') !!}
    <style>
        body {
            font-family: 'futura-light', sans-serif;
            padding: 50px;
        }
    </style>
</head>
<body>
{!! conditions()->text !!}
</body>
</html>