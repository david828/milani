@extends('site.layouts.page')

@section('title') {!! menu()[2]->title !!} @endsection

@section('metadesc'){!! $metadescription !!}@endsection

@section('keywords'){!! $metatags !!}@endsection

@section('menuProd') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[2]->title !!}</h2>

            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[2]->title !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <section class="sidebar-shop shop-pages dart-pt-20">
        <div class="container">
            <div class="content-wrap ">
                <div class="shorter">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <span class="showing-result">{!! $list->firstItem().' - '.$list->lastItem().' '.trans('pagination.of').' '.$list->total().' '.trans('site.products') !!}</span>
                        </div>
                        <div class="col-sm-6 col-xs-6 text-right">
                            <div class="short-by">
                                <span>{!! trans('pagination.order') !!}</span>
                                <select class="selectpicker form-control" id="order" style="">
                                    <option value="c" {!! ($opc['ord'] == 'c')? 'selected' : '' !!}>{!! trans('site.categories') !!}</option>
                                    <option value="n" {!! ($opc['ord'] == 'n')? 'selected' : '' !!}>{!! trans('site.news') !!}</option>
                                    <option value="lp" {!! ($opc['ord'] == 'lp')? 'selected' : '' !!}>{!! trans('site.lprice') !!}</option>
                                    <option value="mp" {!! ($opc['ord'] == 'mp')? 'selected' : '' !!}>{!! trans('site.mprice') !!}</option>
                                    <option value="b" {!! ($opc['ord'] == 'b')? 'selected' : '' !!}>{!! trans('site.best_seller') !!}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-4 col-lg-3">
                        <div class="shop-sidebar mt-20">
                            <aside class="widget">
                                {!! Form::open(['route' => ['products'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar', 'id' => 'formSearch']) !!}
                                <div class="input-group input-group-lg">
                                    <input class="form-control" placeholder="{!! trans('site.search') !!}..." name="search" id="search" type="search" value="{!! $opc['search'] !!}" style="width: 100%;">
                                    <span class="input-group-btn">
                                        <button type="button" onclick="$('.search-bar').submit(); return false;" class="btn btn-lg"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                {!! Form::hidden('o',$opc['ord'],['id'=>'o']) !!}
                                {!! Form::hidden('cat',$opc['cat'],['id'=>'cat']) !!}
                                {!! Form::hidden('sub',$opc['sub'],['id'=>'sub']) !!}
                                {!! Form::hidden('fin',$opc['fin'],['id'=>'fin']) !!}
                                {!! Form::hidden('col',$opc['col'],['id'=>'col']) !!}
                                {!! Form::close() !!}
                            </aside>
                            <aside class="widget">
                                <h2 class="widget-title">{!! trans('site.categories') !!}</h2>
                                <div class="panel-group shop-links-widget" id="accordion" role="tablist"
                                     aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="all">
                                            <div class="panel-title">
                                                <div role="button" class="{!! ($opc['cat'] == '' && $opc['sub'] == '')? 'cat_active':'' !!} opc_cat" opc-target="">
                                                    {!! trans('site.all') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach($list->cat as $c)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="cat_{!! $c->id !!}">
                                            <div class="panel-title">
                                                <div role="button" class="{!! ($opc['cat'] == $c->slug)? 'cat_active':(count($c->subcategory) > 0)? '':'opc_cat' !!}"
                                                    {!! (count($c->subcategory) > 0)? 'href="#cat_s'.$c->id.'" data-toggle="collapse"':'opc-target="'.$c->slug.'"' !!}>
                                                    {!! $c->name !!}
                                                    {!! (count($c->subcategory) > 0)? '<span class="caret"></span>' : '' !!}
                                                </div>
                                            </div>
                                        </div>
                                        @if(count($c->subcategory) > 0)
                                        <div id="cat_s{!! $c->id !!}" class="panel-collapse collapse" role="tabpanel">
                                            <div class="panel-body">
                                                <ul class="list-unstyled">
                                                    <li style="cursor: pointer;" class="opc_cat"
                                                        opc-target="{!! $c->slug !!}">
                                                        <div>{!! trans('site.view_cat') !!} <i class="fa fa-arrow-right" style="float: right;"></i></div>
                                                    </li>
                                                    @foreach($c->subcategory as $sub)
                                                        <li style="cursor: pointer;" class="{!! ($opc['sub'] == $sub->slug)? 'cat_active':'' !!} opc_subcat"
                                                        opc-target="{!! $sub->slug !!}">
                                                            <div>{!! $sub->name !!}</div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    @endforeach
                                    @if(countSale() > 0)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="sale">
                                            <div class="panel-title">
                                                <div role="button" class="{!! ($opc['cat'] == 'sale')? 'cat_active':'' !!} opc_cat" opc-target="sale">
                                                    {!! trans('app.sale') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </aside>
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-8 col-lg-9 border-lft">
                        <div class="product-wrap">
                            <div class="row">
                            @if($list->isEmpty())
                                <div class="col-sm-12 col-md-12">{!! trans('app.no_result') !!}</div>
                            @endif
                            @foreach($list as $item)
                                <div class="col-sm-4 col-md-4">
                                    <div class="wa-theme-design-block pd_list">
                                        <figure class="dark-theme" onclick="location.href='/products/ver/{!! $item->slug !!}'">
                                            {!! Html::image($item->image,(!is_null($item->alt))? $item->alt : $item->name, ['class'=>'img_index']) !!}
                                        @if($item->new == '1')
                                                <div class="ribbon {!! ($item->sale == '1' and !is_null($item->sale_price))? 'with_sale':'' !!}"><span>{!! trans('site.new') !!}</span></div>
                                            @endif
                                            @if($item->st < 1 and $item->soon != '1')
                                                <div class="ribbon2"><span>{!! trans('products.unavailable') !!}</span></div>
                                            @endif
                                            @if($item->soon == '1')
                                                <div class="ribbon3"><span>{!! trans('products.soon') !!}</span></div>
                                            @endif
                                            @if($item->sale == '1' and !is_null($item->sale_price))
                                                <div class="ribbon_sale"><span>{!! $item->sale_percent !!}</span></div>
                                            @endif
                                        </figure>
                                        <div class="block-caption1">
                                            <div class="price">
                                                @if($item->sale == '1' and !is_null($item->sale_price))
                                                    <span class="sell-price sale_price">{!! $item->sale_money !!}</span>
                                                    <span class="sale_price2">{!! $item->money !!}</span>
                                                @else
                                                    <span class="sell-price">{!! $item->money !!}</span>
                                                @endif
                                            </div>
                                            <div class="clear"></div>
                                            <div class="clear"></div>
                                            <a href="/products/ver/{!! $item->slug !!}"><h4 class="t-product">{!! $item->name !!}</h4></a>
                                            <a href="/products/ver/{!! $item->slug !!}"><button {!! ($item->st >= 1 and $item->soon != '1')? '':'style="visibility:hidden;"' !!} class="btn_buy_pd btn rd-stroke-btn border_2px dart-btn-sm">{!! trans('site.buy') !!}</button></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        {!! $list->appends(Request::only(['search']))->render() !!}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('jsBottom')
    {!! Html::script('assets/js/products.js') !!}
@endsection