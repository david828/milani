@extends('site.layouts.page')

@section('metatitle') {!! (!is_null($data->metatitle))? $data->metatitle : appData()->name. ' - ' . trans('site.product_detail') !!} @endsection

@section('metadesc'){!! (!is_null($data->metadescription))? $data->metadescription : getAppSeo()->metadescription !!}@endsection

@section('keywords'){!! $data->metatags . ',' . getAppSeo()->metatags !!}@endsection

@section('menuProd') active @endsection

@section('cssBottom')
<style>

    #scrolling, #next-arrow, #prev-arrow {
        display: none;
    }
    /*set a border on the images to prevent shifting*/
    #gal1 img{border:2px solid white;}
    
    #mobile-indicator {
	    display: none;
	}

    #gal1 {
        text-align: center;
        width: 77%;
        margin-top: 15px;
    }

    /*Change the colour*/
    .active img{
        border: 1px solid {!! appData()->color !!} !important;
        background-color: {!! appData()->color !!} !important;
        border-radius: 20px !important;
    }
    
    @media only screen and (min-width : 320px) and (max-width : 480px) {
	 #gal1 {
	    	width: 100%;
	    	margin-top: 3px;
	    }

	    #mobile-indicator {
	        display: block;
	    }
	    
	    .img-gallery {
	        width: 150px !important;
	        height: auto !important;
	    }
	    
	    .page_title_ctn h2 {
	    	font-size: 18px;
	    }
        #scrolling {
            display: block;
            width: 100%;
            height: 85%;
            position: absolute;
            z-index: 500;
        }

        #gallery-pd {
            position: relative;
        }

        #next-arrow, #prev-arrow{
            display: block;
            position: absolute;
            font-size: 40px;
            color: #000000;
            top: 45%;
            z-index: 501;
            padding: 0 7px;
        }

        #next-arrow i, #prev-arrow i {
            font-weight: bolder;
        }

        #next-arrow:hover, #prev-arrow:hover {
            color: #ffffff;
            background: #000000;
        }

        #prev-arrow {
            left:0;
        }

        #next-arrow {
            right: 0;
        }
	}

    .img-gallery {
        width: 352px !important;
        height: 382px !important;
        margin-left: auto !important;
        margin-right: auto !important;
    }

</style>
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! $data->category[0]->name !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li><a href="/products">{!! menu()[2]->title !!}</a></li>
                <li class="active"><span>{!! $data->name !!}</span></li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <!--Shoping with Sidebar Section-->
    <div class="shop-pages">
        <section class="product-single-wrap section-padding" {!! (!is_null($data->review))? 'style="padding-bottom:0px;"':'' !!}>
            <div class="container">
                <div class="product-content-wrap">
                    <div class="row">
                        <div class="col-md-5 col-sm-8 col-sm-offset-2 col-md-offset-0">
                            <div id="scrolling"></div>
                            <div id="gallery-pd" >
                                <div id="next-arrow"><i class="fa fa-angle-right"></i> </div>
                                <div id="prev-arrow"><i class="fa fa-angle-left"></i> </div>
                            <!-- template -->
                            @if(count($data->productcolor) > 0)
                                @if(count($data->productcolor[0]->images) > 0)
                                    @foreach($data->productcolor[0]->images as $imgp)
                                        @if ($imgp->id === $data->productcolor[0]->images[0]->id)
                                            <img id="img_01" src="/images/products/{!! $imgp->image !!}" data-zoom-image="/images/products/{!! $imgp->image !!}"  alt="{!! $data->name !!}" class="img-gallery"/>
                                            <div id="gal1">
                                                <a href="#" data-image="/images/products/{!! $imgp->image !!}" data-zoom-image="/images/products/{!! $imgp->image !!}" class="active">
                                                    <img id="img_01d" src="/assets/images/dots.png" alt="gallery"/>
                                                </a>
                                            @else
                                                <a href="#" data-image="/images/products/{!! $imgp->image !!}" data-zoom-image="/images/products/{!! $imgp->image !!}">
                                                    <img id="img_01d" src="/assets/images/dots.png" alt="gallery"/>
                                                </a>
                                            @endif
                                    @endforeach
                                            </div>
                                @else
                                    <img id="img_01" src="{!! $data->image !!}" data-zoom-image="{!! $data->image !!}"  alt="{!! $data->name !!}"  class="img-gallery"/>
                                    <div id="gal1">
                                        <a href="#" data-image="{!! $data->image !!}" data-zoom-image="{!! $data->image !!}" class="active">
                                            <img id="img_01d" src="/assets/images/dots.png" alt="gallery" />
                                        </a>
                                    </div>
                                @endif
                            @else
                                @if(count($data->images) > 0)
                                    @foreach($data->images as $img)
                                        @if ($img->id === $data->images[0]->id)
                                            <img id="img_01" src="/images/products/{!! $img->image !!}" data-zoom-image="/images/products/{!! $img->image !!}"  alt="{!! $data->name !!}"  class="img-gallery"/>
                                            <div id="gal1">
                                                <a href="#" data-image="/images/products/{!! $img->image !!}" data-zoom-image="/images/products/{!! $img->image !!}" class="active">
                                                    <img id="img_01d" src="/assets/images/dots.png" alt="gallery" />
                                                </a>
                                        @else
                                                <a href="#" data-image="/images/products/{!! $img->image !!}" data-zoom-image="/images/products/{!! $img->image !!}">
                                                    <img id="img_01d" src="/assets/images/dots.png" alt="gallery" />
                                                </a>
                                        @endif
                                    @endforeach
                                            </div>
                                @else
                                    <img id="img_01" src="{!! $data->image !!}" data-zoom-image="{!! $data->image !!}"  alt="{!! (!is_null($data->alt))? $data->alt:$data->name !!}"  class="img-gallery"/>
                                    <div id="gal1">
                                        <a href="#" data-image="{!! $data->image !!}" data-zoom-image="{!! $data->image !!}" class="active">
                                            <img id="img_01d" src="/assets/images/dots.png" alt="gallery" />
                                        </a>
                                    </div>
                                @endif
                            @endif
                            <!-- end of template -->
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12">
                            <div class="product-content dart-mt-30">
                                <div class="product-title">
                                    <h2>{!! $data->name !!}</h2>
                                </div>
                                <div class="product-price-review">
                                    @if($data->sale == '1' and !is_null($data->sale_price))
                                        <span class="font-semibold black-color sale_price">{!! $data->sale_money !!}</span>
                                        <span class="sale_price2">{!! $data->money !!}</span>
                                    @else
                                        <span class="font-semibold black-color">{!! $data->money !!}</span>
                                    @endif
                                </div>
                                <div class="product-description">
                                    <p>{!! $data->description !!}</p>
                                    <ul class="list-unstyled">
                                        <li>-  REF: <span id="referenceP">{!! (count($data->productcolor) > 0)? $data->productcolor[0]->reference : $data->reference !!}</span></li>
                                        <!--@if(!is_null($data->SKU))
                                            <li>-  SKU: <span id="SKUP">{!! (count($data->productcolor) > 0)? $data->productcolor[0]->SKU : $data->SKU !!}</span></li>
                                        @endif-->
                                        @if(count($data->finish) > 0)
                                        <li>-  {!! trans('site.finish') . ': ' . $data->finish[0]->name !!}</li>
                                        @endif
                                        @php
                                            $st = '</br>';
                                            if($data->soon != '1') {
                                                if(count($data->productcolor) > 0) {
                                                    if($data->productcolor[0]->stock > 0) {
                                                        $st = '</br>';
                                                    } else {
                                                        $st = '- <span style="color:red;">' . trans('products.unavailable') . '</span> - <div id="DivLeave" data-toggle="modal" data-target="#modalData" class="div-leave">'.trans('products.msg_av').'</div>';
                                                    }
                                                } else {
                                                    if($data->stock > 0) {
                                                        //$st = $data->stock . ' '.trans('site.availables');
                                                        $st = '</br>';
                                                    } else {
                                                        $st = '- <span style="color:red;">' . trans('products.unavailable') . '</span> - <div id="DivLeave" data-toggle="modal" data-target="#modalData" class="div-leave">'.trans('products.msg_av').'</div>';
                                                    }
                                                }
                                            }
                                        @endphp
                                        <li>-  {!! trans('site.categories').': '.$data->category[0]->name !!}</li>
                                        <li>  <span id="stockP">{!! $st !!}</span></li>
                                    </ul>
                                    @if(count($data->productcolor) > 0)
                                        <div class="list-colors">
                                            <span class="black-color">{!! trans('site.colors') !!}</span>
                                            <ul class="list-unstyled">
                                                @php($aux = 0)
                                                @foreach($data->productcolor as $col)
                                                    <li class="col-md-3 col-xs-6">
                                                        <div class="opc_color {!! ($aux == 0)? 'c-active':'' !!}" opc-target="{!! $col->id_color !!}" opc-name="{!! $col->color[0]->name !!}">
                                                            {!! Html::image('images/colors/'.$col->color[0]->image,$col->color[0]->name,['class' => 'l-color','title'=>$col->color[0]->name]) !!}
                                                            <span>{!! $col->color[0]->name !!}</span>
                                                        </div>
                                                    </li>
                                                    @php($aux++)
                                                @endforeach
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endif
                                    @if($data->soon == '1')
                                        <hr>
                                        <p style="text-transform: uppercase;">{!! trans('products.soon') !!}</p>
                                    @else
                                    <div class="qty-add-wish-btn">
                                        {!! Form::open(['route'=>['addcart'],'method'=>'POST','name'=>'frmCart','id'=>'frmCart','role'=>'form','enctype'=>'multipart/form-data']) !!}
                                        <span class="quantity">
                                            <input type="text" class="input-text qty text" title="Cantidad" value="1" name="cart" id="cart" required>
                                            <input type="button" class="minus" value="-">
                                            <input type="button" class="plus" value="+">
                                        </span>
                                        <span><input type="submit" class="btn rd-stroke-btn border_2px dart-btn-sm" id="addCart" value="{!! trans('site.add_cart') !!}" /></span>
                                        <p><span id="msg_cart" style="display: none;"></span></p>
                                        {!! Form::hidden('stock',(count($data->productcolor) > 0)? $data->productcolor[0]->stock : $data->stock, ['id' => 'stock']) !!}
                                        {!! Form::hidden('id_p',$data->id, ['id' => 'id_p', 'required']) !!}
                                        {!! Form::hidden('h_color',(count($data->productcolor) > 0)?'S':'N', ['id' => 'h_color']) !!}
                                        {!! Form::hidden('id_c',(count($data->productcolor) > 0)? $data->productcolor[0]->id_color : '', ['id' => 'id_c']) !!}
                                        {!! Form::hidden('name_c',(count($data->productcolor) > 0)? $data->productcolor[0]->color[0]->name : '', ['id' => 'name_c']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                    @endif
                                    <div class="social-media">
                                        <span class="black-color">{!! trans('blog.sharein') !!}</span>
                                        <ul class="social-icons list-unstyled">
                                            <li>
                                                <a href="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                    <span class="fa fa-facebook"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:window.open('https://twitter.com/?status='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                    <span class="fa fa-twitter"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:window.open('https://plus.google.com/share?url='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                    <span class="fa fa-google-plus"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:window.open('https://www.linkedin.com/shareArticle?url='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                    <span class="fa fa-linkedin"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!is_null($data->review))
                <div class="review">{!! $data->review !!}</div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    {!! Html::image('/assets/images/animals.png', 'cruelty free',['class'=>'img-responsive', 'style'=>'margin:10px auto;']) !!}
                </div>
            </div>
        </section>
    </div>
    <div id="mobile-indicator"></div>

    <div class="modal fade" id="modalData" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div id="DivLeaveD" style="display: block;">
                                {!! Form::open(['route'=>'leave','method'=>'POST', 'class' => 'form', 'name'=>'frmLeaveD', 'id' => 'frmLeaveD']) !!}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    {!! Form::email('emailLD',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email'), 'required', 'id'=>'emailLD']) !!}
                                    {!! Form::hidden('hn_productLD', null, ['id'=>'hn_productLD']) !!}
                                    {!! Form::hidden('hn_colorLD', null, ['id'=>'hn_colorLD']) !!}
                                </div>
                                <p id="error-Leave" style="display: none;"></p>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center;">
                                        {!! Form::button(trans('products.btn_av'),['class'=>'btn normal-btn dart-btn-xs', 'id'=>'btn-leave']) !!}
                                    </div><!-- /.col -->
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($data->productcolor as $pc)
        @php( $imgs = '')
        @foreach($pc->images as $imgp)
            @php
                $imgs.= '/images/products/'. $imgp->image .'|';
            @endphp
        @endforeach
        @php
            if($data->soon == '1') {
                $st = '</br>';
            } else {
                if($pc->stock > 0) {
                    $st = '</br>';
                } else {
                    $st = '- <span style="color:red;">' . trans('products.unavailable') . '</span> - <div id="DivLeave" data-toggle="modal" data-target="#modalData" class="div-leave">'.trans('products.msg_av').'</div>';
                }
            }
        @endphp
        {!! Form::hidden('color_'.$pc->id_color,$imgs,['id'=>'color_'.$pc->id_color]) !!}
        {!! Form::hidden('sColor_'.$pc->id_color,(count($pc->images)>0)?'1':'0',['id'=>'sColor_'.$pc->id_color]) !!}
        {!! Form::hidden('stockT_'.$pc->id_color,$st,['id'=>'stockT_'.$pc->id_color]) !!}
        {!! Form::hidden('stock_'.$pc->id_color,$pc->stock,['id'=>'stock_'.$pc->id_color]) !!}
        {!! Form::hidden('reference_'.$pc->id_color,$pc->reference,['id'=>'reference_'.$pc->id_color]) !!}
        {!! Form::hidden('SKU_'.$pc->id_color,$pc->SKU,['id'=>'SKU_'.$pc->id_color]) !!}
    @endforeach
@endsection
@section('jsBottom')
    {!! Html::script('assets/vendor/elevatezoom-master/jquery.elevatezoom.js') !!}
    {!! Html::script('assets/js/product.js') !!}
@endsection

