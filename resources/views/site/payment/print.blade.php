<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! trans('app.invoice') !!} # {!! $data->id !!} - {!! $data->customer_name !!}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {!! Html::style('cms/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@yield('cssTop')
{!! Html::style('cms/dist/css/AdminLTE.min.css') !!}
{!! Html::style('cms/dist/css/general.css') !!}
{!! Html::style('cms/dist/css/skins/_all-skins.min.css') !!}
@yield('cssBottom')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    {!! appData()->name_boucher !!}
                    <small class="pull-right">{!! trans('invoice.date') !!}: {!! date('d/m/Y') !!}</small>
                </h2>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                {!! trans('invoice.from') !!}
                <address>
                    <strong>{!! appData()->name_boucher !!}</strong><br>
                    {!! appData()->address_boucher !!}<br>
                    {!! trans('invoice.phone') !!}: {!! appData()->phone_boucher !!}<br>
                    {!! trans('invoice.email') !!}: {!! appData()->email_boucher !!}<br>
                    {!! trans('invoice.website') !!}: {!! appData()->website_boucher !!}<br>
                </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                {!! trans('invoice.for') !!}
                <address>
                    <strong>{!! $data->customer_name !!}</strong><br>
                    <b>{!! $data->customer_documenttype . ' ' .$data->customer_document !!}</b><br>
                    {!! $data->customer_address !!}<br>
                    {!! trans('invoice.phone') !!}: {!! $data->customer_phone !!}<br>
                    {!! trans('invoice.email') !!}: {!! $data->customer_email !!}
                </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>{!! trans('invoice.invoice') !!} #{!! $data->id !!}</b><br>
                <b>{!! trans('invoice.reference') !!}:</b> {!! $data->reference !!}<br>
                <b>{!! trans('invoice.date') !!}:</b> {!! $data->date !!}<br>
                <b>{!! trans('invoice.status') !!}:</b> {!! $data->status[0]->name !!}
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{!! trans('invoice.quantity') !!}</th>
                        <th>{!! trans('invoice.product').' / '.trans('invoice.service') !!}</th>
                        <th>{!! trans('invoice.subtotal') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data->details as $item)
                        <tr>
                            <td>{!! $item->quantity !!}</td>
                            <td>{!! $item->description !!}</td>
                            <td>${!! number_format($item->total) !!}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    {!! trans('invoice.note') !!}
                </p>
                @if(!is_null($address))
                    <div>
                        <h4>{!! trans('cart.shipping_data') !!}</h4>
                        <p>{!! $address->documenttype.' - '.$address->document !!}</p>
                        <p>{!! $address->name !!}</p>
                        <p>{!! $address->email.' - '. $address->phone!!}</p>
                        <p>{!! $address->address !!}</p>
                        <p>{!! $address->post_code !!}</p>
                        <p>{!! $address->additional !!}</p>
                    </div>
                @endif
            </div><!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">{!! trans('invoice.date') !!} {!! $data->date !!}</p>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">{!! trans('invoice.value') !!}:</th>
                            <td>${!! number_format($data->subtotal) !!}</td>
                        </tr>
                        <tr>
                            <th>{!! trans('invoice.discount') !!} ({!! number_format($data->discount) !!}%)</th>
                            <td>${!! number_format($data->discount_value) !!}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">{!! trans('invoice.subtotal') !!}:</th>
                            <td>${!! number_format($data->subtotal2) !!}</td>
                        </tr>
                        @if(appData()->tax_value!=0)
                            <tr>
                                <th>{!! trans('invoice.tax') !!} ({!! number_format($data->tax) !!}%)</th>
                                <td>${!! number_format($data->tax_value) !!}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>{!! trans('invoice.shipping') !!}:</th>
                            <td>${!! number_format($data->shipping) !!}</td>
                        </tr>
                        <tr>
                            <th>{!! trans('invoice.total') !!}:</th>
                            <td>${!! number_format($data->total) !!}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
{!! Html::script('cms/dist/js/app.min.js') !!}
</body>
</html>
