@extends('site.layouts.page')

@section('title') {!! trans('title.payment_response') !!} @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2> {!! trans('title.payment_response') !!} </h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span> {!! trans('title.payment_response') !!} </span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="block-content">

                        <div class="cart-page-area">
                            <div class="container">
                                <section class="invoice">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                                {!! appData()->name !!}
                                                <small class="pull-right">{!! trans('invoice.date') !!}: {!! date('d/m/Y') !!}</small>
                                            </h2>
                                        </div><!-- /.col -->
                                    </div>
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-4 invoice-col">
                                            {!! trans('invoice.from') !!}
                                            <address>
                                                <strong>{!! appData()->name !!}</strong><br>
                                                {!! appData()->address !!}<br>
                                                {!! trans('invoice.phone') !!}: {!! appData()->phone !!}<br>
                                                {!! trans('invoice.email') !!}: {!! appData()->email !!}<br>
                                                {!! trans('invoice.website') !!}: {!! appData()->website !!}<br>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            {!! trans('invoice.for') !!}
                                            <address>
                                                <strong>{!! $data->customer_name !!}</strong><br>
                                                {!! $data->customer_address !!}<br>
                                                {!! trans('invoice.phone') !!}: {!! $data->customer_phone !!}<br>
                                                {!! trans('invoice.email') !!}: {!! $data->customer_email !!}
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            <b>{!! trans('invoice.invoice') !!} #{!! $data->id !!}</b><br>
                                            <b>{!! trans('invoice.reference') !!}:</b> {!! $data->reference !!}<br>
                                            <b>{!! trans('invoice.date') !!}:</b> {!! $data->date !!}<br>
                                            <b>{!! trans('invoice.status') !!}:</b> {!! $data->status[0]->name !!}
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-xs-12 table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>{!! trans('invoice.quantity') !!}</th>
                                                    <th>{!! trans('invoice.product').' / '.trans('invoice.service') !!}</th>
                                                    <th>{!! trans('invoice.subtotal') !!}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($data->details as $item)
                                                    <tr>
                                                        <td>{!! $item->quantity !!}</td>
                                                        <td>{!! $item->description !!}</td>
                                                        <td>${!! number_format($item->total) !!}</td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                    <div class="row">
                                        <!-- accepted payments column -->
                                        <div class="col-xs-6">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                {!! trans('invoice.note') !!}
                                            </p>
                                        </div><!-- /.col -->
                                        <div class="col-xs-6">
                                            <p class="lead">{!! trans('invoice.date') !!} {!! $data->date !!}</p>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th style="width:50%">{!! trans('invoice.value') !!}:</th>
                                                        <td>${!! number_format($data->subtotal) !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>{!! trans('invoice.discount') !!} ({!! number_format($data->discount) !!}%)</th>
                                                        <td>${!! number_format($data->discount_value) !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:50%">{!! trans('invoice.subtotal') !!}:</th>
                                                        <td>${!! number_format($data->subtotal2) !!}</td>
                                                    </tr>
                                                    @if(appData()->tax_value!=0)
                                                        <tr>
                                                            <th>{!! trans('invoice.tax') !!} ({!! number_format($data->tax) !!}%)</th>
                                                            <td>${!! number_format($data->tax_value) !!}</td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <th>{!! trans('invoice.shipping') !!}:</th>
                                                        <td>${!! number_format($data->shipping) !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>{!! trans('invoice.total') !!}:</th>
                                                        <td>${!! number_format($data->total) !!}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row no-print">
                                        <div class="col-xs-12">
                                            {!! html_entity_decode(link_to('/','<i class="fa fa-arrow-left"></i> '.trans('invoice.finish'),['class'=>'btn-apply-coupon disabled'])) !!}
                                            {!! html_entity_decode(link_to_route('payment.print','<i class="fa fa-print"></i> '.trans('invoice.print'),['id'=>$data->id],['class'=>'pull-right btn-apply-coupon disabled','target'=>'_blank'])) !!}
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection