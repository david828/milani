<html>
<style>
    body {
        margin: 0px;
    }
    .maintable {
        width: 600px;
        border-left: 1px solid gray;
        border-right: 1px solid gray;
    }
    .header {
        background-repeat: no-repeat;
        height: 70px;
        background-color: #8B6A55;
    }
    .company {
        position: relative;
        font-size: 40px;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        color: #FFFFFF;
        width: 300px;
        padding-left: 20px;
    }
    .title {
        color: #8B6A55;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        font-weight: bold;
    }

    .text {
        color: #777676;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
    }

    .padding {
        padding-left: 20px;
    }

    .content {
        padding-left: 20px;
        padding-right: 20px;
    }

    .upbottom {
        height: 328px;
    }

    .bottom {
        background-color: #8B6A55;
        height: 31px;
    }
</style>

<body>
<center>
    <table class="maintable" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2" class="header">
                <div class="company">{!! appData()->name !!}</div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #d8b46b; height: 20px;">

            </td>
        </tr>
        <tr>
            <td class="content title"><br>Correo informativo</td>
        </tr>
        <tr>
            <td class="content text" colspan="2">              <br>
                <p>Alguien está interesado en un producto sin stock en este momento, sería bueno que hicieras un seguimiento.</p>
                <p><b>Correo: </b> {!! $emailData['email']  !!}</p>
                <p><b>Producto: </b>{!! $emailData['product']  !!}</p>
            </td>
        </tr>
        <tr>
            <td class="nomail">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="upbottom">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="text padding">
                                        <br><br>
                                        Cordialmente,<br><br>
                                        Servicio de Contacto<br>
                                        {!! appData()->name !!}<br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="bottom">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
</body>

</html>