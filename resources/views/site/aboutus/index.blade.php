@extends('site.layouts.page')

@section('title') {!! menu()[1]->title !!} @endsection

@section('menuAbout') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[1]->title !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[1]->title !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section id="aboutus-section"><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="img-aboutus">
                    {!! Html::image($home->image, menu()[0]->title,['class' => 'img-responsive']) !!}
                </div>
                <div class="text-aboutus">
                    {!! $home->text !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')

@endsection