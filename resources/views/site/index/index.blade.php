@extends('site.layouts.page')

@section('menuIndex') active @endsection

@section('title') {!! menu()[0]->title !!} @endsection

@section('cssBottom')

@endsection

@section('banner')

@endsection
@php
$aux1 = '<div class="col-sm-6 box-instagram">
            <div class="master-slider ms-skin-default" id="galeria-instagram" style="padding:0 5px;">
            </div>
        </div>
        <div class="col-sm-6 box-instagram">
            <div class="master-slider ms-skin-default" id="galeria-instagram2" style="padding:0 5px;">
            </div>
    </div>';

$aux2 = '<div class="col-sm-12" style="margin-top:5px; margin-left:5px;">
            <img src="images/header/'.$home->header->image_2.'" class="img-responsive" alt="Header">
        </div>';

if($home->header->pos_2 == 0)
{
    $box3 = $aux1;
    $box4 = $aux2;
} else {
    $box3 = $aux2;
    $box4 = $aux1;
}

$box1 = '<div id="owl-header" class="master-slider ms-skin-default">';
foreach($home->imgheader as $img)
{
    $link = '';
    if(!is_null($img->link) && !is_null($img->typelink))
    {
        if($img->typelink == 1) {
            $url = $img->link;
            $tar = '_blank';
        } else {
            $url = '/'.$img->link;
            $tar = '_self';
        }
        $link = '<a href="'.$url.'" target="'.$tar.'"></a>';
    }
    $box1.='<div class="ms-slide" data-delay="15">
            <img src="/assets/vendor/masterslider/style/blank.gif" data-src="images/header/'.$img->image.'" alt="header'.$img->id.'" class="img-responsive">
           '.$link.'
        </div>';
}
$box1.= '</div>';
$box2 = '<div class="clearfix no-gutter">'.$box3.$box4.'</div>';
@endphp
@section('content')

    <section class="dart-no-padding">
        <div class="container-fluid">
            <div class="row no-gutter" style="margin-top:5px;">
                <div class="col-lg-6 col-md-6">
                    {!! ($home->header->pos_1 == 0)?$box2:$box1 !!}
                </div>
                <div class="col-lg-6 col-md-6">
                    {!! ($home->header->pos_1 == 0)?$box1:$box2 !!}
                </div>
            </div>
        </div>
    </section>
    <section class="product-slide">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel" class="tabSix text-center"><!--Style 6-->
                        <!-- Nav tabs -->
                        <ul id="tabSix" class="nav nav-tabs">
                            @if(count($home->featured) > 0)
                            <li class="active">
                                <a href="#contentSix-featureds" data-toggle="tab">
                                    <p>{!! $home->prodtitle->title_featureds !!}</p>
                                </a>
                            </li>
                            @endif
                            @if(count($home->new) > 0)
                            <li {!! (count($home->featured) == 0)? 'class="active"':'' !!}>
                                <a href="#contentSix-new" data-toggle="tab">
                                    <p>{!! $home->prodtitle->title_news !!}</p>
                                </a>
                            </li>
                            @endif
                            @if(count($home->best) > 0)
                            <li {!! (count($home->featured) == 0 and count($home->new) == 0)? 'class="active"':'' !!}>
                                <a href="#contentSix-best" data-toggle="tab">
                                    <p>{!! $home->prodtitle->title_bests !!}</p>
                                </a>
                            </li>
                            @endif
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @if(count($home->featured) > 0)
                            <div class="tab-pane in active" id="contentSix-featureds">
                                <div class="owl-carousel owl-theme">
                                    @foreach($home->featured as $item)
                                        <div class="wa-theme-design-block">
                                            <figure class="dark-theme" onclick="location.href='/products/ver/{!! $item->slug !!}'">
                                                {!! Html::image($item->image,(!is_null($item->alt))? $item->alt : $item->name) !!}
                                                @if($item->new == '1')
                                                    <div class="ribbon {!! ($item->sale == '1' and !is_null($item->sale_price))? 'with_sale':'' !!}"><span>{!! trans('site.new') !!}</span></div>
                                                @endif
                                                @if($item->st < 1 and $item->soon != '1')
                                                    <div class="ribbon2"><span>{!! trans('products.unavailable') !!}</span></div>
                                                @endif
                                                @if($item->soon == '1')
                                                    <div class="ribbon3"><span>{!! trans('products.soon') !!}</span></div>
                                                @endif
                                                @if($item->sale == '1' and !is_null($item->sale_price))
                                                    <div class="ribbon_sale"><span>{!! $item->sale_percent !!}</span></div>
                                                @endif
                                            </figure>
                                            <div class="block-caption1">
                                                <div class="price">
                                                    @if($item->sale == '1' and !is_null($item->sale_price))
                                                        <span class="sell-price sale_price">{!! $item->sale_money !!}</span>
                                                        <span class="sale_price2">{!! $item->money !!}</span>
                                                    @else
                                                        <span class="sell-price">{!! $item->money !!}</span>
                                                    @endif
                                                </div>
                                                <div class="clear"></div>
                                                <a href="/products/ver/{!! $item->slug !!}"><h4 class="t-product">{!! $item->name !!}</h4></a>
                                                @if($item->st >= 1 and $item->soon != '1')
                                                    <a href="/products/ver/{!! $item->slug !!}"><button class="btn_buy_pd btn rd-stroke-btn border_2px dart-btn-sm">{!! trans('site.buy') !!}</button></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            @if(count($home->new) > 0)
                            <div class="tab-pane {!! (count($home->featured) == 0)? 'in active':'' !!}" id="contentSix-new">
                                <div class="owl-carousel owl-theme">
                                    @foreach($home->new as $item)
                                        <div class="wa-theme-design-block">
                                            <figure class="dark-theme" onclick="location.href='/products/ver/{!! $item->slug !!}'">
                                                {!! Html::image($item->image,(!is_null($item->alt))? $item->alt : $item->name) !!}
                                                @if($item->new == '1')
                                                    <div class="ribbon {!! ($item->sale == '1' and !is_null($item->sale_price))? 'with_sale':'' !!}"><span>{!! trans('site.new') !!}</span></div>
                                                @endif
                                                @if($item->st < 1 and $item->soon != '1')
                                                    <div class="ribbon2"><span>{!! trans('products.unavailable') !!}</span></div>
                                                @endif
                                                @if($item->soon == '1')
                                                    <div class="ribbon3"><span>{!! trans('products.soon') !!}</span></div>
                                                @endif
                                                @if($item->sale == '1' and !is_null($item->sale_price))
                                                    <div class="ribbon_sale"><span>{!! $item->sale_percent !!}</span></div>
                                                @endif
                                            </figure>
                                            <div class="block-caption1">
                                                <div class="price">
                                                    @if($item->sale == '1' and !is_null($item->sale_price))
                                                        <span class="sell-price sale_price">{!! $item->sale_money !!}</span>
                                                        <span class="sale_price2">{!! $item->money !!}</span>
                                                    @else
                                                        <span class="sell-price">{!! $item->money !!}</span>
                                                    @endif
                                                </div>
                                                <div class="clear"></div>
                                                <a href="/products/ver/{!! $item->slug !!}"><h4 class="t-product">{!! $item->name !!}</h4></a>
                                                @if($item->st >= 1 and $item->soon != '1')
                                                    <a href="/products/ver/{!! $item->slug !!}"><button class="btn_buy_pd btn rd-stroke-btn border_2px dart-btn-sm">{!! trans('site.buy') !!}</button></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            @if(count($home->best) > 0)
                            <div class="tab-pane {!! (count($home->featured) == 0 and count($home->new) == 0)? 'in active':'' !!}" id="contentSix-best">
                                <div class="tab-pane" id="contentSix-2">
                                    <div class="owl-carousel owl-theme">
                                        @foreach($home->best as $item)
                                        <div class="wa-theme-design-block">
                                            <figure class="dark-theme" onclick="location.href='/products/ver/{!! $item->slug !!}'">
                                                {!! Html::image($item->image,(!is_null($item->alt))? $item->alt : $item->name) !!}
                                                @if($item->new == '1')
                                                    <div class="ribbon {!! ($item->sale == '1' and !is_null($item->sale_price))? 'with_sale':'' !!}"><span>{!! trans('site.new') !!}</span></div>
                                                @endif
                                                @if($item->st < 1 and $item->soon != '1')
                                                    <div class="ribbon2"><span>{!! trans('products.unavailable') !!}</span></div>
                                                @endif
                                                @if($item->soon == '1')
                                                    <div class="ribbon"><span>{!! trans('products.soon') !!}</span></div>
                                                @endif
                                                @if($item->sale == '1' and !is_null($item->sale_price))
                                                    <div class="ribbon_sale"><span>{!! $item->sale_percent !!}</span></div>
                                                @endif
                                            </figure>
                                            <div class="block-caption1">
                                                <div class="price">
                                                    @if($item->sale == '1' and !is_null($item->sale_price))
                                                        <span class="sell-price sale_price">{!! $item->sale_money !!}</span>
                                                        <span class="sale_price2">{!! $item->money !!}</span>
                                                    @else
                                                        <span class="sell-price">{!! $item->money !!}</span>
                                                    @endif
                                                </div>
                                                <div class="clear"></div>
                                                <a href="/products/ver/{!! $item->slug !!}"><h4 class="t-product">{!! $item->name !!}</h4></a>
                                                @if($item->st >= 1 and $item->soon != '1')
                                                    <a href="/products/ver/{!! $item->slug !!}"><button class="btn_buy_pd btn rd-stroke-btn border_2px dart-btn-sm">{!! trans('site.buy') !!}</button></a>
                                                @endif
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    @if($home->productdes)
    <!-- Info banner -->
    <section class="super-deal-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="super-deal">
                        <p class="dart-fs-18">{!! $home->productdes->subtitle !!}</p>
                        <h1 class="dart-fs-48">{!! $home->productdes->title !!}</h1>
                        <p>{!! $home->productdes->text !!}</p>
                        <div class="clearfix"></div>
                        @if(!is_null($home->productdes->link))
                            <a href="{!! $home->productdes->link !!}" class="btn btn-normal" target="{!! ($home->productdes->window === 1)? '_self':'_blank' !!}">{!! trans('site.more_info') !!}</a>
                        @endif
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                    {!! Html::image('images/products/'.$home->productdes->image, 'Product',['class' => 'img-responsive']) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /Info banner -->
    @endif
    @if(count(getArticles(3)) > 2)
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="dart-headingstyle-one dart-mb-60 text-center">  <!--Style 1-->
                    <h2 class="dart-heading">{!! trans('site.last') !!}</h2>
                    {!! Html::image('assets/images/Icon-sep.png','img') !!}
                </div>
            </div>
            <div class="row no-gutter">
                @foreach(getArticles(3) as $art)
                    @if($loop->iteration % 2 != 0)
                        <div class="col-md-4 col-sm-4">
                            <div class="blog-wapper">
                                <div class="blog-img ImageWrapper">
                                    {!! html_entity_decode(link_to_route('article.view',
                                    Html::image('images/articles/sm_'.$art->image,$art->name,['class'=>'img-responsive']).'<div class="PStyleHe"></div>',
                                    ['id'=>$art->slug],['class' => 'bubble-top'])) !!}
                                </div>
                                <div class="blog-content">
                                    <h4 class="blog-title">
                                        {!! html_entity_decode(link_to_route('article.view',$art->name,['id'=>$art->slug])) !!}
                                    </h4>
                                    <p class="post-date">{!! $art->date !!}</p>
                                    <p class="post-content">{!! substr($art->description,0,90) . '...' !!}</p>
                                    {!! html_entity_decode(link_to_route('article.view', trans('site.read_more'),['id'=>$art->slug])) !!}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-4 col-sm-4">
                            <div class="blog-wapper">
                                <div class="blog-content dart-mb-0">
                                    <h4 class="blog-title">
                                        {!! html_entity_decode(link_to_route('article.view',$art->name,['id'=>$art->slug])) !!}
                                    </h4>
                                    <p class="post-date">{!! $art->date !!}</p>
                                    <p class="post-content">{!! substr($art->description,0,90) . '...' !!}</p>
                                    {!! html_entity_decode(link_to_route('article.view', trans('site.read_more'),['id'=>$art->slug])) !!}
                                </div>
                                <div class="blog-img ImageWrapper">
                                    {!! html_entity_decode(link_to_route('article.view',
                                    Html::image('images/articles/sm_'.$art->image,$art->name,['class'=>'img-responsive']).'<div class="PStyleHe"></div>',
                                    ['id'=>$art->slug],['class' => 'bubble-top'])) !!}
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    @endif
@endsection
@section('jsBottom')
    {!! Html::script('assets/js/index.js') !!}
    <script type="application/javascript">
        jQuery(document).ready(function () {
            getInstagram('{!! $home->header->client_id !!}', '{!! $home->header->user_id !!}', '{!! $home->header->accessToken !!}', '{!! $home->header->num_photos !!}')
        });

    </script>
@endsection
