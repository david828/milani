@extends('site.layouts.page')

@section('title') {!! menu()[6]->title !!} @endsection

@section('menuDist') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[6]->title !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[6]->title !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section id="distribution-section"><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 products-content-holder">
                    <!-- Full Width -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="shop-sort">
                                    <h5>{!! trans('pagination.currentPage') . ' ' . $list->currentPage() . ' ' . trans('pagination.lastPage') . ' ' . $list->lastPage() !!}</h5>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <h4>{!! trans('site.searchby') !!}</h4>
                                {!! Form::open(['route' => ['distribution'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar']) !!}
                                <input class="form-control" type="search" placeholder="{!! trans('site.search') !!} &hellip;" value="{!! $list->search !!}" name="search">
                                <a href="#" onclick="$('.search-bar').submit(); return false;"><i class="icon-search4"></i></a>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- Full Width -->
                    <hr>
                    <div class="shop-holder ">
                        <div class="row">
                            @foreach($list as $item)
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xs-full-width">
                                    <div class="product-column distribution-item" id="{!! $item->id !!}">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="distribution-img">
                                                    {!! Html::image($item->image, $item->name, ['id' => 'logo-'.$item->id]) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <h4>{!! $item->name !!}</h4>
                                                {!! ($item->phones !== '')? '<span>'.$item->phones.'</span></br>' : '' !!}
                                                {!! ($item->email !== '')? '<span>'.$item->email.'</span></br>' : '' !!}
                                                {!! ($item->location !== '')? '<span>'.$item->location.'</span></br>' : '' !!}
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Pagination -->
                    <div class="col-sm-12">
                        {!! $list->appends(Request::only(['search']))->render() !!}
                    </div>
                    <!-- Pagination -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')

@endsection