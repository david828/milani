@extends('site.layouts.page')

@section('title') {!! menu()[3]->title !!} @endsection

@section('menuGal') active @endsection

@section('cssBottom')

@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[3]->title !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li><a href="/galleries">{!! menu()[3]->title !!}</a></li>
                <li class="active"><span>{!! $data->name !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section class="blog-single" id="blog_s_post"><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="gallery-item">
                        <h3>{!! $data->name !!}</h3>
                        <p>{!! $data->description !!}</p>
                        <div class="gallery-img">
                            <!-- template -->
                            <div class="ms-showcase2-template ms-showcase2-vertical">
                                <!-- masterslider -->
                                <div class="master-slider ms-skin-default" id="masterslidergallery">
                                    @foreach($data->images as $img)
                                    <div class="ms-slide">
                                        <img src="/assets/vendor/masterslider/style/blank.gif" data-src="/images/galleries/{!! $img->image !!}" alt="{!! $data->name !!}"/>
                                        <img class="ms-thumb" src="/images/galleries/{!! $img->image !!}" alt="{!! $data->name !!}" />
                                    </div>
                                    @endforeach
                                </div>
                                <!-- end of masterslider -->
                            </div>
                            <!-- end of template -->
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <aside class="sidebar">
                        {!! Form::open(['route' => ['galleries'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar']) !!}
                        <div class="input-group input-group-lg">
                            <input class="form-control" placeholder="{!! trans('site.search') !!}..." name="search" id="search" type="search">
                            <span class="input-group-btn">
                                    <button type="button" onclick="$('.search-bar').submit(); return false;" class="btn btn-lg"><i class="fa fa-search"></i></button>
                                </span>
                        </div>
                        {!! Form::close() !!}
                        <hr class="sep-line"/>
                        <h4 class="blue">{!! trans('app.blogcategories') !!}</h4>
                        <ul class="nav nav-list primary">
                            <li>{!! html_entity_decode(link_to_route('galleries', trans('blog.all'))) !!}</li>
                            @foreach($cat as $item)
                                <li>{!! html_entity_decode(link_to_route('galleries', $item->name, ['cat' => $item->slug])) !!}</li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('jsBottom')
    <!--<script src="vendor/masterslider/jquery.min.js"></script>-->
    {!! Html::script('/assets/vendor/masterslider/jquery.easing.min.js') !!}
    <!-- Master Slider -->
    {!! Html::script('/assets/vendor/masterslider/masterslider.min.js') !!}
@endsection