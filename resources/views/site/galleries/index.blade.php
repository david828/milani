@extends('site.layouts.page')

@section('title') {!! menu()[3]->title !!} @endsection

@section('menuGal') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[3]->title !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[3]->title !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <!-- Blog Post Style 1 -->
    <section class="blogstyle-1">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="row">
                        @foreach($list->all as $item)
                        <div class="col-md-4 col-sm-4">
                            <article class="blog-post-container clearfix">
                                <div class="post-thumbnail">
                                    <div id="blogstyle-2Slider" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            @foreach($item->images as $gal)
                                            <div class="item {!! ($loop->iteration == 1)? 'active':'' !!}">
                                                {!! Html::image('/images/galleries/'.$gal->image,'galleries',['class'=>'img-responsive']) !!}
                                            </div>
                                            @endforeach
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#blogstyle-2Slider" role="button" data-slide="prev">
                                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                                            <span class="sr-only">{!! trans('site.prev') !!}</span>
                                        </a>
                                        <a class="right carousel-control" href="#blogstyle-2Slider" role="button" data-slide="next">
                                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                                            <span class="sr-only">{!! trans('site.next') !!}</span>
                                        </a>
                                    </div>
                                </div><!-- /.post-thumbnail -->
                                <div class="gallery-content">
                                    <div class="gallery-header">
                                        <h4>
                                            {!! html_entity_decode(link_to_route('galleries.view',$item->name . ' <i class="fa fa-angle-double-right"></i>',['id'=>$item->slug])) !!}
                                        </h4>
                                    </div>
                                </div>
                            </article>
                        </div>
                        @endforeach
                    </div><!-- /.row -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <aside class="sidebar">
                        {!! Form::open(['route' => ['galleries'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar']) !!}
                            <div class="input-group input-group-lg">
                                <input class="form-control" placeholder="{!! trans('site.search') !!}..." name="search" id="search" type="search" value="{!! $list->search !!}">
                                {!! Form::hidden('cat',$list->cat,['id'=>'cat']) !!}
                                <span class="input-group-btn">
                                    <button type="button" onclick="$('.search-bar').submit(); return false;" class="btn btn-lg"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                        <hr class="sep-line"/>
                        <h4 class="blue">{!! trans('app.blogcategories') !!}</h4>
                        <ul class="nav nav-list primary">
                            <li>{!! html_entity_decode(link_to_route('galleries', trans('blog.all'))) !!}</li>
                            @foreach($cat as $item)
                                <li>{!! html_entity_decode(link_to_route('galleries', $item->name, ['cat' => $item->slug])) !!}</li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('jsBottom')
@endsection