{!! Form::open(['url'=>payuData()->requesturl,'method'=>'POST','name'=>'frmPayu','id'=>'frmPayu','role'=>'form']) !!}
<input name="merchantId" type="hidden" value="{!! payuData()->merchantid !!}">
<input name="referenceCode" type="hidden" value="{!! $data->invoice !!}">
<input name="description" type="hidden" value="{!! trans('site.text_buy').appData()->name !!}">
<input name="amount" type="hidden" value="{!! $data->priceTotal !!}">
<input name="tax" type="hidden" value="{!! $data->tax !!}">
<input name="taxReturnBase" type="hidden" value="{!! $data->value !!}">
<input name="signature" type="hidden" value="{!! $data->signature !!}">
<input name="accountId" type="hidden" value="{!! payuData()->accountid !!}">
<input name="currency" type="hidden" value="{!! appData()->currency[0]->name !!}">
<input name="buyerFullName" type="hidden" value="{!! $data->name !!}">
<input name="buyerEmail" type="hidden" value="{!! $data->email !!}">

<input name="test" type="hidden" value="{!! payuData()->testing !!}">

<input name="responseUrl" type="hidden" value="{!! payuData()->responseurl !!}">
<input name="confirmationUrl" type="hidden" value="{!! payuData()->confirmationurl !!}">

<input name="telephone" type="hidden" value="{!! $data->phone !!}">
<input name="shippingAddress" type="hidden" value="{!! $data->addressS !!}">
<input name="shippingCity" type="hidden" value="{!! $data->cityName !!}">
<input name="shippingCountry" type="hidden" value="{!! $data->countryIso !!}">
{!! Form::close() !!}