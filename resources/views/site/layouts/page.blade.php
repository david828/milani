<!DOCTYPE html>
<html lang="{!! getAppSeo()->language !!}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('metatitle',appData()->name)</title>
    @if(!is_null(getAppSeo()->charset))
        <meta charset="{!! getAppSeo()->charset !!}">
    @else
        <meta charset="UTF-8">
    @endif
    <!-- meta -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="application-name" content="{!! getAppSeo()->application_name !!}"/>
    <meta name="author" content="{!! getAppSeo()->author !!}">
    <meta name="description" content="@yield('metadesc',getAppSeo()->metadescription)">
    <meta name="keywords" content="@yield('keywords',getAppSeo()->metatags)">
    <meta name="robots" content="{!! getAppSeo()->robots !!}">
    <meta name="google-site-verification" content="oKvEh-ffv8F0qIuPfwNZFwIz-3hPa7ZFk3-n4NdvoKs" />

    <link rel="icon" href="{!! url('assets/images/favicon.png') !!}" sizes="32x32"/>
    <!-- Bootstrap core CSS -->
    {!! Html::style('assets/vendor/bootstrap/css/bootstrap.css') !!}

    {!! Html::script('assets/js/ie-emulation-modes-warning.js') !!}
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- Base MasterSlider style sheet -->
    {!! Html::style('assets/vendor/masterslider/style/masterslider.css') !!}

<!-- Master Slider Skin -->
    {!! Html::style('assets/vendor/masterslider/skins/default/style.css') !!}

<!-- masterSlider Template Style -->
    {!! Html::style('assets/vendor/masterslider/style/ms-layers-style.css') !!}

<!-- owl Slider Style -->
    {!! Html::style('assets/vendor/owlcarousel/dist/assets/owl.carousel.min.css') !!}
    {!! Html::style('assets/vendor/owlcarousel/dist/assets/owl.theme.default.min.css') !!}

    <!-- StyleSheets -->
    {!! Html::style('assets/vendor/font-awesome/css/font-awesome.min.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('/css/callCss') !!}

    <!-- Custom Fonts -->
    {!! Html::style('assets/fonts/futura/fonts.css') !!}
    {!! Html::style('https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700') !!}

    @yield('cssBottom')

    {!! getAppSeo()->pixel_facebook !!}
    {!! getAppSeo()->hotjar !!}
    {!! getAppSeo()->google_analytics !!}
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- loader start -->
<div class="loader" style="display: none !important;">
    <div id="awsload-pageloading">
        <div>
            {!! Html::image('/assets/images/loader.gif','', ['class' => 'imgLoader']) !!}
        </div>
    </div>
</div>
<!-- loader end -->

<!-- modals -->
{!! getModals() !!}

@include('site.layouts.header')
@include('site.layouts.login')
@yield('banner')
@yield('content')
@include('site.layouts.footer')

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
{!! Html::script('assets/js/ie10-viewport-bug-workaround.js') !!}

<!-- jQuery -->
{!! Html::script('assets/vendor/jquery/jquery.min.js') !!}
<!-- Bootstrap Core JavaScript -->
{!! Html::script('assets/vendor/bootstrap/js/bootstrap.min.js') !!}
<!-- Nav JavaScript -->
{!! Html::script('assets/js/awesomenav.js') !!}
<!--<script src="vendor/masterslider/jquery.min.js"></script>-->
{!! Html::script('assets/vendor/masterslider/jquery.easing.min.js') !!}
<!-- Master Slider -->
{!! Html::script('assets/vendor/masterslider/masterslider.min.js') !!}
<!-- owl Slider JavaScript -->
{!! Html::script('assets/vendor/owlcarousel/dist/owl.carousel.min.js') !!}
<!-- Validation -->
{!! Html::script('cms/plugins/jquery-validation/dist/jquery.validate.min.js') !!}
{!! Html::script('cms/plugins/jquery-validation/dist/additional-methods.min.js') !!}
{!! Html::script('cms/plugins/jquery-validation/src/localization/messages_es.js') !!}
{!! Html::script('cms/dist/js/display-errors.js') !!}
<!-- Counter required files -->
{!! Html::script('assets/js/dscountdown.min.js') !!}
<!-- Input-mask -->
{!! Html::script('assets/vendor/input-mask/jquery.inputmask.js') !!}
{!! Html::script('assets/vendor/input-mask/jquery.inputmask.phone.extensions.js') !!}
{!! Html::script('assets/vendor/input-mask/jquery.inputmask.extensions.js') !!}
<!-- custom JavaScript -->
{!! Html::script('assets/js/custom.js') !!}
<!-- template JavaScript -->
{!! Html::script('assets/js/template.js') !!}
<!-- Google Analytics -->
{!! getAppSeo()->chat !!}
<!-- Modals -->
<script>
    $(document).ready(function()
    {
        $(".moodleModalS").modal("show");
    });
</script>

@yield('jsBottom')
</body>
</html>

