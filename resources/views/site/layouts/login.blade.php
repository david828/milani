@if(!Auth::check())
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">{!! trans('site.user_data') !!}</div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <h3>{!! trans('ph.login') !!}</h3>
                        {!! Form::open(['route' => 'authlogin','method'=>'POST', 'class' => 'form, dart-pt-20', 'name'=>'frmLogin', 'id' => 'frmLogin']) !!}
                            <div class="form-group">
                                {!! Form::email('emailL',old('emailL'),['class'=>'dart-form-control','placeholder'=>trans('ph.email'), 'required','id'=>'emailL']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::password('passwordL',['class'=>'dart-form-control','placeholder'=>trans('ph.password'), 'required','id'=>'passwordL']) !!}
                            </div>
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('rememberL',null,null,['id'=>'rememberL']) !!}
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    {!! trans('login.remember') !!}
                                </label>
                            </div>
                            <p id="error-Login" class="error_cart" style="display: none;"></p>
                            <button type="button" id="btn-login" class="btn normal-btn dart-btn-xs">{!! trans('app.signin') !!}</button>
                            <button type="button" id="btn-forgot"  class="btn normal-btn dart-btn-xs">{!! trans('login.forgot') !!}</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12" style="border-left: 1px solid #e5e5e5">
                        <h3>{!! trans('login.create_account') !!}</h3>
                        {!! Form::open(['route'=>'register.data','method'=>'POST','name'=>'frmRegister','id'=>'frmRegister','class'=>'form-login, dart-pt-20','role'=>'form']) !!}
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    {!! Form::label('personR', trans('user.type')) !!} *
                                    {!! Form::select('personR', getPerson(), null,['class'=>'dart-form-control', 'required','id'=>'personR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::select('doctypeR', getDocType(), null,['class'=>'dart-form-control', 'required','id'=>'doctypeR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::text('documentR', null,['class'=>'dart-form-control','placeholder'=>trans('user.document').' *', 'required','id'=>'documentR']) !!}
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    {!! Form::text('nameR', null,['class'=>'dart-form-control','placeholder'=>trans('user.reason').' *', 'required','id'=>'nameR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::text('phoneR', null,['class'=>'dart-form-control','placeholder'=>trans('ph.phone').' *', 'required','id'=>'phoneR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::email('emailR', null,['class'=>'dart-form-control','placeholder'=>trans('ph.email').' *', 'required','id'=>'emailR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::password('passwordR',['class'=>'dart-form-control','placeholder'=>trans('ph.password').' *', 'required','minlength'=>6,'id'=>'passwordR']) !!}
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    {!! Form::password('repasswordR',['class'=>'dart-form-control','placeholder'=>trans('ph.repeat_password').' *', 'required','minlength'=>6,'id'=>'repasswordR']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="checkbox col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <label>
                                        <input type="checkbox" id="politics" name="politics" value="" aria-required="true">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        He leído y acepto las
                                        <a href="/politics" target="_blank">Políticas de Tratamiendo de Datos Personales</a>
                                    </label>
                                </div>
                            </div>
                            <p id="error-Register" class="error_cart" style="display: none;"></p>
                            <div class="row">
                                <div class="col-xs-12">
                                    {!! Form::button(trans('ph.register'),['class'=>'btn normal-btn dart-btn-xs', 'id'=>'btn-register']) !!}
                                </div><!-- /.col -->
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">{!! trans('login.reset_password') !!}</div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div id="rd_login_form" style="display: block;">
                            {!! Form::open(['route'=>'recovery','method'=>'POST', 'class' => 'form, form-signin', 'name'=>'frmRecovery', 'id' => 'frmRecovery']) !!}
                            {{ csrf_field() }}
                            <div class="form-group">
                                {!! Form::email('emailF',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email'), 'required', 'id'=>'emailF']) !!}
                            </div>
                            <p id="error-Recovery" class="error_cart" style="display: none;"></p>
                            <div class="row">
                                <div class="col-xs-12">
                                    {!! Form::button(trans('login.send_password_link'),['class'=>'btn normal-btn dart-btn-xs', 'id'=>'btn-recovery']) !!}
                                </div><!-- /.col -->
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="successModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <h2>{!! trans('site.wellcome') !!}</h2>
                        <p id="nameS"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{!! Form::open(['url'=>'/','method'=>'POST', 'id' => 'frmData']) !!}
{!! Form::hidden('person',null, ['id'=>'person']) !!}
{!! Form::hidden('doctype',null, ['id'=>'doctype']) !!}
{!! Form::hidden('name',null, ['id'=>'name']) !!}
{!! Form::hidden('document',null, ['id'=>'document']) !!}
{!! Form::hidden('email',null, ['id'=>'email']) !!}
{!! Form::hidden('phone',null, ['id'=>'phone']) !!}
{!! Form::checkbox('remember',null, null, ['id'=>'remember', 'style' => 'display:none !important;']) !!}
{!! Form::password('password', ['id'=>'password', 'style' => 'display:none !important;']) !!}
{!! Form::close() !!}
@else
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-title">{!! trans('account.delete_account') !!}</div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div id="rd_login_form" style="display: block;">
                                {!! Form::open(['route'=>'deleteuser','method'=>'POST', 'class' => 'form, form-signin', 'name'=>'frmDelete', 'id' => 'frmDelete']) !!}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    {!! Form::label('email', trans('ph.email')) !!} *
                                    {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('ph.email'), 'required', 'id'=>'email']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', trans('ph.password')) !!} *
                                    {!! Form::password('password',['class'=>'dart-form-control','placeholder'=>trans('ph.password'), 'required','minlength'=>6]) !!}
                                </div>
                                <p id="error-Delete" class="error_cart" style="display: none;"></p>
                                <p class="wait_cart"> {!! trans('account.delete_confirm') !!}</p>
                                <div class="row">
                                    <div class="col-xs-12">
                                        {!! Form::submit(trans('site.yes'),['class'=>'btn normal-btn dart-btn-xs pull-left', 'id'=>'btn-delete']) !!}
                                        {!! Form::button(trans('site.no'),['class'=>'btn normal-btn dart-btn-xs pull-right', 'data-dismiss'=>'modal']) !!}
                                    </div><!-- /.col -->
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
