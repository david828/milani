<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="footer-block about">
                    <h3>{!! footer()->title_1 !!}</h3>
                    <p>{!! footer()->text_1 !!}</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="footer-block twitter">
                    <h3>{!! trans('site.contact') !!}</h3>
                    <p>{!! appData()->address !!}<br>{!! appData()->phone !!} - {!! appData()->cellphone !!}<br>{!! appData()->email !!}</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="footer-block contact">
                    <h3>{!! trans('site.md_pay') !!}</h3>
                    <div style="display: table" class="md_pay">
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/payu-logo.png" alt="PayU">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/PSE.png" alt="PSE">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/mastercard.png" alt="Mastercard">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/visa.png" alt="Visa">
                            </div>
                        </div>
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/american_express.png" alt="American Express">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/DinersClub_PayU.png" alt="Diners Club">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/bancolombia.png" alt="Bancolombia">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/bancobog-1.png" alt="Banco de Bogotá">
                            </div>
                        </div>
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/davivienda.png" alt="Davivienda">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/efe.png" alt="Efecty">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/baloto.png" alt="Baloto">
                            </div>
                            <div style="display: table-cell">
                                <img src="/assets/images/pays/su_red.png" alt="Su Red">
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/payu-logo.png" alt="PayU">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/PSE.png" alt="PSE">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/mastercard.png" alt="Mastercard">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/visa.png" alt="Visa">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/american_express.png" alt="American Express">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/DinersClub_PayU.png" alt="Diners Club">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/bancolombia.png" alt="Bancolombia">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/bancobog-1.png" alt="Banco de Bogotá">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/davivienda.png" alt="Davivienda">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/efe.png" alt="Efecty">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/baloto.png" alt="Baloto">
                    </div>
                    <div class="col-md-3 col-xs-3 md_pay">
                        <img src="/assets/images/pays/su_red.png" alt="Su Red">
                    </div>-->
                </div>
            </div>
            <div class="col-md-12  col-sm-12">
                <div class="social">
                    <h5>{!! trans('site.followus') !!}</h5>
                    <ul class="list-inline">
                        @foreach(social() as $item)
                            <li><a href="{!! $item->social[0]->link.$item->link !!}" target="_blank">
                                <i class="{!! $item->social[0]->icon !!}"></i>
                            </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="copy">
                <p>{!! appData()->copyright !!} - <span>{!! appData()->name !!}</span></p>
            </div>
        </div>
    </div>
</footer>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-xs-12 ppt_com">
            <span>
                Sitio web desarrollado por <a href="https://ppt.oroogga.com" target="_blank">PPT Comunicaciones S.A.S.</a> para <a href="https://fielatubelleza.com" target="_blank">Comercializadora Fiel a tu Belleza S.A.S.</a>
            </span>
        </div>
    </div>
</div>