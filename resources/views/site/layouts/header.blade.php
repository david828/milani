<!-- Start top area -->
<nav class="navbar navbar-default navbar-sticky awesomenav">

<div class="top-container">
    <div class="container-fluid">
        <div class="row">
            <div class="top-column-left">
                @if(appData()->min_value != 0)
                    <p class="ship_header">{!! trans('app.free_shipping2',['value'=>number_format(appData()->min_value, 0, ',', '.')]) !!}</p>
                @endif
            </div>
            <div class="top-column-right">
                <ul class="contact-line">
                    <li><i class="fa fa-phone"></i> {!! appData()->phone !!}</li>
                    <li><i class="fa fa-envelope"></i> {!! appData()->email !!}</li>
                    <li>
                        <div class="top-social-network">
                            @foreach(social() as $item)
                                <a href="{!! $item->social[0]->link.$item->link !!}" target="_blank">
                                    <i class="{!! $item->social[0]->icon !!}"></i>
                                </a>
                            @endforeach
                        </div>
                    </li>
                </ul>
                <ul class="register">
                    @if(Auth::check())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-user"></i> {!! userData()->name !!}</a>
                            <ul class="dropdown-menu singin">
                                <!-- User image -->
                                <li class="user-header">
                                    <p>
                                        {!! Html::image(userData()->imageU, userData()->name,['class'=>'img-circle']) !!}
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        {!! link_to_route('myaccount',trans('app.profile'),null,['class'=>'btn normal-btn dart-btn-xs']) !!}
                                    </div>
                                    <div class="pull-right">
                                        {!! link_to_route('userlogout',trans('app.logout'),null,['class'=>'btn normal-btn dart-btn-xs']) !!}
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <div data-toggle="modal" data-target="#loginModal" class="btn-login">
                                <i class="fa fa-user"></i> {!! trans('site.login') !!}
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End top area -->
<div class="clearfix"></div>
<!--<nav class="navbar navbar-default navbar-sticky awesomenav">-->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container-fluid">
            {!! Form::open(['route' => ['products'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar', 'id' => 'formSearchB']) !!}
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" id="searchB" name="search" placeholder="{!! trans('site.search') !!}">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Top Search -->
    <div class="container-fluid">

        <!-- Start Atribute Navigation -->
        <div class="attr-nav">
            <ul>
                <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                <li class="dropdown">
                    {!! get_cart() !!}
                </li>
                <li class="side-menu"><a href="#"><i class="fa fa-bars"></i></a></li>
            </ul>
        </div>
        <!-- End Atribute Navigation -->

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/">
                {!! Html::image('images/app/' . appData()->logo,appData()->name,['class' => 'logo']) !!}
            </a>
        </div>
        <!-- End Header Navigation -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                <li class="@yield('menuIndex')"><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="@yield('menuAbout')"><a href="/aboutus">{!! menu()[1]->title !!}</a></li>
                <li class="dropdown megamenu-fw @yield('menuProd')">
                    <a href="/products" class="dropdown-toggle">{!! menu()[2]->title !!}</a>
                    <ul class="dropdown-menu megamenu-content" role="menu">
                        <div class="container">
                        <div class="row">
                            @foreach(menuP() as $cat)
                            <div class="col-menu col-md-3 item-cat">
                                <a href="/products?cat={!! $cat->slug !!}"><h6 class="title cat_menu">{!! $cat->name !!}</h6></a>
                                <div class="content">
                                    <ul class="menu-col">
                                        @foreach($cat->subcategory as $sub)
                                        <li><a href="/products?sub={!! $sub->slug !!}">{!! $sub->name !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                            @if(countSale() > 0)
                                <div class="col-md-3 item-cat">
                                    <a href="/products?cat=sale" class="go-sale"><h6 class="title cat_menu">{!! trans('app.sale') !!}</h6></a>
                                </div>
                            @endif
                        </div><!-- end row -->
                        </div>
                    </ul>
                </li>
                @if(countGalleries() > 0)
                <li class="@yield('menuGal')"><a href="/galleries">{!! menu()[3]->title !!}</a></li>
                @endif
                <li class="@yield('menuDist')"><a href="/distribution">{!! menu()[6]->title !!}</a></li>
                @if(count(getArticles(2)) > 0)
                    <li class="@yield('menuBlog')"><a href="/articles">{!! menu()[4]->title !!}</a></li>
                @endif
                <li class="@yield('menuContactus')"><a href="/contact">{!! menu()[5]->title !!}</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
    <!-- Start Side Menu -->
    <div class="side">
        <a href="#" class="close-side"><i class="fa fa-times"></i></a>
        <div class="widget">
            <h6 class="title">{!! trans('site.links_f') !!}</h6>
            <ul class="link">
                @if(Auth::check())
                <li>{!! link_to_route('myaccount',trans('account.my_account')) !!}</li>
                <li>{!! link_to_route('userorders',trans('account.order_history')) !!}</li>
                @endif
                <li>{!! link_to_route('cart',trans('site.cart')) !!}</li>
                <li><a href="/politics" target="_blank">{!! trans('app.politics') !!}</a></li>
            </ul>
        </div>
    </div>
    <!-- End Side Menu -->
</nav>