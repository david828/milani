@extends('site.layouts.page')

@section('title') {!! menu()[4]->title !!} @endsection

@section('menuBlog') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[4]->title !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[4]->title !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <!-- Blog Post Style 1 -->
    <section class="blogstyle-1">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="row">
                        @foreach($list->all as $item)
                        <div class="col-md-4 col-sm-4">
                            <article class="blog-post-container clearfix">
                                <div class="post-thumbnail">
                                    {!! Html::image('images/articles/xs_'.$item->image, $item->name) !!}
                                </div><!-- /.post-thumbnail -->
                                <div class="blog-content">
                                    <div class="dart-header">
                                        <h4 class="dart-title">
                                            {!! html_entity_decode(link_to_route('article.view',$item->name,['id'=>$item->slug])) !!}
                                        </h4>
                                        <div class="dart-meta">
                                            <ul class="list-unstyled">
                                                <li><span class="posted-date">{!! $item->date !!}</span></li>
                                            </ul>
                                        </div><!-- /.dart-meta -->
                                    </div><!-- /.dart-header -->
                                    <div class="dart-content">
                                        <p>{!! substr($item->description,0,100) . '...' !!}</p>
                                    </div><!-- /.dart-content -->
                                    <div class="dart-footer">
                                        <ul class="dart-meta clearfix list-unstyled">
                                            <li>
                                                {!! html_entity_decode(link_to_route('article.view', trans('site.view_more') . ' <i class="fa fa-angle-double-right"></i>',['id'=>$item->slug],['class'=>'pull-right'])) !!}
                                            </li>
                                        </ul>
                                    </div><!-- /.dart-footer -->
                                </div><!-- /.blog-content -->
                            </article>
                        </div>
                        @endforeach
                    </div><!-- /.row -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <aside class="sidebar">
                        {!! Form::open(['route' => ['articles'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar']) !!}
                            <div class="input-group input-group-lg">
                                <input class="form-control" placeholder="{!! trans('site.search') !!}..." name="search" id="search" type="search" value="{!! $list->search !!}">
                                {!! Form::hidden('cat',$list->cat,['id'=>'cat']) !!}
                                {!! Form::hidden('et',$list->et,['id'=>'et']) !!}
                                <span class="input-group-btn">
                                    <button type="button" onclick="$('.search-bar').submit(); return false;" class="btn btn-lg"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                        <hr class="sep-line"/>
                        <h4 class="blue">{!! trans('app.blogcategories') !!}</h4>
                        <ul class="nav nav-list primary">
                            <li>{!! html_entity_decode(link_to_route('articles', trans('blog.all'))) !!}</li>
                            @foreach($cat as $item)
                                <li>{!! html_entity_decode(link_to_route('articles', $item->name, ['cat' => $item->slug])) !!}</li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('jsBottom')
@endsection