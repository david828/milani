@extends('site.layouts.page')

@section('title') {!! menu()[4]->title !!}  @endsection

@section('metadesc'){!! $data->metadescription !!}@endsection

@section('keywords'){!! $data->metatags !!}@endsection

@section('menuBlog') active @endsection

@section('cssBottom')
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('site.article') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li><a href="/articles">{!! menu()[4]->title !!}</a></li>
                <li class="active"><span>{!! $data->name !!}</span></li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <section class="blog-single" id="blog_s_post"><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="blog-posts single-post">
                        <article class="post post-large blog-single-post">
                            @if($data->image)
                                <div class="post-image">
                                    {!! Html::image($data->image,$data->name,['class' => 'img-responsive']) !!}
                                </div>
                            @endif
                            <div class="post-date">
                                <span class="day">{!! $data->day !!}</span>
                                <span class="month">{!! $data->month !!}</span>
                            </div>
                            <div class="post-content">
                                <h2>{!! $data->name !!}</h2>
                                <div class="post-meta">
                                    <span><i class="fa fa-user"></i> {!! trans('site.by') . ' ' . $data->user[0]->firstname !!}</span>
                                </div>
                                {!! $data->description !!}
                                @if(count($data->images) != 0)
                                <div class="post-thumbnail slider-blog">
                                    <div id="blogstyle-2Slider" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            @foreach($data->images as $item)
                                            <div class="item {!! ($loop->iteration == 1)? 'active':'' !!}">
                                                {!! Html::image('images/articles/sm_'.$item->image, $data->name, ['class'=>'img-responsive']) !!}
                                            </div>
                                            @endforeach
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#blogstyle-2Slider" role="button" data-slide="prev">
                                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#blogstyle-2Slider" role="button" data-slide="next">
                                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div><!-- /.post-thumbnail -->
                                @endif
                                <div class="post-block post-share">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3><i class="fa fa-share"></i>{!! trans('blog.sharein') !!}</h3>
                                        </div>
                                        <div class="col-md-8 share-icon">
                                            <a href="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="javascript:window.open('https://twitter.com/?status='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a href="javascript:window.open('https://plus.google.com/share?url='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                            <a href="javascript:window.open('https://www.linkedin.com/shareArticle?url='+document.URL,'','width=600,height=400,left=50,top=50,toolbar=yes')">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-block post-leave-comment">
                                    <h3>{!! trans('blog.writecom') !!}</h3>
                                    {!! Form::open(['route'=>['article.comment'],'method'=>'POST','name'=>'comment-form','id'=>'comment-form','role'=>'form']) !!}
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>{!! trans('blog.name') !!} *</label>
                                                    {!! Form::text('name', (Auth::check())? userData()->name : null, ['id' => 'name', 'class' => 'form-control', 'required']) !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <label>{!! trans('blog.email') !!} *</label>
                                                    {!! Form::email('email', (Auth::check())? userData()->email : null, ['id' => 'email', 'class' => 'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12"><br>
                                                    <label>{!! trans('blog.comment') !!} *</label>
                                                    {!! Form::textarea('comment', null, ['id' => 'comment', 'class' => 'form-control', 'required', 'rows' => '4']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br>
                                                <input type="submit" value="{!! trans('blog.send') !!}" id="send_comment" class="btn btn-blue" data-loading-text="Loading...">
                                            </div>
                                        </div>
                                        {!! Form::hidden('id_article', $data->id, ['id' => 'id_article']) !!}
                                    {!! Form::close() !!}
                                </div>
                                <div class="post-block post-comments clearfix">
                                    <h3><i class="fa fa-comments"></i>{!! trans('blog.comments') .' (<span id="tot_com">' . count($data->comments) . '</span>)' !!}</h3>
                                    {!! Form::hidden('num_comm', count($data->comments), ['id' => 'num_comm']) !!}
                                    <ul class="comments" id="comments-1">
                                        @foreach($data->comments as $com)
                                        <li>
                                            <div class="comment">
                                                <div class="img-thumbnail">
                                                    {!! Html::image('assets/images/user.png','avatar',['class'=>'avatar']) !!}
                                                </div>
                                                <div class="comment-block">
                                                    <div class="comment-arrow"></div>
                                                    <span class="comment-by">
                                                        <strong>{!! $com->name !!}</strong>
                                                    </span>
                                                    <p>{!! $com->comment !!}</p>
                                                    <span class="date pull-right">{!! $com->date !!}</span>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <aside class="sidebar">
                        {!! Form::open(['route' => ['articles'], 'method' => 'GET', 'role' => 'search', 'class' => 'search-bar']) !!}
                        <div class="input-group input-group-lg">
                            <input class="form-control" placeholder="{!! trans('site.search') !!}..." name="search" id="search" type="search">
                            <span class="input-group-btn">
                                    <button type="button" onclick="$('.search-bar').submit(); return false;" class="btn btn-lg"><i class="fa fa-search"></i></button>
                                </span>
                        </div>
                        {!! Form::close() !!}
                        <hr class="sep-line"/>
                        <h4 class="blue">{!! trans('app.blogcategories') !!}</h4>
                        <ul class="nav nav-list primary">
                            <li>{!! html_entity_decode(link_to_route('articles', trans('blog.all'))) !!}</li>
                            @foreach($cat as $item)
                                <li>{!! html_entity_decode(link_to_route('articles', $item->name, ['cat' => $item->slug])) !!}</li>
                            @endforeach
                        </ul>
                    </aside>
                    <aside class="widget widget_tag_cloud">
                        <h4 class="blue">{!! trans('site.tags') !!}</h4>
                        <ul class="list-inline clearfix">
                            @foreach($data->tags as $et)
                                <li>{!! html_entity_decode(link_to_route('articles', $et, ['et' => $et])) !!}</li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('jsBottom')
    {!! Html::script('assets/vendor/woocommerce-FlexSlider/jquery.flexslider.js') !!}
    {!! Html::script('assets/js/blog-script.js') !!}
    {!! Html::script('assets/js/blog.js') !!}
@endsection