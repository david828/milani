@extends('site.layouts.page')

@section('title') {!! trans('site.checkout') !!} @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('cart.checkout') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('cart.checkout') !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <div class="modal fade modal-checkout" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-4 cart-payment">
                            {!! Html::image('assets/images/payu_logo.png','PayU',['class' => 'img-responsive']) !!}
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <h4>{!! trans('site.transfer_to') !!}</h4>
                            <p>{!! trans('site.transfer_text') !!}</p>
                        </div>
                    </div>
                    <div class="payu-form">

                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>'cart.checkout','method'=>'POST','name'=>'frmCheck','id'=>'frmCheck','role'=>'form']) !!}
                    {!! Form::button(trans('app.accept'),['id'=>'bt_checkout','name'=>'bt_checkout','class'=>'btn normal-btn dart-btn-xs pull-left','type'=>'button','style'=>'margin-bottom:10px;']) !!}
                    {!! Form::button(trans('app.cancel'),['id'=>'bt_cancelcheck','name'=>'bt_cancelcheck','class'=>'btn normal-btn dart-btn-xs pull-right','data-dismiss'=>'modal','type'=>'button','style'=>'margin-bottom:10px;']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!--Shoping Cart-->
    <section class="dart-pt-30">
        <div class="container">
            @if(!Auth::check())
            <div class="row" id="divLogin">
                <div class="col-md-6 col-sm-6">
                    <div id="rd_login_form" style="display: block;">
                        <h3>{!! trans('ph.login') !!}</h3>
                        {!! Form::open(['route' => 'authlogin','method'=>'POST', 'class' => 'form, dart-pt-20', 'name'=>'frmLoginC', 'id' => 'frmLoginC']) !!}
                        <div class="form-group">
                            {!! Form::label('emailLC', trans('site.email')) !!} *
                            {!! Form::email('emailLC',old('emailL'),['class'=>'dart-form-control','placeholder'=>trans('ph.email'), 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('passwordLC', trans('ph.password')) !!} *
                            {!! Form::password('passwordLC',['class'=>'dart-form-control','placeholder'=>trans('ph.password'), 'required']) !!}
                        </div>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('rememberLC',null,null,['id'=>'rememberLC']) !!}
                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                {!! trans('login.remember') !!}
                            </label>
                        </div>
                        <p id="error-LoginC" class="error_cart" style="display: none;"></p>
                        <button type="button" id="btn-login-cart" class="btn normal-btn dart-btn-xs">{!! trans('app.signin') !!}</button>
                        <button type="button" class="btn normal-btn dart-btn-xs" data-toggle="modal" data-target="#resetModal">{!! trans('login.forgot') !!}</button>
                        <button type="button" class="btn normal-btn dart-btn-xs" style="background-color: #000000;" id="btn_like_guess">{!! trans('site.like_guest') !!}</button>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="rd_guest_checkout">
                        <h3>{!! trans('login.create_account') !!}</h3>
                        {!! Form::open(['route'=>'register.data','method'=>'POST','name'=>'frmRegisterC','id'=>'frmRegisterC','class'=>'form-login, dart-pt-20','role'=>'form']) !!}
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                {!! Form::label('personRC', trans('user.type')) !!} *
                                {!! Form::select('personRC', getPerson(), null,['class'=>'dart-form-control', 'required','id'=>'personRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('doctypeRC', trans('user.doctype')) !!} *
                                {!! Form::select('doctypeRC', getDocType(), null,['class'=>'dart-form-control', 'required','id'=>'doctypeRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('documentRC', trans('user.document')) !!} *
                                {!! Form::text('documentRC', null,['class'=>'dart-form-control','placeholder'=>trans('user.document').' *', 'required','id'=>'documentRC']) !!}
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                {!! Form::label('nameRC', trans('user.reason')) !!} *
                                {!! Form::text('nameRC', null,['class'=>'dart-form-control','placeholder'=>trans('user.reason'), 'required', 'id'=>'nameRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('phoneRC', trans('ph.phone')) !!} *
                                {!! Form::text('phoneRC', null,['class'=>'dart-form-control','placeholder'=>trans('ph.phone'), 'required', 'id'=>'phoneRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('emailRC', trans('ph.email')) !!} *
                                {!! Form::email('emailRC', null,['class'=>'dart-form-control','placeholder'=>trans('ph.email'), 'required', 'id'=>'emailRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('passwordRC', trans('ph.password')) !!} *
                                {!! Form::password('passwordRC',['class'=>'dart-form-control','placeholder'=>trans('ph.password'), 'required','minlength'=>6, 'id'=>'passwordRC']) !!}
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                {!! Form::label('repasswordRC', trans('ph.repeat_password')) !!} *
                                {!! Form::password('repasswordRC',['class'=>'dart-form-control','placeholder'=>trans('ph.repeat_password'), 'required','minlength'=>6, 'id'=>'repasswordRC']) !!}
                            </div>
                        </div>
                        <p id="error-RegisterC" class="error_cart" style="display: none;"></p>
                        <div class="row">
                            <div class="checkbox col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <label>
                                    <input type="checkbox" id="politicsC" name="politicsC" value="" aria-required="true">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    He leído y acepto las
                                    <a href="/politics" target="_blank">Políticas de Tratamiendo de Datos Personales</a>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                {!! Form::button(trans('ph.register'),['class'=>'btn normal-btn dart-btn-xs', 'id'=>'btn-registerC']) !!}
                            </div><!-- /.col -->
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="row sepCart" style="display:none;">
                <div class="col-md-12 col-sm-12">
                    <h3 >
                        <button type="button" class="btn normal-btn dart-btn-xs" id="btn_create_account" style="float: left; margin:0;" title="{!! trans('site.back_account') !!}"><i class="fa fa-reply"></i> </button>
                        {!! trans('site.like_guest') !!}
                    </h3>
                </div>
            </div>
            @endif
            <div class="row" id="all_account" {!! (!Auth::check())? 'style="display:none;"':'' !!}>
                {!! Form::open(['url'=>'#', 'id' => 'frmDataInvoice']) !!}
                <div class="col-md-6 col-sm-6">
                    <h3>{!! trans('cart.invoice_data') !!}</h3>
                    <div id="billing-form" name="billing-form" class="row dart-pt-20">
                        <div class="col-sm-12">
                            {!! Form::label('sl_person',trans('user.type')) !!} *
                            {!! Form::select('sl_person', getPerson(),$user->id_person,['id'=>'sl_person','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-6">
                            {!! Form::label('sl_doctype',trans('user.doctype')) !!} *
                            {!! Form::select('sl_doctype', getDocType(),$user->id_documenttype,['id'=>'sl_doctype','class'=>'dart-form-control invoiceC','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_document',trans('user.document')) !!} *
                            {!! Form::text('tx_document',$user->document,['id'=>'tx_document','class'=>'dart-form-control invoiceC','required']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12 ">
                            {!! Form::label('tx_name',trans('user.reason')) !!} *
                            {!! Form::text('tx_name',$user->name,['id'=>'tx_name','class'=>'dart-form-control invoiceC','required']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-6 ">
                            {!! Form::label('tx_phone',trans('ph.phone')) !!}
                            {!! Form::text('tx_phone',$user->phone,['id'=>'tx_phone','class'=>'dart-form-control invoiceC']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_cellphone',trans('user.cellphone')) !!}
                            {!! Form::text('tx_cellphone',$user->cellphone,['id'=>'tx_cellphone','class'=>'dart-form-control invoiceC']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            {!! Form::label('tx_address',trans('ph.address')) !!} *
                            {!! Form::text('tx_address',$user->address_invoice,['id'=>'tx_address','class'=>'dart-form-control invoiceC','required']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('sl_country',trans('app.country')) !!} *
                            {!! Form::select('sl_country',$data2->countryList,$user->countryId,['id'=>'sl_country','class'=>'dart-form-control select2 invoiceC','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('sl_state',trans('app.state')) !!} *
                            {!! Form::hidden('hn_state',url('state/IDX/search'),['id'=>'hn_state']) !!}
                            {!! Form::select('sl_state',$user->stateList, $user->stateId,['id'=>'sl_state','class'=>'dart-form-control select2 invoiceC','required']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('sl_city',trans('app.city')) !!} *
                            {!! Form::hidden('hn_city',url('city/IDX/search'),['id'=>'hn_city']) !!}
                            {!! Form::select('sl_city',$user->cityList, $user->cityId,['id'=>'sl_city','class'=>'dart-form-control select2 invoiceC','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_email',trans('ph.email')) !!} *
                            {!! Form::email('tx_email',$user->email,['id'=>'tx_email','class'=>'dart-form-control invoiceC customeEmail','required']) !!}
                        </div>
                        <div class="checkbox col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label>
                                <input type="checkbox" id="politicsB" name="politicsB" value="" aria-required="true">
                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                He leído y acepto las
                                <a href="/politics" target="_blank">Políticas de Tratamiendo de Datos Personales</a>
                            </label>
                        </div>
                        @if(!Auth::check())
                        <div class="checkbox col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <label>
                                <input type="checkbox" id="btn_copy_info" name="btn_copy_info" value="" aria-required="true">
                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                {!! trans('site.copy_info') !!}
                            </label>
                        </div>
                        @endif
                    </div>
                    @if(Auth::check())
                        <h3>{!! trans('account.address') !!}</h3>
                        <div class="formulario">
                            <div class="radio">
                                <input type="radio" name="opc_address" id="IG" value="IG" checked="checked" class="opt_address">
                                <label for="IG" class="new_address">{!! trans('user.address_inv') !!}</label>
                                <br>
                                <input type="radio" name="opc_address" id="NN" value="NN" class="opt_address">
                                <label for="NN" class="new_address">{!! trans('user.new_address') !!}</label>
                                <br>
                                @foreach($data2->address as $item)
                                <input type="radio" name="opc_address" id="{!! $item->id !!}" class="opt_address">
                                <label for="{!! $item->id !!}">
                                    <b>{!! $item->title !!}</b><br>
                                    <span>{!! $item->address.', '.$item->location->cityName !!}</span>
                                </label>
                                <br>
                                {!! Form::hidden('documenttype_'.$item->id,$item->id_documenttype,['id'=>'documenttype_'.$item->id]) !!}
                                {!! Form::hidden('title_'.$item->id,$item->title,['id'=>'title_'.$item->id]) !!}
                                {!! Form::hidden('document_'.$item->id,$item->document,['id'=>'document_'.$item->id]) !!}
                                {!! Form::hidden('name_'.$item->id,$item->name,['id'=>'name_'.$item->id]) !!}
                                {!! Form::hidden('phone_'.$item->id,$item->phone,['id'=>'phone_'.$item->id]) !!}
                                {!! Form::hidden('address_'.$item->id,$item->address,['id'=>'address_'.$item->id]) !!}
                                {!! Form::hidden('country_'.$item->id,$item->location->countryId,['id'=>'country_'.$item->id]) !!}
                                {!! Form::hidden('zipcode_'.$item->id,$item->post_code,['id'=>'zipcode_'.$item->id]) !!}
                                {!! Form::hidden('additional_'.$item->id,$item->additional,['id'=>'additional_'.$item->id]) !!}
                                {!! Form::select('listState_'.$item->id,$item->stateList,$item->location->stateId,['id'=>'listState_'.$item->id, 'style'=>'display:none;']) !!}
                                {!! Form::select('listCity_'.$item->id,$item->cityList,$item->id_city,['id'=>'listCity_'.$item->id, 'style'=>'display:none;']) !!}
                                {!! Form::hidden('shipV_'.$item->id,$item->shipV,['id'=>'shipV_'.$item->id]) !!}
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-6 col-sm-6">
                    <h3>
                        {!! trans('cart.shipping_data') !!}
                    </h3>
                    <div id="shipping-form" name="shipping-form" class="row dart-pt-20">
                        @if(Auth::check())
                        <div class="col-sm- col-md-12">
                            {!! Form::label('tx_titleS',trans('user.title')) !!} *
                            {!! Form::text('tx_titleS','--',['id'=>'tx_titleS','class'=>'dart-form-control','required']) !!}
                        </div>
                        @endif
                        <div class="col-sm-6">
                            {!! Form::label('sl_doctypeS',trans('user.doctype')) !!} *
                            {!! Form::select('sl_doctypeS', getDocType(),$user->id_documenttype,['id'=>'sl_doctypeS','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_documentS',trans('user.document')) !!} *
                            {!! Form::text('tx_documentS',$user->document,['id'=>'tx_documentS','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-12 col-sm-12">
                            {!! Form::label('tx_nameS',trans('user.reason')) !!} *
                            {!! Form::text('tx_nameS',$user->name,['id'=>'tx_nameS','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('tx_emailS',trans('ph.email')) !!} *
                            {!! Form::email('tx_emailS',$user->email,['id'=>'tx_emailS','class'=>'dart-form-control customeEmail','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_phoneS',trans('ph.phone')) !!} *
                            {!! Form::text('tx_phoneS',$user->phone,['id'=>'tx_phoneS','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            {!! Form::label('tx_addressS',trans('ph.address')) !!} *
                            {!! Form::text('tx_addressS',$user->address_invoice,['id'=>'tx_addressS','class'=>'dart-form-control','required']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('sl_countryS',trans('app.country')) !!} *
                            {!! Form::select('sl_countryS',$data2->countryList,$user->countryId,['id'=>'sl_countryS','class'=>'dart-form-control select2','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('sl_stateS',trans('app.state')) !!} *
                            {!! Form::hidden('hn_stateS',url('state/IDX/search'),['id'=>'hn_stateS']) !!}
                            {!! Form::select('sl_stateS',$user->stateList, $user->stateId,['id'=>'sl_stateS','class'=>'dart-form-control select2','required']) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('sl_cityS',trans('app.city')) !!} *
                            {!! Form::hidden('hn_cityS',url('city/IDX/search'),['id'=>'hn_cityS']) !!}
                            {!! Form::select('sl_cityS',$user->cityList, $user->cityId,['id'=>'sl_cityS','class'=>'dart-form-control select2','required']) !!}
                        </div>
                        <div class="col-sm-6 col_last">
                            {!! Form::label('tx_code_postal',trans('ph.zipcode')) !!}
                            {!! Form::text('tx_code_postal',null,['id'=>'tx_code_postal','class'=>'dart-form-control']) !!}
                        </div>
                        <div class="col-sm-12">
                            {!! Form::label('ta_additional',trans('ph.additional')) !!}
                            {!! Form::textarea('ta_additional',null,['id'=>'ta_additional','class'=>'dart-form-control', 'rows' => '6']) !!}
                        </div>
                        {!! Form::hidden('hn_id_address',(Auth::check())? 'IG':null,['id'=>'hn_id_address']) !!}
                    @if(Auth::check())
                            <div class="checkbox col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <label>
                                    <input type="checkbox" id="ck_refresh" name="ck_refresh" value="10" aria-required="true">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    {!! trans('site.refresh_info') !!}
                                </label>
                                <!--<p>{!! trans('site.refresh_info2') !!}</p>-->
                            </div>
                        @endif
                    </div>
                </div>
                    {!! Form::hidden('billing-coupon',null,['id'=>'billing-coupon']) !!}
                {!! Form::close() !!}
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="table-responsive">
                        <h3 class="dart-pb-20">{!! trans('cart.your_order') !!}</h3>
                        <table class="table cart checkout">
                            <thead>
                            <tr>
                                <th class="cart-product-thumbnail">{!! trans('site.product') !!}</th>
                                <th class="cart-product-name">{!! trans('site.description') !!}</th>
                                <th class="cart-product-quantity">{!! trans('site.qty') !!}</th>
                                <th class="cart-product-subtotal">{!! trans('site.subtotal') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr class="cart_item">
                                    <td class="cart-product-thumbnail">
                                        {!! Html::image($item->image,$item->name,['width'=>'64','height'=>'64']) !!}
                                    </td>
                                    <td class="cart-product-name">
                                        {!! $item->name !!}{!! (!is_null($item->nameColor))? ' - '.$item->nameColor:'' !!}
                                    </td>
                                    <td class="quantity">
                                        <span>{!! $item->cant !!}</span>
                                    </td>
                                    <td class="cart-product-subtotal">
                                        <span class="amount">{!! $item->totalV !!}</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="cart_item coupon-check">
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['route'=>['appCoupon'],'method'=>'POST','name'=>'frmCoup','id'=>'frmCoup','role'=>'form','enctype'=>'multipart/form-data']) !!}
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {!! Form::text('coupon_d',null,['id'=>'coupon_d','class'=>'dart-form-control','placeholder'=>trans('site.addCoupon'),'required']) !!}
                                    <div id="r_msg_coupon" style="display:none;"><p id="msg_coupon"></p></div>
                                </div>
                                <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input type="button" class="btn normal-btn dart-btn-xs" id="btnCoupon" value="{!! trans('site.app_coupon') !!}" />
                                    <input type="button" class="btn normal-btn dart-btn-xs" id="btnLimpiar" style="display: none;" value="{!! trans('site.clean') !!}" />
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6  col-sm-6 clearfix">
                    <div class="table-responsive totle-cart">
                        <h3 class="dart-pb-20">{!! trans('site.totals') !!}</h3>
                        <table class="table cart">
                            <tbody>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.value') !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount" id="subValue">{!! $data2->subtotalV !!}</span>
                                </td>
                            </tr>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.discount') !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount" id="discountValue">$0</span>
                                </td>
                            </tr>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.subtotal') !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount" id="subValue2">{!! $data2->subtotalV !!}</span>
                                </td>
                            </tr>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.tax') !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount" id="taxValue">{!! $data2->taxV !!}</span>
                                </td>
                            </tr>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.shipping') !!}{!! (appData()->min_value !== 0)? '**':'' !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount" id="shipValue">{!! $data2->shipV !!}</span>
                                </td>
                            </tr>
                            <tr class="cart_item cart_totle">
                                <td class="cart-product-name">
                                    <strong>{!! trans('invoice.total') !!}</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="blue"><strong id="TotalValue">{!! $data2->totalV !!}</strong></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @if(appData()->min_value !== 0)
                        <p>{!! trans('app.free_shipping',['value'=>number_format(appData()->min_value, 0, ',', '.')]).trans('app.apply_').' ' !!}<a href="/conditions" target="_blank">{!! trans('app.condi_') !!}</a> </p>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <p class="error_cart" id="msg-buy"></p>
                        <div class="btn rd-3d-btn dart-btn-sm dart-fright" id="btnBuy">{!! trans('cart.buy_now') !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::open(['url'=>'#','id'=>'frmValues']) !!}
    {!! Form::hidden('hn_subtotal',$data2->subtotal,['id'=>'hn_subtotal']) !!}
    {!! Form::hidden('hn_discount',0,['id'=>'hn_discount']) !!}
    {!! Form::hidden('hn_ship',$data2->ship,['id'=>'hn_ship']) !!}
    {!! Form::hidden('hn_coupon',null,['id'=>'hn_coupon']) !!}
    {!! Form::hidden('hn_city2',$user->cityId,['id'=>'hn_city2']) !!}
    {!! Form::close() !!}
@endsection

@section('jsBottom')
    {!! Html::script('assets/js/checkout.js') !!}
@endsection
