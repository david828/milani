@extends('site.layouts.page')

@section('title') {!! trans('site.cart') !!} @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('site.cart') !!}</h2>

            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('site.cart') !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <!--Shoping Cart-->
    <section>
        <div class="container">
            @if(count($data) === 0)
                <div class="row">
                    <div class="col-md-12">
                        <p>{!! trans('site.cart_empty') !!} <a href="/products">{!! trans('site.here') !!}</a> </p>
                    </div>
                </div>
            @else
            <div class="row" id="allCart">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table cart">
                            <thead>
                            <tr>
                                <th class="cart-product-thumbnail">{!! trans('site.product') !!}</th>
                                <th class="cart-product-name">{!! trans('site.description') !!}</th>
                                <th class="cart-product-price">{!! trans('site.priceU') !!}</th>
                                <th class="cart-product-quantity">{!! trans('site.qty') !!}</th>
                                <th class="cart-product-subtotal">{!! trans('site.subtotal') !!}</th>
                                <th class="cart-product-remove">{!! trans('site.remove') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                @php($col = (!is_null($item->idColor))? '_'.$item->idColor:'')
                            <tr class="cart_item" id="row_{!! $item->id.$col !!}">
                                <td class="cart-product-thumbnail">
                                    <a href="/products/ver/{!! $item->slug !!}">
                                        {!! Html::image($item->image,$item->image,['width'=>'64','height'=>'64']) !!}
                                    </a>
                                </td>
                                <td class="cart-product-name">
                                    <a href="/products/ver/{!! $item->slug !!}">{!! $item->name !!}{!! (!is_null($item->nameColor))? ' - '.$item->nameColor:'' !!}</a>
                                </td>
                                <td class="cart-product-price">
                                    <span class="amount">{!! $item->money !!}</span>
                                </td>
                                <td class="quantity">
                                    <div class="quantity buttons-add-minus">
                                        <input type="text" id="cart{!! $item->id.$col !!}" value="{!! $item->cant !!}" title="Qty" class="input-text qty text" disabled>
                                        <input type="button" value="-" class="minus" id="m{!! $item->id.$col !!}" target-p="{!! $item->id !!}" target-c="{!! $item->idColor !!}" {!! ($item->cant <= 1)? 'disabled':''!!}>
                                        <input type="button" value="+" class="plus" id="p{!! $item->id.$col !!}" target-p="{!! $item->id !!}" target-c="{!! $item->idColor !!}">
                                    </div>
                                </td>
                                <td class="cart-product-subtotal">
                                    <span class="amount" id="sub{!! $item->id.$col !!}">{!! $item->totalV !!}</span>
                                </td>
                                <td class="cart-product-remove">
                                    <div class="remove" title="{!! trans('site.remove') !!}" target-p="{!! $item->id !!}" target-c="{!! $item->idColor !!}"><i class="fa fa-remove"></i></div>
                                </td>
                            </tr>
                            <tr id="r_msg_{!! $item->id.$col !!}" class="row_msg">
                                <td colspan="6">
                                    <p id="msg_{!! $item->id.$col !!}"></p>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! Form::open(['route'=>['chcart'],'method'=>'POST','name'=>'frmCart','id'=>'frmCart','role'=>'form','enctype'=>'multipart/form-data']) !!}
                        {!! Form::hidden('id_p',null, ['id' => 'id_p', 'required']) !!}
                        {!! Form::hidden('id_c',null, ['id' => 'id_c']) !!}
                        {!! Form::hidden('change',null, ['id' => 'change']) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['route'=>['delcart'],'method'=>'POST','name'=>'frmDelCart','id'=>'frmDelCart','role'=>'form','enctype'=>'multipart/form-data']) !!}
                        {!! Form::hidden('id_p2',null, ['id' => 'id_p2', 'required']) !!}
                        {!! Form::hidden('id_c2',null, ['id' => 'id_c2']) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['route'=>['confirmcart'],'method'=>'POST','name'=>'frmConfirm','id'=>'frmConfirm','role'=>'form','enctype'=>'multipart/form-data']) !!}
                        {!! Form::hidden('data','confirm', ['id' => 'data', 'required']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="cart_item coupon-check">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="total-cart">
                                    <strong>{!! trans('site.subtotal') !!}</strong>
                                    <span class="amount" id="totalF" style="float: right;">{!! $totalV !!}</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                    <!--<a href="#" class="btn rd-stroke-btn border_1px dart-btn-xs">Update Cart</a>-->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <button class="btn rd-stroke-btn border_1px dart-btn-xs" id="btn_confirm">{!! trans('site.confirm') !!}</button>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p class="error_cart" id="msg_confirm" style="display: none;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>

@endsection

@section('jsBottom')
    {!! Html::script('assets/js/cart.js') !!}
@endsection
