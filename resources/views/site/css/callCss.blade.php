/*------------------------------------------------------------------
[Master Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/

/*--------"default-template-style.css" --------*/
/*------------------------------------------------------------------
[Default Template Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/


/*------------------------------------------------------------------
[Table of contents]

1. DEFAULT FONT SIZE STYLE
2. Font Weight
3. Some Commen Css

-------------------------------------------------------------------*/

@charset "utf-8";
/* CSS Document */



body{
font-size:14px;
}
/*========== DEFAULT FONT SIZE STYLE==========*/
.dart-fs-8{
font-size:0.5714285714285714em;
}
.dart-fs-10{
font-size:0.7142857142857143em;
}
.dart-fs-12, h6, .h6{
font-size:0.8571428571428571em;
}
.dart-fs-14, h5, .h5{
font-size:1em;
}
.dart-fs-16{
font-size:1.1428571428571428em;
}
.dart-fs-18, h4, .h4{
font-size:1.2857142857142858em;
}
.dart-fs-20{
font-size:1.4285714285714286em;
}
.dart-fs-24, h3, .h3{
font-size:1.7142857142857142em;
}
.dart-fs-26{
font-size:1.8571428571428572em;
}
.dart-fs-30, h2, .h2{
font-size:2.142857142857143em;
}
.dart-fs-36, h1, .h1{
font-size:2.5714285714285716em;
}
.dart-fs-40{
font-size:2.857142857142857em;
}
.dart-fs-42{
font-size:3em;
}
.dart-fs-48{
font-size:3.4285714285714284em;
}
.dart-fs-60{
font-size:4.285714285714286em;
}
.dart-fs-72{
font-size:5.142857142857143em;
}


/*----------------font-weight---------------------*/

.dart-fw-100{
font-weight:100;
}
.dart-fw-200{
font-weight:200;
}
.dart-fw-300{
font-weight:300;
}
.dart-fw-400{
font-weight:400;
}
.dart-fw-500{
font-weight:500;
}
.dart-fw-600{
font-weight:600;
}
.dart-fw-700{
font-weight:700;
}
.dart-fw-800{
font-weight:800;
}
.dart-fw-900{
font-weight:900;
}
.dart-fw-bold{
font-weight:bold;
}

/*----------------end default font style---------------------*/


/*----Some Commen Css---*/

.no-gutter > [class*='col-'] {
padding-right: 0;
padding-left: 0;
}
.yes-gutter > [class*='col-'] {
padding-top: 15px;
padding-bottom: 15px;
}

.dart-no-padding {
padding: 0;
}
.dart-no-padding-tb {
padding-top: 0;
padding-bottom: 0;
}
.dart-no-gutter > [class*='col-'] {
padding-right: 0;
padding-left: 0;
}
.dart-mt-0{
margin-top:0px;
}
.dart-mt-10{
margin-top:10px;
}
.dart-mt-20{
margin-top:20px;
}
.dart-mt-30{
margin-top:30px;
}
.dart-mt-40{
margin-top:40px;
}
.dart-mt-50{
margin-top:50px;
}
.dart-mt-60{
margin-top:60px;
}
.dart-mt-100{
margin-top:100px;
}
.dart-mb-0{
margin-bottom:0px !important;
}
.dart-mb-10{
margin-bottom:10px;
}
.dart-mb-20{
margin-bottom:20px;
}
.dart-mb-30{
margin-bottom:30px !important;
}
.dart-mb-40{
margin-bottom:40px;
}
.dart-mb-50{
margin-bottom:50px;
}
.dart-mb-60{
margin-bottom:60px;
}
.dart-pt-0{
padding-top:0px;
}
.dart-pt-20{
padding-top:20px;
}
.dart-pt-30{
padding-top:30px;
}
.dart-pt-100{
padding-top:100px;
}
.dart-pb-0{
padding-bottom:0px;
}
.dart-pb-20{
padding-bottom:20px;
}
.dart-pb-30{
padding-bottom:30px;
}
.dart-pb-40{
padding-bottom:40px;
}
.dart-pb-50{
padding-bottom:50px;
}
.dart-pb-60{
padding-bottom:60px;
}
.dart-pb-80{
padding-bottom:80px;
}
.dart-pb-100{
padding-bottom:100px;
}
.dart-pr-0{
padding-right:0px;
}
.dart-pr-10{
padding-right:10px;
}
.dart-pr-20{
padding-right:20px;
}
.dart-pr-30{
padding-right:30px;
}
.dart-pr-40{
padding-right:40px;
}
.dart-pr-50{
padding-right:50px;
}
.dart-pl-0{
padding-left:0px;
}
.dart-pl-10{
padding-left:10px;
}
.dart-pl-20{
padding-left:20px;
}
.dart-pl-30{
padding-left:30px;
}
.dart-pl-40{
padding-left:40px;
}
.dart-pl-50{
padding-left:50px;
}

@media (min-width: 768px) {
.dart-pl-30{
padding-left:30px;
}
.dart-pr-30{
padding-right:30px;
}
}

.list-style-1{
list-style-position: outside;
list-style-image: url(../images/bullet%20points/bullet-1.png);
list-style-type: none;
padding-left: 15px;
}
.list-style-1 li{
padding:5px 5px;
}

.tail{
position: relative;
display:block;
border-color: rgba(255, 255, 255, 0.3);
background-color: transparent;
}
.dart-fleft {
float:left;
}
.dart-fright {
float:right;
}
.dart-hr{
}



/*-------- "shop-style.css" --------*/

/*------------------------------------------------------------------
[Shoping Template Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template



/*---Shoping Page Style Here---*/

.wa-theme-design-block {
float: left;
width: calc(100% - 2px);
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
overflow: hidden;
}
.shop-pages .wa-theme-design-block{
margin-bottom: 15px;
margin-top: 15px;
}
figure {
float: left;
position: relative;
}
.wa-theme-design-block figure:before {
content: '';
position: absolute;
top: 0;
left: 0;
width: 100%;
height: 100%;
opacity: 0;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.wa-theme-design-block figure.dark-theme:before {
background-color: #000;
}
figure img {
width: 100%;
}
.ribbon {
position: absolute;
top: 0px;
left: 0px;
display: inline-block;
background-color: {!! $color !!};
}

.ribbon.with_sale{
top: 39px;
}

.ribbon span {
color: white;
text-transform: capitalize;
padding: 22px 0px;
display: inline-block;
transform: rotate(-90deg);
font-size: 17px;
font-weight: 600;
}
.ribbon:after {
content: "";
width: 0px;
height: 0;
border-right: 4px solid transparent;
border-left: 4px solid transparent;
position: absolute;
bottom: -4px;
left: 50%;
margin-left: -4px;
}
.ribbon:after {
border-top: 4px solid {!! $color !!};
}
.ribbon2 {
position: absolute;
top: 123px;
right: 0px;
display: block;
background-color: #b30202;
width: 49px;
height: 74px;
}

.ribbon2 span {
color: white;
text-transform: capitalize;
padding: 32px 0px;
display: block;
transform: rotate(-90deg);
font-size: 17px;
font-weight: 600;
}
.ribbon2:after {
content: "";
width: 0px;
height: 0;
border-right: 4px solid transparent;
border-left: 4px solid transparent;
position: absolute;
bottom: -4px;
left: 50%;
margin-left: -4px;
}
.ribbon2:after {
border-top: 4px solid #b30202;
}

.ribbon3 {
position: absolute;
top: 0px;
right: 0px;
display: block;
background-color: {!! $color !!};
width: 49px;
height: 117px;
}

.ribbon3 span {
color: white;
text-transform: capitalize;
padding: 75px 0px;
display: block;
transform: rotate(-90deg);
font-size: 17px;
font-weight: 600;
}
.ribbon3:after {
content: "";
width: 0px;
height: 0;
border-right: 4px solid transparent;
border-left: 4px solid transparent;
position: absolute;
bottom: -4px;
left: 50%;
margin-left: -4px;
}
.ribbon3:after {
border-top: 4px solid {!! $color !!};
}

.ribbon_sale {
position: absolute;
top: 0;
left: 0;
display: inline-block;
background-color: red;
}

.ribbon_sale span {
color: white;
text-transform: capitalize;
padding: 7px 6px;
display: inline-block;
font-size: 17px;
font-weight: 600;
}

.block-sticker-tag1 {
width: 50px;
height: 50px;
background-color: #313131;
position: absolute;
top: 50%;
margin-top: -75px;
right: -51px;
color: #fff;
opacity: 0;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.off_tag {
position: absolute;
top: 9px;
left: 0;
font-size: 13px;
color: #fff;
line-height: 15px;
text-transform: uppercase;
text-align: center;
}
.block-sticker-tag1 strong {
font-size: 18px;
text-align: center;
padding: 7px 0px 0px 15px;
display: inline-block;
}
.block-sticker-tag2 {
width: 50px;
height: 50px;
background-color: #383838;
position: absolute;
top: 50%;
margin-top: -25px;
right: -51px;
color: #fff;
opacity: 0;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.off_tag1 {
position: absolute;
top: 20px;
left: 0;
font-size: 13px;
color: #fff;
line-height: 15px;
text-transform: uppercase;
text-align: center;
}
.block-sticker-tag2 strong {
font-size: 18px;
text-align: center;
padding: 0px 0px 0px 16px;
display: inline-block;
margin-top: -2px;
}
.block-sticker-tag3 {
width: 50px;
height: 50px;
background-color: #434343;
position: absolute;
top: 50%;
margin-top: 25px;
right: -51px;
color: #fff;
opacity: 0;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.off_tag2 {
position: absolute;
top: 20px;
left: 0;
font-size: 13px;
color: #fff;
line-height: 15px;
text-transform: uppercase;
text-align: center;
}
.block-sticker-tag3 strong {
font-size: 18px;
text-align: center;
padding: 0px 0px 0px 16px;
display: inline-block;
margin-top: -2px;
}
.block-caption1 {
width: 100%;
display: inline-block;
position: relative;
text-align: center;
}
.block-caption1 h4 {
font-size:1.4285714285714286em;
font-weight: 400;
margin-top: 10px;
color: #363636;
}
.text_left {
padding-left: 0px;
}
.price-text-color {
font-size: 17px;
line-height: 20px;
color: #b5b5b5;
font-weight: 400;
}
.review_right {
padding: 0px;
}
.wv_rating {
list-style: none;
padding: 0px;
margin-bottom: 0px;
line-height: 26px;
text-align: right;
}
.wv_rating li {
display: inline-block;
font-size: 11px;
padding: 0px;
margin: 0px;
line-height: 13px;
}
.wv_rating i.fa {
margin-right: 0px;
}
.price {
padding: 0px;
margin-top: 6px;
margin-bottom: 10px;
}
.sell-price {
font-size:1.2em;
font-weight: 700;
margin-right: 10px;
color: {!! $color !!};
}
.price .actual-price {
color: #bababa;
font-size:1.3em;
text-decoration: line-through;
}
.wa-theme-design-block:hover {
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.wa-theme-design-block:hover figure:before {
opacity: 0.4;
cursor:pointer;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.wa-theme-design-block:hover figure .block-sticker-tag, .wa-theme-design-block:hover figure .block-sticker-tag1, .wa-theme-design-block:hover figure .block-sticker-tag2, .wa-theme-design-block:hover figure .block-sticker-tag3 {
opacity: 1;
right: 0;
-webkit-transition: all 0.45s ease-in-out;
-moz-transition: all 0.45s ease-in-out;
-o-transition: all 0.45s ease-in-out;
-ms-transition: all 0.45s ease-in-out;
transition: all 0.45s ease-in-out;
}
.block-sticker-tag1:hover strong, .block-sticker-tag2:hover strong, .block-sticker-tag3:hover strong{
color: {!! $color !!};
}



.shop-pages .shorter {
border-top: 1px solid #f1f1f1;
border-bottom: 1px solid #f1f1f1;
display: inline-block;
vertical-align: middle;
font-size: 13px;
width: 100%;
text-transform: capitalize;
color: #333;
padding: 20px 0;
}
.shop-pages .shorter span {
vertical-align: middle;
}
.shop-pages .shorter span {
vertical-align: middle;
}
.shop-pages .form-control {
height: 53px;
padding: 15px 20px;
font-size: 13px;
line-height: 24px;
border: 1px solid #F1F1F1;
border-radius: 0;
box-shadow: none;
}
.shop-pages .form-control {
border: none;
width: auto;
padding: 3px 10px;
height: auto;
float:right;
}
/*.shop-pages .btn {
border: none;
padding: 4px 0;
text-transform: none;
}*/
.shop-pages .btn .filter-option {
color: #3399cc;
}
.shop-pages .shorter span {
vertical-align: middle;
}
.shop-pages .shorter span {
vertical-align: middle;
}
.shop-pages .dropdown-menu.open {
left: auto;
right: 0;
padding: 0;
border: none;
color: #222;
border-radius: 0;
overflow: visible !important;
box-shadow: none;
}
.shop-pages .dropdown-menu.inner {
top: 0;
border: 1px solid #aaa;
border-radius: 0;
padding: 0;
}
.shop-pages .dropdown-menu.inner li:first-child {
border-top: transparent solid 1px;
}
.shop-pages .dropdown-menu.inner li {
border-top: transparent solid 1px;
border-bottom: transparent solid 1px;
}
.shop-pages .dropdown-menu.inner span.glyphicon.check-mark {
display: none;
}
.shop-pages .dropdown-menu.inner {
top: 0;
border: 1px solid #aaa;
border-radius: 0;
padding: 0;
}
.shop-pages .dropdown-menu.open {
left: auto;
right: 0;
padding: 0;
border: none;
color: #222;
border-radius: 0;
overflow: visible !important;
box-shadow: none;
}
.shop-pages .product-box {
margin-top: 30px;
}
.shop-pages .border-lft {
padding-bottom: 95px;
border-left: 1px solid rgb(241, 241, 241);
}
.shop-sidebar .widget ul {
padding-left: 0px;
}

.shop-pages .product-wrap{
border-bottom:1px solid #f1f1f1;
display:inline-block;
vertical-align:middle;
width:100%;
}

.shop-sidebar .widget-title{
font-size:1.2857142857142858em;
font-weight:600;
text-transform:uppercase;
position:relative;
padding:5px 0;
display:inline-block;
width:100%;
}
.shop-sidebar .widget{
margin-bottom:25px;
}
.shop-sidebar .widget li{
border-bottom:1px solid #f1f1f1;
padding:10px 0;
text-transform:capitalize;
}
.shop-sidebar .widget li:last-child{
border-bottom:none;
}
.shop-sidebar .widget.widget_size li{
display:inline-block;
padding:0;
text-transform:uppercase;
border:none;
}
.shop-sidebar .widget .ui-selectmenu-button{
background:transparent!important;
border-radius:0;
color:#898989!important;
vertical-align:middle;
width:100%!important;
}
.shop-sidebar .widget .ui-selectmenu-button span.ui-selectmenu-text{
padding-left:0;
}
.shop-sidebar .widget.widget_size li a{
border:1px solid #f1f1f1;
display:inline-block;
height:35px;line-height:32px;
margin-bottom:10px;
margin-right:10px;
text-align:center;
width:35px;
}
.shop-sidebar .widget.widget_size li a:hover,.shop-sidebar .widget.widget_size li a:focus{
border-color:#333;
}
.shop-sidebar .widget-title:after{
background:{!! $color !!};
height:1px;
width:25px;
position:absolute;
bottom:0;
left:0;
content:"";
}
.shop-sidebar .widget li a{
color:#898989;
}
.shop-sidebar .widget li a:hover{
color:{!! $color !!};
text-decoration: none;
padding-left: 5px;
}
.shop-sidebar .widget.widget_size li a:hover {
color: #fff;
background-color:{!! $color !!};
text-decoration: none;
padding-left: 0px;
}
.shop-pages .widget-content #slider-range{
background-color:#f1f1f1;
background-image:none;
border-color:#f1f1f1
;border-radius:0;
height:6px;
margin:15px 0;
}
.shop-pages .widget-content #amount{
background:transparent;
border:none;
}
.shop-pages #slider-range .ui-widget-header{
background-image:none;
background-color:#333;
border-radius:0;
}
.shop-pages #slider-range .ui-slider-handle{
background-color:#333!important;
background-image:none;
border:2px solid #333!important;
border-radius:0;
height:16px;
margin:0;
top:-6px;
}
.shop-pages .widget.widget_size .filter-btn{
border:2px solid #f1f1f1;
color:#898989;
font-size:10px;
font-weight:700;
padding:10px 20px;
float:right;
}
.shop-pages .shop-links-widget .panel{
border: none;
border-radius: 0;
box-shadow: none;
margin: 0 !important;
}
.shop-pages .shop-links-widget .panel-heading{
background: none;
border: none;
border-radius: 0;
padding-left: 0;
padding-bottom: 0;
}
.shop-pages .shop-links-widget .panel-title{
padding: 0px 0 5px;
text-transform: capitalize;
border-bottom: 1px solid #f1f1f1;
}
.shop-pages .shop-links-widget .panel-title a{
font-size: 13px;
font-weight: 400;
color: #898989;
display: block;
width: 100%;
position: relative;
padding: 0px 0px 10px;
}
.shop-pages .shop-links-widget .panel-title a:hover {
text-decoration: none;
color: {!! $color !!};
}

.shop-pages .shop-links-widget .panel-collapse{
background-color: #f5f5f5;
}
.shop-pages .shop-links-widget .panel-body{
border-top: none !important;
padding-top: 0 !important;
padding-bottom: 0 !important;
}
.shop-pages .shop-links-widget a .caret{
position: absolute;
right: 0;
top: 10px;
-webkit-transition: all 500ms ease;
-moz-transition: all 500ms ease;
-ms-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;
}
.shop-pages .shop-links-widget a[aria-expanded="true"] .caret{
-moz-transform: rotate(180deg);
-webkit-transform: rotate(180deg);
-o-transform: rotate(180deg);
-ms-transform: rotate(180deg);
transform: rotate(180deg);
}
.shop-pages .shop-links-widget a[aria-expanded="false"] .caret{
-moz-transform: rotate(0deg);
-webkit-transform: rotate(0deg);
-o-transform: rotate(0deg);
-ms-transform: rotate(0deg);
transform: rotate(0deg);
}
@media (max-width : 767px) {

.shop-pages .product-box{
display:table;
margin:30px auto auto;
}
}
.shop-sidebar .btn-group{
width: 100%;
}
.shop-sidebar .btn-group.btn {
color: #898989;
width: 100%;
}
.shop-sidebar .btn .filter-option {
color: #898989;
}
.shop-sidebar .btn .filter-option:hover {
color: #3399cc;
}
.shop-sidebar .btn-group.btn .filter-option {
float: left;
}
.shop-sidebar .btn-group.btn .caret {
position: absolute;
top: 13px;right: 0;
}
.shop-sidebar .dropdown-menu.open,.shop-sidebar .dropdown-menu.open ul {
width: 100%;
}
.shop-sidebar .dropdown-menu.open ul li {
padding: 0px;
}
/*product pagination*/
.pagination-wrap ul li{
display:inline-block;
margin:5px;
}
.pagination-wrap ul{
margin:25px 0 0;
}
.pagination-wrap li a{
border:1px solid transparent;
color:#333;
display:inline-block;
height:35px;
line-height:32px;
width:35px;
text-align:center;
}
.pagination-wrap li a:hover,.pagination-wrap li a:focus{
border-color:#ccc;
background-color: {!! $color !!};
color: #fff;
text-decoration: none;
}
.pagination-wrap li.disabled a{
border-color:#ccc;
color:#ccc;
}
.pagination-wrap li.active a{
border-color:#ccc;
background-color: {!! $color !!};
color: #fff;
text-decoration: none;
}
.pagination-wrap li.next a{
border-color:#333;
}
/*product pagination*/



.shop-pages .product-title {
clear: both;
display: inline-block;
width: 100%;
}
.shop-pages .product-title p {
margin-bottom:10px;
}
.shop-pages .product-title > h2 {
float: left;
margin-top:0px;
}
.shop-pages .product-title .pagination-next-prev {
float: right;
margin-top: -15px;
}
.shop-pages .pagination-next-prev {
border: 1px solid #333;
border-radius: 3em;
color: #333;
display: inline-block;
height: 35px;
line-height: 32px;
text-align: center;
width: 35px;
}
.shop-pages .product-price-review {
border-bottom: 1px solid #f1f1f1;
border-top: 1px solid #f1f1f1;
display: inline-block;
width: 100%;
padding: 20px 0;
margin: 15px 0;
}
.shop-pages .product-price-review .font-semibold {
display: inline-block;
font-size:1.2857142857142858em;
margin-right: 15px;
vertical-align: middle;
}
.shop-pages .black-color {
color: #313131;
}
.shop-pages .product-price-review .rating {
display: inline-block;
vertical-align: middle;
}
.shop-pages .rating span.star.active::before, .rating.add-rating span.star:hover::before, .rating span.star:hover span.star::before {
color: #eabe12;
content: "\f005";
font-family: FontAwesome;
}
.shop-pages .rating span.star::before {
color: #eabe12;
content: "\f006";
font-family: FontAwesome;
}
.shop-pages .rating span.text {
padding: 0 5px;
}
.shop-pages .product-description > p {
line-height: 2;
}
.shop-pages .qty-add-wish-btn {
margin: 35px 0;
}
.shop-pages .qty-add-wish-btn .quantity {
display: inline-block;
vertical-align: middle;
width: 90px;
}
.shop-pages .quantity .qty {
border: 2px solid #f1f1f1;
float: left;
font-weight: 700;
height: 60px;
text-align: center;
width: 40px;
}
.shop-pages .quantity .minus {
border-width: 2px 2px 2px 0;
height: 29px;
}
.shop-pages .quantity .minus, .quantity .plus {
background: rgba(0,0,0,0) none repeat scroll 0 0;
display: table;
font-size:1.2857142857142858em;
font-weight: 700;
border-style: solid;
border-color: #f1f1f1;
width: 30px;
height: 30px;
}
.shop-pages .quantity .plus {
border-width: 0 2px 2px 0;
}
.shop-pages .quantity .minus, .shop-pages .quantity .plus {
background: rgba(0,0,0,0) none repeat scroll 0 0;
display: table;
font-size:1.2857142857142858em;
font-weight: 700;
border-style: solid;
border-color: #f1f1f1;
width: 30px;
height: 30px;
}

.shop-pages .social-media {
border-bottom: 1px solid #f1f1f1;
border-top: 1px solid #f1f1f1;
display: inline-block;
margin: 18px 0 0;
padding: 25px 0;
width: 100%;
}
.shop-pages .black-color {
color: #313131;
}
.shop-pages .social-media .social-icons {
display: inline-block;
float: right;
text-align: right;
vertical-align: middle;
}
.shop-pages .social-icons li {
display: inline-block;
font-size:1.1428571428571428em;
margin: 0 10px;
}
.shop-pages .social-icons li a {
color: #dadada;
}


@media (min-width: 992px) and (max-width: 1024px) {
.qty-add-wish-btn .btn{margin-right:15px}
}
@media (max-width: 768px) {
.shop-pages .social-media{margin:18px 0 50px}
}
@media (max-width: 480px) {
.addinal-info li .black-color{width:30%}
.addinal-info span + span{display:inline-block;vertical-align:middle;width:67%}
.reviewer-info{display:inline-block;margin-left:14px;width:70%}
.qty-add-wish-btn > span{display:block;margin-bottom:15px}
.review-title .rating{float:none;margin-left:-5px}
}
@media (max-width: 380px) {
.add-review .btn{float:none;margin-top:25px}
}
/* Single Product Page Ends */




/*---Shopping Cart---*/

table.table.cart {
border: 1px solid #ebebeb;
}
.cart thead {
background-color: #ebebeb;
}
.cart thead tr th{
text-transform:uppercase;
}
.cart thead tr th, .cart tbody tr td {
padding: 20px;
vertical-align: middle;
}
.cart .qty-add-wish-btn {
margin: 35px 0;
}
.cart .qty-add-wish-btn .quantity {
display: inline-block;
vertical-align: middle;
width: 90px;
}
.cart .quantity .qty {
border: 2px solid #f1f1f1;
float: left;
font-weight: 700;
height: 60px;
text-align: center;
width: 40px;
}
.cart .quantity .minus {
border-width: 2px 2px 2px 0;
height: 29px;
}
.cart .quantity .minus, .cart .quantity .plus {
background: rgba(0,0,0,0) none repeat scroll 0 0;
display: table;
font-size:1.2857142857142858em;
font-weight: 700;
border-style: solid;
border-color: #f1f1f1;
width: 30px;
height: 30px;
}
.cart .quantity .plus {
border-width: 0 2px 2px 0;
}
.cart .quantity .minus, .cart .quantity .plus {
background: rgba(0,0,0,0) none repeat scroll 0 0;
display: table;
font-size:1.2857142857142858em;
font-weight: 700;
border-style: solid;
border-color: #f1f1f1;
width: 30px;
height: 30px;
}
.cart tr td.cart-product-name a{
color:#333;
}
.cart tr td.cart-product-name a:hover{
text-decoration:none;
color:{!! $color !!};
}
.dart-form-control {
display: block;
width: 100%;
padding: 8px 14px;
font-size: 1em;
line-height: 1.42857143;
color: #555;
background-color: #fff;
background-image: none;
border: 2px solid #DDD;
border-radius: 5px;
margin-top: 10px;
margin-bottom: 10px;
-webkit-transition: border-color ease-in-out .15s;
-o-transition: border-color ease-in-out .15s;
transition: border-color ease-in-out .15s;
height: auto;
}

.cart thead tr.cart_totle th, .cart tbody tr.cart_totle td {
padding: 15px;
vertical-align: middle;
border-color:#ebebeb;
}
.cart tbody tr.cart_totle td:first-child{
background-color: #f8f8f8;
}
.cart-product-remove a{
color:red;
}
.cart_item.coupon-check {
background-color: #f7f7f7;
padding: 20px;
margin-bottom: 15px;
}
@media (max-width: 768px) {
.cart tbody tr td {
padding: 10px;
}
.coupon-check .text-right{
text-align:left;
}
.cart_item.coupon-check {
padding: 0;
}
.cart_item.coupon-check .dart-form-control {
margin-bottom: 0px;
}
.totle-cart{
border:none;
}
}


/*---Checkout---*/

#rd_login_form form .form-group .required{
color: red;
font-weight: bold;
border: 0;
}
.rd_guest_acc {
margin-top: 43px!important;
width:100%;
}

.payments-options ul li{
line-height: 54px;
text-align: left;
margin: 0;
font-weight: normal;
font-size: 1em;
}

.payments-options ul li input {
margin: 0 1em 0 0;
}
.cart.checkout thead tr th, .cart.checkout tbody tr td {
padding: 10px;
vertical-align: middle;
}
@media (max-width: 768px) {
.rd_guest_acc {
margin-top: 15px !important;
width: 100%;
}
}

.rd-stroke-btn {
color: #333;
background: #ffffff;
border-style: solid;
border-color: #333;
margin-top: 10px;
margin-bottom: 10px;
font-weight: 900;
border-radius: 5px;
text-transform: uppercase;
}
.border_2px {
border-width: 2px;
}
.dart-btn-sm {
font-size: 1.1428571428571428em;
min-width: 170px;
max-width: 100%;
padding: 15px 20px;
vertical-align: middle;
max-height: 55px;
}
.rd-stroke-btn:hover, .rd-stroke-btn:focus {
color: #fff;
background: {!! $color !!};
border-color: {!! $color !!};
}
.btn, .btn:hover {
-webkit-transition: all 0.35s;
-moz-transition: all 0.35s;
transition: all 0.35s;
}
.normal-btn:hover, .normal-btn:focus {
color: #fff;
background: #333333;
}
.normal-btn {
color: #ffffff;
background: {!! $color !!};
margin-top: 10px;
margin-bottom: 10px;
border-radius: 5px;
text-transform: uppercase;
}
.rd-3d-btn.dart-btn-sm {
max-height: 60px;
}
.rd-3d-btn {
border-bottom: 5px solid rgba(0,0,0,.2);
}
.rd-3d-btn {
color: #ffffff;
background: #64b63e;
margin-top: 10px;
margin-bottom: 10px;
font-weight: 900;
border-radius: 5px;
text-transform: uppercase;
}
.dart-btn-sm {
font-size: 1.1428571428571428em;
min-width: 170px;
max-width: 100%;
padding: 15px 20px;
vertical-align: middle;
max-height: 55px;
}
.rd-3d-btn:hover, .rd-3d-btn:focus {
color: #fff;
background: rgba(100, 182, 62, 0.8);
}




/*-------- "template-blog.css" --------*/

/*------------------------------------------------------------------
[Blog Post Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/

.blog-posts article {
border-bottom: 1px solid #DDD;
margin-bottom: 50px;
padding-bottom: 10px;
}

.blog-posts .pagination {
margin: -10px 0 20px;
}

/* Post */
article.post h2 a {
text-decoration: none;
}

article.post .post-meta {
font-size: 0.9em;
margin-bottom: 7px;
}

article.post .post-meta > span {
display: inline-block;
padding-right: 8px;
}

article.post .post-meta i {
margin-right: 3px;
}

article.post .post-date {
float: left;
margin-right: 10px;
text-align: center;
}

article.post .post-date .month {
background: #000000;
border-radius: 0 0 2px 2px;
color: #FFF;
font-size: 0.9em;
padding: 0 10px 2px;
}

article.post .post-date .day {
background: {!! $color !!};
border-radius: 2px 2px 0 0;
color: #fff;
display: block;
font-size: 16px;
font-weight: 500;
font-weight: bold;
padding: 10px;
}

article.post .post-image .owl-carousel {
width: 100.1%;
}

article .post-video {
-webkit-transition: all 0.2s ease-in-out;
-moz-transition: all 0.2s ease-in-out;
transition: all 0.2s ease-in-out;
padding: 0;
background-color: #FFF;
border: 1px solid #DDD;
border-radius: 8px;
display: block;
height: auto;
position: relative;
margin: 0 0 30px 0;
padding-bottom: 61%;
}

article .post-video iframe {
bottom: 0;
height: auto;
left: 0;
margin: 0;
min-height: 100%;
min-width: 100%;
padding: 4px;
position: absolute;
right: 0;
top: 0;
width: auto;
}

article .post-audio {
-webkit-transition: all 0.2s ease-in-out;
-moz-transition: all 0.2s ease-in-out;
transition: all 0.2s ease-in-out;
padding: 0;
background-color: #FFF;
border: 1px solid #DDD;
border-radius: 8px;
display: block;
height: auto;
position: relative;
margin: 0 0 30px 0;
padding-bottom: 25%;
}

article .post-audio iframe {
bottom: 0;
height: auto;
left: 0;
margin: 0;
min-height: 100%;
min-width: 100%;
padding: 4px;
position: absolute;
right: 0;
top: 0;
width: auto;
}

article.post-medium .post-image .owl-carousel {
width: 100.2%;
}

article.post-large {
margin-left: 60px;
}

article.post-large h2 {
margin-bottom: 5px;
}

article.post-large .post-image, article.post-large .post-date {
margin-left: -60px;
}

article.post-large .post-image {
margin-bottom: 15px;
}

article.post-large .post-image.single {
margin-bottom: 30px;
}

article.post-large .post-video {
margin-left: -60px;
}

article.post-large .post-audio {
margin-left: -60px;
}

/* Single Post */
.single-post article {
border-bottom: 0;
margin-bottom: 0;
}

article.blog-single-post .post-meta {
margin-bottom: 20px;
}

/* Post Block */
.post-block {
border-top: 1px solid #DDD;
margin: 15px 0 0 0;
padding: 20px 0 15px 0;
}

.post-block h3 {
font-size: 1.8em;
font-weight: 200;
margin: 0 0 20px;
text-transform: none;
}

.post-block h3 i {
margin-right: 7px;
}

/* Post Author */
.post-author {
margin: 15px 0 0 0;
}

.post-author img {
max-height: 80px;
max-width: 80px;
}

.post-author p {
font-size: 0.9em;
line-height: 22px;
margin: 0;
padding: 0;
}

.post-author p .name {
font-size: 1.1em;
}

.post-author .img-thumbnail {
display: inline-block;
float: left;
margin-right: 20px;
}

/* Post Share */
.post-share {
margin: 55px 0 0 0;
padding-bottom: 0;
}

/* Post Comments */
.post-comments {
margin-top: 45px;
}

ul.comments {
list-style: none;
margin: 0;
padding: 0;
}

ul.comments li {
clear: both;
padding: 10px 0 0 115px;
}

ul.comments li img.avatar {
height: 80px;
width: 80px;
}

ul.comments li ul.reply {
margin: 0;
}

ul.comments li a {
text-decoration: none;
}

ul.comments li .img-thumbnail {
margin-left: -115px;
position: absolute;
}

ul.comments li .comment {
margin-bottom: 10px;
}

ul.comments .comment-arrow {
border-bottom: 15px solid transparent;
border-right: 15px solid #F4F4F4;
border-top: 15px solid transparent;
height: 0;
left: -15px;
position: absolute;
top: 28px;
width: 0;
}

ul.comments .comment-block {
background: #F4F4F4;
border-radius: 5px;
padding: 20px 20px 30px;
position: relative;
}

ul.comments .comment-block p {
font-size: 0.9em;
line-height: 21px;
margin: 0;
padding: 0;
}

ul.comments .comment-block .comment-by {
display: block;
font-size: 1em;
line-height: 21px;
margin: 0;
padding: 0 0 5px 0;
}

ul.comments .comment-block .date {
color: #999;
font-size: 0.9em;
}

/* Leave a Comment */
.post-leave-comment {
margin-top: 25px;
padding-top: 45px;
}

.post-leave-comment h3 {
margin: 0 0 40px;
}

/* Recent Posts */
.recent-posts h4 {
margin-bottom: 7px;
}

.recent-posts article.recent-post h4 {
margin: 0 0 3px 0;
}

.recent-posts article.recent-post h4 a {
display: block;
}

.recent-posts .date {
float: left;
margin-right: 10px;
margin-top: 8px;
text-align: center;
}

.recent-posts .date .month {
background: #CCC;
border-radius: 0 0 2px 2px;
box-shadow: 0 -1px 0 0 rgba(0, 0, 0, 0.07) inset;
color: #FFF;
font-size: 0.9em;
padding: 0 10px 2px;
}

.recent-posts .date .day {
background: #F7F7F7;
color: #CCC;
display: block;
font-size: 18px;
font-weight: 500;
font-weight: bold;
padding: 8px;
}

section.featured .recent-posts .date .day {
background: #FFF;
}

/* Simple Post List */
ul.simple-post-list {
list-style: none;
margin: 0;
padding: 0;
}

ul.simple-post-list li {
*zoom: 1;
border-bottom: 1px dotted #E2E2E2;
padding: 15px 0;
}

ul.simple-post-list li:before, ul.simple-post-list li:after {
content: " ";
display: table;
}

ul.simple-post-list li:after {
clear: both;
}

ul.simple-post-list li:last-child {
border-bottom: 0;
}

ul.simple-post-list .post-image {
float: left;
margin-right: 12px;
}

ul.simple-post-list .post-meta {
color: #888;
font-size: 0.8em;
}

/* Responsive */
@media (max-width: 479px) {
ul.comments li {
border-left: 8px solid #DDDDDD;
clear: both;
padding: 0 0 0 10px;
}

ul.comments li .img-thumbnail {
display: none;
}

ul.comments .comment-arrow {
display: none;
}
}

/*---Sidebar---*/

hr.sep-line {
background-color:#ebebeb;
border: 0;
height: 1px;
margin: 22px 0 22px 0;
}
ul.nav-list.primary > li a {
-webkit-transition: all 0.3s;
-moz-transition: all 0.3s;
transition: all 0.3s;
border-bottom: 1px solid #EDEDDE;
padding: 8px 0;
border-radius:0;
}
aside ul.nav-list > li > a {
color: #666;
font-size: 0.9em;
border-radius:0;
}
ul.nav-list.primary > li a:before {
content: "";
display: inline-block;
width: 0;
height: 0;
border-top: 4px solid transparent;
border-bottom: 4px solid transparent;
border-left: 4px solid #333;
margin-left: 5px;
margin-right: 5px;
position: relative;
}
ul.nav-list.primary li {
line-height: 24px;
}
ul.nav-list.primary  {
margin-top:15px;
margin-bottom:30px;
}
.blog-single .tab-content {
border-radius: 0 0 0px 0px;
background-color: #f7f7f7;
border: 1px solid #EEE;
border-top: 0;
padding: 15px;
}
.blog-single .nav-tabs {
border-bottom-color: #EEE;
}
.blog-single .tabs ul.nav-tabs li.active a {
border-top-color: {!! $color !!};
color: {!! $color !!};
border-radius:0;
}
.blog-single .nav-tabs li.active a, .blog-single .nav-tabs li.active a:hover, .blog-single .nav-tabs li.active a:focus {
background: #f7f7f7;
border-left-color: #EEE;
border-right-color: #EEE;
border-top: 3px solid #CCC;
color: #CCC;
border-radius:0;
}
.blog-single .nav-tabs li a, .blog-single .nav-tabs li a:hover {
background: #fff;
border-bottom: none;
border-left: 1px solid #EEE;
border-right: 1px solid #EEE;
border-top: 3px solid #EEE;
color: #363636;
border-radius:0;
}

.widgetClientTestimonial .flex-viewport{
background-color:#fff;
border:1px solid #eee;
}
.widgetClientTestimonial blockquote{
padding:20px 20px;
margin:0;
font-size:13px;
line-height:24px;
border-left:0;
background-color: #f7f7f7;
}
.widgetClientTestimonial blockquote footer.client-name{
font-size:11px;
text-transform:uppercase;
color:{!! $color !!};
}
.widgetClientTestimonial blockquote footer.client-name .client-title{
color:#969595;
}
.widgetClientTestimonial blockquote .small:before,.widgetClientTestimonial blockquote footer:before,.widgetClientTestimonial blockquote small:before{
content:''
}
.widgetClientTestimonial .flex-control-thumbs{
margin:10px 0 0;
}
.widgetClientTestimonial .flex-control-thumbs li{
width:auto;
margin-right:5px;
}
.widgetClientTestimonial .flex-control-thumbs li:last-child{
margin-right:0;
}
.widgetClientTestimonial .flex-control-thumbs img{
width:39px;
opacity:1;
}
.widgetClientTestimonial .flex-control-thumbs .flex-active, .widgetClientTestimonial .flex-control-thumbs img:hover {
opacity: 0.4;
}
.widget_tag_cloud ul li{
margin-bottom:10px;
float:left;
}
.widget_tag_cloud ul li a{
display:block;
padding:3px 10px;
color:#898989;
background-color:#ebebeb;
}
.footer-widget.widget_tag_cloud ul li a{
background-color:transparent;
}
.widget_tag_cloud ul li a:hover{
background-color:{!! $color !!};
color:#fff;
}




/*------- "awesomenav.css" ---------*/
/*------------------------------------------------------------------
[Nav Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/

/*Nav style css here*/


/* MEGAMENU STYLE
=================================*/
nav.awesomenav .dropdown.megamenu-fw {
position: static;
}

nav.awesomenav .container {
position: relative;
}

nav.awesomenav .megamenu-fw .dropdown-menu {
left: auto;
}

nav.awesomenav .megamenu-content {
padding: 15px;
width: 100% !important;
}

nav.awesomenav .megamenu-content .title{
margin-top: 0;
}

nav.awesomenav .dropdown.megamenu-fw .dropdown-menu {
left: 0;
right: 0;
}

/* Navbar
=================================*/
nav.navbar.awesomenav{
margin-bottom: 0;
-moz-border-radius: 0px;
-webkit-border-radius: 0px;
-o-border-radius: 0px;
border-radius: 0px;
background-color: #000000;
border: none;
border-bottom: solid 1px #000000;
z-index: 999;
}

nav.navbar.awesomenav ul.nav > li > a{
color: #FFFFFF;
background-color: transparent;
outline: none;
margin-bottom: -2px;
}

nav.navbar.awesomenav ul.nav li.megamenu-fw > a:hover,
nav.navbar.awesomenav ul.nav li.megamenu-fw > a:focus,
nav.navbar.awesomenav ul.nav li.active > a:hover,
nav.navbar.awesomenav ul.nav li.active > a:focus,
nav.navbar.awesomenav ul.nav li.active > a{
background-color: transparent;
}

nav.navbar.awesomenav .navbar-toggle{
background-color: {!! $color !!};
border: none;
padding: 5px 15px;
font-size: 18px;
position: relative;
top: 5px;
color: #fff;
}

nav.navbar.awesomenav ul.nav .dropdown-menu .dropdown-menu{
top: 0;
left: 100%;
}

nav.navbar.awesomenav ul.nav ul.dropdown-menu > li > a{
white-space:normal;
}


ul.menu-col{
padding: 0;
margin: 0;
list-style: none;
}

ul.menu-col li a{
color: #6f6f6f;
}

ul.menu-col li a:hover,
ul.menu-col li a:focus{
text-decoration: none;
}

/* Navbar Full
=================================*/
nav.awesomenav.navbar-full{
padding-bottom: 10px;
padding-top: 10px;
}

nav.awesomenav.navbar-full .navbar-header{
display: block;
width: 100%;
}

nav.awesomenav.navbar-full .navbar-toggle{
display: inline-block;
margin-right: 0;
position: relative;
top: 0;
font-size: 30px;
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out;
-o-transition: all 1s ease-in-out;
-ms-transition: all 1s ease-in-out;
transition: all 1s ease-in-out;
}

nav.awesomenav.navbar-full .navbar-collapse{
position: fixed;
width: 100%;
height: 100% !important;
top: 0;
left: 0;
padding: 0;
display: none !important;
z-index: 9;
}

nav.awesomenav.navbar-full .navbar-collapse.in{
display: block !important;
}

nav.awesomenav.navbar-full .navbar-collapse .nav-full{
overflow: auto;
}

nav.awesomenav.navbar-full .navbar-collapse .wrap-full-menu{
display: table-cell;
vertical-align: middle;
background-color: #fff;
overflow: auto;
}

nav.awesomenav.navbar-full .navbar-collapse .nav-full::-webkit-scrollbar {
width: 0;
}

nav.awesomenav.navbar-full .navbar-collapse .nav-full::-moz-scrollbar {
width: 0;
}

nav.awesomenav.navbar-full .navbar-collapse .nav-full::-ms-scrollbar {
width: 0;
}

nav.awesomenav.navbar-full .navbar-collapse .nav-full::-o-scrollbar {
width: 0;
}


nav.awesomenav.navbar-full .navbar-collapse ul.nav{
display: block;
width: 100%;
overflow: auto;
}

nav.awesomenav.navbar-full .navbar-collapse ul.nav a:hover,
nav.awesomenav.navbar-full .navbar-collapse ul.nav a:focus,
nav.awesomenav.navbar-full .navbar-collapse ul.nav a{
background-color: transparent;
}

nav.awesomenav.navbar-full .navbar-collapse ul.nav > li{
float: none;
display: block;
text-align: center;
}

nav.awesomenav.navbar-full .navbar-collapse ul.nav > li > a{
display: table;
margin: auto;
text-transform: uppercase;
font-weight: bold;
letter-spacing: 2px;
font-size: 24px;
padding: 10px 15px;
}

li.close-full-menu > a{
padding-top: 0px;
padding-bottom: 0px;
}

li.close-full-menu{
padding-top: 30px;
padding-bottom: 30px;
}

/* Atribute Navigation
=================================*/
.attr-nav{
float: right;
display: inline-block;
margin-left: 13px;
margin-right: -15px;
}

.attr-nav > ul{
padding: 0;
margin: 0 0 -7px 0;
list-style: none;
display: inline-block;
}

.attr-nav > ul > li{
float: left;
display: block;
}

.attr-nav > ul > li > a{
color: #FFFFFF;
display: block;
/*padding: 28px 15px;*/
position: relative;
}
.attr-nav > ul > li{
padding: 28px 15px;
}
.attr-nav > ul > li > a span.badge{
position: absolute;
top: 50%;
margin-top: -15px;
right: 5px;
font-size: 10px;
padding: 0;
width: 15px;
height: 15px;
padding-top: 2px;
}

.attr-nav > ul > li.dropdown ul.dropdown-menu{
-moz-border-radius: 0px;
-webkit-border-radius: 0px;
-o-border-radius: 0px;
border-radius: 0px;
-moz-box-shadow: 0px 0px 0px;
-webkit-box-shadow: 0px 0px 0px;
-o-box-shadow: 0px 0px 0px;
box-shadow: 0px 0px 0px;
border: solid 1px #e0e0e0;
}

ul.cart-list{
padding: 0 !important;
width: 250px !important;
}

ul.cart-list > li{
position: relative;
border-bottom: solid 1px #efefef;
padding: 15px 15px 23px 15px !important;
}

ul.cart-list > li > a.photo{
padding: 0 !important;
margin-right: 15px;
float: left;
display: block;
width: 50px;
height: 50px;
left: 15px;
top: 15px;
}

ul.cart-list > li img{
width: 50px;
height: 50px;
border: solid 1px #efefef;
}

ul.cart-list > li > h6{
margin: 0;
}

ul.cart-list > li > h6 > a.photo{
padding: 0 !important;
display: block;
}

ul.cart-list > li > p{
margin-bottom: 0;
}

ul.cart-list > li.total{
background-color: #f5f5f5;
padding-bottom: 15px !important;
}

ul.cart-list > li.total > .btn{
display: inline-block;
border-bottom: solid 1px #efefef;
}

ul.cart-list > li .price{
font-weight: bold;
}

ul.cart-list > li.total > span{
padding-top: 8px;
}

/* Top Search
=================================*/
.top-search{
//background-color: #333;
background-color: #ffffff;
padding: 10px 0;
display: none;
}

.top-search input.form-control{
//background-color: transparent;
border: none;
-moz-box-shadow: 0px 0px 0px;
-webkit-box-shadow: 0px 0px 0px;
-o-box-shadow: 0px 0px 0px;
box-shadow: 0px 0px 0px;
//color: #fff;
color: #333;
height: 40px;
padding: 0 15px;
}

.top-search .input-group-addon{
background-color: transparent;
border: none;
//color: #fff;
color: #000000;
padding-left: 0;
padding-right: 0;
}

.top-search .input-group-addon.close-search{
cursor: pointer;
}

/* Side Menu
=================================*/
body{
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}

body.on-side{
margin-left: -280px;
}

.side{
position: fixed;
overflow-y: auto;
top: 0;
right: -280px;
width: 280px;
padding: 25px 30px;
height: 100%;
display: block;
background-color: #333;
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
z-index: 9;
}

.side.on{
right: 0;
}

.side .close-side{
float: right;
color: #fff;
position: relative;
z-index: 2;
font-size: 16px;
}

.side .widget{
position: relative;
z-index: 1;
margin-bottom: 25px;
}

.side .widget .title{
color: #fff;
margin-bottom: 15px;
}

.side .widget ul.link{
padding: 0;
margin: 0;
list-style: none;
}

.side .widget ul.link li a{
color: #9f9f9f;
letter-spacing: 1px;
}

.side .widget ul.link li a:focus,
.side .widget ul.link li a:hover{
color: #fff;
text-decoration: none;
}

/* Share
=================================*/
nav.navbar.awesomenav .share{
padding: 0 30px;
margin-bottom: 30px;
}

nav.navbar.awesomenav .share ul{
display: inline-block;
padding: 0;
margin: 0 0 -7px 0;
list-style: none;
}

nav.navbar.awesomenav .share ul > li{
float: left;
display: block;
margin-right: 5px;
}

nav.navbar.awesomenav .share ul > li > a{
display: table-cell;
vertical-align: middle;
text-align: center;
width: 35px;
height: 35px;
-moz-border-radius: 50%;
-webkit-border-radius: 50%;
-o-border-radius: 50%;
border-radius: 50%;
background-color: #cfcfcf;
color: #fff;
}

/* Transparent
=================================*/
nav.navbar.awesomenav.navbar-fixed{
position: fixed;
display: block;
width: 100%;
}

nav.navbar.awesomenav.no-background{
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out;
-o-transition: all 1s ease-in-out;
-ms-transition: all 1s ease-in-out;
transition: all 1s ease-in-out;
}

/* Navbar Sticky
=================================*/
.wrap-sticky{
position: relative;
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}

.wrap-sticky nav.navbar.awesomenav{
position: absolute;
width: 100%;
left: 0;
top: 0;
}

.wrap-sticky nav.navbar.awesomenav.sticked{
position: fixed;
-webkit-transition: all 0.2s ease-in-out;
-moz-transition: all 0.2s ease-in-out;
-o-transition: all 0.2s ease-in-out;
-ms-transition: all 0.2s ease-in-out;
transition: all 0.2s ease-in-out;
}

body.on-side .wrap-sticky nav.navbar.awesomenav.sticked{
left: -280px;
}

/* Navbar Responsive
=================================*/
@media (min-width: 1024px) and (max-width:1400px) {
body.wrap-nav-sidebar .wrapper .container{
width: 100%;
padding-left: 30px;
padding-right: 30px;
}
}

@media (min-width: 1024px) {
/* General Navbar
=================================*/
nav.navbar.awesomenav ul.nav .dropdown-menu .dropdown-menu{
margin-top: -2px;
}

nav.navbar.awesomenav ul.nav.navbar-right .dropdown-menu .dropdown-menu{
/*left: -200px;*/
}

nav.navbar.awesomenav ul.nav > li > a{
padding: 0px 0px;
font-weight: 500;
text-transform: uppercase;
}
nav.navbar.awesomenav ul.nav > li{
padding: 30px 15px;
}
nav.navbar.awesomenav ul.nav > li.dropdown > a.dropdown-toggle:after{
font-family: 'FontAwesome';
content: "\f0d7";
margin-left: 5px;
margin-top: 2px;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu{
-moz-box-shadow: 0px 0px 0px;
-webkit-box-shadow: 0px 0px 0px;
-o-box-shadow: 0px 0px 0px;
box-shadow: 0px 0px 0px;
-moz-border-radius: 0px;
-webkit-border-radius: 0px;
-o-border-radius: 0px;
border-radius: 0px;
padding: 0;
width: 200px;
background: #fff;
border: solid 1px #e0e0e0;
/*border-top: solid 5px;*/
overflow: auto;
max-height: 78vh;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu > li a:hover,
nav.navbar.awesomenav li.dropdown ul.dropdown-menu > li a:hover{
background-color: transparent;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu > li > a{
padding: 10px 15px;
border-bottom: solid 1px #eee;
color: #6f6f6f;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu > li:last-child > a{
border-bottom: none;
}

nav.navbar.awesomenav ul.navbar-right li.dropdown ul.dropdown-menu li a{
text-align: right;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu li.dropdown > a.dropdown-toggle:before{
font-family: 'FontAwesome';
float: right;
content: "\f105";
margin-top: 0;
}

nav.navbar.awesomenav ul.navbar-right li.dropdown ul.dropdown-menu li.dropdown > a.dropdown-toggle:before{
font-family: 'FontAwesome';
float: right;
content: "\f105";
margin-top: 0;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu ul.dropdown-menu{
top: -3px;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content{
padding: 0 15px !important;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content > li{
padding: 25px 0 20px;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content.tabbed{
padding: 0;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content.tabbed > li{
padding: 0;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .col-menu{
padding: 0 30px;
margin: 0 -0.5px;
border-left: solid 1px #f0f0f0;
border-right: solid 1px #f0f0f0;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .col-menu:first-child{
border-left: none;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .col-menu:last-child{
border-right: none;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .content{
display: none;
}

nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .content ul.menu-col li a{
text-align: left;
padding: 5px 0;
display: block;
width: 100%;
margin-bottom: 0;
border-bottom: none;
color: #6f6f6f;
}

nav.navbar.awesomenav.on ul.dropdown-menu.megamenu-content .content{
display: block !important;
height: auto !important;
}

/* Navbar Transparent
=================================*/
nav.navbar.awesomenav.no-background{
background-color: transparent;
border: none;
}

nav.navbar.awesomenav.navbar-transparent .attr-nav{
padding-left: 15px;
margin-left: 30px;
}

nav.navbar.awesomenav.navbar-transparent.white{
background-color: rgba(255,255,255,0.3);
border-bottom: solid 1px #bbb;
}

nav.navbar.navbar-inverse.awesomenav.navbar-transparent.dark,
nav.navbar.awesomenav.navbar-transparent.dark{
background-color: rgba(0,0,0,0.3);
border-bottom: solid 1px #555;
}

nav.navbar.awesomenav.navbar-transparent.white .attr-nav{
border-left: solid 1px #bbb;
}

nav.navbar.navbar-inverse.awesomenav.navbar-transparent.dark .attr-nav,
nav.navbar.awesomenav.navbar-transparent.dark .attr-nav{
border-left: solid 1px #555;
}

nav.navbar.awesomenav.no-background.white .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.white .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.white ul.nav > li > a,
nav.navbar.awesomenav.no-background.white ul.nav > li > a{
color: #fff;
}

nav.navbar.awesomenav.navbar-transparent.dark .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.dark ul.nav > li > a{
color: #eee;
}

nav.navbar.awesomenav.navbar-fixed.navbar-transparent .logo-scrolled,
nav.navbar.awesomenav.navbar-fixed.no-background .logo-scrolled{
display: none;
}

nav.navbar.awesomenav.navbar-fixed.navbar-transparent .logo-display,
nav.navbar.awesomenav.navbar-fixed.no-background .logo-display{
display: block;
}

nav.navbar.awesomenav.navbar-fixed .logo-display{
display: none;
}

nav.navbar.awesomenav.navbar-fixed .logo-scrolled{
display: block;
}

/* Atribute Navigation
=================================*/
.attr-nav > ul > li.dropdown ul.dropdown-menu{
margin-top: 0;
margin-left: 55px;
width: 250px;
left: -250px;
}

/* Menu Center
=================================*/
nav.navbar.awesomenav.menu-center .container{
position: relative;
}

nav.navbar.awesomenav.menu-center ul.nav.navbar-center{
float:none;
margin: 0 auto;
display: table;
table-layout: fixed;
}

nav.navbar.awesomenav.menu-center .navbar-header,
nav.navbar.awesomenav.menu-center .attr-nav{
position: absolute;
}

nav.navbar.awesomenav.menu-center .attr-nav{
right: 15px;
}

/* Navbar Brand top
=================================*/
nav.awesomenav.navbar-brand-top .navbar-header{
display: block;
width: 100%;
text-align: center;
}

nav.awesomenav.navbar-brand-top ul.nav > li.dropdown > ul.dropdown-menu{
margin-top: 2px;
}

nav.awesomenav.navbar-brand-top ul.nav > li.dropdown.megamenu-fw > ul.dropdown-menu{
margin-top: 0;
}

nav.awesomenav.navbar-brand-top .navbar-header .navbar-brand{
display: inline-block;
float: none;
margin: 0;
}

nav.awesomenav.navbar-brand-top .navbar-collapse{
text-align: center;
}

nav.awesomenav.navbar-brand-top ul.nav{
display: inline-block;
float: none;
margin: 0 0 -5px 0;
}

/* Navbar Center
=================================*/
nav.awesomenav.brand-center .navbar-header{
display: block;
width: 100%;
position: absolute;
text-align: center;
top: 0;
left: 0;
}

nav.awesomenav.brand-center .navbar-brand{
display: inline-block;
float: none;
}

nav.awesomenav.brand-center .navbar-collapse{
text-align: center;
display: inline-block;
padding-left: 0;
padding-right: 0;
}

nav.awesomenav.brand-center ul.nav > li.dropdown > ul.dropdown-menu{
margin-top: 2px;
}

nav.awesomenav.brand-center ul.nav > li.dropdown.megamenu-fw > ul.dropdown-menu{
margin-top: 0;
}

nav.awesomenav.brand-center .navbar-collapse .col-half{
width: 50%;
float: left;
display: block;
}

nav.awesomenav.brand-center .navbar-collapse .col-half.left{
text-align: right;
padding-right: 100px;
}

nav.awesomenav.brand-center .navbar-collapse .col-half.right{
text-align: left;
padding-left: 100px;
}

nav.awesomenav.brand-center ul.nav{
float: none !important;
margin-bottom: -5px !important;
display: inline-block !important;
}

nav.awesomenav.brand-center ul.nav.navbar-right{
margin: 0;
}

nav.awesomenav.brand-center.center-side .navbar-collapse .col-half.left{
text-align: left;
padding-right: 100px;
}

nav.awesomenav.brand-center.center-side .navbar-collapse .col-half.right{
text-align: right;
padding-left: 100px;
}

/* Navbar Sidebar
=================================*/
body.wrap-nav-sidebar .wrapper{
padding-left: 260px;
overflow-x: hidden;
}

nav.awesomenav.navbar-sidebar{
position: fixed;
width: 260px;
overflow: hidden;
left: 0;
padding: 0  0 0 0 !important;
background: #fff;
border-right: solid 1px #dfdfdf;
}

nav.awesomenav.navbar-sidebar .scroller{
width: 280px;
overflow-y:auto;
overflow-x: hidden;
}

nav.awesomenav.navbar-sidebar .container-fluid,
nav.awesomenav.navbar-sidebar .container{
padding: 0 !important;
}

nav.awesomenav.navbar-sidebar .navbar-header{
float: none;
display: block;
width: 260px;
padding: 10px 15px;
margin: 10px 0 0 0 !important;
}

nav.awesomenav.navbar-sidebar .navbar-collapse{
padding: 0 !important;
width: 260px;
}

nav.awesomenav.navbar-sidebar ul.nav{
float: none;
display: block;
width: 100%;
padding: 0 15px !important;
margin: 0 0 30px 0;
}

nav.awesomenav.navbar-sidebar ul.nav li{
float: none !important;
}

nav.awesomenav.navbar-sidebar ul.nav > li > a{
padding: 10px 15px;
font-weight: bold;
}

nav.awesomenav.navbar-sidebar ul.nav > li.dropdown > a:after{
float: right;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu{
left: 100%;
top: 0;
position: relative !important;
left: 0 !important;
width: 100% !important;
height: auto !important;
background-color: transparent;
border: none !important;
padding: 0;
-moz-box-shadow: 0px 0px 0px;
-webkit-box-shadow: 0px 0px 0px;
-o-box-shadow: 0px 0px 0px;
box-shadow: 0px 0px 0px;
}

nav.awesomenav.navbar-sidebar ul.nav .megamenu-content .col-menu{
border: none !important;
}

nav.awesomenav.navbar-sidebar ul.nav > li.dropdown > ul.dropdown-menu{
margin-bottom: 15px;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu{
padding-left: 0;
float: none;
margin-bottom: 0;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu li a{
padding:  5px 15px;
color: #6f6f6f;
border: none;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu ul.dropdown-menu{
padding-left: 15px;
margin-top: 0;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu li.dropdown > a:before{
font-family: 'FontAwesome';
content: "\f105";
float: right;
}

nav.awesomenav.navbar-sidebar ul.nav li.dropdown.on ul.dropdown-menu li.dropdown.on > a:before{
content: "\f107";
}

nav.awesomenav.navbar-sidebar ul.dropdown-menu.megamenu-content > li{
padding: 0 !important;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu{
display: block;
float: none !important;
padding: 0;
margin: 0;
width: 100%;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu .title{
padding: 7px 0;
text-transform: none;
font-weight: 400;
letter-spacing: 0px;
margin-bottom: 0;
cursor: pointer;
color: #6f6f6f;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu .title:before{
font-family: 'FontAwesome';
content: "\f105";
float: right;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu.on .title:before{
content: "\f107";
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu{
border: none;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu .content{
padding: 0 0 0 15px;
}

nav.awesomenav.navbar-sidebar .dropdown .megamenu-content .col-menu ul.menu-col li a{
padding: 3px 0 !important;
}
}

@media (max-width: 992px) {
/* Navbar Responsive
=================================*/
nav.navbar.awesomenav .navbar-brand    {
display: inline-block;
float: none !important;
margin: 0 !important;
}

nav.navbar.awesomenav .navbar-header {
float: none;
display: block;
text-align: center;
padding-left: 30px;
padding-right: 30px;
}

nav.navbar.awesomenav .navbar-toggle {
display: inline-block;
float: left;
margin-right: -200px;
margin-top: 4px;
}

nav.navbar.awesomenav .navbar-collapse {
border: none;
margin-bottom: 0;
}

nav.navbar.awesomenav.no-full .navbar-collapse{
max-height: 350px;
overflow-y: auto !important;
}

nav.navbar.awesomenav .navbar-collapse.collapse {
display: none !important;
}

nav.navbar.awesomenav .navbar-collapse.collapse.in {
display: block !important;
}

nav.navbar.awesomenav .navbar-nav {
float: none !important;
padding-left: 30px;
padding-right: 30px;
margin: 0px -15px;
}

nav.navbar.awesomenav .navbar-nav > li {
float: none;
}

nav.navbar.awesomenav li.dropdown a.dropdown-toggle:before{
font-family: 'FontAwesome';
content: "\f105";
float: right;
font-size: 16px;
margin-left: 10px;
}

nav.navbar.awesomenav li.dropdown.on > a.dropdown-toggle:before{
content: "\f107";
}

nav.navbar.awesomenav .navbar-nav > li > a{
display: block;
width: 100%;
border-bottom: solid 1px #e0e0e0;
padding: 10px 10px;
border-top: solid 1px #e0e0e0;
margin-bottom: -1px;
text-transform: uppercase;
}

nav.navbar.awesomenav .navbar-nav > li:first-child > a{
border-top: none;
}

nav.navbar.awesomenav ul.navbar-nav.navbar-left > li:last-child > ul.dropdown-menu{
border-bottom: solid 1px #e0e0e0;
}

nav.navbar.awesomenav ul.nav li.dropdown li a.dropdown-toggle{
float: none !important;
position: relative;
display: block;
width: 100%;
}

nav.navbar.awesomenav ul.nav li.dropdown ul.dropdown-menu{
width: 100%;
position: relative !important;
background-color: transparent;
float: none;
border: none;
padding: 0 0 0 15px !important;
margin: 0 0 -1px 0 !important;
-moz-box-shadow: 0px 0px 0px;
-webkit-box-shadow: 0px 0px 0px;
-o-box-shadow: 0px 0px 0px;
box-shadow: 0px 0px 0px;
-moz-border-radius: 0px 0px 0px;
-webkit-border-radius: 0px 0px 0px;
-o-border-radius: 0px 0px 0px;
border-radius: 0px 0px 0px;
}

nav.navbar.awesomenav ul.nav li.dropdown ul.dropdown-menu  > li > a{
display: block;
width: 100%;
border-bottom: solid 1px #e0e0e0;
padding: 10px 0;
color: #6f6f6f;
}

nav.navbar.awesomenav ul.nav ul.dropdown-menu li a:hover,
nav.navbar.awesomenav ul.nav ul.dropdown-menu li a:focus{
background-color: transparent;
}

nav.navbar.awesomenav ul.nav ul.dropdown-menu ul.dropdown-menu{
float: none !important;
left: 0;
padding: 0 0 0 15px;
position: relative;
background: transparent;
width: 100%;
}

nav.navbar.awesomenav ul.nav ul.dropdown-menu li.dropdown.on > ul.dropdown-menu{
display: inline-block;
margin-top: -10px;
}

nav.navbar.awesomenav li.dropdown ul.dropdown-menu li.dropdown > a.dropdown-toggle:after{
display: none;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu .title{
padding: 10px 15px 10px 0;
line-height: 24px;
text-transform: none;
font-weight: 400;
letter-spacing: 0px;
margin-bottom: 0;
cursor: pointer;
border-bottom: solid 1px #e0e0e0;
color: #6f6f6f;
}

.go-sale {
    display: block;
    width: 100%;
    border-bottom: solid 1px #e0e0e0;
    padding: 8px 0;  
    line-height: 24px;
    text-transform: none;
    font-weight: 400;
    letter-spacing: 0px;
    margin-bottom: 0;
    cursor: pointer;
    border-bottom: solid 1px #e0e0e0;
    color: #6f6f6f;
}

.go-sale h6 {
    color: #6f6f6f;
    padding: 0 !important;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu ul > li > a{
display: block;
width: 100%;
border-bottom: solid 1px #e0e0e0;
padding: 8px 0;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu .title:before{
font-family: 'FontAwesome';
content: "\f105";
float: right;
font-size: 16px;
margin-left: 10px;
position: relative;
right: -15px;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu:last-child .title{
border-bottom: none;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu.on:last-child .title{
border-bottom: solid 1px #e0e0e0;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu:last-child ul.menu-col li:last-child a{
border-bottom: none;
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu.on .title:before{
content: "\f107";
}

nav.navbar.awesomenav .dropdown .megamenu-content .col-menu .content{
padding: 0 0 0 15px;
}

nav.awesomenav.brand-center .navbar-collapse{
display: block;
}

nav.awesomenav.brand-center ul.nav{
margin-bottom: 0px !important;
}

nav.awesomenav.brand-center .navbar-collapse .col-half{
width: 100%;
float: none;
display: block;
}

nav.awesomenav.brand-center .navbar-collapse .col-half.left{
margin-bottom: 0;
}

nav.awesomenav .megamenu-content{
padding: 0;
}

nav.awesomenav .megamenu-content .col-menu{
padding-bottom: 0;
}

nav.awesomenav .megamenu-content .title{
cursor: pointer;
display: block;
padding: 10px 15px;
margin-bottom: 0;
font-weight: normal;
}

nav.awesomenav .megamenu-content .content{
display: none;
}

.attr-nav{
position: absolute;
right: 60px;
}

.attr-nav > ul{
padding: 0;
margin: 0 -15px -7px 0;
}

.attr-nav > ul > li > a{
padding: 16px 15px 15px;
}

.attr-nav > ul > li.dropdown > a.dropdown-toggle:before{
display: none;
}

.attr-nav > ul > li.dropdown ul.dropdown-menu{
margin-top: 2px;
margin-left: 55px;
width: 250px;
left: -250px;
/*border-top: solid 5px;*/
}

.top-search .container{
padding: 0 45px;
}

/* Navbar full Responsive
=================================*/
nav.awesomenav.navbar-full ul.nav{
margin-left: 0;
}

nav.awesomenav.navbar-full ul.nav > li > a{
border: none;
}

nav.awesomenav.navbar-full .navbar-brand    {
float: left !important;
padding-left: 0;
}

nav.awesomenav.navbar-full .navbar-toggle {
display: inline-block;
float: right;
margin-right: 0;
margin-top: 10px;
}

nav.awesomenav.navbar-full .navbar-header {
padding-left: 15px;
padding-right: 15px;
}

/* Navbar Sidebar
=================================*/
nav.navbar.awesomenav.navbar-sidebar .share{
padding: 30px 15px;
margin-bottom: 0;
}

/* Tabs
=================================*/
nav.navbar.awesomenav .megamenu-content.tabbed{
padding-left: 0 !mportant;
}

nav.navbar.awesomenav .tabbed > li{
padding: 25px 0;
margin-left: -15px !important;
}

/* Mobile Navigation
=================================*/
body > .wrapper{
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}

body.side-right > .wrapper{
margin-left: 280px;
margin-right: -280px !important;
}

nav.navbar.awesomenav.navbar-mobile .navbar-collapse{
position: fixed;
overflow-y: auto !important;
overflow-x: hidden !important;
display: block;
background: #fff;
z-index: 99;
width: 280px;
height: 100% !important;
left: -280px;
top: 0;
padding: 0;
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s ease-in-out;
-ms-transition: all 0.3s ease-in-out;
transition: all 0.3s ease-in-out;
}

nav.navbar.awesomenav.navbar-mobile .navbar-collapse.in{
left: 0;
}

nav.navbar.awesomenav.navbar-mobile ul.nav{

width: 293px;
padding-right: 0;
padding-left: 15px;
}

nav.navbar.awesomenav.navbar-mobile ul.nav > li > a{
padding: 15px 15px;
}

nav.navbar.awesomenav.navbar-mobile ul.nav ul.dropdown-menu > li > a{
padding-right: 15px !important;
padding-top: 15px !important;
padding-bottom: 15px !important;
}

nav.navbar.awesomenav.navbar-mobile ul.nav ul.dropdown-menu .col-menu .title{
padding-right: 30px !important;
padding-top: 13px !important;
padding-bottom: 13px !important;
}

nav.navbar.awesomenav.navbar-mobile ul.nav ul.dropdown-menu .col-menu ul.menu-col li a{
padding-top: 13px !important;
padding-bottom: 13px !important;
}

nav.navbar.awesomenav.navbar-mobile .navbar-collapse [class*=' col-'] {
width: 100%;
}

nav.navbar.awesomenav.navbar-fixed .logo-scrolled{
display: block !important;
}

nav.navbar.awesomenav.navbar-fixed .logo-display{
display: none !important;
}

nav.navbar.awesomenav.navbar-mobile .tab-menu,
nav.navbar.awesomenav.navbar-mobile .tab-content{
width: 100%;
display: block;
}
}

@media (max-width: 767px) {
nav.navbar.awesomenav .navbar-header {
padding-left: 15px;
padding-right: 15px;
}

nav.navbar.awesomenav .navbar-nav {
padding-left: 15px;
padding-right: 15px;
}

.attr-nav{
right: 30px;
}

.attr-nav > ul{
margin-right: -10px;
}

.attr-nav > ul > li > a{
padding: 16px 10px 15px;
padding-left: 0 !important;
}

.attr-nav > ul > li.dropdown ul.dropdown-menu{
left: -275px;
}

.top-search .container{
padding: 0 15px;
}

nav.awesomenav.navbar-full .navbar-collapse{
left: 15px;
}

nav.awesomenav.navbar-full .navbar-header{
padding-right: 0;
}

nav.awesomenav.navbar-full .navbar-toggle {
margin-right: -15px;
}

nav.awesomenav.navbar-full ul.nav > li > a{
font-size: 18px !important;
line-height: 24px !important;
padding: 5px 10px !important;
}

/* Navbar Sidebar
=================================*/
nav.navbar.awesomenav.navbar-sidebar .share{
padding: 30px 15px !important;
}

/* Navbar Sidebar
=================================*/
nav.navbar.awesomenav.navbar-sidebar .share{
padding: 30px 0 !important;
margin-bottom: 0;
}

nav.navbar.awesomenav.navbar-mobile.navbar-sidebar .share{
padding: 30px 15px !important;
margin-bottom: 0;
}

/* Mobile Navigation
=================================*/
body.side-right > .wrapper{
margin-left: 280px;
margin-right: -280px !important;
}

nav.navbar.awesomenav.navbar-mobile .navbar-collapse{
margin-left: 0;
}

nav.navbar.awesomenav.navbar-mobile ul.nav{
margin-left: -15px;
}

nav.navbar.awesomenav.navbar-mobile ul.nav{
border-top: solid 1px #fff;
}

li.close-full-menu{
padding-top: 15px !important;
padding-bottom: 15px !important;
}
}

@media (min-width: 480px) and (max-width: 640px) {
nav.awesomenav.navbar-full ul.nav{
padding-top: 30px;
padding-bottom: 30px;
}
}



/*--------"awesomenav-style.css"--------*/

/*------------------------------------------------------------------
[Nav Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Last change:	01/06/2017
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/

/*Nav style css here*/


/* Navbar Atribute ------*/
.attr-nav > ul > li > a{
/*padding: 31px 15px;*/
}

ul.cart-list > li.total > .btn{
border-bottom: solid 1px #cfcfcf !important;
color: #fff !important;
padding: 10px 15px;
}

@media (min-width: 1024px) {
/* Navbar General ------*/
nav.navbar ul.nav > li > a{
padding: 30px 15px;
font-weight: 600;
}

nav.navbar .navbar-brand{
margin-top: 0;
}

/*nav.navbar .navbar-brand img.logo{
width: 50px;
}*/

nav.navbar .navbar-brand{
margin-top: 0;
}

/*nav.navbar .navbar-brand img.logo{
width: 50px;
}*/

nav.navbar li.dropdown ul.dropdown-menu{
border-top: solid 5px;
}

/* Navbar Center ------*/
nav.navbar-center .navbar-brand{
margin: 0 !important;
}

/* Navbar Brand Top ------*/
nav.navbar-brand-top .navbar-brand{
margin: 10px !important;
}

/* Navbar Full ------*/
nav.navbar-full .navbar-brand{
position: relative;
top: -15px;
}

/* Navbar Sidebar ------*/
nav.navbar-sidebar ul.nav,
nav.navbar-sidebar .navbar-brand{
margin-bottom: 50px;
}

nav.navbar-sidebar ul.nav > li > a{
padding: 10px 15px;
font-weight: bold;
}

/* Navbar Transparent & Fixed ------*/
nav.navbar.awesomenav.navbar-transparent.white{
background-color: rgba(255,255,255,0.3);
border-bottom: solid 1px #bbb;
}

nav.navbar.navbar-inverse.awesomenav.navbar-transparent.dark,
nav.navbar.awesomenav.navbar-transparent.dark{
background-color: rgba(0,0,0,0.3);
border-bottom: solid 1px #555;
}

nav.navbar.awesomenav.navbar-transparent.white .attr-nav{
border-left: solid 1px #bbb;
}

nav.navbar.navbar-inverse.awesomenav.navbar-transparent.dark .attr-nav,
nav.navbar.awesomenav.navbar-transparent.dark .attr-nav{
border-left: solid 1px #555;
}

nav.navbar.awesomenav.no-background.white .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.white .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.white ul.nav > li > a,
nav.navbar.awesomenav.no-background.white ul.nav > li > a{
color: #fff;
}

nav.navbar.awesomenav.navbar-transparent.dark .attr-nav > ul > li > a,
nav.navbar.awesomenav.navbar-transparent.dark ul.nav > li > a{
color: #eee;
}
}

@media (max-width: 992px) {
/* Navbar General ------*/
nav.navbar .navbar-brand{
margin-top: 0;
position: relative;
top: -10px;
}

/*nav.navbar .navbar-brand img.logo{
width: 30px;
}*/

.attr-nav > ul > li > a{
padding: 16px 15px 15px;
}

/* Navbar Mobile slide ------*/
nav.navbar.navbar-mobile ul.nav > li > a{
padding: 15px 15px;
}

nav.navbar.navbar-mobile ul.nav ul.dropdown-menu > li > a{
padding-right: 15px !important;
padding-top: 15px !important;
padding-bottom: 15px !important;
}

nav.navbar.navbar-mobile ul.nav ul.dropdown-menu .col-menu .title{
padding-right: 30px !important;
padding-top: 13px !important;
padding-bottom: 13px !important;
}

nav.navbar.navbar-mobile ul.nav ul.dropdown-menu .col-menu ul.menu-col li a{
padding-top: 13px !important;
padding-bottom: 13px !important;
}

/* Navbar Full ------*/
nav.navbar-full .navbar-brand{
top: 0;
padding-top: 10px;
}
}

/* Navbar Inverse
=================================*/
nav.navbar.navbar-inverse{
background-color: #222;
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse ul.cart-list > li.total > .btn{
border-bottom: solid 1px #222 !important;
}

nav.navbar.navbar-inverse ul.cart-list > li.total .pull-right{
color: #fff;
}

nav.navbar.navbar-inverse.megamenu ul.dropdown-menu.megamenu-content .content ul.menu-col li a,
nav.navbar.navbar-inverse ul.nav > li > a{
color: #eee;
}

nav.navbar.navbar-inverse ul.nav > li.dropdown > a{
background-color: #222;
}

nav.navbar.navbar-inverse li.dropdown ul.dropdown-menu > li > a{
color: #999;
}

nav.navbar.navbar-inverse ul.nav .dropdown-menu h1,
nav.navbar.navbar-inverse ul.nav .dropdown-menu h2,
nav.navbar.navbar-inverse ul.nav .dropdown-menu h3,
nav.navbar.navbar-inverse ul.nav .dropdown-menu h4,
nav.navbar.navbar-inverse ul.nav .dropdown-menu h5,
nav.navbar.navbar-inverse ul.nav .dropdown-menu h6{
color: #fff;
}

nav.navbar.navbar-inverse .form-control{
background-color: #333;
border-color: #303030;
color: #fff;
}

nav.navbar.navbar-inverse .attr-nav > ul > li > a{
color: #eee;
}

nav.navbar.navbar-inverse .attr-nav > ul > li.dropdown ul.dropdown-menu{
background-color: #222;
border-left: solid 1px #303030;
border-bottom: solid 1px #303030;
border-right: solid 1px #303030;
}

nav.navbar.navbar-inverse ul.cart-list > li{
border-bottom: solid 1px #303030;
color: #eee;
}

nav.navbar.navbar-inverse ul.cart-list > li img{
border: solid 1px #303030;
}

nav.navbar.navbar-inverse ul.cart-list > li.total{
background-color: #333;
}

nav.navbar.navbar-inverse .share ul > li > a{
background-color: #555;
}

nav.navbar.navbar-inverse .dropdown-tabs .tab-menu{
border-right: solid 1px #303030;
}

nav.navbar.navbar-inverse .dropdown-tabs .tab-menu > ul > li > a{
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse .dropdown-tabs .tab-content{
border-left: solid 1px #303030;
}

nav.navbar.navbar-inverse .dropdown-tabs .tab-menu > ul > li > a:hover,
nav.navbar.navbar-inverse .dropdown-tabs .tab-menu > ul > li > a:focus,
nav.navbar.navbar-inverse .dropdown-tabs .tab-menu > ul > li.active > a{
background-color: #333 !important;
}

nav.navbar-inverse.navbar-full ul.nav > li > a{
border:none;
}

nav.navbar-inverse.navbar-full .navbar-collapse .wrap-full-menu{
background-color: #222;
}

nav.navbar-inverse.navbar-full .navbar-toggle{
background-color: #222 !important;
color: #6f6f6f;
}

@media (min-width: 1024px) {
nav.navbar.navbar-inverse ul.nav .dropdown-menu{
background-color: #222 !important;
border-left: solid 1px #303030 !important;
border-bottom: solid 1px #303030 !important;
border-right: solid 1px #303030 !important;
}

nav.navbar.navbar-inverse li.dropdown ul.dropdown-menu > li > a{
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse ul.dropdown-menu.megamenu-content .col-menu{
border-left: solid 1px #303030;
border-right: solid 1px #303030;
}

nav.navbar.navbar-inverse.navbar-transparent.dark{
background-color: rgba(0,0,0,0.3);
border-bottom: solid 1px #999;
}

nav.navbar.navbar-inverse.navbar-transparent.dark .attr-nav{
border-left: solid 1px #999;
}

nav.navbar.navbar-inverse.no-background.white .attr-nav > ul > li > a,
nav.navbar.navbar-inverse.navbar-transparent.dark .attr-nav > ul > li > a,
nav.navbar.navbar-inverse.navbar-transparent.dark ul.nav > li > a,
nav.navbar.navbar-inverse.no-background.white ul.nav > li > a{
color: #fff;
}

nav.navbar.navbar-inverse.no-background.dark .attr-nav > ul > li > a,
nav.navbar.navbar-inverse.no-background.dark .attr-nav > ul > li > a,
nav.navbar.navbar-inverse.no-background.dark ul.nav > li > a,
nav.navbar.navbar-inverse.no-background.dark ul.nav > li > a{
color: #3f3f3f;
}
}
@media (max-width: 992px)  {
nav.navbar.navbar-inverse .navbar-toggle{
color: #eee;
background-color: #222 !important;
}

nav.navbar.navbar-inverse .navbar-nav > li > a{
border-top: solid 1px #303030;
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse ul.nav li.dropdown ul.dropdown-menu  > li > a{
color: #999;
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse .dropdown .megamenu-content .col-menu .title{
border-bottom: solid 1px #303030;
color: #eee;
}

nav.navbar.navbar-inverse .dropdown .megamenu-content .col-menu ul > li > a{
border-bottom: solid 1px #303030;
color: #999 !important;
}

nav.navbar.navbar-inverse .dropdown .megamenu-content .col-menu.on:last-child .title{
border-bottom: solid 1px #303030;
}

nav.navbar.navbar-inverse .dropdown-tabs .tab-menu > ul{
border-top: solid 1px #303030;
}

nav.navbar.navbar-inverse.navbar-mobile .navbar-collapse{
background-color: #222;
}
}

@media (max-width: 767px)  {
nav.navbar.navbar-inverse.navbar-mobile ul.nav{
border-top: solid 1px #222;
}
}

/*
Color
=========================== */
nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .content ul.menu-col li a:hover,
.side .widget ul.link li a:hover,
.side .widget ul.link li a:focus,
.check-list li:before,
ul.cart-list > li > h6 > a,
.attr-nav > ul > li > a:hover,
.attr-nav > ul > li > a:focus,
nav.navbar-sidebar ul.nav li.dropdown.on > a,
nav.navbar-sidebar .dropdown .megamenu-content .col-menu.on .title,
nav.navbar-sidebar ul.nav li.dropdown ul.dropdown-menu li a:hover,
nav.navbar ul.nav li.dropdown.on > a,
nav.navbar.navbar-inverse ul.nav li.dropdown.on > a,
nav.navbar-sidebar ul.nav li.dropdown.on ul.dropdown-menu li.dropdown.on > a,
nav.navbar .dropdown .megamenu-content .col-menu.on .title,
nav.navbar ul.nav > li > a:hover,
nav.navbar ul.nav > li.active > a:hover,
nav.navbar ul.nav li.active > a,
nav.navbar li.dropdown ul.dropdown-menu > li a:hover{
color: {!! $color !!};
background-color: transparent;
border-bottom: 1px solid {!! $color !!};
}
.attr-nav > ul > li > a:hover, .attr-nav > ul > li > a:focus {
border-bottom: none;
}
nav.navbar.navbar-transparent ul.nav > li > a:hover,
nav.navbar.no-background ul.nav > li > a:hover,
nav.navbar ul.nav li.scroll.active > a,
nav.navbar.navbar-dark ul.nav li.dropdown ul.dropdown-menu  > li > a:hover,
nav.navbar ul.nav li.dropdown.on > a,
nav.navbar-dark ul.nav li.dropdown.on > a{
color: {!! $color !!} !important;
background-color: transparent;
}
nav.navbar ul.nav li.scroll.active > a{
color: #fff !important;
background-color: {!! $color !!};
}
nav.navbar.no-background ul.nav li.scroll.active > a{
color: {!! $color !!} !important;
background-color: transparent;
}
@media(max-width:920px){
nav.navbar .dropdown .megamenu-content .col-menu ul > li > a:hover,
nav.navbar.navbar-dark .dropdown .megamenu-content .col-menu .title:hover{
color: {!! $color !!} !important;
}
}

/*
Border
=========================== */
ul.cart-list > li.total > .btn{
border-color: {!! $color !!};
}

nav.navbar li.dropdown ul.dropdown-menu{
border-top-color: {!! $color !!} !important;
}

/*
Background
=========================== */
ul.cart-list > li.total > .btn,
.attr-nav > ul > li > a span.badge,
nav.navbar .share ul > li > a:hover,
nav.navbar .share ul > li > a:focus{
background-color: {!! $color !!};
}

ul.cart-list > li.total > .btn:hover,
ul.cart-list > li.total > .btn:focus{
background-color: #333333!important;
}



/*---------"awesomenav-animate.css"---------*/

@charset "UTF-8";

/*!
Animate.css - http://daneden.me/animate
Licensed under the MIT license

Copyright (c) 2013 Daniel Eden

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

.animated {
-webkit-animation-duration: 1s;
animation-duration: 1s;
-webkit-animation-fill-mode: both;
animation-fill-mode: both;
}

.animated.infinite {
-webkit-animation-iteration-count: infinite;
animation-iteration-count: infinite;
}

.animated.hinge {
-webkit-animation-duration: 2s;
animation-duration: 2s;
}

@-webkit-keyframes bounce {
0%, 20%, 50%, 80%, 100% {
-webkit-transform: translateY(0);
transform: translateY(0);
}

40% {
-webkit-transform: translateY(-30px);
transform: translateY(-30px);
}

60% {
-webkit-transform: translateY(-15px);
transform: translateY(-15px);
}
}

@keyframes bounce {
0%, 20%, 50%, 80%, 100% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

40% {
-webkit-transform: translateY(-30px);
-ms-transform: translateY(-30px);
transform: translateY(-30px);
}

60% {
-webkit-transform: translateY(-15px);
-ms-transform: translateY(-15px);
transform: translateY(-15px);
}
}

.bounce {
-webkit-animation-name: bounce;
animation-name: bounce;
}

@-webkit-keyframes flash {
0%, 50%, 100% {
opacity: 1;
}

25%, 75% {
opacity: 0;
}
}

@keyframes flash {
0%, 50%, 100% {
opacity: 1;
}

25%, 75% {
opacity: 0;
}
}

.flash {
-webkit-animation-name: flash;
animation-name: flash;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes pulse {
0% {
-webkit-transform: scale(1);
transform: scale(1);
}

50% {
-webkit-transform: scale(1.1);
transform: scale(1.1);
}

100% {
-webkit-transform: scale(1);
transform: scale(1);
}
}

@keyframes pulse {
0% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}

50% {
-webkit-transform: scale(1.1);
-ms-transform: scale(1.1);
transform: scale(1.1);
}

100% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}
}

.pulse {
-webkit-animation-name: pulse;
animation-name: pulse;
}

@-webkit-keyframes rubberBand {
0% {
-webkit-transform: scale(1);
transform: scale(1);
}

30% {
-webkit-transform: scaleX(1.25) scaleY(0.75);
transform: scaleX(1.25) scaleY(0.75);
}

40% {
-webkit-transform: scaleX(0.75) scaleY(1.25);
transform: scaleX(0.75) scaleY(1.25);
}

60% {
-webkit-transform: scaleX(1.15) scaleY(0.85);
transform: scaleX(1.15) scaleY(0.85);
}

100% {
-webkit-transform: scale(1);
transform: scale(1);
}
}

@keyframes rubberBand {
0% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}

30% {
-webkit-transform: scaleX(1.25) scaleY(0.75);
-ms-transform: scaleX(1.25) scaleY(0.75);
transform: scaleX(1.25) scaleY(0.75);
}

40% {
-webkit-transform: scaleX(0.75) scaleY(1.25);
-ms-transform: scaleX(0.75) scaleY(1.25);
transform: scaleX(0.75) scaleY(1.25);
}

60% {
-webkit-transform: scaleX(1.15) scaleY(0.85);
-ms-transform: scaleX(1.15) scaleY(0.85);
transform: scaleX(1.15) scaleY(0.85);
}

100% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}
}

.rubberBand {
-webkit-animation-name: rubberBand;
animation-name: rubberBand;
}

@-webkit-keyframes shake {
0%, 100% {
-webkit-transform: translateX(0);
transform: translateX(0);
}

10%, 30%, 50%, 70%, 90% {
-webkit-transform: translateX(-10px);
transform: translateX(-10px);
}

20%, 40%, 60%, 80% {
-webkit-transform: translateX(10px);
transform: translateX(10px);
}
}

@keyframes shake {
0%, 100% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

10%, 30%, 50%, 70%, 90% {
-webkit-transform: translateX(-10px);
-ms-transform: translateX(-10px);
transform: translateX(-10px);
}

20%, 40%, 60%, 80% {
-webkit-transform: translateX(10px);
-ms-transform: translateX(10px);
transform: translateX(10px);
}
}

.shake {
-webkit-animation-name: shake;
animation-name: shake;
}

@-webkit-keyframes swing {
20% {
-webkit-transform: rotate(15deg);
transform: rotate(15deg);
}

40% {
-webkit-transform: rotate(-10deg);
transform: rotate(-10deg);
}

60% {
-webkit-transform: rotate(5deg);
transform: rotate(5deg);
}

80% {
-webkit-transform: rotate(-5deg);
transform: rotate(-5deg);
}

100% {
-webkit-transform: rotate(0deg);
transform: rotate(0deg);
}
}

@keyframes swing {
20% {
-webkit-transform: rotate(15deg);
-ms-transform: rotate(15deg);
transform: rotate(15deg);
}

40% {
-webkit-transform: rotate(-10deg);
-ms-transform: rotate(-10deg);
transform: rotate(-10deg);
}

60% {
-webkit-transform: rotate(5deg);
-ms-transform: rotate(5deg);
transform: rotate(5deg);
}

80% {
-webkit-transform: rotate(-5deg);
-ms-transform: rotate(-5deg);
transform: rotate(-5deg);
}

100% {
-webkit-transform: rotate(0deg);
-ms-transform: rotate(0deg);
transform: rotate(0deg);
}
}

.swing {
-webkit-transform-origin: top center;
-ms-transform-origin: top center;
transform-origin: top center;
-webkit-animation-name: swing;
animation-name: swing;
}

@-webkit-keyframes tada {
0% {
-webkit-transform: scale(1);
transform: scale(1);
}

10%, 20% {
-webkit-transform: scale(0.9) rotate(-3deg);
transform: scale(0.9) rotate(-3deg);
}

30%, 50%, 70%, 90% {
-webkit-transform: scale(1.1) rotate(3deg);
transform: scale(1.1) rotate(3deg);
}

40%, 60%, 80% {
-webkit-transform: scale(1.1) rotate(-3deg);
transform: scale(1.1) rotate(-3deg);
}

100% {
-webkit-transform: scale(1) rotate(0);
transform: scale(1) rotate(0);
}
}

@keyframes tada {
0% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}

10%, 20% {
-webkit-transform: scale(0.9) rotate(-3deg);
-ms-transform: scale(0.9) rotate(-3deg);
transform: scale(0.9) rotate(-3deg);
}

30%, 50%, 70%, 90% {
-webkit-transform: scale(1.1) rotate(3deg);
-ms-transform: scale(1.1) rotate(3deg);
transform: scale(1.1) rotate(3deg);
}

40%, 60%, 80% {
-webkit-transform: scale(1.1) rotate(-3deg);
-ms-transform: scale(1.1) rotate(-3deg);
transform: scale(1.1) rotate(-3deg);
}

100% {
-webkit-transform: scale(1) rotate(0);
-ms-transform: scale(1) rotate(0);
transform: scale(1) rotate(0);
}
}

.tada {
-webkit-animation-name: tada;
animation-name: tada;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes wobble {
0% {
-webkit-transform: translateX(0%);
transform: translateX(0%);
}

15% {
-webkit-transform: translateX(-25%) rotate(-5deg);
transform: translateX(-25%) rotate(-5deg);
}

30% {
-webkit-transform: translateX(20%) rotate(3deg);
transform: translateX(20%) rotate(3deg);
}

45% {
-webkit-transform: translateX(-15%) rotate(-3deg);
transform: translateX(-15%) rotate(-3deg);
}

60% {
-webkit-transform: translateX(10%) rotate(2deg);
transform: translateX(10%) rotate(2deg);
}

75% {
-webkit-transform: translateX(-5%) rotate(-1deg);
transform: translateX(-5%) rotate(-1deg);
}

100% {
-webkit-transform: translateX(0%);
transform: translateX(0%);
}
}

@keyframes wobble {
0% {
-webkit-transform: translateX(0%);
-ms-transform: translateX(0%);
transform: translateX(0%);
}

15% {
-webkit-transform: translateX(-25%) rotate(-5deg);
-ms-transform: translateX(-25%) rotate(-5deg);
transform: translateX(-25%) rotate(-5deg);
}

30% {
-webkit-transform: translateX(20%) rotate(3deg);
-ms-transform: translateX(20%) rotate(3deg);
transform: translateX(20%) rotate(3deg);
}

45% {
-webkit-transform: translateX(-15%) rotate(-3deg);
-ms-transform: translateX(-15%) rotate(-3deg);
transform: translateX(-15%) rotate(-3deg);
}

60% {
-webkit-transform: translateX(10%) rotate(2deg);
-ms-transform: translateX(10%) rotate(2deg);
transform: translateX(10%) rotate(2deg);
}

75% {
-webkit-transform: translateX(-5%) rotate(-1deg);
-ms-transform: translateX(-5%) rotate(-1deg);
transform: translateX(-5%) rotate(-1deg);
}

100% {
-webkit-transform: translateX(0%);
-ms-transform: translateX(0%);
transform: translateX(0%);
}
}

.wobble {
-webkit-animation-name: wobble;
animation-name: wobble;
}

@-webkit-keyframes bounceIn {
0% {
opacity: 0;
-webkit-transform: scale(.3);
transform: scale(.3);
}

50% {
opacity: 1;
-webkit-transform: scale(1.05);
transform: scale(1.05);
}

70% {
-webkit-transform: scale(.9);
transform: scale(.9);
}

100% {
opacity: 1;
-webkit-transform: scale(1);
transform: scale(1);
}
}

@keyframes bounceIn {
0% {
opacity: 0;
-webkit-transform: scale(.3);
-ms-transform: scale(.3);
transform: scale(.3);
}

50% {
opacity: 1;
-webkit-transform: scale(1.05);
-ms-transform: scale(1.05);
transform: scale(1.05);
}

70% {
-webkit-transform: scale(.9);
-ms-transform: scale(.9);
transform: scale(.9);
}

100% {
opacity: 1;
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}
}

.bounceIn {
-webkit-animation-name: bounceIn;
animation-name: bounceIn;
}

@-webkit-keyframes bounceInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}

60% {
opacity: 1;
-webkit-transform: translateY(30px);
transform: translateY(30px);
}

80% {
-webkit-transform: translateY(-10px);
transform: translateY(-10px);
}

100% {
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes bounceInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}

60% {
opacity: 1;
-webkit-transform: translateY(30px);
-ms-transform: translateY(30px);
transform: translateY(30px);
}

80% {
-webkit-transform: translateY(-10px);
-ms-transform: translateY(-10px);
transform: translateY(-10px);
}

100% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.bounceInDown {
-webkit-animation-name: bounceInDown;
animation-name: bounceInDown;
}

@-webkit-keyframes bounceInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}

60% {
opacity: 1;
-webkit-transform: translateX(30px);
transform: translateX(30px);
}

80% {
-webkit-transform: translateX(-10px);
transform: translateX(-10px);
}

100% {
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes bounceInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}

60% {
opacity: 1;
-webkit-transform: translateX(30px);
-ms-transform: translateX(30px);
transform: translateX(30px);
}

80% {
-webkit-transform: translateX(-10px);
-ms-transform: translateX(-10px);
transform: translateX(-10px);
}

100% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.bounceInLeft {
-webkit-animation-name: bounceInLeft;
animation-name: bounceInLeft;
}

@-webkit-keyframes bounceInRight {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}

60% {
opacity: 1;
-webkit-transform: translateX(-30px);
transform: translateX(-30px);
}

80% {
-webkit-transform: translateX(10px);
transform: translateX(10px);
}

100% {
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes bounceInRight {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}

60% {
opacity: 1;
-webkit-transform: translateX(-30px);
-ms-transform: translateX(-30px);
transform: translateX(-30px);
}

80% {
-webkit-transform: translateX(10px);
-ms-transform: translateX(10px);
transform: translateX(10px);
}

100% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.bounceInRight {
-webkit-animation-name: bounceInRight;
animation-name: bounceInRight;
}

@-webkit-keyframes bounceInUp {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}

60% {
opacity: 1;
-webkit-transform: translateY(-30px);
transform: translateY(-30px);
}

80% {
-webkit-transform: translateY(10px);
transform: translateY(10px);
}

100% {
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes bounceInUp {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}

60% {
opacity: 1;
-webkit-transform: translateY(-30px);
-ms-transform: translateY(-30px);
transform: translateY(-30px);
}

80% {
-webkit-transform: translateY(10px);
-ms-transform: translateY(10px);
transform: translateY(10px);
}

100% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.bounceInUp {
-webkit-animation-name: bounceInUp;
animation-name: bounceInUp;
}

@-webkit-keyframes bounceOut {
0% {
-webkit-transform: scale(1);
transform: scale(1);
}

25% {
-webkit-transform: scale(.95);
transform: scale(.95);
}

50% {
opacity: 1;
-webkit-transform: scale(1.1);
transform: scale(1.1);
}

100% {
opacity: 0;
-webkit-transform: scale(.3);
transform: scale(.3);
}
}

@keyframes bounceOut {
0% {
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}

25% {
-webkit-transform: scale(.95);
-ms-transform: scale(.95);
transform: scale(.95);
}

50% {
opacity: 1;
-webkit-transform: scale(1.1);
-ms-transform: scale(1.1);
transform: scale(1.1);
}

100% {
opacity: 0;
-webkit-transform: scale(.3);
-ms-transform: scale(.3);
transform: scale(.3);
}
}

.bounceOut {
-webkit-animation-name: bounceOut;
animation-name: bounceOut;
}

@-webkit-keyframes bounceOutDown {
0% {
-webkit-transform: translateY(0);
transform: translateY(0);
}

20% {
opacity: 1;
-webkit-transform: translateY(-20px);
transform: translateY(-20px);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}
}

@keyframes bounceOutDown {
0% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

20% {
opacity: 1;
-webkit-transform: translateY(-20px);
-ms-transform: translateY(-20px);
transform: translateY(-20px);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}
}

.bounceOutDown {
-webkit-animation-name: bounceOutDown;
animation-name: bounceOutDown;
}

@-webkit-keyframes bounceOutLeft {
0% {
-webkit-transform: translateX(0);
transform: translateX(0);
}

20% {
opacity: 1;
-webkit-transform: translateX(20px);
transform: translateX(20px);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

@keyframes bounceOutLeft {
0% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

20% {
opacity: 1;
-webkit-transform: translateX(20px);
-ms-transform: translateX(20px);
transform: translateX(20px);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

.bounceOutLeft {
-webkit-animation-name: bounceOutLeft;
animation-name: bounceOutLeft;
}

@-webkit-keyframes bounceOutRight {
0% {
-webkit-transform: translateX(0);
transform: translateX(0);
}

20% {
opacity: 1;
-webkit-transform: translateX(-20px);
transform: translateX(-20px);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}
}

@keyframes bounceOutRight {
0% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

20% {
opacity: 1;
-webkit-transform: translateX(-20px);
-ms-transform: translateX(-20px);
transform: translateX(-20px);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}
}

.bounceOutRight {
-webkit-animation-name: bounceOutRight;
animation-name: bounceOutRight;
}

@-webkit-keyframes bounceOutUp {
0% {
-webkit-transform: translateY(0);
transform: translateY(0);
}

20% {
opacity: 1;
-webkit-transform: translateY(20px);
transform: translateY(20px);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

@keyframes bounceOutUp {
0% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

20% {
opacity: 1;
-webkit-transform: translateY(20px);
-ms-transform: translateY(20px);
transform: translateY(20px);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

.bounceOutUp {
-webkit-animation-name: bounceOutUp;
animation-name: bounceOutUp;
}

@-webkit-keyframes fadeIn {
0% {
opacity: 0;
}

100% {
opacity: 1;
}
}

@keyframes fadeIn {
0% {
opacity: 0;
}

100% {
opacity: 1;
}
}

.fadeIn {
-webkit-animation-name: fadeIn;
animation-name: fadeIn;
}

@-webkit-keyframes fadeInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-20px);
transform: translateY(-20px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes fadeInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-20px);
-ms-transform: translateY(-20px);
transform: translateY(-20px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.fadeInDown {
-webkit-animation-name: fadeInDown;
animation-name: fadeInDown;
}

@-webkit-keyframes fadeInDownBig {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes fadeInDownBig {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.fadeInDownBig {
-webkit-animation-name: fadeInDownBig;
animation-name: fadeInDownBig;
}

@-webkit-keyframes fadeInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-20px);
transform: translateX(-20px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes fadeInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-20px);
-ms-transform: translateX(-20px);
transform: translateX(-20px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.fadeInLeft {
-webkit-animation-name: fadeInLeft;
animation-name: fadeInLeft;
}

@-webkit-keyframes fadeInLeftBig {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes fadeInLeftBig {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.fadeInLeftBig {
-webkit-animation-name: fadeInLeftBig;
animation-name: fadeInLeftBig;
}

@-webkit-keyframes fadeInRight {
0% {
opacity: 0;
-webkit-transform: translateX(20px);
transform: translateX(20px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes fadeInRight {
0% {
opacity: 0;
-webkit-transform: translateX(20px);
-ms-transform: translateX(20px);
transform: translateX(20px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.fadeInRight {
-webkit-animation-name: fadeInRight;
animation-name: fadeInRight;
}

@-webkit-keyframes fadeInRightBig {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes fadeInRightBig {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.fadeInRightBig {
-webkit-animation-name: fadeInRightBig;
animation-name: fadeInRightBig;
}

@-webkit-keyframes fadeInUp {
0% {
opacity: 0;
-webkit-transform: translateY(20px);
transform: translateY(20px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes fadeInUp {
0% {
opacity: 0;
-webkit-transform: translateY(20px);
-ms-transform: translateY(20px);
transform: translateY(20px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.fadeInUp {
-webkit-animation-name: fadeInUp;
animation-name: fadeInUp;
}

@-webkit-keyframes fadeInUpBig {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes fadeInUpBig {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.fadeInUpBig {
-webkit-animation-name: fadeInUpBig;
animation-name: fadeInUpBig;
}

@-webkit-keyframes fadeOut {
0% {
opacity: 1;
}

100% {
opacity: 0;
}
}

@keyframes fadeOut {
0% {
opacity: 1;
}

100% {
opacity: 0;
}
}

.fadeOut {
-webkit-animation-name: fadeOut;
animation-name: fadeOut;
}

@-webkit-keyframes fadeOutDown {
0% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(20px);
transform: translateY(20px);
}
}

@keyframes fadeOutDown {
0% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(20px);
-ms-transform: translateY(20px);
transform: translateY(20px);
}
}

.fadeOutDown {
-webkit-animation-name: fadeOutDown;
animation-name: fadeOutDown;
}

@-webkit-keyframes fadeOutDownBig {
0% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}
}

@keyframes fadeOutDownBig {
0% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}
}

.fadeOutDownBig {
-webkit-animation-name: fadeOutDownBig;
animation-name: fadeOutDownBig;
}

@-webkit-keyframes fadeOutLeft {
0% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-20px);
transform: translateX(-20px);
}
}

@keyframes fadeOutLeft {
0% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-20px);
-ms-transform: translateX(-20px);
transform: translateX(-20px);
}
}

.fadeOutLeft {
-webkit-animation-name: fadeOutLeft;
animation-name: fadeOutLeft;
}

@-webkit-keyframes fadeOutLeftBig {
0% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

@keyframes fadeOutLeftBig {
0% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

.fadeOutLeftBig {
-webkit-animation-name: fadeOutLeftBig;
animation-name: fadeOutLeftBig;
}

@-webkit-keyframes fadeOutRight {
0% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(20px);
transform: translateX(20px);
}
}

@keyframes fadeOutRight {
0% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(20px);
-ms-transform: translateX(20px);
transform: translateX(20px);
}
}

.fadeOutRight {
-webkit-animation-name: fadeOutRight;
animation-name: fadeOutRight;
}

@-webkit-keyframes fadeOutRightBig {
0% {
opacity: 1;
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}
}

@keyframes fadeOutRightBig {
0% {
opacity: 1;
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}
}

.fadeOutRightBig {
-webkit-animation-name: fadeOutRightBig;
animation-name: fadeOutRightBig;
}

@-webkit-keyframes fadeOutUp {
0% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-20px);
transform: translateY(-20px);
}
}

@keyframes fadeOutUp {
0% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-20px);
-ms-transform: translateY(-20px);
transform: translateY(-20px);
}
}

.fadeOutUp {
-webkit-animation-name: fadeOutUp;
animation-name: fadeOutUp;
}

@-webkit-keyframes fadeOutUpBig {
0% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

@keyframes fadeOutUpBig {
0% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

.fadeOutUpBig {
-webkit-animation-name: fadeOutUpBig;
animation-name: fadeOutUpBig;
}

@-webkit-keyframes flip {
0% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}

40% {
-webkit-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}

50% {
-webkit-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}

80% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}

100% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}
}

@keyframes flip {
0% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
-ms-transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
transform: perspective(400px) translateZ(0) rotateY(0) scale(1);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}

40% {
-webkit-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
-ms-transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
transform: perspective(400px) translateZ(150px) rotateY(170deg) scale(1);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}

50% {
-webkit-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
-ms-transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
transform: perspective(400px) translateZ(150px) rotateY(190deg) scale(1);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}

80% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
-ms-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
transform: perspective(400px) translateZ(0) rotateY(360deg) scale(.95);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}

100% {
-webkit-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
-ms-transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
transform: perspective(400px) translateZ(0) rotateY(360deg) scale(1);
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}
}

.animated.flip {
-webkit-backface-visibility: visible;
-ms-backface-visibility: visible;
backface-visibility: visible;
-webkit-animation-name: flip;
animation-name: flip;
}

@-webkit-keyframes flipInX {
0% {
-webkit-transform: perspective(400px) rotateX(90deg);
transform: perspective(400px) rotateX(90deg);
opacity: 0;
}

40% {
-webkit-transform: perspective(400px) rotateX(-10deg);
transform: perspective(400px) rotateX(-10deg);
}

70% {
-webkit-transform: perspective(400px) rotateX(10deg);
transform: perspective(400px) rotateX(10deg);
}

100% {
-webkit-transform: perspective(400px) rotateX(0deg);
transform: perspective(400px) rotateX(0deg);
opacity: 1;
}
}

@keyframes flipInX {
0% {
-webkit-transform: perspective(400px) rotateX(90deg);
-ms-transform: perspective(400px) rotateX(90deg);
transform: perspective(400px) rotateX(90deg);
opacity: 0;
}

40% {
-webkit-transform: perspective(400px) rotateX(-10deg);
-ms-transform: perspective(400px) rotateX(-10deg);
transform: perspective(400px) rotateX(-10deg);
}

70% {
-webkit-transform: perspective(400px) rotateX(10deg);
-ms-transform: perspective(400px) rotateX(10deg);
transform: perspective(400px) rotateX(10deg);
}

100% {
-webkit-transform: perspective(400px) rotateX(0deg);
-ms-transform: perspective(400px) rotateX(0deg);
transform: perspective(400px) rotateX(0deg);
opacity: 1;
}
}

.flipInX {
-webkit-backface-visibility: visible !important;
-ms-backface-visibility: visible !important;
backface-visibility: visible !important;
-webkit-animation-name: flipInX;
animation-name: flipInX;
}

@-webkit-keyframes flipInY {
0% {
-webkit-transform: perspective(400px) rotateY(90deg);
transform: perspective(400px) rotateY(90deg);
opacity: 0;
}

40% {
-webkit-transform: perspective(400px) rotateY(-10deg);
transform: perspective(400px) rotateY(-10deg);
}

70% {
-webkit-transform: perspective(400px) rotateY(10deg);
transform: perspective(400px) rotateY(10deg);
}

100% {
-webkit-transform: perspective(400px) rotateY(0deg);
transform: perspective(400px) rotateY(0deg);
opacity: 1;
}
}

@keyframes flipInY {
0% {
-webkit-transform: perspective(400px) rotateY(90deg);
-ms-transform: perspective(400px) rotateY(90deg);
transform: perspective(400px) rotateY(90deg);
opacity: 0;
}

40% {
-webkit-transform: perspective(400px) rotateY(-10deg);
-ms-transform: perspective(400px) rotateY(-10deg);
transform: perspective(400px) rotateY(-10deg);
}

70% {
-webkit-transform: perspective(400px) rotateY(10deg);
-ms-transform: perspective(400px) rotateY(10deg);
transform: perspective(400px) rotateY(10deg);
}

100% {
-webkit-transform: perspective(400px) rotateY(0deg);
-ms-transform: perspective(400px) rotateY(0deg);
transform: perspective(400px) rotateY(0deg);
opacity: 1;
}
}

.flipInY {
-webkit-backface-visibility: visible !important;
-ms-backface-visibility: visible !important;
backface-visibility: visible !important;
-webkit-animation-name: flipInY;
animation-name: flipInY;
}

@-webkit-keyframes flipOutX {
0% {
-webkit-transform: perspective(400px) rotateX(0deg);
transform: perspective(400px) rotateX(0deg);
opacity: 1;
}

100% {
-webkit-transform: perspective(400px) rotateX(90deg);
transform: perspective(400px) rotateX(90deg);
opacity: 0;
}
}

@keyframes flipOutX {
0% {
-webkit-transform: perspective(400px) rotateX(0deg);
-ms-transform: perspective(400px) rotateX(0deg);
transform: perspective(400px) rotateX(0deg);
opacity: 1;
}

100% {
-webkit-transform: perspective(400px) rotateX(90deg);
-ms-transform: perspective(400px) rotateX(90deg);
transform: perspective(400px) rotateX(90deg);
opacity: 0;
}
}

.flipOutX {
-webkit-animation-name: flipOutX;
animation-name: flipOutX;
-webkit-backface-visibility: visible !important;
-ms-backface-visibility: visible !important;
backface-visibility: visible !important;
}

@-webkit-keyframes flipOutY {
0% {
-webkit-transform: perspective(400px) rotateY(0deg);
transform: perspective(400px) rotateY(0deg);
opacity: 1;
}

100% {
-webkit-transform: perspective(400px) rotateY(90deg);
transform: perspective(400px) rotateY(90deg);
opacity: 0;
}
}

@keyframes flipOutY {
0% {
-webkit-transform: perspective(400px) rotateY(0deg);
-ms-transform: perspective(400px) rotateY(0deg);
transform: perspective(400px) rotateY(0deg);
opacity: 1;
}

100% {
-webkit-transform: perspective(400px) rotateY(90deg);
-ms-transform: perspective(400px) rotateY(90deg);
transform: perspective(400px) rotateY(90deg);
opacity: 0;
}
}

.flipOutY {
-webkit-backface-visibility: visible !important;
-ms-backface-visibility: visible !important;
backface-visibility: visible !important;
-webkit-animation-name: flipOutY;
animation-name: flipOutY;
}

@-webkit-keyframes lightSpeedIn {
0% {
-webkit-transform: translateX(100%) skewX(-30deg);
transform: translateX(100%) skewX(-30deg);
opacity: 0;
}

60% {
-webkit-transform: translateX(-20%) skewX(30deg);
transform: translateX(-20%) skewX(30deg);
opacity: 1;
}

80% {
-webkit-transform: translateX(0%) skewX(-15deg);
transform: translateX(0%) skewX(-15deg);
opacity: 1;
}

100% {
-webkit-transform: translateX(0%) skewX(0deg);
transform: translateX(0%) skewX(0deg);
opacity: 1;
}
}

@keyframes lightSpeedIn {
0% {
-webkit-transform: translateX(100%) skewX(-30deg);
-ms-transform: translateX(100%) skewX(-30deg);
transform: translateX(100%) skewX(-30deg);
opacity: 0;
}

60% {
-webkit-transform: translateX(-20%) skewX(30deg);
-ms-transform: translateX(-20%) skewX(30deg);
transform: translateX(-20%) skewX(30deg);
opacity: 1;
}

80% {
-webkit-transform: translateX(0%) skewX(-15deg);
-ms-transform: translateX(0%) skewX(-15deg);
transform: translateX(0%) skewX(-15deg);
opacity: 1;
}

100% {
-webkit-transform: translateX(0%) skewX(0deg);
-ms-transform: translateX(0%) skewX(0deg);
transform: translateX(0%) skewX(0deg);
opacity: 1;
}
}

.lightSpeedIn {
-webkit-animation-name: lightSpeedIn;
animation-name: lightSpeedIn;
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}

@-webkit-keyframes lightSpeedOut {
0% {
-webkit-transform: translateX(0%) skewX(0deg);
transform: translateX(0%) skewX(0deg);
opacity: 1;
}

100% {
-webkit-transform: translateX(100%) skewX(-30deg);
transform: translateX(100%) skewX(-30deg);
opacity: 0;
}
}

@keyframes lightSpeedOut {
0% {
-webkit-transform: translateX(0%) skewX(0deg);
-ms-transform: translateX(0%) skewX(0deg);
transform: translateX(0%) skewX(0deg);
opacity: 1;
}

100% {
-webkit-transform: translateX(100%) skewX(-30deg);
-ms-transform: translateX(100%) skewX(-30deg);
transform: translateX(100%) skewX(-30deg);
opacity: 0;
}
}

.lightSpeedOut {
-webkit-animation-name: lightSpeedOut;
animation-name: lightSpeedOut;
-webkit-animation-timing-function: ease-in;
animation-timing-function: ease-in;
}

@-webkit-keyframes rotateIn {
0% {
-webkit-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(-200deg);
transform: rotate(-200deg);
opacity: 0;
}

100% {
-webkit-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

@keyframes rotateIn {
0% {
-webkit-transform-origin: center center;
-ms-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(-200deg);
-ms-transform: rotate(-200deg);
transform: rotate(-200deg);
opacity: 0;
}

100% {
-webkit-transform-origin: center center;
-ms-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

.rotateIn {
-webkit-animation-name: rotateIn;
animation-name: rotateIn;
}

@-webkit-keyframes rotateInDownLeft {
0% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

@keyframes rotateInDownLeft {
0% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

.rotateInDownLeft {
-webkit-animation-name: rotateInDownLeft;
animation-name: rotateInDownLeft;
}

@-webkit-keyframes rotateInDownRight {
0% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

@keyframes rotateInDownRight {
0% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(90deg);
-ms-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

.rotateInDownRight {
-webkit-animation-name: rotateInDownRight;
animation-name: rotateInDownRight;
}

@-webkit-keyframes rotateInUpLeft {
0% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

@keyframes rotateInUpLeft {
0% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(90deg);
-ms-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

.rotateInUpLeft {
-webkit-animation-name: rotateInUpLeft;
animation-name: rotateInUpLeft;
}

@-webkit-keyframes rotateInUpRight {
0% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

@keyframes rotateInUpRight {
0% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}

100% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}
}

.rotateInUpRight {
-webkit-animation-name: rotateInUpRight;
animation-name: rotateInUpRight;
}

@-webkit-keyframes rotateOut {
0% {
-webkit-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(200deg);
transform: rotate(200deg);
opacity: 0;
}
}

@keyframes rotateOut {
0% {
-webkit-transform-origin: center center;
-ms-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: center center;
-ms-transform-origin: center center;
transform-origin: center center;
-webkit-transform: rotate(200deg);
-ms-transform: rotate(200deg);
transform: rotate(200deg);
opacity: 0;
}
}

.rotateOut {
-webkit-animation-name: rotateOut;
animation-name: rotateOut;
}

@-webkit-keyframes rotateOutDownLeft {
0% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}
}

@keyframes rotateOutDownLeft {
0% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(90deg);
-ms-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}
}

.rotateOutDownLeft {
-webkit-animation-name: rotateOutDownLeft;
animation-name: rotateOutDownLeft;
}

@-webkit-keyframes rotateOutDownRight {
0% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}
}

@keyframes rotateOutDownRight {
0% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}
}

.rotateOutDownRight {
-webkit-animation-name: rotateOutDownRight;
animation-name: rotateOutDownRight;
}

@-webkit-keyframes rotateOutUpLeft {
0% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}
}

@keyframes rotateOutUpLeft {
0% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: left bottom;
-ms-transform-origin: left bottom;
transform-origin: left bottom;
-webkit-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
transform: rotate(-90deg);
opacity: 0;
}
}

.rotateOutUpLeft {
-webkit-animation-name: rotateOutUpLeft;
animation-name: rotateOutUpLeft;
}

@-webkit-keyframes rotateOutUpRight {
0% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}
}

@keyframes rotateOutUpRight {
0% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
opacity: 1;
}

100% {
-webkit-transform-origin: right bottom;
-ms-transform-origin: right bottom;
transform-origin: right bottom;
-webkit-transform: rotate(90deg);
-ms-transform: rotate(90deg);
transform: rotate(90deg);
opacity: 0;
}
}

.rotateOutUpRight {
-webkit-animation-name: rotateOutUpRight;
animation-name: rotateOutUpRight;
}

@-webkit-keyframes slideInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}

100% {
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes slideInDown {
0% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}

100% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.slideInDown {
-webkit-animation-name: slideInDown;
animation-name: slideInDown;
}

@-webkit-keyframes slideInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}

100% {
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes slideInLeft {
0% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}

100% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.slideInLeft {
-webkit-animation-name: slideInLeft;
animation-name: slideInLeft;
}

@-webkit-keyframes slideInRight {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}

100% {
-webkit-transform: translateX(0);
transform: translateX(0);
}
}

@keyframes slideInRight {
0% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}

100% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}
}

.slideInRight {
-webkit-animation-name: slideInRight;
animation-name: slideInRight;
}

@-webkit-keyframes slideOutLeft {
0% {
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

@keyframes slideOutLeft {
0% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(-2000px);
-ms-transform: translateX(-2000px);
transform: translateX(-2000px);
}
}

.slideOutLeft {
-webkit-animation-name: slideOutLeft;
animation-name: slideOutLeft;
}

@-webkit-keyframes slideOutRight {
0% {
-webkit-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
transform: translateX(2000px);
}
}

@keyframes slideOutRight {
0% {
-webkit-transform: translateX(0);
-ms-transform: translateX(0);
transform: translateX(0);
}

100% {
opacity: 0;
-webkit-transform: translateX(2000px);
-ms-transform: translateX(2000px);
transform: translateX(2000px);
}
}

.slideOutRight {
-webkit-animation-name: slideOutRight;
animation-name: slideOutRight;
}

@-webkit-keyframes slideOutUp {
0% {
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

@keyframes slideOutUp {
0% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(-2000px);
-ms-transform: translateY(-2000px);
transform: translateY(-2000px);
}
}

.slideOutUp {
-webkit-animation-name: slideOutUp;
animation-name: slideOutUp;
}

@-webkit-keyframes slideInUp {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
transform: translateY(0);
}
}

@keyframes slideInUp {
0% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}

100% {
opacity: 1;
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}
}

.slideInUp {
-webkit-animation-name: slideInUp;
animation-name: slideInUp;
}

@-webkit-keyframes slideOutDown {
0% {
-webkit-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
transform: translateY(2000px);
}
}

@keyframes slideOutDown {
0% {
-webkit-transform: translateY(0);
-ms-transform: translateY(0);
transform: translateY(0);
}

100% {
opacity: 0;
-webkit-transform: translateY(2000px);
-ms-transform: translateY(2000px);
transform: translateY(2000px);
}
}

.slideOutDown {
-webkit-animation-name: slideOutDown;
animation-name: slideOutDown;
}

@-webkit-keyframes hinge {
0% {
-webkit-transform: rotate(0);
transform: rotate(0);
-webkit-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

20%, 60% {
-webkit-transform: rotate(80deg);
transform: rotate(80deg);
-webkit-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

40% {
-webkit-transform: rotate(60deg);
transform: rotate(60deg);
-webkit-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

80% {
-webkit-transform: rotate(60deg) translateY(0);
transform: rotate(60deg) translateY(0);
-webkit-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
opacity: 1;
}

100% {
-webkit-transform: translateY(700px);
transform: translateY(700px);
opacity: 0;
}
}

@keyframes hinge {
0% {
-webkit-transform: rotate(0);
-ms-transform: rotate(0);
transform: rotate(0);
-webkit-transform-origin: top left;
-ms-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

20%, 60% {
-webkit-transform: rotate(80deg);
-ms-transform: rotate(80deg);
transform: rotate(80deg);
-webkit-transform-origin: top left;
-ms-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

40% {
-webkit-transform: rotate(60deg);
-ms-transform: rotate(60deg);
transform: rotate(60deg);
-webkit-transform-origin: top left;
-ms-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

80% {
-webkit-transform: rotate(60deg) translateY(0);
-ms-transform: rotate(60deg) translateY(0);
transform: rotate(60deg) translateY(0);
-webkit-transform-origin: top left;
-ms-transform-origin: top left;
transform-origin: top left;
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
opacity: 1;
}

100% {
-webkit-transform: translateY(700px);
-ms-transform: translateY(700px);
transform: translateY(700px);
opacity: 0;
}
}

.hinge {
-webkit-animation-name: hinge;
animation-name: hinge;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollIn {
0% {
opacity: 0;
-webkit-transform: translateX(-100%) rotate(-120deg);
transform: translateX(-100%) rotate(-120deg);
}

100% {
opacity: 1;
-webkit-transform: translateX(0px) rotate(0deg);
transform: translateX(0px) rotate(0deg);
}
}

@keyframes rollIn {
0% {
opacity: 0;
-webkit-transform: translateX(-100%) rotate(-120deg);
-ms-transform: translateX(-100%) rotate(-120deg);
transform: translateX(-100%) rotate(-120deg);
}

100% {
opacity: 1;
-webkit-transform: translateX(0px) rotate(0deg);
-ms-transform: translateX(0px) rotate(0deg);
transform: translateX(0px) rotate(0deg);
}
}

.rollIn {
-webkit-animation-name: rollIn;
animation-name: rollIn;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollOut {
0% {
opacity: 1;
-webkit-transform: translateX(0px) rotate(0deg);
transform: translateX(0px) rotate(0deg);
}

100% {
opacity: 0;
-webkit-transform: translateX(100%) rotate(120deg);
transform: translateX(100%) rotate(120deg);
}
}

@keyframes rollOut {
0% {
opacity: 1;
-webkit-transform: translateX(0px) rotate(0deg);
-ms-transform: translateX(0px) rotate(0deg);
transform: translateX(0px) rotate(0deg);
}

100% {
opacity: 0;
-webkit-transform: translateX(100%) rotate(120deg);
-ms-transform: translateX(100%) rotate(120deg);
transform: translateX(100%) rotate(120deg);
}
}

.rollOut {
-webkit-animation-name: rollOut;
animation-name: rollOut;
}

@-webkit-keyframes zoomIn {
0% {
opacity: 0;
-webkit-transform: scale(.3);
transform: scale(.3);
}

50% {
opacity: 1;
}
}

@keyframes zoomIn {
0% {
opacity: 0;
-webkit-transform: scale(.3);
-ms-transform: scale(.3);
transform: scale(.3);
}

50% {
opacity: 1;
}
}

.zoomIn {
-webkit-animation-name: zoomIn;
animation-name: zoomIn;
}

@-webkit-keyframes zoomInDown {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateY(-2000px);
transform: scale(.1) translateY(-2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateY(60px);
transform: scale(.475) translateY(60px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

@keyframes zoomInDown {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateY(-2000px);
-ms-transform: scale(.1) translateY(-2000px);
transform: scale(.1) translateY(-2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateY(60px);
-ms-transform: scale(.475) translateY(60px);
transform: scale(.475) translateY(60px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

.zoomInDown {
-webkit-animation-name: zoomInDown;
animation-name: zoomInDown;
}

@-webkit-keyframes zoomInLeft {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateX(-2000px);
transform: scale(.1) translateX(-2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateX(48px);
transform: scale(.475) translateX(48px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

@keyframes zoomInLeft {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateX(-2000px);
-ms-transform: scale(.1) translateX(-2000px);
transform: scale(.1) translateX(-2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateX(48px);
-ms-transform: scale(.475) translateX(48px);
transform: scale(.475) translateX(48px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

.zoomInLeft {
-webkit-animation-name: zoomInLeft;
animation-name: zoomInLeft;
}

@-webkit-keyframes zoomInRight {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateX(2000px);
transform: scale(.1) translateX(2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateX(-48px);
transform: scale(.475) translateX(-48px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

@keyframes zoomInRight {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateX(2000px);
-ms-transform: scale(.1) translateX(2000px);
transform: scale(.1) translateX(2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateX(-48px);
-ms-transform: scale(.475) translateX(-48px);
transform: scale(.475) translateX(-48px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

.zoomInRight {
-webkit-animation-name: zoomInRight;
animation-name: zoomInRight;
}

@-webkit-keyframes zoomInUp {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateY(2000px);
transform: scale(.1) translateY(2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateY(-60px);
transform: scale(.475) translateY(-60px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

@keyframes zoomInUp {
0% {
opacity: 0;
-webkit-transform: scale(.1) translateY(2000px);
-ms-transform: scale(.1) translateY(2000px);
transform: scale(.1) translateY(2000px);
-webkit-animation-timing-function: ease-in-out;
animation-timing-function: ease-in-out;
}

60% {
opacity: 1;
-webkit-transform: scale(.475) translateY(-60px);
-ms-transform: scale(.475) translateY(-60px);
transform: scale(.475) translateY(-60px);
-webkit-animation-timing-function: ease-out;
animation-timing-function: ease-out;
}
}

.zoomInUp {
-webkit-animation-name: zoomInUp;
animation-name: zoomInUp;
}

@-webkit-keyframes zoomOut {
0% {
opacity: 1;
-webkit-transform: scale(1);
transform: scale(1);
}

50% {
opacity: 0;
-webkit-transform: scale(.3);
transform: scale(.3);
}

100% {
opacity: 0;
}
}

@keyframes zoomOut {
0% {
opacity: 1;
-webkit-transform: scale(1);
-ms-transform: scale(1);
transform: scale(1);
}

50% {
opacity: 0;
-webkit-transform: scale(.3);
-ms-transform: scale(.3);
transform: scale(.3);
}

100% {
opacity: 0;
}
}

.zoomOut {
-webkit-animation-name: zoomOut;
animation-name: zoomOut;
}

@-webkit-keyframes zoomOutDown {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateY(-60px);
transform: scale(.475) translateY(-60px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateY(2000px);
transform: scale(.1) translateY(2000px);
-webkit-transform-origin: center bottom;
transform-origin: center bottom;
}
}

@keyframes zoomOutDown {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateY(-60px);
-ms-transform: scale(.475) translateY(-60px);
transform: scale(.475) translateY(-60px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateY(2000px);
-ms-transform: scale(.1) translateY(2000px);
transform: scale(.1) translateY(2000px);
-webkit-transform-origin: center bottom;
-ms-transform-origin: center bottom;
transform-origin: center bottom;
}
}

.zoomOutDown {
-webkit-animation-name: zoomOutDown;
animation-name: zoomOutDown;
}

@-webkit-keyframes zoomOutLeft {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateX(42px);
transform: scale(.475) translateX(42px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateX(-2000px);
transform: scale(.1) translateX(-2000px);
-webkit-transform-origin: left center;
transform-origin: left center;
}
}

@keyframes zoomOutLeft {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateX(42px);
-ms-transform: scale(.475) translateX(42px);
transform: scale(.475) translateX(42px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateX(-2000px);
-ms-transform: scale(.1) translateX(-2000px);
transform: scale(.1) translateX(-2000px);
-webkit-transform-origin: left center;
-ms-transform-origin: left center;
transform-origin: left center;
}
}

.zoomOutLeft {
-webkit-animation-name: zoomOutLeft;
animation-name: zoomOutLeft;
}

@-webkit-keyframes zoomOutRight {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateX(-42px);
transform: scale(.475) translateX(-42px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateX(2000px);
transform: scale(.1) translateX(2000px);
-webkit-transform-origin: right center;
transform-origin: right center;
}
}

@keyframes zoomOutRight {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateX(-42px);
-ms-transform: scale(.475) translateX(-42px);
transform: scale(.475) translateX(-42px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateX(2000px);
-ms-transform: scale(.1) translateX(2000px);
transform: scale(.1) translateX(2000px);
-webkit-transform-origin: right center;
-ms-transform-origin: right center;
transform-origin: right center;
}
}

.zoomOutRight {
-webkit-animation-name: zoomOutRight;
animation-name: zoomOutRight;
}

@-webkit-keyframes zoomOutUp {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateY(60px);
transform: scale(.475) translateY(60px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateY(-2000px);
transform: scale(.1) translateY(-2000px);
-webkit-transform-origin: center top;
transform-origin: center top;
}
}

@keyframes zoomOutUp {
40% {
opacity: 1;
-webkit-transform: scale(.475) translateY(60px);
-ms-transform: scale(.475) translateY(60px);
transform: scale(.475) translateY(60px);
-webkit-animation-timing-function: linear;
animation-timing-function: linear;
}

100% {
opacity: 0;
-webkit-transform: scale(.1) translateY(-2000px);
-ms-transform: scale(.1) translateY(-2000px);
transform: scale(.1) translateY(-2000px);
-webkit-transform-origin: center top;
-ms-transform-origin: center top;
transform-origin: center top;
}
}

.zoomOutUp {
-webkit-animation-name: zoomOutUp;
animation-name: zoomOutUp;
}



/*------------------------------------------------------------------
[Table of contents]

1. Body
2. loader
3. Nav
4. Page Tilte
5. Shop Block
6. Super deal
7. Featured- product
8. Blog
9. newsletter
10. footer
11. Contact
12. Responsive Media Screen style

-------------------------------------------------------------------*/

/*------------------------------------------------------------------
# [Color codes]

# Black (text): #1b1b1b
# Blue : {!! $color !!}

------------------------------------------------------------------*/

/*------------------------------------------------------------------
[Typography]

Body :		'Lato', sans-serif;
Title and logo:		'Montserrat', sans-serif;

-------------------------------------------------------------------*/



/*---Body---*/

/*loader start */
.loader {
background: #000000;
height: 100%;
width: 100%;
position: fixed;
overflow: hidden;
z-index: 1200;
}

.loader > div {
margin: auto;
text-align: center;
display: table;
width: 100%;
height: 100%;
}

.loader > div > div {
position: relative;
box-sizing: border-box;
display: table-cell;
vertical-align: middle;
}

/*.imgLoader {
animation : scales 0.5s ease infinite alternate;
}

@keyframes scales{
from{transform : scale(0.8)}
to{transform: scale(1.1)}
}*/

/*loader End */


html,
body {
height: 100%;
width: 100%;
}

body {
color:#898989;
font-family: 'futura-light', sans-serif;
}

a {
color: {!! $color !!};
-webkit-transition: all 500ms ease-in-out;
-o-transition: all 500ms ease-in-out;
-moz-transition: all 500ms ease-in-out;
transition: all 500ms ease-in-out;
}
.btn, .btn:hover {
-webkit-transition: all 500ms ease-in-out;
-o-transition: all 500ms ease-in-out;
-moz-transition: all 500ms ease-in-out;
transition: all 500ms ease-in-out;
}
a:hover,
a:focus {
color: {!! $color !!};
outline: none;
outline-offset: 0px;
text-decoration:none !important;
}

h1,
h2,
h3,
h4,
h5,
h6 {
font-family: 'futura-light', sans-serif;
color:#1b1b1b;
}

p {
line-height: 1.5;
margin-bottom: 20px;
}

.pink{
color:{!! $color !!};
}
.white{
color: #fff;
}
.black{
color:#1b1b1b;
}
.gray{
color:#898989;
}
.bg-pink{
background-color:{!! $color !!};
}
.light-gray{
background-color:#f7f7f7;
}
section{
padding:40px 0px;
}

/*--- Nav ---*/
nav{
font-family: 'futura-light', sans-serif;
}
@media (max-width: 991px) and (min-width: 768px){
.top-bar-nav .wrap-sticky nav.navbar.divinnav.sticked {
top: 24px;
}
}
.navbar-default {
background-color: #fff;
border: none;
margin-bottom: 0;
}
nav.navbar.awesomenav ul.dropdown-menu.megamenu-content .content ul.menu-col li a:hover{
border-bottom: none;
}
/*--- Nav top bar ---*/

.top-container {
float: left;
width: 100%;
color: #fff;
padding: 5px 0;
background-color: {!! $color !!} ;
}
.top-column-left, .top-column-right {
float: left;
}
.top-social-network, ul.contact-line, ul.register {
float: left;
margin: 0;
padding: 0;
}
ul.register {
margin-right: 15px;
}
/*ul.contact-line li:first-child {
border-left: none;
padding: 0;
}*/
ul.contact-line li, ul.register li {
float: left;
font-size: 12px;
list-style: none;
margin: 0 0 0 10px;
padding: 0 0 0 10px;
border-left: 1px solid rgba(255, 255, 255, 0.6);
}
.top-column-right {
float: right;
}
.top-social-network a {
margin: 2px;
}
.top-container a, .top-container a:hover, .top-container a:focus, .top-container a:active {
color: #fff;
}

@media (max-width: 767px) {
.top-container .top-column-left {

}

.top-container .top-column-right {
padding-right: 15px;
}
.top-container {
font-size:13px;
}
}




/*---heading---*/

/*---Page Tilte---*/


.page_title_ctn {
padding-top: 15px;
padding-bottom: 15px;
background-color:{!! $color !!};
background-size: cover;
border-bottom: 1px solid #ecf0f1;
}
.page_title_ctn h1, .page_title_ctn h2, .breadcrumb, .breadcrumb a, .breadcrumb li.active {
color: #ffffff;
}
.breadcrumb li.active span {
text-decoration:underline;
}
.page_title_ctn h2 {
font-weight: 400;
margin-top:10px;
font-size: 24px;
line-height: 34px;
max-width: 420px;
float: left;
}
.breadcrumb {
margin-bottom: 20px;
list-style: none;
background-color: transparent;
border-radius: 0px;
position: relative;
margin-top: 15px;
float: right;
margin-bottom: 0;
padding:0;
}
.breadcrumb > li + li:before {
color: #fff;
}




/*---Shop Block---*/

.product-slide {
border: 5px solid #fff;
/*background-color: #f5f5f5;*/
}
.product-slide .row{
width: auto ;
height: auto;
margin: 0 auto;
}
.tabSix .nav-tabs{
border-bottom:none;
}
.tabSix .nav-tabs>li>a{
margin-right:0px;
line-height:1.42857143;
background-color:transparent;
border-style:solid;
border-color:#898989;
color:#999;
border-width:0px 0px 0px 0px;
border-radius:0px;
padding:10px 25px;
font-weight:600;
}
.tabSix .nav-tabs>li.active>a, .tabSix .nav-tabs>li.active>a:focus, .tabSix .nav-tabs>li.active>a:hover, .tabSix .nav-tabs>li>a:hover{
color:#000;
cursor:pointer;
border-width:0px 0px 0px 0px;
background-color: transparent;
}
.tabSix .nav-tabs li a p{
margin-bottom:0;
font-size:1.7142857142857142em;
font-family: 'futura-light', sans-serif;
text-transform: uppercase;
}
.tabSix .nav-tabs li {
display: inline-block;
float: none;
}
@media (min-width: 768px) {
.tabSix .tab-content{
margin-top:24px;
}
}
.sep-dot {
font-size: 50px;
line-height: 0;
vertical-align: super;
display: none;
}
.tabSix ul li::after{
font: normal normal normal 14px/1 FontAwesome;
content: "\f111";
display: inline-block;
position: absolute;
right: -4px;
top: 25px;
font-size: 10px;
}
.tabSix ul li:last-child::after{
display: none;
}


/*---super-deal-section---*/

.super-deal-section{
background-color: #000;
border: 5px solid #fff;
}
.super-deal h1{
color: #fff;
font-weight: 600;
}
.super-deal h1 span{
color: {!! $color !!};
}
.super-deal a.btn {
border: 1px solid #ccc;
padding: 10px 25px;
color: #fff;
}
.super-deal a.btn:hover {
border: 1px solid {!! $color !!};
color: {!! $color !!};
background-color: #fff;
}
.demo2.dsCountDown {
margin-bottom: 30px;
margin-top: 20px;
display: block;
}
.ds-element {
display: inline-block;
width: auto;
text-align: center;
margin-right: 15px;
}
.ds-element-value {
width: 70px;
height: 70px;
background-color: #fff;
display: block;
font-size: 30px;
line-height: 70px;
text-align: center;
}
.ds-element-title {
margin-top: 15px;
color: #666;
font-size: 14px;
}
.super-deal {
margin-top: 50px;
}
@media only screen and (max-width: 991px) {
.ds-element-value {
width: 50px;
height: 50px;
background-color: #fff;
display: block;
font-size: 24px;
line-height: 50px;
}
.demo2.dsCountDown {
margin-bottom: 20px;
margin-top: 0px;
display: block;
}
.super-deal {
margin-top: 0px;
}
.super-deal h1 {
color: #fff;
font-weight: 600;
font-size: 24px;
}
}

/*---featured-product---*/
.featured-product{
border: 5px solid #fff;
background-color: #f5f5f5;
}

/*---Blog Post---*/

.blog-title a {
color: #333;
}
.blog-img .bubble-top:before {
pointer-events: none;
position: absolute;
z-index: 999;
content: '';
border-style: solid;
-webkit-transition-duration: 0.3s;
transition-duration: 0.3s;
-webkit-transition-property: top;
transition-property: top;
left: calc(50% - 20px);
bottom:0;
border-width: 0 20px 20px 20px;
border-color: transparent transparent #fff transparent;
}
.blog-img .bubble-bottom:before {
pointer-events: none;
position: absolute;
z-index: 999;
content: '';
border-style: solid;
-webkit-transition-duration: 0.3s;
transition-duration: 0.3s;
-webkit-transition-property: top;
transition-property: top;
left: calc(50% - 20px);
top:0;
border-width: 20px 20px 0 20px;
border-color: #fff transparent transparent transparent;
}
.ImageWrapper .PStyleHe {
position: absolute;
background: url(../images/plus.png) no-repeat scroll center center / 100% 100% #222222;
width: 100%;
height: 100%;
z-index: 199;
-webkit-background-origin: padding-box, padding-box;
-moz-background-origin: padding-box, padding-box;
-ms-background-origin: padding-box, padding-box;
-o-background-origin: padding-box, padding-box;
background-origin: padding-box, padding-box;
background-position: center center;
background-repeat: no-repeat;
-webkit-background-size: 10px 10px, 100% 100%;
-moz-background-size: 10px 10px, 100% 100%;
-ms-background-size: 10px 10px, 100% 100%;
-o-background-size: 10px 10px, 100% 100%;
background-size: 10px 10px, 100% 100%;
opacity: 0;
top: 0;
-webkit-transition: all 0.3s ease 0s;
-moz-transition: all 0.3s ease 0s;
-ms-transition: all 0.3s ease 0s;
-o-transition: all 0.3s ease 0s;
transition: all 0.3s ease 0s;
}
.ImageWrapper {
display: block;
overflow: hidden;
position: relative;
}
.ImageWrapper:hover .PStyleHe {
opacity: .6;
-webkit-background-size: 60px 60px, 100% 100%;
-moz-background-size: 60px 60px, 100% 100%;
-ms-background-size: 60px 60px, 100% 100%;
-o-background-size: 60px 60px, 100% 100%;
background-size: 60px 60px, 100% 100%;
visibility: visible;
}
.blog-content{
padding: 10px;
min-height: 219px;
}
@media only screen and (max-width: 1024px) {
.blog-content {
min-height: 182px;
}
}
@media only screen and (max-width: 991px) {
.blog-content {
padding: 1px 10px;
min-height: 141px;
}
.blog-title h4{
margin-top: 0;
margin-bottom: 5px;
}
.post-date{
margin-bottom: 5px;
}
.post-content{
margin-bottom: 10px;
}
}
@media only screen and (max-width: 767px){
.blog-content {
padding: 15px;
min-height: 170px;
background-color: #f7f7f7;
margin-bottom: 10px;
}
.blog-img .bubble-top:before {
border-color: transparent transparent #f7f7f7 transparent;
}
.blog-img .bubble-bottom:before {
border-color: #f7f7f7 transparent transparent transparent;
}
}

/*---Blog Style 1---*/
.blogstyle-1 .blog-post-container {
margin-bottom:15px;
}
.blogstyle-1 .blog-post-container .post-thumbnail {
position: relative;
}
.blogstyle-1 .blog-post-container .post-thumbnail img {
width: 100%;
}
.blogstyle-1 .blog-post-container .blog-content {
border: 1px solid #ededed;
}
.blogstyle-1 .blog-post-container .dart-header {
padding: 20px 20px 0;
}
.blogstyle-1 .blog-post-container .dart-title {
text-transform: capitalize;
}
.blogstyle-1 .blog-post-container .dart-title a {
color: #333;
}
.blogstyle-1 .blog-post-container .dart-header .dart-meta {
font-size:0.7142857142857143em;
text-transform: uppercase;
}
.blogstyle-1 .blog-post-container .dart-footer .dart-meta {
margin-bottom:0px;
}
.blogstyle-1 .blog-post-container .dart-header .dart-meta li {
display: inline-block;
border-right: 1px solid #E7E7E7;
line-height: 10px;
padding-right: 15px;
margin-right: 15px;
}
.blogstyle-1 .blog-post-container .dart-header .dart-meta li:last-child {
border-right: 0;
padding-right: 0;
margin-right: 0;
}
.blogstyle-1 .blog-post-container .dart-header .dart-meta li a {
color: #898989;
}
.blogstyle-1 .blog-post-container .dart-content {
padding: 20px;
}
.blogstyle-1 .blog-post-container .dart-content p {
margin: 0;
}
.blogstyle-1 .blog-post-container .dart-footer {
background-color: #f7f7f7;
padding: 8px 20px;
border-top: 1px solid #ebebeb;
}
.blogstyle-1 .blog-post-container .dart-footer .dart-meta a {
color: #898989;
font-size:0.8571428571428571em;
text-transform: capitalize;
}
#blogstyle-1Slider .carousel-control.left, #blogstyle-1Slider .carousel-control.right {
background-image: none;
background-color: #fff;
}
#blogstyle-1Slider .carousel-control {
top: 50%;
margin-top: -13px;
bottom: auto;
width: 26px;
height: 26px;
font-size: 18px;
color: #898989;
text-shadow: none;
filter: alpha(opacity=100);
opacity: 1;
}
.blogstyle-1 .blog-post-container .play-trigger {
position: absolute;
left: 50%;
top: 55%;
width: 70px;
height: 45px;
margin-left: -30px;
margin-top: -30px;
border: 0;
border-radius: 10%;
color: #fff;
background: #cd201f;
font-size:1.4285714285714286em;
line-height: 45px;
text-align: center;
-webkit-transition: all .3s ease 0s;
-moz-transition: all .3s ease 0s;
-o-transition: all .3s ease 0s;
transition: all .3s ease 0s;
}
.blogstyle-1 .blog-post-container .play-trigger i {
margin-left: 5px;
}
.blogstyle-1 .blog-post-container .play-trigger:hover {
color: #cd201f;
background: #fff;
}
.blogstyle-1 .blog-post-container .dart-title a:hover, .blogstyle-1 .blog-post-container .dart-title a:focus {
color: {!! $color !!};
text-decoration:none;
}
.blogstyle-1 .blog-post-container .dart-footer .dart-meta a:hover, .blogstyle-1 .blog-post-container .dart-footer .dart-meta a:focus  {
color: {!! $color !!};
text-decoration:none
}
.blogstyle-1 .blog-post-container .dart-header .dart-meta li a:hover, .blogstyle-1 .blog-post-container .dart-header .dart-meta li a:focus {
color: {!! $color !!};
text-decoration:none;
}
@media (max-width : 991px) {
.blogstyle-1 .blog-post-container .dart-header {
padding: 5px 10px 0;
}
.blogstyle-1 .blog-post-container .dart-content {
padding: 10px;
}
.blogstyle-1 .blog-post-container .dart-footer {
padding: 8px 10px;
}
}

/*---newsletter---*/

.newsletter-bg{
background-image: url(../../examples/newsletter.jpg);
background-position: top right;
background-size: cover;
background-repeat: no-repeat;
padding-bottom: 60px;
}
.newsletter-bg form {
text-align: center;
}
.newsletter {
width: auto;
display: inline-block;
margin: 0 auto;
}
.form-inline .newsletter .form-control {
border: 1px solid {!! $color !!};
border-radius: 0;
width: 280px;
}
.newsletter .btn-default {
border: 1px solid {!! $color !!};
border-radius: 0;
background-color: {!! $color !!};
color: #fff;
-webkit-transition: all 500ms ease-in-out;
-o-transition: all 500ms ease-in-out;
-moz-transition: all 500ms ease-in-out;
transition: all 500ms ease-in-out;
}
.newsletter .btn-default:hover {
border: 1px solid {!! $color !!};
background-color: #fff;
color: {!! $color !!};
-webkit-transition: all 500ms ease-in-out;
-o-transition: all 500ms ease-in-out;
-moz-transition: all 500ms ease-in-out;
transition: all 500ms ease-in-out;
}
.newsletter .btn-default i {
margin-left: 15px;
}
@media only screen and (max-width: 991px) {
.newsletter-bg{
background-position: top ;
}
.newsletter .btn-default {
margin-top: -1px;
}
}


/*---footer---*/
.footer {
text-align: center;
padding: 80px 0px 0px;
background-color: #f3f3f3;
}
.footer h3 {
font-weight: normal;
text-transform: uppercase;
}
.footer .social h5 {
color: #898989;
font-weight: normal;
text-transform: uppercase;
}
.footer .social {
margin-top: 50px;
margin-bottom: 30px;
}
.footer .social a{
color: #666;
}
.footer .social a:hover{
color: {!! $color !!};
}
.footer .copy p {
margin-bottom: 0;
}
.footer .copy {
background-color: #111;
color: #fff;
padding: 15px 0px;
}
.footer .copy p span {
color: {!! $color !!};
}

@media only screen and (max-width: 991px) {
.footer .social {
margin-top: 30px;
margin-bottom: 30px;
}
.footer {
padding: 30px 0px 0px;
}
}


/*---contact form---*/

.contactus-one .contact-form .form-control {
border-top: none;
border-left: none;
border-right: none;
border-bottom: 1px solid #fff;
border-radius: 0px;
background-color: transparent;
color: #8e8e8e;
height: 50px;
}
.contactus-one .contact-form .form-control {
border-bottom: 1px solid #ccc;
box-shadow: none;
}
.contact-info > div > p {
margin-bottom: 10px;
}
.contactus-one .contact-info p i {
margin-right: 15px;
color: {!! $color !!};
}
.map iframe {
pointer-events: none;
width: 100%;
border: none;
}
.map {
-webkit-filter: grayscale(100%);
filter: grayscale(100%);
}
.map iframe{
pointer-events: none;
width:100%;
border:none;
}
.contactus-one .dart-headingstyle-one .dart-heading:after{
left:15px;
}






/*---Responsive Media Screen style---*/


@media only screen and (max-width: 3500px) {
/*body*/
body {
overflow-x: hidden;
}
}

@media only screen and (max-width: 2500px) {}

@media only screen and (max-width: 2100px) {}

@media only screen and (max-width: 1600px) {}

@media only screen and (max-width: 1566px) {}

@media only screen and (max-width: 1366px) {}

@media only screen and (min-width: 1300px) {}

@media only screen and (max-width: 1280px) {}

@media (min-width: 1280px) {}

@media only screen and (max-width: 1199px) {}

@media only screen and (max-width: 1024px) {
body{
font-size:100%;
}
}

@media only screen and (max-width: 991px) {
body{
font-size:100%;
}
.attr-nav > ul > li {
padding: 0px 0px;
}
.attr-nav > ul {
margin: 5px -15px -7px 0;
}
}


/*--------------iPhone 6 plus landscape----------------*/
@media only screen and (max-width: 767px) {
body{
font-size:100%;
}
nav.navbar.awesomenav .navbar-brand {
width: auto;
display: inline;
top: 12px;
padding: 12px;
}
.navbar-brand img.logo {
width: 50%;
display: inline-block;
max-width: 120px;
margin-bottom: 20px;
}

.
}

/*--------------iPhone 6 landscape----------------*/
@media only screen and (max-width: 667px) {}

/*--------------iPhone 5 landscape----------------*/
@media only screen and (max-width: 568px) {}

@media only screen and (max-width: 480px) {}

/*--------------iPhone 6 plus portrait----------------*/
@media only screen and (max-width: 414px) {}

/*--------------iPhone 6 portrait----------------*/
@media only screen and (max-width: 375px) {}

/*--------------iPhone 5 portrait----------------*/
@media only screen and (max-width: 320px) {}

@media only screen and (max-width: 300px) {}

@media only screen and (max-width: 200px) {}




/*------------------------------------------------------------------
[Custom Template Stylesheet]

Project:	Cosmetic Agency Html Responsive Template
Version:	1.1
Primary use:	Cosmetic Agency Html Responsive Template
-------------------------------------------------------------------*/

/*add your style css here*/

/*---------Area de contacto---------*/

.message_box {
display: none;
position: absolute;
z-index: 1000;
left: 50%;
top: 50%;
max-width: 80%;
padding: 2em;
line-height: 1.2em;
border: 1px solid #07759C;
background-color: #B6DDF3;
color: #07759C;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
-webkit-transform: translateX(-50%) translateY(-50%);
-moz-transform: translateX(-50%) translateY(-50%);
-ms-transform: translateX(-50%) translateY(-50%);
transform: translateX(-50%) translateY(-50%);
-webkit-box-shadow: 4px 4px 16px 0 rgba(0, 0, 200, 0.3);
-moz-box-shadow: 4px 4px 16px 0 rgba(0, 0, 200, 0.3);
box-shadow: 4px 4px 16px 0 rgba(0, 0, 200, 0.3);
}

form .message_box {
min-width: 60%
}
.message_box_error {
border: 1px solid #A00000;
background-color: #FDCDCD;
color: #A00000;
-webkit-box-shadow: 4px 4px 16px 0 rgba(200, 0, 0, 0.3);
-moz-box-shadow: 4px 4px 16px 0 rgba(200, 0, 0, 0.3);
box-shadow: 4px 4px 16px 0 rgba(200, 0, 0, 0.3);
}
.message_box_success {
border: 1px solid #00A000;
background-color: #CDFDCD;
color: #00A000;
-webkit-box-shadow: 4px 4px 16px 0 rgba(0, 200, 0, 0.3);
-moz-box-shadow: 4px 4px 16px 0 rgba(0, 200, 0, 0.3);
box-shadow: 4px 4px 16px 0 rgba(0, 200, 0, 0.3);
}
.message_box p {
margin: 0;
line-height: 1.2em;
}
.message_box p+p {
margin-top: 0.2em;
}

#CargadorEnviar {
font-size: 30px;
color: #fff;
display: none;
}

.contact-map {
height: 520px;
}
/*---------Area de contacto ---------*/

#aboutus-section{
font-size: 16px;
}

.share-icon a{
font-size: 23px;
padding: 0 5px;
margin-top: 0px;
}

.share-icon a:hover{
padding: 0 20px;
}

.slider-blog{
width: 80%;
height: auto;
margin-left: auto;
margin-right: auto;
}

/*------ About Us Section -----*/
#aboutus-section > .container{
border-bottom: 1px solid #DDD;
}

.img-aboutus {
float:left;
padding: 20px;
width: 40%;
}

.img-aboutus img {
padding-right: 20px;
}

.text-aboutus {
padding: 20px;
}
/*------ /About Us Section -----*/

/*----- Galleries ------*/
.gallery-content {
border: 1px solid #ededed;
}

.gallery-content h4 {
text-align: center;
padding: 5px;
}

.gallery-content i {
float: right;
}

.gallery-item {
text-align: center;
}

.gallery-item h3{
color: {!! $color !!};
}
/*----- /Galleries ------*/

/*----- SHOP ------*/
/* -------- Cambios -------*/
.cat_active {
color:{!! $color !!} !important;
}

.shop-sidebar .widget li a{
color:#898989;
}
.shop-sidebar .widget li div:hover{
color:{!! $color !!};
text-decoration: none;
padding-left: 5px;
}

.shop-pages .shop-links-widget div .caret{
position: absolute;
right: 0;
top: 10px;
-webkit-transition: all 500ms ease;
-moz-transition: all 500ms ease;
-ms-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;
}
.shop-pages .shop-links-widget div[aria-expanded="true"] .caret{
-moz-transform: rotate(180deg);
-webkit-transform: rotate(180deg);
-o-transform: rotate(180deg);
-ms-transform: rotate(180deg);
transform: rotate(180deg);
}
.shop-pages .shop-links-widget div[aria-expanded="false"] .caret{
-moz-transform: rotate(0deg);
-webkit-transform: rotate(0deg);
-o-transform: rotate(0deg);
-ms-transform: rotate(0deg);
transform: rotate(0deg);
}

.shop-pages .shop-links-widget .panel-title div{
font-size: 14px;
font-weight: 400;
color: #898989;
display: block;
width: 100%;
position: relative;
padding: 0px 0px 10px;
text-transform: uppercase;
}
.shop-pages .shop-links-widget .panel-title div:hover {
text-decoration: none;
color: {!! $color !!};
}


.shop-sidebar .widget.widget_size li div, .list-colors li div{
border:1px solid #f1f1f1;
height: 35px;
font-size: 10px;

display: flex;
align-items: center;
}

.list-colors li div img {
    padding-right: 3px;
}


.shop-sidebar .widget.widget_size li div:hover,.shop-sidebar .widget.widget_size li div:focus,
.list-colors li div:hover, .list-colors li div:focus{
border-color:#fff;
}
.shop-sidebar .widget-title:after{
background:{!! $color !!};
height:1px;
width:25px;
position:absolute;
bottom:0;
left:0;
content:"";
}
.shop-sidebar .widget li div, .list-colors li div{
color:#898989;
}
.shop-sidebar .widget li div:hover, .list-colors li div:hover{
color:{!! $color !!};
text-decoration: none;
padding-left: 5px;
}
.shop-sidebar .widget.widget_size li div:hover, .list-colors li div:hover {
color: #fff;
background-color: #3f3f3f;
text-decoration: none;
padding-left: 0px;
}

.p-color {
width: 100%;
}

.c-active {
border-color:#333 !important;
}

.list-colors {
border-bottom: 1px solid #f1f1f1;
border-top: 1px solid #f1f1f1;
padding: 10px;
cursor: pointer;
}

.list-colors li{
display: inline-block;
padding: 0;
}

.l-color {
width: 35px;
}

.review {
padding: 2% 6%;
color: #535353;
font-size: 16px;
border-top: 10px ridge #b7986c;
margin: 20px 30px 30px 20px;
}

.cart-product-remove div{
color:red;
cursor: pointer;
}

.error_cart, .error {
color: #882607;
}

.success_cart {
color: #42882c;
}

.wait_cart {
color: #181f67;
}

.row_msg {
display: none;
}
.row_msg td {
padding: 5px;
height: auto;
}
.row_msg td p{
text-align: center;
}

.total-cart strong{
text-transform: uppercase;
}

.total-cart {
padding:10px;
}

.sepCart {
border-bottom: 1px solid #a9a9a94a;
border-top: 1px solid #a9a9a94a;
text-align: center;
padding: 20px;
margin: 20px;
color: {!! $color !!};
}
/*----- /SHOP ------*/

/*----- Menu -----------*/
.item-cat {
padding-top: 10px !important;
padding-bottom:10px !important;
border-top: {!! $color !!} 3px solid;
}
@media only screen and (max-width: 767px) {
.item-cat {
border-top: rgba(255,255,255,0) 1px solid;
}
}

.btn-login {
cursor: pointer;
}

.btn-login:hover {
color:#000000;
}

.modal-header {
background-color: {!! $color !!};
color: #ffffff;
}

.modal-body {
padding: 20px;
}

.modal-header div {
font-size: 23px;
text-transform: uppercase;
}

.dropdown-menu.singin {
left: auto;
right: -10px;
position: absolute;
width: 200px;
padding: 1px 0 0 0;
border-top-width: 0;
background-color: #ab8d62;
}

.dropdown-menu.singin >li.user-header{
height: auto;
padding: 10px;
text-align: center;
margin: 0;
width: 100%;
}

.dropdown-menu.singin >li.user-footer {
background-color: #f9f9f9;
padding: 10px;
margin: 0;
width: 100%;
}
/*----- /Menu -----------*/

/* ----- CHECKBOX ---------*/

.checkbox label:after,
.radio label:after {
content: '';
display: table;
clear: both;
}

.checkbox .cr,
.radio .cr {
position: relative;
display: inline-block;
border: 1px solid #a9a9a9;
border-radius: .25em;
width: 1.3em;
height: 1.3em;
float: left;
margin-right: .5em;
}

.radio .cr {
border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
position: absolute;
font-size: .8em;
line-height: 0;
top: 50%;
left: 20%;
}

.radio .cr .cr-icon {
margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
display: none;
}

.checkbox a{
color: {!! $color !!};
display: inline;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
transform: scale(3) rotateZ(-20deg);
opacity: 0;
transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
transform: scale(1) rotateZ(0deg);
opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
opacity: .5;
}

.formulario > div {
padding: 20px 0;
border-bottom: 1px solid #ccc;
}
.formulario label {
display: inline-block;
cursor: pointer;
color: #898989;
position: relative;
padding: 5px 15px 5px 51px;
font-size: 1em;
border-radius: 5px;
-webkit-transition: all 0.3s ease;
-o-transition: all 0.3s ease;
transition: all 0.3s ease;
}
.formulario label:hover {
background: rgba(0, 116, 217, 0.1);
}
.formulario label:before {
content: "";
display: inline-block;
width: 17px;
height: 17px;
position: absolute;
left: 15px;
border-radius: 50%;
background: none;
border: 3px solid {!! $color !!};
margin-top: 7px;
}
.formulario input[type="radio"] {
display: none;
}
.formulario input[type="radio"]:checked + label:before {
display: none;
}
.formulario input[type="radio"]:checked + label {
padding: 5px 15px;
background: {!! $color !!};
border-radius: 2px;
color: #fff;
}

.new_address:before {
margin-top: 2px !important;
}

/* ----- /CHECKBOX ---------*/

/* ------ ACCOUNT ---------*/
#msg-account {
display: none;
padding: 5px 20px;
}

.input-file-button {
font-size: 17px;
color: {!! $color !!};
cursor: pointer;
}

.input-file-button:hover {
color: #000000;
}

.divDelete {
-webkit-transition: all 0.3s;
-moz-transition: all 0.3s;
transition: all 0.3s;
border-bottom: 1px solid #EDEDDE;
padding: 8px 0;
border-radius:0;
cursor: pointer;
}
.divDelete {
color: #666;
font-size: 0.9em;
border-radius:0;
}
.divDelete:before {
content: "";
display: inline-block;
width: 0;
height: 0;
border-top: 4px solid transparent;
border-bottom: 4px solid transparent;
border-left: 4px solid #333;
margin-left: 5px;
margin-right: 5px;
position: relative;
}
/* ------ /ACCOUNT ---------*/

.t-product {
    height: 88px;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0 2px;
}

.t-product:hover {
    overflow: auto;
}

@media (max-width: 767px) {

    .ship_header {
        font-size: 10px !important;
    }

    .wa-theme-design-block {
        padding-bottom: 20px;
    }

    .t-product {
        height: 60px;
    }
}

.ship_header {
    margin: 0;
    padding-left: 4px;
    font-size: 12px;
    text-transform: uppercase;
}

.btn_buy_pd {
    font-size: 15px;
    display: block;
    margin: auto;
    margin-top: 10px;
    margin-bottom: 10px;
    min-width: 140px;
}

.pd_list {
    border: 1px solid #dedede;
}

.md_pay {

}

.md_pay > div {

}

.md_pay > div > div {
    padding: 4px;
}

.md_pay > div > div > img {
    max-width:100%;
}

.hidden {
display: none;
}

.ppt_com {
background-color: #BABD31;
text-align: center;
}

.ppt_com span {
color: #4F5159;
}

.ppt_com a {
color: #2d2f37;
font-weight: bold;
}

/* ------- DISTRIBUTION ---------*/
.distribution-img img {
border: 1px solid #4f5159;
padding: 2px;
max-width: 150px;
}

/* -------- ACCOUNT ADDRESS --------*/
.trash-address {
float: right;
cursor: pointer;
font-size: 23px;
}

.trash-address:hover {
color: {!! $color !!};
}

.text-address {
color: #666;
font-size: 0.9em;
}

.trash-confirm {
float: right;
display: none;
}

.trash-confirm i {
font-size: 23px;
cursor: pointer;
padding: 8px;
}

.trash-error {
float: right;
display: none;
color: #882607;
}

.trash-spinner {
float: right;
display: none;
font-size: 30px;
}

@media (min-width: 768px) {
.img_index {
height: 282px;
}
}

.cat_menu {
font-size: 14px;
text-transform: uppercase;
}

.div-leave {
cursor: pointer;
color: {!! $color !!};
display: inline-block;
}

.div-leave:hover {
color: #000000;
}

.nv-contact a {
background-color: {!! $color !!};
color: #ffffff;
}

.sale_price {
color: red !important;
margin-right: 2px;
}

.sale_price2 {
text-decoration: line-through;
}

/***** MODALS *****/
.moodleModal {
    top: 50% !important;
    transform: translateY(-50%) !important;
    margin: auto !important;
    max-width: 70% !important;
    width: auto !important;
}

.moodleModal > .modal-content {
    border-radius: 0 !important;
    border: 2px solid #65656585;
    box-shadow: 10px 10px 5px 0px {!! $color !!};
}

.btnMoodleModal {
    background: #00000070 !important;
    color: #989898;
    font-size: 15px;
    border-radius: 0 !important;
    position: absolute;
    right: 0;
    top: 0;
}

.modal_column_2 {
    display: flex;
    align-items: center;
    flex-wrap: wrap;
}

.modal_column_2 > div {
    width: 100%;
    padding-top: 10px;
}

.soloImage > .modal-content {
    background: none !important;
}

.soloImage > .modal-content > .modal-body {
    padding: 0 !important;
}