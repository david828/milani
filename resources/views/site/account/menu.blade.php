<div class="col-md-3 col-sm-4 col-xs-12">
    <aside class="sidebar">
        <hr class="sep-line"/>
        <h4 class="blue">{!! trans('account.account') !!}</h4>
        <ul class="nav nav-list primary">
            <li @yield('mainfo')>{!! link_to_route('myaccount',trans('account.account_info')) !!}</li>
            <li @yield('maimage')>{!! link_to_route('imageprofile',trans('account.account_image')) !!}</li>
            <li @yield('mapass')>{!! link_to_route('changepassword',trans('account.change_password')) !!}</li>
            <li @yield('maaddress')>{!! link_to_route('useraddress',trans('account.address')) !!}</li>
            <li @yield('maorders')>{!! link_to_route('userorders',trans('account.order_history')) !!}</li>
            <li><div class="divDelete" data-toggle="modal" data-target="#deleteModal">{!! trans('account.delete_account') !!}</div></li>
        </ul>
    </aside>
</div>