@extends('site.layouts.page')

@section('title') {!! trans('account.your_account_info') !!} @endsection

@section('cssBottom')
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('account.account_info') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('account.account') !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section><!-- Section id-->
        <div class="container">
            <div class="block-form-login">
                <div class="block-form-create">
                    <h4>{!! trans('account.address') !!}</h4>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default" id="panel_NN">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#address_NN"><i class="fa fa-plus"></i> {!! trans('user.new_address') !!}</a>
                                            <br>
                                            <br>
                                        </h4>
                                    </div>
                                    <div id="address_NN" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="block-content">
                                                <div class="row">
                                                    {!! Form::open(['name'=>'frmData_NN','id'=>'frmData_NN', 'class' => 'formAddress']) !!}
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_title_NN',null,['id'=>'tx_title_NN','class'=>'dart-form-control','required','placeholder'=>trans('user.title')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::select('sl_documenttype_NN',$data->doctype, null,['class'=>'dart-form-control','id'=>'sl_documenttype_NN','required']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_document_NN',null,['id'=>'tx_document_NN','class'=>'dart-form-control','required','placeholder'=>trans('user.document')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_name_NN',null,['id'=>'tx_name_NN','class'=>'dart-form-control','required','placeholder'=>trans('user.reason')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_phone_NN',null,['id'=>'tx_phone_NN','class'=>'dart-form-control','required','placeholder'=>trans('app.phone')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_address_NN',null,['id'=>'tx_address_NN','class'=>'dart-form-control','required','placeholder'=>trans('app.address')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">

                                                        <div class="form-group selectpicker-wrapper">
                                                            {!! Form::select('sl_country_NN',$data->countryList, null,['id'=>'sl_country_NN','class'=>'dart-form-control select2 country_class','data-placeholder'=>trans('app.country'),'required', 'target'=>'NN']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group selectpicker-wrapper">
                                                            {!! Form::select('sl_state_NN',[],null,['id'=>'sl_state_NN','class'=>'dart-form-control select2 state_class','required','data-placeholder'=>trans('app.state'), 'target'=>'NN']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group selectpicker-wrapper">
                                                            {!! Form::select('sl_city_NN',[],null,['id'=>'sl_city_NN','class'=>'dart-form-control select2','required','data-placeholder'=>trans('app.city')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::text('tx_zipcode_NN',null,['id'=>'tx_zipcode_NN','class'=>'dart-form-control','placeholder'=>trans('ph.zipcode')]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            {!! Form::textarea('ta_additional_NN',null,['id'=>'ta_additional_NN','class'=>'dart-form-control','rows'=>'4','placeholder'=>trans('ph.additional')]) !!}
                                                        </div>
                                                    </div>
                                                    <p id="msg-account_NN"></p>
                                                    <div class="col-md-12 col-sm-12">
                                                        {!! Form::submit(trans('app.apply'),['name'=>'bt_apply_NN','id'=>'bt_apply_NN','class'=>'btn normal-btn dart-btn-xs btn_ap_address', 'target'=>'NN']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @foreach($data->address as $address)
                                    <div class="panel panel-default" id="panel_{!! $address->id !!}">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#address_{!! $address->id !!}"><i class="fa fa-pencil"></i> {!! $address->title !!}</a>
                                                <span class="trash-address" id="trash_{!! $address->id !!}" target="{!! $address->id !!}"><i class="fa fa-trash" title="{!! trans('app.delete') !!}"></i></span>
                                                <span class="trash-confirm" id="confirm_{!! $address->id !!}">
                                                    {!! trans('user.sure') !!}<br>
                                                    <i class="fa fa-check-circle-o confirm_trash" target="{!! $address->id !!}"></i><i class="fa fa-times-circle-o refuse_trash" target="{!! $address->id !!}"></i>
                                                </span>
                                                <span class="trash-error" id="error_{!! $address->id !!}">{!! trans('user.error') !!}</span>
                                                <span class="trash-spinner" id="spinner_{!! $address->id !!}"><i class="fa fa-spinner fa-spin"></i></span>
                                                <br>
                                                <br>
                                                <span class="text-address">{!! $address->address.', '.$address->location->cityName !!}</span>
                                            </h4>
                                        </div>
                                        <div id="address_{!! $address->id !!}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="block-content">
                                                    <div class="row">
                                                        {!! Form::open(['name'=>'frmData_'.$address->id,'id'=>'frmData_'.$address->id, 'class' => 'formAddress']) !!}
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_title_'.$address->id,$address->title,['id'=>'tx_title_'.$address->id,'class'=>'dart-form-control','required','placeholder'=>trans('user.title')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                {!! Form::select('sl_documenttype_'.$address->id,$data->doctype, $address->id_documenttype,['class'=>'dart-form-control','id'=>'sl_documenttype_'.$address->id,'required']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_document_'.$address->id,$address->document,['id'=>'tx_document_'.$address->id,'class'=>'dart-form-control','required','placeholder'=>trans('user.document')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_name_'.$address->id,$address->name,['id'=>'tx_name_'.$address->id,'class'=>'dart-form-control','required','placeholder'=>trans('user.reason')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_phone_'.$address->id,$address->phone,['id'=>'tx_phone_'.$address->id,'class'=>'dart-form-control','required','placeholder'=>trans('app.phone')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_address_'.$address->id,$address->address,['id'=>'tx_address_'.$address->id,'class'=>'dart-form-control','required','placeholder'=>trans('app.address')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">

                                                            <div class="form-group selectpicker-wrapper">
                                                                {!! Form::select('sl_country_'.$address->id,$data->countryList, $address->location->countryId,['id'=>'sl_country_'.$address->id,'class'=>'dart-form-control select2 country_class','data-placeholder'=>trans('app.country'),'required', 'target'=>$address->id]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group selectpicker-wrapper">
                                                                {!! Form::select('sl_state_'.$address->id,$address->stateList,$address->location->stateId,['id'=>'sl_state_'.$address->id,'class'=>'dart-form-control select2 state_class','required','data-placeholder'=>trans('app.state'), 'target'=>$address->id]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group selectpicker-wrapper">
                                                                {!! Form::select('sl_city_'.$address->id,$address->cityList,$address->id_city,['id'=>'sl_city_'.$address->id,'class'=>'dart-form-control select2','required','data-placeholder'=>trans('app.city')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                {!! Form::text('tx_zipcode_'.$address->id,$address->post_code,['id'=>'tx_zipcode_'.$address->id,'class'=>'dart-form-control','placeholder'=>trans('ph.zipcode')]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::textarea('ta_additional_'.$address->id,$address->additional,['id'=>'ta_additional_'.$address->id,'class'=>'dart-form-control','rows'=>'4','placeholder'=>trans('ph.additional')]) !!}
                                                            </div>
                                                        </div>
                                                        <p id="msg-account_{!! $address->id !!}"></p>
                                                        <div class="col-md-12 col-sm-12">
                                                            {!! Form::submit(trans('app.apply'),['name'=>'bt_apply_'.$address->id,'id'=>'bt_apply_'.$address->id,'class'=>'btn normal-btn dart-btn-xs btn_ap_address', 'target'=>$address->id]) !!}
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            </div>
                        </div>
                        {!! Form::open(['route'=>['changeaddress', 'id'=>'IDX'],'method'=>'POST','name'=>'frmData','id'=>'frmData','class'=>'form-delivery','role'=>'form']) !!}
                        {!! Form::hidden('tx_title',null,['id'=>'tx_title']) !!}
                        {!! Form::hidden('sl_documenttype',null,['id'=>'sl_documenttype']) !!}
                        {!! Form::hidden('tx_document',null,['id'=>'tx_document']) !!}
                        {!! Form::hidden('tx_name',null,['id'=>'tx_name']) !!}
                        {!! Form::hidden('tx_phone',null,['id'=>'tx_phone']) !!}
                        {!! Form::hidden('tx_address',null,['id'=>'tx_address']) !!}
                        {!! Form::hidden('sl_country',null,['id'=>'sl_country']) !!}
                        {!! Form::hidden('sl_state',null,['id'=>'sl_state']) !!}
                        {!! Form::hidden('sl_city',null,['id'=>'sl_city']) !!}
                        {!! Form::hidden('tx_zipcode',null,['id'=>'tx_zipcode']) !!}
                        {!! Form::hidden('ta_additional',null,['id'=>'ta_additional']) !!}
                        {!! Form::hidden('hn_id',null,['id'=>'hn_id']) !!}
                        {!! Form::close() !!}

                        {!! Form::hidden('hn_state',url('state/IDX/search'),['id'=>'hn_state']) !!}
                        {!! Form::hidden('hn_city',url('city/IDX/search'),['id'=>'hn_city']) !!}

                        {!! Form::open(['route'=>['deleteaddress', 'id'=>'IDX'],'method'=>'POST','name'=>'frmDeleteAddress','id'=>'frmDeleteAddress','class'=>'form-delivery','role'=>'form']) !!}
                        {!! Form::hidden('hn_trash',null,['id'=>'hn_trash']) !!}
                        {!! Form::close() !!}
                    @section('maaddress') class="active" @endsection
                        @include('site.account.menu')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('jsBottom')
    {!! Html::script('assets/js/account/address.js') !!}
@endsection