@extends('site.layouts.page')

@section('title') {!! trans('account.your_account_info') !!} @endsection

@section('cssBottom')
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('account.account_info') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('account.account') !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section><!-- Section id-->
        <div class="container">
            <div class="block-form-login">
                <div class="block-form-create">
                    <h4>{!! trans('account.change_password') !!}</h4>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="block-content">
                                {!! Form::open(['route'=>'changepass','method'=>'POST','name'=>'frmData','id'=>'frmData','class'=>'form-delivery','role'=>'form']) !!}
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::password('tx_password',['id'=>'tx_password','class'=>'dart-form-control','required','minlength'=>'6','placeholder'=>trans('ph.new_password')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::password('tx_rpassword',['id'=>'tx_rpassword','class'=>'dart-form-control','required','minlength'=>'6','equalTo'=>'#tx_password','placeholder'=>trans('ph.confirm_password')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <p id="msg-account"></p>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        {!! Form::submit(trans('app.apply'),['name'=>'bt_apply','id'=>'bt_apply','class'=>'btn normal-btn dart-btn-xs']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                        @section('mapass') class="active" @endsection
                        @include('site.account.menu')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')
    {!! Html::script('assets/js/account/password.js') !!}
@endsection