@extends('site.layouts.page')
@section('title') {!! trans('title.orders') !!}  @endsection
@section('content')


    <div class="container breadcrumb-page">
        <ol class="breadcrumb">
            <li>{!! link_to('/',trans('site.home')) !!}</li>
            <li class="active">{!! trans('title.orders') !!}</li>
        </ol>
    </div>

    <div class="page-title-base container">
        <h1 class="title-base">{!! trans('title.orders') !!}</h1>
    </div>

    <div class="container">
        <div class="block-form-login">
            <div class="block-form-create">
                <div class="block-title">
                    {!! trans('title.orders') !!}
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cart-page-top table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td class="cart-form-heading">{!! trans('invoice.invoice') !!}</td>
                                    <td class="cart-form-heading">{!! trans('invoice.reference') !!}</td>
                                    <td class="cart-form-heading">{!! trans('invoice.total') !!}</td>
                                    <td class="cart-form-heading">{!! trans('invoice.date') !!}</td>
                                    <td class="cart-form-heading">{!! trans('invoice.track') !!}</td>
                                    <td class="cart-form-heading">{!! trans('invoice.status') !!}</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody id="quantity-holder">
                                @foreach($data as $item)
                                    <tr>
                                        <td class="description">{!! $item->id !!}</td>
                                        <td class="description">{!! $item->reference !!}</td>
                                        <td class="total">${!! number_format($item->total) !!}</td>
                                        <td class="order-id"> {!! $item->date !!}</td>
                                        <td> {!! $item->idtrack !!}</td>
                                        <td class="diliver-date"> {!! $item->status[0]->name !!}</td>
                                        <td class="order-status">
                                            {!! link_to_route('vieworder',trans('site.view'),['ref'=>$item->id],['class'=>'btn btn-theme btn-theme-dark']) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>{!! trans('site.track') !!}<a href="{!! env('SEARCH_COORD') !!}" target="_blank"> aquí.</a> </p>
                            <div class="update-button">
                                {!! link_to_route('myaccount',trans('account.account_info'),null,['class'=>'btn-apply-coupon disabled']) !!}
                            </div>
                            <div class="pagination-wrapper">
                                {!! $data->render() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>





@endsection