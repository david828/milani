@extends('site.layouts.page')

@section('title') {!! trans('account.your_account_info') !!} @endsection

@section('cssBottom')
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('account.account_info') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('account.account') !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <section><!-- Section id-->
        <div class="container">
            <div class="block-form-login">
                <div class="block-form-create">
                    <h4>{!! trans('account.your_account_info') !!}</h4>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="block-content">
                                {!! Form::open(['route'=>'updateaccount','method'=>'POST','name'=>'frmData','id'=>'frmData','class'=>'form-delivery','role'=>'form']) !!}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12" style="text-align: center; padding: 20px">
                                        <a href="/imagen">
                                            {!! Html::image(userData()->imageU,'profile',['class' => 'img-circle']) !!}
                                        </a>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            {!! Form::label('sl_person',trans('user.type')) !!}
                                            {!! Form::select('sl_person',$data->person,$data->id_person,['class'=>'dart-form-control','id'=>'sl_person','required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <div class="form-group">
                                            {!! Form::select('sl_documenttype',$data->doctype,$data->id_documenttype,['class'=>'dart-form-control','id'=>'sl_documenttype','required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="form-group">
                                            {!! Form::text('tx_document',$data->document,['id'=>'tx_document','class'=>'dart-form-control','required','placeholder'=>trans('user.document')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            {!! Form::text('tx_name',$data->name,['id'=>'tx_name','class'=>'dart-form-control','required','placeholder'=>trans('user.reason')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::email('tx_email',$data->email,['id'=>'tx_email','class'=>'dart-form-control','required','email','placeholder'=>trans('ph.email')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            {!! Form::text('tx_phone',$data->phone,['id'=>'tx_phone','class'=>'dart-form-control','required','placeholder'=>trans('ph.phone')]) !!}
                                        </div>
                                    </div>
                                    <p id="msg-account"></p>
                                    <div class="col-md-12 col-sm-12">
                                        {!! Form::submit(trans('app.apply'),['name'=>'bt_apply','id'=>'bt_apply','class'=>'btn normal-btn dart-btn-xs']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                        @section('mainfo') class="active" @endsection
                        @include('site.account.menu')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')
    {!! Html::script('assets/js/account/personal.js') !!}
@endsection
