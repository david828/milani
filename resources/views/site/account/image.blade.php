@extends('site.layouts.page')

@section('title') {!! trans('account.your_account_info') !!} @endsection

@section('cssBottom')
    {!! Html::style('cms/plugins/bootstrap3-wysiwyg-master/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('cms/plugins/cropper-master/dist/cropper.css') !!}
@endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! trans('account.account_info') !!}</h2>
            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! trans('account.account') !!}</span></li>
            </ol>
        </div>
    </div>
    <!-- Blog Post Style 1 -->
@endsection

@section('content')
    <div class="modal fade modal-profile" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['imageprofile.data'],'method'=>'POST','name'=>'frmData','id'=>'frmData','role'=>'form','enctype'=>'multipart/form-data']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!! trans('site.change_profile_image') !!}</h4>
                </div>
                <div class="modal-body ">
                    <div class="panel-body text-center crop-images-d">
                        {!! Html::image('cms/dist/img/article-bg.png',null,['class'=>'p-image']) !!}
                    </div>

                    <div class="panel-body docs-buttons">
                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-plus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_in')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-search-minus"></span></span>',['class'=>'btn btn-primary','data-method'=>'zoom','data-option'=>'-0.1', 'data-toggle' =>'tooltip','title'=>trans('site.zoom_out')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'-10', 'data-toggle' =>'tooltip','title'=>trans('site.move_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'0','data-option'=>'10', 'data-toggle' =>'tooltip','title'=>trans('site.move_right')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-up"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'-10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_up')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrow-down"></span></span>',['class'=>'btn btn-primary','data-method'=>'move','data-second-option'=>'10','data-option'=>'0', 'data-toggle' =>'tooltip','title'=>trans('site.move_down')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-left"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'-45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_left')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-rotate-right"></span></span>',['class'=>'btn btn-primary','data-method'=>'rotate','data-option'=>'45', 'data-toggle' =>'tooltip','title'=>trans('site.rotate_right')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-h"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleX','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_horizontal')])) !!}
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-arrows-v"></span></span>',['class'=>'btn btn-primary','data-method'=>'scaleY','data-option'=>'-1', 'data-toggle' =>'tooltip','title'=>trans('site.flip_vertical')])) !!}
                        </div>

                        <div class="btn-group">
                            {!! html_entity_decode(Form::button('<span class="docs-tooltip"><span class="fa fa-refresh"></span></span>',['class'=>'btn btn-primary','data-method'=>'reset', 'data-toggle' =>'tooltip','title'=>trans('site.reset')])) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer docs-buttons">
                    {!! Form::button(trans('site.cancel'),['id'=>'bt_cancelProfile','name'=>'bt_cancelProfile','class'=>'btn btn-default pull-left','data-dismiss'=>'modal']) !!}
                    {!! Form::hidden('hn_file',null,['id'=>'hn_file']) !!}
                    {!! Form::submit(trans('site.apply'),['id'=>'articleCrop','name'=>'articleCrop','class'=>'btn btn-primary','data-dismiss'=>'modal']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <section><!-- Section id-->
        <div class="container">
            <div class="block-form-login">
                <div class="block-form-create">
                    <h4>{!! trans('account.account_image') !!}</h4>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div class="block-content">
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-md-6 col-sm-6" style="text-align: center">
                                        <div class="content-viewer">
                                            {!! Html::image(userData()->imageU,'profile',['class' => 'img-circle','id'=>'img-account']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <figure>
                                            <label class="tg-uploadimg" for="inputImage">
                                                {!! Form::file('file',['id'=>'inputImage','class'=>'sr-only upload-profile','accept'=>'image/*']) !!}
                                                <div class="input-file-button" data-toggle="tooltip"><i class="fa fa-image"></i> {!! trans('app.upload_image') !!}</div>
                                            </label>
                                            <br>
                                            <small>{!! '<b>'.trans('note.note').':</b> '.trans('note.profile_size').'. '.trans('note.image_extensions') !!}</small>
                                            <p id="msg-account"></p>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @section('maimage') class="active" @endsection
                        @include('site.account.menu')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')
    {!! Html::script('cms/plugins/cropper-master/dist/cropper.js') !!}
    {!! Html::script('assets/js/account/image.js') !!}
@endsection
