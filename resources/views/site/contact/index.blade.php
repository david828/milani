@extends('site.layouts.page')

@section('title') {!! menu()[5]->title !!} @endsection

@section('metadesc'){!! $contact->metadescription !!}@endsection

@section('keywords'){!! $contact->metatags !!}@endsection

@section('menuContactus') active @endsection

@section('banner')
    <!--Page Title-->
    <div class="page_title_ctn">
        <div class="container-fluid">
            <h2>{!! menu()[5]->title !!}</h2>

            <ol class="breadcrumb">
                <li><a href="/">{!! menu()[0]->title !!}</a></li>
                <li class="active"><span>{!! menu()[5]->title !!}</span></li>
            </ol>

        </div>
    </div>
@endsection

@section('content')
    <!-- Contact Section -->
    <section class="contactus-one" id="contactus"><!-- Section id-->
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="map">
                        @if($contact->maptype == 'C')
                            <div class="contact-map"
                                 data-lat="{!! $contact->latitude !!}"
                                 data-long = "{!! $contact->longitude !!}"
                                 icon = "{!! appData()->urldata . '/assets/images/marker.png' !!}">
                            </div>
                        @else
                            {!! $contact->framemap !!}
                        @endif
                    </div>
                </div>
                <div class=" col-md-5 col-sm-6">
                    <div class="contact-block">
                        <div class="dart-headingstyle-one dart-mb-20">  <!--Style 1-->
                            <h2 class="dart-heading">{!! trans('site.contactus') !!}</h2>
                        </div>
                        <div>
                            <ul class="nav nav-tabs">
                                <li class="active nv-contact" id="contact-1"><a href="#">{!! trans('site.contact2') !!}</a></li>
                                <li class="nv-contact" id="contact-2"><a href="#">PQRS</a></li>
                            </ul>
                        </div>
                        <div class="contact-form"><!-- contact form -->
                            {!! Form::open(['route'=>['contact.send','id'=>1],'method'=>'POST','name'=>'feedback-form','id'=>'feedback-form','role'=>'form','class'=>'row']) !!}
                            <div class="form-group col-md-6">
                                {!! Form::text('nameCon',(Auth::check())? userData()->name : null,['class'=>'form-control','id'=>'nameCon', 'placeholder'=>trans('contact.yourname'),'aria-required' => 'true']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::text('emailCon',(Auth::check())? userData()->email : null,['class'=>'form-control','id'=>'emailCon', 'placeholder'=>'correo@correo.com','aria-required' => 'true']) !!}
                            </div>
                            <div class="form-group col-md-12" id="div-type">
                                {!! Form::text('type',null,['class'=>'form-control','id'=>'type', 'placeholder'=>trans('contact.subject'),'aria-required' => 'true']) !!}
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::textarea('message',null,['class'=>'form-control','id'=>'message', 'placeholder'=>trans('contact.write_message'), 'rows' => '3','aria-required' => 'true']) !!}
                            </div>
                            <div class="col-md-12">
                                <div class="g-recaptcha" data-sitekey="{!! env('GOOGLE_RECAPTCHA_KEY') !!}"></div>
                            </div>
                            <div class="col-md-12">
                                <button name="submit" type="submit" class="btn normal-btn dart-btn-xs">{!! trans('contact.sendmessage') !!}</button>
                            </div>
                            <div class="message_box sc_form_result resultBox">
                                <strong id="res_error" class="resultBox">{!! trans('contact.error') !!}</strong><br/>
                                <strong id="res_success"
                                        class="resultBox">{!! trans('contact.congratulations') !!}</strong><br/>
                                <div id="result_error1"
                                     class="resultBox">{!! trans('contact.errorform') !!}</div>
                                <div id="result_ error2"
                                     class="resultBox">{!! trans('contact.no_sent') !!}</div>
                                <div id="result_success"
                                     class="resultBox">{!! trans('contact.sent_message') !!}</div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <hr>
                        <div class=" row contact-info">
                            <div class="col-md-12 col-sm-12">
                                <p class="addre"><i class="fa fa-map-marker"></i>{!! appData()->address !!}</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <p class="phone-call"><i class="fa fa-phone"></i> <a href="tel:{!! appData()->phone !!}">{!! appData()->phone !!}</a></p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <p class="cellphone-call"><i class="fa fa-mobile"></i> {!! appData()->cellphone !!}</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <p class="mail-id"><i class="fa fa-envelope"></i>{!! appData()->email !!}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsBottom')
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key={!! $contact->api_google !!}'></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    {!! Html::script('assets/js/gmap3.min.js') !!}
    {!! Html::script('assets/js/mail.js') !!}
    {!! Html::script('assets/js/maps.js') !!}
@endsection
