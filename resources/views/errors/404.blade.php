<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{!! url('assets/images/favicon.png') !!}" sizes="32x32"/>
    <title>Error - Pagina no encontrada</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #000000;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 95vh;
            margin: 0;

            display: flex;
            justify-content: center;
            align-items: center;
        }

        .content {
            max-width: 900px;
            width: 90%;
            margin: auto;
            -webkit-box-shadow: 0px 0px 35px 2px rgba(255,255,255,1);
            -moz-box-shadow: 0px 0px 35px 2px rgba(255,255,255,1);
            box-shadow: 0px 0px 35px 2px rgba(255,255,255,1);
        }

        .col-1 {
            text-align: center;
            width: 80%;
            margin: auto;
        }

        .col-1 img {
         max-width: 100%;
        }

        .col-2 {
            text-align: center;
            padding: 20px;
            color: #edcd7e;
        }

        .col-2 h1 {
            font-size: 2.5rem;
            font-weight: normal;
            color: #edcd7e;
        }

        .col-2 h1 span {
            font-weight: bold;
            font-size: 3rem;
        }

        .col-2 p {
            padding: 10px;
            font-size: 1.5rem;
        }

        a {
            font-size: 1.5 em;
            background: #edcd7e;
            color: #000;
            padding: 10px 20px;
            text-decoration: none;
            font-weight: bold;
            border-radius: 2px;
            margin-bottom: 30px;
            transition: 2s all ease;
            border: none;
        }

        a:hover {
            background: #000;
            color: #edcd7e;
            border: 1px solid #edcd7e;
        }

        @media only screen and (min-width: 768px) {
            .content {

                display: flex;
                justify-content: center;
                align-items: center;
            }

            .col-1,
            .col-2 {
                width: 48%;
            }
        }

    </style>
</head>
<body>
<div class="content">
    <div class="col-1">
        <img src="/assets/images/404.png" alt="Error 404">
    </div>
    <div class="col-2">
        <h1><span>LO SENTIMOS</span><br> ESTA PÁGINA NO EXISTE</h1>
        <p>Puedes regresar a {!! appData()->name !!} haciendo clic </p>
        <a href="/">AQUÍ</a>
    </div>
</div>
</body>
</html>