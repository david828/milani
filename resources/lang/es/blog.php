<?php

return [
    'title'                             => 'Título',
    'text'                              => 'Texto',
    'image'                             => 'Imagen',
    'want_delete'                       => '¿Realmente desea eliminar esta información?.',
    'blog'                              => 'Blog',
    'viewmore'                          => 'Ver más',
    'categories'                        => 'Categoría',
    'last_post'                         => 'Últimos post',
    'all'                               => 'Todas',
    'tag'                               => 'Tags:',
    'gallery'                           => 'Galería de imágenes:',
    'sharein'                           => 'Compartir en',
    'comments'                          => 'Comentarios',
    'writecom'                          => 'Deja tu comentario',
    'name'                              => 'Nombre',
    'email'                             => 'Email',
    'comment'                           => 'Comentario',
    'congratulations'           => '¡Felicitaciones!',
    'error'                     => '¡Error!',
    'sent_message'              => 'Su comentario se ha enviado correctamente',
    'no_sent'                   => 'No se ha logrado enviar el comentario, por favor intente de nuevo',
    'errorform'                 => 'Por favor digite todos los campos',
    'send'                      => 'Enviar',
    'recent'                      => 'Reciente en nuestro',

];