<?php


return [
    'login'                     => 'Iniciar Sesión',
    'register_n'                => 'Registrar usuario nuevo',
    'register'                  => 'Crear cuenta',
    'email'                     => 'Correo electrónico',
    'phone'                     => 'Teléfono',
    'password'                  => 'Contraseña',
    'new_password'              => 'Nueva contraseña',
    'confirm_password'          => 'Confirmar contraseña',
    'document'                  => 'Documento',
    'search'                    => 'Buscar',
    'search_in'                 => 'Buscar en ',
    'keyword'                   => 'Palabra clave',
    'firstname'                 => 'Nombre',
    'lastname'                  => 'Apellidos',
    'repeat_email'              => 'Repetir correo electrónico',
    'repeat_password'           => 'Repetir contraseña',
    'address'                   => 'Dirección',
    'zipcode'                   => 'Código postal',
    'additional'                => 'Información adicional',
];