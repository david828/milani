<?php


return [
    'title'         => 'Título',
    'text'          => 'Texto',
    'confirmation'  => 'Confirmación',
    'subject'       => 'Asunto',
    'msg'           => 'Mensaje',
    'name'          => 'Nombre',
    'address'       => 'Dirección',
    'country'       => 'Pais',
    'state'         => 'Departamento',
    'city'          => 'Ciudad',
    'phone'         => 'Teléfono',
    'phones'        => 'Teléfonos',
    'cellphone'     => 'Celular',
    'document'      => 'Documento',
    'email'         => 'Email',
    'logo'          => 'Logo',
    'comment'       => 'Comentario',
    'send'          => 'Enviar',
    'info'          => 'Información del distribuidor',
    'congratulations'           => '¡Felicitaciones!',
    'error'                     => '¡Error!',
    'no_sent'                   => 'No se han logrado enviar los datos, por favor intente de nuevo',
    'errorform'                 => 'Por favor digite todos los campos',
    'distribution'          => 'Puntos de venta',
];