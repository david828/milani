<?php


return [
    'question_distribution'         => 'Pregunta para formulario de distribuidor',
    'botton_distribution'           => 'Texto para botón de distribuidor',
    'text'                          => 'Texto',
    'social'                        => '¿Desea que se vean las redes sociales?',
];