<?php

return[
    'follow'                    => 'Síguenos',
    'findus'                    => 'Ubícanos',
    'yourname'                  =>  'Nombre',
    'subject'                  =>  'Asunto',
    'youremail'                 => 'Email',
    'ouremail'                  => 'Nuestro email',
    'yourmessage'               =>  'Mensaje',
    'write_message'             => 'Escribe tu mensaje aquí',
    'sendmessage'               =>  'Enviar mensaje',
    'website'                   =>  'Tu sitio web',
    'phone'                     =>  'Teléfono',
    'cel'                       =>  'Celular',
    'address'                   =>  'Dirección',
    'email'                     =>  'Email',
    'message_title'             => 'NUEVO MENSAJE',
    'have_message'              => 'Hola, hay un mensaje nuevo',
    'new_message'               => 'Hay un nuevo mensaje ',
    'sent_message'              => 'Su mensaje se ha enviado correctamente',
    'no_sent'                   => 'No se ha logrado enviar el mensaje, por favor intente de nuevo',
    'message_description'       => 'Un usuario ha dejado un mensaje, en el formulario de contacto de [name] [title]. A continuación se muestra la información del usuario que escribió y su comentario al respecto.',
    'user'                      => 'Usuario',
    'errorform'                 => 'Por favor digite todos los campos',
    'congratulations'           => '¡Felicitaciones!',
    'error'                     => '¡Error!',
    'location'                  => 'Ubicación',
    'title_location'            => 'Título ubicación',
    'text_location'             => 'Texto ubicación',
    'schedule'                  => 'Horario',
    'ourcontacts'               => 'Nuestros contactos',
    'details'                   => 'Detalles',
    'type'                      => 'Tipo de contacto',
    'p'                         => 'Petición',
    'q'                         => 'Queja',
    'r'                         => 'Reclamo',
    's'                         => 'Sugerencia',
    'f'                         => 'Felicitación',
    'o'                         => 'Otro',
    'api_google'                => 'API key Google',



];