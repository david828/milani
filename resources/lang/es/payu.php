<?php

return [
    'accountId'                     =>  'Account Id',
    'merchantId'                    =>  'Merchant Id',
    'apiLogin'                      =>  'API Login',
    'apikey'                        =>  'API Key',
    'publickey'                     =>  'Public Key',
    'responseurl'                   =>  'Url de respuesta',
    'requesturl'                    =>  'Url de envío',
    'confirmationurl'               =>  'Url de confirmación',
    'testing'                       =>  'Pruebas',
];