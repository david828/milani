<?php


return [
    'account'                               =>  'Cuenta',
    'my_account'                            =>  'Mi cuenta',
    'account_info'                          =>  'Información de cuenta',
    'account_image'                         =>  'Cambiar imagen',
    'your_account_info'                     =>  'Información sobre tu cuenta',
    'change_details'                        =>  'Cambiar datos personales',
    'my_account'                            =>  'Mi cuenta',
    'change_password'                       =>  'Cambiar contraseña',
    'change_your_password'                  =>  'Cambia tu contraseña',
    'address'                               =>  'Dirección de envío',
    'order_history'                         =>  'Historial de órdenes',
    'delete_account'                        =>  'Cerrar cuenta',
    'delete_confirm'                        =>  'Al cerrar su cuenta sus datos serán borrados y no podrá volver a acceder a ellos. ¿Está seguro(a) de eliminar sus datos?',
    'error_info'                            =>  'Error al intentar cerrar su cuenta, los datos no coinciden',
];