<?php


return [
    'text'              => 'Texto',
    'textarea'          => 'Area de texto',
    'number'            => 'Numérico',
    'email'             => 'E-Mail',
    'phone'             => 'Teléfono',
    'mobile'            => 'No. Móvil',
    'email'             => 'Correo electrónico',
    'address'           => 'Dirección',
    'website'           => 'Sitio web',
    'fax'               => 'No. Fax',
    'facebook'          => 'Facebook',
    'twitter'           => 'Twitter',
    'google'            => 'Google +',
    'linkedin'          => 'LinkedIn',
    'skype'             => 'Skype',
];