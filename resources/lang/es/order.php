<?php

return[
    'reorder'              => 'Cambiar orden',
    'title_order'          => 'Cambiar orden de las imágenes',
    'drag'                 => '1. Arrastre las fotos para cambiar orden',
    'save'                 => '2. Click en Aplicar para guardar.',
];