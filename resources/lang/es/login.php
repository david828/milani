<?php


return [
    'administrator'                 =>  'Administrador de contenido',
    'remember'                      =>  'Recordarme',
    'forgot'                        =>  'Olvidé mi contraseña',
    'back_login'                    =>  'Volver al login',
    'send_password_link'            =>  'Enviar link de contraseña',
    'reset_password'                =>  'Restaurar contraseña',
    'login_link'                    =>  'Ir a inicio de sesión',
    'login_title'                   =>  'Iniciar sesión',
    'go_login'                      =>  'Ir al login',
    'login_text'                    =>  'Bienvenido a su cuenta de usuario',
    'create_account'                =>  'Crear una cuenta',
    'create_account_text'           =>  'Crea tu cuenta en ',
    'signup_now'                    =>  'Regístrate ahora y podrás',
    'login_problems'                =>  'Error al tratar de iniciar sesión, es posible que su correo electrónico o contraseña estén incorrectos. Por favor intente nuevamente.',
    'pass_no_same'                  =>  'Las contraseñas no coinciden',
    'logout'                        =>  'Cerrar sesión',
];