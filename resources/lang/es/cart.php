<?php

return [
    'like_guest'                => 'Continuar como invitado',
    'checkout'                  => 'Checkout',
    'invoice_data'              => 'Datos de facturación',
    'shipping_data'             =>  'Datos de envío',
    'address_s'                 =>  'Direcciones de envío',
    'your_order'                =>  'Tu pedido',
    'subtotal'                  =>  'Subtotal',
    'totals'                    =>  'Totales',
    'shipping'                  =>  'Envío',
    'total'                     =>  'Total',
    'tax'                       =>  'IVA',
    'place_order'               =>  'Realizar pedido',
    'buy_now'                   =>  'Comprar ahora',

    ];