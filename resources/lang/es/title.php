<?php


return [
    'welcome'                           =>  'Bienvenid@',
    'about_us'                          =>  'Nosotros',
    'cart'                              =>  'Carrito de compras',
    'payment_response'                  =>  'Resultado de tu compra',
    'orders'                            =>  'Mis ordenes',
    'reviews'                           =>  'Comentarios',
];