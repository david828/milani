<?php

return[
    'distribution'             => 'Distribución del encabezado',
    'change'                   => 'Cambiar de posición',
    'slide'                    => 'Tira de imágenes',
    'image'                    => 'Imagen',
    'instagram'                => 'Galería de Instagram',
    'instagram_fields'         => 'Estos datos son generados en Instagram y son necesarios para hacer la conexión',
    'numphotos'                => 'Número de fotos',
    'typelink'                 => 'Enlace: ',
    'int'                      => 'Interno',
    'ext'                      => 'Externo',
    'change_link'              => 'Modificar link',
    'empty'                    => 'Dejar vacío para no crear link',
    ];