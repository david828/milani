<?php

return [
    'note'                  => 'Nota',
    'icon_size'             => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>178px (ancho) x 57px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'article_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>1200px (ancho) x 675px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'gallery_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>1280px (ancho) x 720px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'mosaic_size'           => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>600px (ancho) x 600px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'profile_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>120px (ancho) x 120px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'productdes_size'       => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>891px (ancho) x 731px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'header2_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>960px (ancho) x 200px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'header1_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>959px (ancho) x 681px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'color_size'            => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>200px (ancho) x 200px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'distribution_image'    => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>200px (ancho) x 200px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'image_extensions'      => 'Recuerde que la imagen debe estar en formato .PNG, .JPG ó .GIF',
    'file_resource'         => 'Recuerde que el archivo debe estar en formato <b> .PDF</b>',
    'product_size'          => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>540px (ancho) x 586px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'modal_size'            => 'Se recomienda que la imagen tenga un tamaño igual o mayor a <b>1280px (ancho) x 720px (alto)</b> y un peso no mayor a <b>2MB</b>',
    'password_length'       =>  'Por favor ingresa una contraseña que sea mayo a 6 dígitos',

];